
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- acciones
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `acciones`;

CREATE TABLE `acciones`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`accion` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- alquiler
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `alquiler`;

CREATE TABLE `alquiler`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`arrendatario` INTEGER NOT NULL,
	`contrato` INTEGER NOT NULL,
	`fecha_alquiler` DATETIME NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`registrador` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `alquiler_ibfk_1` (`contrato`),
	INDEX `alquiler_ibfk_4` (`arrendatario`),
	INDEX `registrador` (`registrador`),
	CONSTRAINT `alquiler_ibfk_1`
		FOREIGN KEY (`contrato`)
		REFERENCES `contrato` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `alquiler_ibfk_4`
		FOREIGN KEY (`arrendatario`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `alquiler_ibfk_5`
		FOREIGN KEY (`registrador`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- contactenos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contactenos`;

CREATE TABLE `contactenos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(50) NOT NULL,
	`telefono` VARCHAR(10) NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`asunto` TEXT NOT NULL,
	`estado` VARCHAR(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- contrato
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contrato`;

CREATE TABLE `contrato`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`numero_contrato` VARCHAR(255) NOT NULL,
	`folio_id` INTEGER NOT NULL,
	`fecha_contrato` DATETIME NOT NULL,
	`fecha_inicio` DATETIME NOT NULL,
	`fecha_termino` DATETIME NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `folio_id` (`folio_id`),
	CONSTRAINT `contrato_ibfk_1`
		FOREIGN KEY (`folio_id`)
		REFERENCES `folio` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cultivo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cultivo`;

CREATE TABLE `cultivo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`fecha_cultivo` DATETIME,
	`tipo_siembra` INTEGER(255),
	`zona` INTEGER(255),
	`cultivador` INTEGER,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`cantidad_produccion` INTEGER,
	`fecha_produccion` DATETIME,
	`cantidad_sembrada` INTEGER,
	`area` INTEGER(100) NOT NULL,
	`coordenadas` VARCHAR(1000) NOT NULL,
	`color` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `tipo_siembra` (`tipo_siembra`(255)),
	INDEX `zona` (`zona`(255)),
	INDEX `cultivador` (`cultivador`),
	CONSTRAINT `cultivo_ibfk_1`
		FOREIGN KEY (`tipo_siembra`)
		REFERENCES `siembra` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `cultivo_ibfk_2`
		FOREIGN KEY (`zona`)
		REFERENCES `zonas` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `cultivo_ibfk_3`
		FOREIGN KEY (`cultivador`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- cultivo_alquiler
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `cultivo_alquiler`;

CREATE TABLE `cultivo_alquiler`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`cultivo_id` INTEGER NOT NULL,
	`alquiler_id` INTEGER NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- departamento
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `departamento`;

CREATE TABLE `departamento`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`departamento` VARCHAR(20),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- distrito
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `distrito`;

CREATE TABLE `distrito`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`distrito` VARCHAR(50),
	`provincia_id` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- familia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `familia`;

CREATE TABLE `familia`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombre_familia` VARCHAR(255) NOT NULL,
	`codigo` VARCHAR(255) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- folio
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `folio`;

CREATE TABLE `folio`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`serie` VARCHAR(255),
	`hojas` INTEGER,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- grupos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `grupos`;

CREATE TABLE `grupos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`descripcion` VARCHAR(50) NOT NULL,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- miembros_familia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `miembros_familia`;

CREATE TABLE `miembros_familia`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`rol_miembro` INTEGER NOT NULL,
	`familia` INTEGER NOT NULL,
	`persona` INTEGER NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `familia` (`familia`),
	INDEX `tipo_familia` (`rol_miembro`),
	INDEX `miembro` (`persona`),
	CONSTRAINT `miembros_familia_ibfk_1`
		FOREIGN KEY (`familia`)
		REFERENCES `familia` (`id`),
	CONSTRAINT `miembros_familia_ibfk_2`
		FOREIGN KEY (`rol_miembro`)
		REFERENCES `rol_miembro` (`id`),
	CONSTRAINT `miembros_familia_ibfk_3`
		FOREIGN KEY (`persona`)
		REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- modulos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `modulos`;

CREATE TABLE `modulos`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos`;

CREATE TABLE `permisos`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idaccion` INTEGER NOT NULL,
	`idsubmodulo` INTEGER,
	`nombre` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `idaccion` (`idaccion`),
	INDEX `idmodulo` (`idsubmodulo`),
	INDEX `permisos_ibfk_3` (`idmodulo`),
	CONSTRAINT `permisos_ibfk_1`
		FOREIGN KEY (`idaccion`)
		REFERENCES `acciones` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_2`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_ibfk_3`
		FOREIGN KEY (`idsubmodulo`)
		REFERENCES `submodulo` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_grupo`;

CREATE TABLE `permisos_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idpermiso` INTEGER NOT NULL,
	`idgrupo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `permisos_grupo_ibfk_1` (`idpermiso`),
	INDEX `permisos_grupo_ibfk_2` (`idgrupo`),
	CONSTRAINT `permisos_grupo_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_grupo_ibfk_2`
		FOREIGN KEY (`idgrupo`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- permisos_usuario
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `permisos_usuario`;

CREATE TABLE `permisos_usuario`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`idusuario` INTEGER,
	`idpermiso` INTEGER,
	PRIMARY KEY (`id`),
	INDEX `permisos_usuario_ibfk_1` (`idpermiso`),
	INDEX `permisos_usuario_ibfk_2` (`idusuario`),
	CONSTRAINT `permisos_usuario_ibfk_1`
		FOREIGN KEY (`idpermiso`)
		REFERENCES `permisos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `permisos_usuario_ibfk_2`
		FOREIGN KEY (`idusuario`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- predio
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `predio`;

CREATE TABLE `predio`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`numero_predio` VARCHAR(255) NOT NULL,
	`area` INTEGER(100),
	`fecha_registro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`familia_id` INTEGER NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`coordenadas` VARCHAR(1000),
	`direccion` VARCHAR(255) NOT NULL,
	`color` VARCHAR(30),
	PRIMARY KEY (`id`),
	INDEX `familia_id` (`familia_id`),
	CONSTRAINT `predio_ibfk_1`
		FOREIGN KEY (`familia_id`)
		REFERENCES `familia` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- provincia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `provincia`;

CREATE TABLE `provincia`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`provincia` VARCHAR(255),
	`departamento_id` INTEGER,
	`idd` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- rol_miembro
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rol_miembro`;

CREATE TABLE `rol_miembro`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombre_miembro` VARCHAR(255) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- siembra
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `siembra`;

CREATE TABLE `siembra`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`siembra` VARCHAR(255),
	`descripcion` VARCHAR(255),
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- submodulo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `submodulo`;

CREATE TABLE `submodulo`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`url` VARCHAR(255) NOT NULL,
	`icon` VARCHAR(255) NOT NULL,
	`idmodulo` INTEGER NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `pk_mod` (`idmodulo`),
	CONSTRAINT `submodulo_ibfk_1`
		FOREIGN KEY (`idmodulo`)
		REFERENCES `modulos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- tipo_zona
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tipo_zona`;

CREATE TABLE `tipo_zona`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`tipo_zona` VARCHAR(255) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ubicacion_zona
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ubicacion_zona`;

CREATE TABLE `ubicacion_zona`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`zona` VARCHAR(255),
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ubigeo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ubigeo`;

CREATE TABLE `ubigeo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`departamento` VARCHAR(50),
	`provincia` VARCHAR(50),
	`distrito` VARCHAR(50),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- unidad_medida
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `unidad_medida`;

CREATE TABLE `unidad_medida`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(255) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`simbolo` VARCHAR(4) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- urls_permitidas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urls_permitidas`;

CREATE TABLE `urls_permitidas`
(
	`id` INTEGER(255) NOT NULL AUTO_INCREMENT,
	`url` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nombres` VARCHAR(50) NOT NULL,
	`apellidos` VARCHAR(50) NOT NULL,
	`clave` VARCHAR(50) NOT NULL,
	`esta_activo` TINYINT(20) DEFAULT 0 NOT NULL,
	`correo` VARCHAR(50),
	`telefono` INTEGER,
	`direccion` VARCHAR(60),
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`dni` VARCHAR(8) NOT NULL,
	`ubigeo` INTEGER NOT NULL,
	`fecha_nacimiento` DATE NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `ubigeo` (`ubigeo`),
	CONSTRAINT `usuarios_ibfk_1`
		FOREIGN KEY (`ubigeo`)
		REFERENCES `ubigeo` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- usuarios_grupo
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios_grupo`;

CREATE TABLE `usuarios_grupo`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`usuario_id` INTEGER NOT NULL,
	`grupo_id` INTEGER NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
	`deleted_at` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `usuario_id` (`usuario_id`),
	INDEX `grupo_id` (`grupo_id`),
	CONSTRAINT `usuarios_grupo_ibfk_1`
		FOREIGN KEY (`usuario_id`)
		REFERENCES `usuarios` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `usuarios_grupo_ibfk_2`
		FOREIGN KEY (`grupo_id`)
		REFERENCES `grupos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- zonas
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `zonas`;

CREATE TABLE `zonas`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`zona_ubicacion_id` INTEGER NOT NULL,
	`predio_id` INTEGER NOT NULL,
	`tipo_zona` INTEGER NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME,
	`deleted_at` DATETIME,
	`miembro_id` INTEGER NOT NULL,
	`numero_zona` VARCHAR(255) NOT NULL,
	`area` INTEGER(100) NOT NULL,
	`fecha_registro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`coordenadas` VARCHAR(1000) NOT NULL,
	`direccion` VARCHAR(255) NOT NULL,
	`color` VARCHAR(30) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `zonas_ibfk_1` (`tipo_zona`),
	INDEX `zonas_ibfk_2` (`zona_ubicacion_id`),
	INDEX `miembro_id` (`miembro_id`),
	INDEX `predio_id` (`predio_id`),
	CONSTRAINT `zonas_ibfk_1`
		FOREIGN KEY (`tipo_zona`)
		REFERENCES `tipo_zona` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `zonas_ibfk_2`
		FOREIGN KEY (`zona_ubicacion_id`)
		REFERENCES `ubicacion_zona` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `zonas_ibfk_3`
		FOREIGN KEY (`miembro_id`)
		REFERENCES `miembros_familia` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `zonas_ibfk_4`
		FOREIGN KEY (`predio_id`)
		REFERENCES `predio` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
