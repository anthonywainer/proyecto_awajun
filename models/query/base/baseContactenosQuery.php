<?php

use Dabl\Query\Query;

abstract class baseContactenosQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Contactenos::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ContactenosQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ContactenosQuery($table_name, $alias);
	}

	/**
	 * @return Contactenos[]
	 */
	function select() {
		return Contactenos::doSelect($this);
	}

	/**
	 * @return Contactenos
	 */
	function selectOne() {
		return Contactenos::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Contactenos::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Contactenos::doCount($this);
	}

	/**
	 * @return ContactenosQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Contactenos::isTemporalType($type)) {
			$value = Contactenos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Contactenos::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ContactenosQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Contactenos::isTemporalType($type)) {
			$value = Contactenos::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Contactenos::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andId($integer) {
		return $this->addAnd(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdNull() {
		return $this->andNull(Contactenos::ID);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Contactenos::ID);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Contactenos::ID, $integer, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orId($integer) {
		return $this->or(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdNull() {
		return $this->orNull(Contactenos::ID);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Contactenos::ID);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Contactenos::ID, $integer, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Contactenos::ID, $integer);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Contactenos::ID, $integer);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Contactenos::ID, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Contactenos::ID, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupById() {
		return $this->groupBy(Contactenos::ID);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombres($varchar) {
		return $this->addAnd(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresNot($varchar) {
		return $this->andNot(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresLike($varchar) {
		return $this->andLike(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresNotLike($varchar) {
		return $this->andNotLike(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresGreater($varchar) {
		return $this->andGreater(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresLess($varchar) {
		return $this->andLess(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresLessEqual($varchar) {
		return $this->andLessEqual(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresNull() {
		return $this->andNull(Contactenos::NOMBRES);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresNotNull() {
		return $this->andNotNull(Contactenos::NOMBRES);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresBetween($varchar, $from, $to) {
		return $this->andBetween(Contactenos::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresBeginsWith($varchar) {
		return $this->andBeginsWith(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresEndsWith($varchar) {
		return $this->andEndsWith(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andNombresContains($varchar) {
		return $this->andContains(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombres($varchar) {
		return $this->or(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresNot($varchar) {
		return $this->orNot(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresLike($varchar) {
		return $this->orLike(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresNotLike($varchar) {
		return $this->orNotLike(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresGreater($varchar) {
		return $this->orGreater(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresLess($varchar) {
		return $this->orLess(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresLessEqual($varchar) {
		return $this->orLessEqual(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresNull() {
		return $this->orNull(Contactenos::NOMBRES);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresNotNull() {
		return $this->orNotNull(Contactenos::NOMBRES);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresBetween($varchar, $from, $to) {
		return $this->orBetween(Contactenos::NOMBRES, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresBeginsWith($varchar) {
		return $this->orBeginsWith(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresEndsWith($varchar) {
		return $this->orEndsWith(Contactenos::NOMBRES, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orNombresContains($varchar) {
		return $this->orContains(Contactenos::NOMBRES, $varchar);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByNombresAsc() {
		return $this->orderBy(Contactenos::NOMBRES, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByNombresDesc() {
		return $this->orderBy(Contactenos::NOMBRES, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupByNombres() {
		return $this->groupBy(Contactenos::NOMBRES);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefono($varchar) {
		return $this->addAnd(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoNot($varchar) {
		return $this->andNot(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoLike($varchar) {
		return $this->andLike(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoNotLike($varchar) {
		return $this->andNotLike(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoGreater($varchar) {
		return $this->andGreater(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoLess($varchar) {
		return $this->andLess(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoLessEqual($varchar) {
		return $this->andLessEqual(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoNull() {
		return $this->andNull(Contactenos::TELEFONO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoNotNull() {
		return $this->andNotNull(Contactenos::TELEFONO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoBetween($varchar, $from, $to) {
		return $this->andBetween(Contactenos::TELEFONO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoBeginsWith($varchar) {
		return $this->andBeginsWith(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoEndsWith($varchar) {
		return $this->andEndsWith(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andTelefonoContains($varchar) {
		return $this->andContains(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefono($varchar) {
		return $this->or(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoNot($varchar) {
		return $this->orNot(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoLike($varchar) {
		return $this->orLike(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoNotLike($varchar) {
		return $this->orNotLike(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoGreater($varchar) {
		return $this->orGreater(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoLess($varchar) {
		return $this->orLess(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoLessEqual($varchar) {
		return $this->orLessEqual(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoNull() {
		return $this->orNull(Contactenos::TELEFONO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoNotNull() {
		return $this->orNotNull(Contactenos::TELEFONO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoBetween($varchar, $from, $to) {
		return $this->orBetween(Contactenos::TELEFONO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoBeginsWith($varchar) {
		return $this->orBeginsWith(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoEndsWith($varchar) {
		return $this->orEndsWith(Contactenos::TELEFONO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orTelefonoContains($varchar) {
		return $this->orContains(Contactenos::TELEFONO, $varchar);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByTelefonoAsc() {
		return $this->orderBy(Contactenos::TELEFONO, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByTelefonoDesc() {
		return $this->orderBy(Contactenos::TELEFONO, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupByTelefono() {
		return $this->groupBy(Contactenos::TELEFONO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmail($varchar) {
		return $this->addAnd(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailNot($varchar) {
		return $this->andNot(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailLike($varchar) {
		return $this->andLike(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailNotLike($varchar) {
		return $this->andNotLike(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailGreater($varchar) {
		return $this->andGreater(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailLess($varchar) {
		return $this->andLess(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailLessEqual($varchar) {
		return $this->andLessEqual(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailNull() {
		return $this->andNull(Contactenos::EMAIL);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailNotNull() {
		return $this->andNotNull(Contactenos::EMAIL);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailBetween($varchar, $from, $to) {
		return $this->andBetween(Contactenos::EMAIL, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailBeginsWith($varchar) {
		return $this->andBeginsWith(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailEndsWith($varchar) {
		return $this->andEndsWith(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEmailContains($varchar) {
		return $this->andContains(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmail($varchar) {
		return $this->or(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailNot($varchar) {
		return $this->orNot(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailLike($varchar) {
		return $this->orLike(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailNotLike($varchar) {
		return $this->orNotLike(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailGreater($varchar) {
		return $this->orGreater(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailLess($varchar) {
		return $this->orLess(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailLessEqual($varchar) {
		return $this->orLessEqual(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailNull() {
		return $this->orNull(Contactenos::EMAIL);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailNotNull() {
		return $this->orNotNull(Contactenos::EMAIL);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailBetween($varchar, $from, $to) {
		return $this->orBetween(Contactenos::EMAIL, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailBeginsWith($varchar) {
		return $this->orBeginsWith(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailEndsWith($varchar) {
		return $this->orEndsWith(Contactenos::EMAIL, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEmailContains($varchar) {
		return $this->orContains(Contactenos::EMAIL, $varchar);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByEmailAsc() {
		return $this->orderBy(Contactenos::EMAIL, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByEmailDesc() {
		return $this->orderBy(Contactenos::EMAIL, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupByEmail() {
		return $this->groupBy(Contactenos::EMAIL);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsunto($varchar) {
		return $this->addAnd(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoNot($varchar) {
		return $this->andNot(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoLike($varchar) {
		return $this->andLike(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoNotLike($varchar) {
		return $this->andNotLike(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoGreater($varchar) {
		return $this->andGreater(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoLess($varchar) {
		return $this->andLess(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoLessEqual($varchar) {
		return $this->andLessEqual(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoNull() {
		return $this->andNull(Contactenos::ASUNTO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoNotNull() {
		return $this->andNotNull(Contactenos::ASUNTO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoBetween($varchar, $from, $to) {
		return $this->andBetween(Contactenos::ASUNTO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoBeginsWith($varchar) {
		return $this->andBeginsWith(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoEndsWith($varchar) {
		return $this->andEndsWith(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andAsuntoContains($varchar) {
		return $this->andContains(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsunto($varchar) {
		return $this->or(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoNot($varchar) {
		return $this->orNot(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoLike($varchar) {
		return $this->orLike(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoNotLike($varchar) {
		return $this->orNotLike(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoGreater($varchar) {
		return $this->orGreater(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoLess($varchar) {
		return $this->orLess(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoLessEqual($varchar) {
		return $this->orLessEqual(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoNull() {
		return $this->orNull(Contactenos::ASUNTO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoNotNull() {
		return $this->orNotNull(Contactenos::ASUNTO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoBetween($varchar, $from, $to) {
		return $this->orBetween(Contactenos::ASUNTO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoBeginsWith($varchar) {
		return $this->orBeginsWith(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoEndsWith($varchar) {
		return $this->orEndsWith(Contactenos::ASUNTO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orAsuntoContains($varchar) {
		return $this->orContains(Contactenos::ASUNTO, $varchar);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByAsuntoAsc() {
		return $this->orderBy(Contactenos::ASUNTO, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByAsuntoDesc() {
		return $this->orderBy(Contactenos::ASUNTO, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupByAsunto() {
		return $this->groupBy(Contactenos::ASUNTO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstado($varchar) {
		return $this->addAnd(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoNot($varchar) {
		return $this->andNot(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoLike($varchar) {
		return $this->andLike(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoNotLike($varchar) {
		return $this->andNotLike(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoGreater($varchar) {
		return $this->andGreater(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoLess($varchar) {
		return $this->andLess(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoLessEqual($varchar) {
		return $this->andLessEqual(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoNull() {
		return $this->andNull(Contactenos::ESTADO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoNotNull() {
		return $this->andNotNull(Contactenos::ESTADO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoBetween($varchar, $from, $to) {
		return $this->andBetween(Contactenos::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoBeginsWith($varchar) {
		return $this->andBeginsWith(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoEndsWith($varchar) {
		return $this->andEndsWith(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function andEstadoContains($varchar) {
		return $this->andContains(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstado($varchar) {
		return $this->or(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoNot($varchar) {
		return $this->orNot(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoLike($varchar) {
		return $this->orLike(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoNotLike($varchar) {
		return $this->orNotLike(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoGreater($varchar) {
		return $this->orGreater(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoLess($varchar) {
		return $this->orLess(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoLessEqual($varchar) {
		return $this->orLessEqual(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoNull() {
		return $this->orNull(Contactenos::ESTADO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoNotNull() {
		return $this->orNotNull(Contactenos::ESTADO);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoBetween($varchar, $from, $to) {
		return $this->orBetween(Contactenos::ESTADO, $varchar, $from, $to);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoBeginsWith($varchar) {
		return $this->orBeginsWith(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoEndsWith($varchar) {
		return $this->orEndsWith(Contactenos::ESTADO, $varchar);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orEstadoContains($varchar) {
		return $this->orContains(Contactenos::ESTADO, $varchar);
	}


	/**
	 * @return ContactenosQuery
	 */
	function orderByEstadoAsc() {
		return $this->orderBy(Contactenos::ESTADO, self::ASC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function orderByEstadoDesc() {
		return $this->orderBy(Contactenos::ESTADO, self::DESC);
	}

	/**
	 * @return ContactenosQuery
	 */
	function groupByEstado() {
		return $this->groupBy(Contactenos::ESTADO);
	}


}