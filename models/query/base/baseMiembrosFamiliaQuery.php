<?php

use Dabl\Query\Query;

abstract class baseMiembrosFamiliaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = MiembrosFamilia::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return MiembrosFamiliaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new MiembrosFamiliaQuery($table_name, $alias);
	}

	/**
	 * @return MiembrosFamilia[]
	 */
	function select() {
		return MiembrosFamilia::doSelect($this);
	}

	/**
	 * @return MiembrosFamilia
	 */
	function selectOne() {
		return MiembrosFamilia::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return MiembrosFamilia::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return MiembrosFamilia::doCount($this);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MiembrosFamilia::isTemporalType($type)) {
			$value = MiembrosFamilia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MiembrosFamilia::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && MiembrosFamilia::isTemporalType($type)) {
			$value = MiembrosFamilia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = MiembrosFamilia::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andId($integer) {
		return $this->addAnd(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdNull() {
		return $this->andNull(MiembrosFamilia::ID);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(MiembrosFamilia::ID);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(MiembrosFamilia::ID, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orId($integer) {
		return $this->or(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdNull() {
		return $this->orNull(MiembrosFamilia::ID);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(MiembrosFamilia::ID);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(MiembrosFamilia::ID, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(MiembrosFamilia::ID, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(MiembrosFamilia::ID, $integer);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(MiembrosFamilia::ID, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(MiembrosFamilia::ID, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupById() {
		return $this->groupBy(MiembrosFamilia::ID);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembro($integer) {
		return $this->addAnd(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroNot($integer) {
		return $this->andNot(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroLike($integer) {
		return $this->andLike(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroNotLike($integer) {
		return $this->andNotLike(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroGreater($integer) {
		return $this->andGreater(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroGreaterEqual($integer) {
		return $this->andGreaterEqual(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroLess($integer) {
		return $this->andLess(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroLessEqual($integer) {
		return $this->andLessEqual(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroNull() {
		return $this->andNull(MiembrosFamilia::ROL_MIEMBRO);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroNotNull() {
		return $this->andNotNull(MiembrosFamilia::ROL_MIEMBRO);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroBetween($integer, $from, $to) {
		return $this->andBetween(MiembrosFamilia::ROL_MIEMBRO, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroBeginsWith($integer) {
		return $this->andBeginsWith(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroEndsWith($integer) {
		return $this->andEndsWith(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andRolMiembroContains($integer) {
		return $this->andContains(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembro($integer) {
		return $this->or(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroNot($integer) {
		return $this->orNot(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroLike($integer) {
		return $this->orLike(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroNotLike($integer) {
		return $this->orNotLike(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroGreater($integer) {
		return $this->orGreater(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroGreaterEqual($integer) {
		return $this->orGreaterEqual(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroLess($integer) {
		return $this->orLess(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroLessEqual($integer) {
		return $this->orLessEqual(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroNull() {
		return $this->orNull(MiembrosFamilia::ROL_MIEMBRO);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroNotNull() {
		return $this->orNotNull(MiembrosFamilia::ROL_MIEMBRO);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroBetween($integer, $from, $to) {
		return $this->orBetween(MiembrosFamilia::ROL_MIEMBRO, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroBeginsWith($integer) {
		return $this->orBeginsWith(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroEndsWith($integer) {
		return $this->orEndsWith(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orRolMiembroContains($integer) {
		return $this->orContains(MiembrosFamilia::ROL_MIEMBRO, $integer);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByRolMiembroAsc() {
		return $this->orderBy(MiembrosFamilia::ROL_MIEMBRO, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByRolMiembroDesc() {
		return $this->orderBy(MiembrosFamilia::ROL_MIEMBRO, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByRolMiembro() {
		return $this->groupBy(MiembrosFamilia::ROL_MIEMBRO);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamilia($integer) {
		return $this->addAnd(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaNot($integer) {
		return $this->andNot(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaLike($integer) {
		return $this->andLike(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaNotLike($integer) {
		return $this->andNotLike(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaGreater($integer) {
		return $this->andGreater(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaGreaterEqual($integer) {
		return $this->andGreaterEqual(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaLess($integer) {
		return $this->andLess(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaLessEqual($integer) {
		return $this->andLessEqual(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaNull() {
		return $this->andNull(MiembrosFamilia::FAMILIA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaNotNull() {
		return $this->andNotNull(MiembrosFamilia::FAMILIA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaBetween($integer, $from, $to) {
		return $this->andBetween(MiembrosFamilia::FAMILIA, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaBeginsWith($integer) {
		return $this->andBeginsWith(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaEndsWith($integer) {
		return $this->andEndsWith(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andFamiliaContains($integer) {
		return $this->andContains(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamilia($integer) {
		return $this->or(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaNot($integer) {
		return $this->orNot(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaLike($integer) {
		return $this->orLike(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaNotLike($integer) {
		return $this->orNotLike(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaGreater($integer) {
		return $this->orGreater(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaGreaterEqual($integer) {
		return $this->orGreaterEqual(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaLess($integer) {
		return $this->orLess(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaLessEqual($integer) {
		return $this->orLessEqual(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaNull() {
		return $this->orNull(MiembrosFamilia::FAMILIA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaNotNull() {
		return $this->orNotNull(MiembrosFamilia::FAMILIA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaBetween($integer, $from, $to) {
		return $this->orBetween(MiembrosFamilia::FAMILIA, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaBeginsWith($integer) {
		return $this->orBeginsWith(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaEndsWith($integer) {
		return $this->orEndsWith(MiembrosFamilia::FAMILIA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orFamiliaContains($integer) {
		return $this->orContains(MiembrosFamilia::FAMILIA, $integer);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByFamiliaAsc() {
		return $this->orderBy(MiembrosFamilia::FAMILIA, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByFamiliaDesc() {
		return $this->orderBy(MiembrosFamilia::FAMILIA, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByFamilia() {
		return $this->groupBy(MiembrosFamilia::FAMILIA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersona($integer) {
		return $this->addAnd(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaNot($integer) {
		return $this->andNot(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaLike($integer) {
		return $this->andLike(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaNotLike($integer) {
		return $this->andNotLike(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaGreater($integer) {
		return $this->andGreater(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaGreaterEqual($integer) {
		return $this->andGreaterEqual(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaLess($integer) {
		return $this->andLess(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaLessEqual($integer) {
		return $this->andLessEqual(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaNull() {
		return $this->andNull(MiembrosFamilia::PERSONA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaNotNull() {
		return $this->andNotNull(MiembrosFamilia::PERSONA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaBetween($integer, $from, $to) {
		return $this->andBetween(MiembrosFamilia::PERSONA, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaBeginsWith($integer) {
		return $this->andBeginsWith(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaEndsWith($integer) {
		return $this->andEndsWith(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andPersonaContains($integer) {
		return $this->andContains(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersona($integer) {
		return $this->or(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaNot($integer) {
		return $this->orNot(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaLike($integer) {
		return $this->orLike(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaNotLike($integer) {
		return $this->orNotLike(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaGreater($integer) {
		return $this->orGreater(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaGreaterEqual($integer) {
		return $this->orGreaterEqual(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaLess($integer) {
		return $this->orLess(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaLessEqual($integer) {
		return $this->orLessEqual(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaNull() {
		return $this->orNull(MiembrosFamilia::PERSONA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaNotNull() {
		return $this->orNotNull(MiembrosFamilia::PERSONA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaBetween($integer, $from, $to) {
		return $this->orBetween(MiembrosFamilia::PERSONA, $integer, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaBeginsWith($integer) {
		return $this->orBeginsWith(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaEndsWith($integer) {
		return $this->orEndsWith(MiembrosFamilia::PERSONA, $integer);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orPersonaContains($integer) {
		return $this->orContains(MiembrosFamilia::PERSONA, $integer);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByPersonaAsc() {
		return $this->orderBy(MiembrosFamilia::PERSONA, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByPersonaDesc() {
		return $this->orderBy(MiembrosFamilia::PERSONA, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByPersona() {
		return $this->groupBy(MiembrosFamilia::PERSONA);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(MiembrosFamilia::CREATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(MiembrosFamilia::CREATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MiembrosFamilia::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(MiembrosFamilia::CREATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(MiembrosFamilia::CREATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MiembrosFamilia::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MiembrosFamilia::CREATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(MiembrosFamilia::CREATED_AT, $timestamp);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(MiembrosFamilia::CREATED_AT, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(MiembrosFamilia::CREATED_AT, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(MiembrosFamilia::CREATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(MiembrosFamilia::UPDATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(MiembrosFamilia::UPDATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MiembrosFamilia::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(MiembrosFamilia::UPDATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(MiembrosFamilia::UPDATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MiembrosFamilia::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(MiembrosFamilia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(MiembrosFamilia::UPDATED_AT, $timestamp);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(MiembrosFamilia::UPDATED_AT, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(MiembrosFamilia::UPDATED_AT, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(MiembrosFamilia::UPDATED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(MiembrosFamilia::DELETED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(MiembrosFamilia::DELETED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(MiembrosFamilia::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(MiembrosFamilia::DELETED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(MiembrosFamilia::DELETED_AT);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(MiembrosFamilia::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(MiembrosFamilia::DELETED_AT, $timestamp);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(MiembrosFamilia::DELETED_AT, $timestamp);
	}


	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(MiembrosFamilia::DELETED_AT, self::ASC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(MiembrosFamilia::DELETED_AT, self::DESC);
	}

	/**
	 * @return MiembrosFamiliaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(MiembrosFamilia::DELETED_AT);
	}


}