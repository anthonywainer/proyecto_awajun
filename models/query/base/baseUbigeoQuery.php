<?php

use Dabl\Query\Query;

abstract class baseUbigeoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Ubigeo::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UbigeoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UbigeoQuery($table_name, $alias);
	}

	/**
	 * @return Ubigeo[]
	 */
	function select() {
		return Ubigeo::doSelect($this);
	}

	/**
	 * @return Ubigeo
	 */
	function selectOne() {
		return Ubigeo::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Ubigeo::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Ubigeo::doCount($this);
	}

	/**
	 * @return UbigeoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Ubigeo::isTemporalType($type)) {
			$value = Ubigeo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Ubigeo::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UbigeoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Ubigeo::isTemporalType($type)) {
			$value = Ubigeo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Ubigeo::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andId($integer) {
		return $this->addAnd(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdNull() {
		return $this->andNull(Ubigeo::ID);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Ubigeo::ID);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Ubigeo::ID, $integer, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orId($integer) {
		return $this->or(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdNull() {
		return $this->orNull(Ubigeo::ID);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Ubigeo::ID);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Ubigeo::ID, $integer, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Ubigeo::ID, $integer);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Ubigeo::ID, $integer);
	}


	/**
	 * @return UbigeoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Ubigeo::ID, self::ASC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Ubigeo::ID, self::DESC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function groupById() {
		return $this->groupBy(Ubigeo::ID);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamento($varchar) {
		return $this->addAnd(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoNot($varchar) {
		return $this->andNot(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoLike($varchar) {
		return $this->andLike(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoNotLike($varchar) {
		return $this->andNotLike(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoGreater($varchar) {
		return $this->andGreater(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoLess($varchar) {
		return $this->andLess(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoLessEqual($varchar) {
		return $this->andLessEqual(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoNull() {
		return $this->andNull(Ubigeo::DEPARTAMENTO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoNotNull() {
		return $this->andNotNull(Ubigeo::DEPARTAMENTO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoBetween($varchar, $from, $to) {
		return $this->andBetween(Ubigeo::DEPARTAMENTO, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoBeginsWith($varchar) {
		return $this->andBeginsWith(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoEndsWith($varchar) {
		return $this->andEndsWith(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDepartamentoContains($varchar) {
		return $this->andContains(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamento($varchar) {
		return $this->or(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoNot($varchar) {
		return $this->orNot(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoLike($varchar) {
		return $this->orLike(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoNotLike($varchar) {
		return $this->orNotLike(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoGreater($varchar) {
		return $this->orGreater(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoLess($varchar) {
		return $this->orLess(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoLessEqual($varchar) {
		return $this->orLessEqual(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoNull() {
		return $this->orNull(Ubigeo::DEPARTAMENTO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoNotNull() {
		return $this->orNotNull(Ubigeo::DEPARTAMENTO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoBetween($varchar, $from, $to) {
		return $this->orBetween(Ubigeo::DEPARTAMENTO, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoBeginsWith($varchar) {
		return $this->orBeginsWith(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoEndsWith($varchar) {
		return $this->orEndsWith(Ubigeo::DEPARTAMENTO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDepartamentoContains($varchar) {
		return $this->orContains(Ubigeo::DEPARTAMENTO, $varchar);
	}


	/**
	 * @return UbigeoQuery
	 */
	function orderByDepartamentoAsc() {
		return $this->orderBy(Ubigeo::DEPARTAMENTO, self::ASC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orderByDepartamentoDesc() {
		return $this->orderBy(Ubigeo::DEPARTAMENTO, self::DESC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function groupByDepartamento() {
		return $this->groupBy(Ubigeo::DEPARTAMENTO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvincia($varchar) {
		return $this->addAnd(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaNot($varchar) {
		return $this->andNot(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaLike($varchar) {
		return $this->andLike(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaNotLike($varchar) {
		return $this->andNotLike(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaGreater($varchar) {
		return $this->andGreater(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaLess($varchar) {
		return $this->andLess(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaLessEqual($varchar) {
		return $this->andLessEqual(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaNull() {
		return $this->andNull(Ubigeo::PROVINCIA);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaNotNull() {
		return $this->andNotNull(Ubigeo::PROVINCIA);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaBetween($varchar, $from, $to) {
		return $this->andBetween(Ubigeo::PROVINCIA, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaBeginsWith($varchar) {
		return $this->andBeginsWith(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaEndsWith($varchar) {
		return $this->andEndsWith(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andProvinciaContains($varchar) {
		return $this->andContains(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvincia($varchar) {
		return $this->or(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaNot($varchar) {
		return $this->orNot(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaLike($varchar) {
		return $this->orLike(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaNotLike($varchar) {
		return $this->orNotLike(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaGreater($varchar) {
		return $this->orGreater(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaLess($varchar) {
		return $this->orLess(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaLessEqual($varchar) {
		return $this->orLessEqual(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaNull() {
		return $this->orNull(Ubigeo::PROVINCIA);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaNotNull() {
		return $this->orNotNull(Ubigeo::PROVINCIA);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaBetween($varchar, $from, $to) {
		return $this->orBetween(Ubigeo::PROVINCIA, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaBeginsWith($varchar) {
		return $this->orBeginsWith(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaEndsWith($varchar) {
		return $this->orEndsWith(Ubigeo::PROVINCIA, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orProvinciaContains($varchar) {
		return $this->orContains(Ubigeo::PROVINCIA, $varchar);
	}


	/**
	 * @return UbigeoQuery
	 */
	function orderByProvinciaAsc() {
		return $this->orderBy(Ubigeo::PROVINCIA, self::ASC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orderByProvinciaDesc() {
		return $this->orderBy(Ubigeo::PROVINCIA, self::DESC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function groupByProvincia() {
		return $this->groupBy(Ubigeo::PROVINCIA);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistrito($varchar) {
		return $this->addAnd(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoNot($varchar) {
		return $this->andNot(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoLike($varchar) {
		return $this->andLike(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoNotLike($varchar) {
		return $this->andNotLike(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoGreater($varchar) {
		return $this->andGreater(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoLess($varchar) {
		return $this->andLess(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoLessEqual($varchar) {
		return $this->andLessEqual(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoNull() {
		return $this->andNull(Ubigeo::DISTRITO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoNotNull() {
		return $this->andNotNull(Ubigeo::DISTRITO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoBetween($varchar, $from, $to) {
		return $this->andBetween(Ubigeo::DISTRITO, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoBeginsWith($varchar) {
		return $this->andBeginsWith(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoEndsWith($varchar) {
		return $this->andEndsWith(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function andDistritoContains($varchar) {
		return $this->andContains(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistrito($varchar) {
		return $this->or(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoNot($varchar) {
		return $this->orNot(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoLike($varchar) {
		return $this->orLike(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoNotLike($varchar) {
		return $this->orNotLike(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoGreater($varchar) {
		return $this->orGreater(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoLess($varchar) {
		return $this->orLess(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoLessEqual($varchar) {
		return $this->orLessEqual(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoNull() {
		return $this->orNull(Ubigeo::DISTRITO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoNotNull() {
		return $this->orNotNull(Ubigeo::DISTRITO);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoBetween($varchar, $from, $to) {
		return $this->orBetween(Ubigeo::DISTRITO, $varchar, $from, $to);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoBeginsWith($varchar) {
		return $this->orBeginsWith(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoEndsWith($varchar) {
		return $this->orEndsWith(Ubigeo::DISTRITO, $varchar);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orDistritoContains($varchar) {
		return $this->orContains(Ubigeo::DISTRITO, $varchar);
	}


	/**
	 * @return UbigeoQuery
	 */
	function orderByDistritoAsc() {
		return $this->orderBy(Ubigeo::DISTRITO, self::ASC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function orderByDistritoDesc() {
		return $this->orderBy(Ubigeo::DISTRITO, self::DESC);
	}

	/**
	 * @return UbigeoQuery
	 */
	function groupByDistrito() {
		return $this->groupBy(Ubigeo::DISTRITO);
	}


}