<?php

use Dabl\Query\Query;

abstract class baseUnidadMedidaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = UnidadMedida::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UnidadMedidaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UnidadMedidaQuery($table_name, $alias);
	}

	/**
	 * @return UnidadMedida[]
	 */
	function select() {
		return UnidadMedida::doSelect($this);
	}

	/**
	 * @return UnidadMedida
	 */
	function selectOne() {
		return UnidadMedida::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return UnidadMedida::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return UnidadMedida::doCount($this);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UnidadMedida::isTemporalType($type)) {
			$value = UnidadMedida::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UnidadMedida::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UnidadMedida::isTemporalType($type)) {
			$value = UnidadMedida::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UnidadMedida::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andId($integer) {
		return $this->addAnd(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdNull() {
		return $this->andNull(UnidadMedida::ID);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(UnidadMedida::ID);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(UnidadMedida::ID, $integer, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orId($integer) {
		return $this->or(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdNull() {
		return $this->orNull(UnidadMedida::ID);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(UnidadMedida::ID);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(UnidadMedida::ID, $integer, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(UnidadMedida::ID, $integer);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(UnidadMedida::ID, $integer);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(UnidadMedida::ID, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(UnidadMedida::ID, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupById() {
		return $this->groupBy(UnidadMedida::ID);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombre($varchar) {
		return $this->addAnd(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreNot($varchar) {
		return $this->andNot(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreLike($varchar) {
		return $this->andLike(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreNotLike($varchar) {
		return $this->andNotLike(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreGreater($varchar) {
		return $this->andGreater(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreGreaterEqual($varchar) {
		return $this->andGreaterEqual(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreLess($varchar) {
		return $this->andLess(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreLessEqual($varchar) {
		return $this->andLessEqual(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreNull() {
		return $this->andNull(UnidadMedida::NOMBRE);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreNotNull() {
		return $this->andNotNull(UnidadMedida::NOMBRE);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreBetween($varchar, $from, $to) {
		return $this->andBetween(UnidadMedida::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreBeginsWith($varchar) {
		return $this->andBeginsWith(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreEndsWith($varchar) {
		return $this->andEndsWith(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andNombreContains($varchar) {
		return $this->andContains(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombre($varchar) {
		return $this->or(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreNot($varchar) {
		return $this->orNot(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreLike($varchar) {
		return $this->orLike(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreNotLike($varchar) {
		return $this->orNotLike(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreGreater($varchar) {
		return $this->orGreater(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreGreaterEqual($varchar) {
		return $this->orGreaterEqual(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreLess($varchar) {
		return $this->orLess(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreLessEqual($varchar) {
		return $this->orLessEqual(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreNull() {
		return $this->orNull(UnidadMedida::NOMBRE);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreNotNull() {
		return $this->orNotNull(UnidadMedida::NOMBRE);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreBetween($varchar, $from, $to) {
		return $this->orBetween(UnidadMedida::NOMBRE, $varchar, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreBeginsWith($varchar) {
		return $this->orBeginsWith(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreEndsWith($varchar) {
		return $this->orEndsWith(UnidadMedida::NOMBRE, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orNombreContains($varchar) {
		return $this->orContains(UnidadMedida::NOMBRE, $varchar);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByNombreAsc() {
		return $this->orderBy(UnidadMedida::NOMBRE, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByNombreDesc() {
		return $this->orderBy(UnidadMedida::NOMBRE, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupByNombre() {
		return $this->groupBy(UnidadMedida::NOMBRE);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(UnidadMedida::CREATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(UnidadMedida::CREATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UnidadMedida::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(UnidadMedida::CREATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(UnidadMedida::CREATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UnidadMedida::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UnidadMedida::CREATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(UnidadMedida::CREATED_AT, $timestamp);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(UnidadMedida::CREATED_AT, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(UnidadMedida::CREATED_AT, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(UnidadMedida::CREATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(UnidadMedida::UPDATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(UnidadMedida::UPDATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UnidadMedida::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(UnidadMedida::UPDATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(UnidadMedida::UPDATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UnidadMedida::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UnidadMedida::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(UnidadMedida::UPDATED_AT, $timestamp);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(UnidadMedida::UPDATED_AT, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(UnidadMedida::UPDATED_AT, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(UnidadMedida::UPDATED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(UnidadMedida::DELETED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(UnidadMedida::DELETED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UnidadMedida::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(UnidadMedida::DELETED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(UnidadMedida::DELETED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UnidadMedida::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(UnidadMedida::DELETED_AT, $timestamp);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(UnidadMedida::DELETED_AT, $timestamp);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(UnidadMedida::DELETED_AT, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(UnidadMedida::DELETED_AT, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(UnidadMedida::DELETED_AT);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimbolo($varchar) {
		return $this->addAnd(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloNot($varchar) {
		return $this->andNot(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloLike($varchar) {
		return $this->andLike(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloNotLike($varchar) {
		return $this->andNotLike(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloGreater($varchar) {
		return $this->andGreater(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloGreaterEqual($varchar) {
		return $this->andGreaterEqual(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloLess($varchar) {
		return $this->andLess(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloLessEqual($varchar) {
		return $this->andLessEqual(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloNull() {
		return $this->andNull(UnidadMedida::SIMBOLO);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloNotNull() {
		return $this->andNotNull(UnidadMedida::SIMBOLO);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloBetween($varchar, $from, $to) {
		return $this->andBetween(UnidadMedida::SIMBOLO, $varchar, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloBeginsWith($varchar) {
		return $this->andBeginsWith(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloEndsWith($varchar) {
		return $this->andEndsWith(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function andSimboloContains($varchar) {
		return $this->andContains(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimbolo($varchar) {
		return $this->or(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloNot($varchar) {
		return $this->orNot(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloLike($varchar) {
		return $this->orLike(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloNotLike($varchar) {
		return $this->orNotLike(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloGreater($varchar) {
		return $this->orGreater(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloGreaterEqual($varchar) {
		return $this->orGreaterEqual(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloLess($varchar) {
		return $this->orLess(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloLessEqual($varchar) {
		return $this->orLessEqual(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloNull() {
		return $this->orNull(UnidadMedida::SIMBOLO);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloNotNull() {
		return $this->orNotNull(UnidadMedida::SIMBOLO);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloBetween($varchar, $from, $to) {
		return $this->orBetween(UnidadMedida::SIMBOLO, $varchar, $from, $to);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloBeginsWith($varchar) {
		return $this->orBeginsWith(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloEndsWith($varchar) {
		return $this->orEndsWith(UnidadMedida::SIMBOLO, $varchar);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orSimboloContains($varchar) {
		return $this->orContains(UnidadMedida::SIMBOLO, $varchar);
	}


	/**
	 * @return UnidadMedidaQuery
	 */
	function orderBySimboloAsc() {
		return $this->orderBy(UnidadMedida::SIMBOLO, self::ASC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function orderBySimboloDesc() {
		return $this->orderBy(UnidadMedida::SIMBOLO, self::DESC);
	}

	/**
	 * @return UnidadMedidaQuery
	 */
	function groupBySimbolo() {
		return $this->groupBy(UnidadMedida::SIMBOLO);
	}


}