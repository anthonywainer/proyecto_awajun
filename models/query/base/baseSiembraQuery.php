<?php

use Dabl\Query\Query;

abstract class baseSiembraQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Siembra::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return SiembraQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new SiembraQuery($table_name, $alias);
	}

	/**
	 * @return Siembra[]
	 */
	function select() {
		return Siembra::doSelect($this);
	}

	/**
	 * @return Siembra
	 */
	function selectOne() {
		return Siembra::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Siembra::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Siembra::doCount($this);
	}

	/**
	 * @return SiembraQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Siembra::isTemporalType($type)) {
			$value = Siembra::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Siembra::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return SiembraQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Siembra::isTemporalType($type)) {
			$value = Siembra::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Siembra::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return SiembraQuery
	 */
	function andId($integer) {
		return $this->addAnd(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdNull() {
		return $this->andNull(Siembra::ID);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Siembra::ID);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Siembra::ID, $integer, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orId($integer) {
		return $this->or(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdNull() {
		return $this->orNull(Siembra::ID);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Siembra::ID);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Siembra::ID, $integer, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Siembra::ID, $integer);
	}

	/**
	 * @return SiembraQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Siembra::ID, $integer);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Siembra::ID, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Siembra::ID, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupById() {
		return $this->groupBy(Siembra::ID);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembra($varchar) {
		return $this->addAnd(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraNot($varchar) {
		return $this->andNot(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraLike($varchar) {
		return $this->andLike(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraNotLike($varchar) {
		return $this->andNotLike(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraGreater($varchar) {
		return $this->andGreater(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraGreaterEqual($varchar) {
		return $this->andGreaterEqual(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraLess($varchar) {
		return $this->andLess(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraLessEqual($varchar) {
		return $this->andLessEqual(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraNull() {
		return $this->andNull(Siembra::SIEMBRA);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraNotNull() {
		return $this->andNotNull(Siembra::SIEMBRA);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraBetween($varchar, $from, $to) {
		return $this->andBetween(Siembra::SIEMBRA, $varchar, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraBeginsWith($varchar) {
		return $this->andBeginsWith(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraEndsWith($varchar) {
		return $this->andEndsWith(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andSiembraContains($varchar) {
		return $this->andContains(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembra($varchar) {
		return $this->or(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraNot($varchar) {
		return $this->orNot(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraLike($varchar) {
		return $this->orLike(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraNotLike($varchar) {
		return $this->orNotLike(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraGreater($varchar) {
		return $this->orGreater(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraGreaterEqual($varchar) {
		return $this->orGreaterEqual(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraLess($varchar) {
		return $this->orLess(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraLessEqual($varchar) {
		return $this->orLessEqual(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraNull() {
		return $this->orNull(Siembra::SIEMBRA);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraNotNull() {
		return $this->orNotNull(Siembra::SIEMBRA);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraBetween($varchar, $from, $to) {
		return $this->orBetween(Siembra::SIEMBRA, $varchar, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraBeginsWith($varchar) {
		return $this->orBeginsWith(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraEndsWith($varchar) {
		return $this->orEndsWith(Siembra::SIEMBRA, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orSiembraContains($varchar) {
		return $this->orContains(Siembra::SIEMBRA, $varchar);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderBySiembraAsc() {
		return $this->orderBy(Siembra::SIEMBRA, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderBySiembraDesc() {
		return $this->orderBy(Siembra::SIEMBRA, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupBySiembra() {
		return $this->groupBy(Siembra::SIEMBRA);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcion($varchar) {
		return $this->addAnd(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionNot($varchar) {
		return $this->andNot(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionLike($varchar) {
		return $this->andLike(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionNotLike($varchar) {
		return $this->andNotLike(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionGreater($varchar) {
		return $this->andGreater(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionLess($varchar) {
		return $this->andLess(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionLessEqual($varchar) {
		return $this->andLessEqual(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionNull() {
		return $this->andNull(Siembra::DESCRIPCION);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionNotNull() {
		return $this->andNotNull(Siembra::DESCRIPCION);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionBetween($varchar, $from, $to) {
		return $this->andBetween(Siembra::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionBeginsWith($varchar) {
		return $this->andBeginsWith(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionEndsWith($varchar) {
		return $this->andEndsWith(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDescripcionContains($varchar) {
		return $this->andContains(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcion($varchar) {
		return $this->or(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionNot($varchar) {
		return $this->orNot(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionLike($varchar) {
		return $this->orLike(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionNotLike($varchar) {
		return $this->orNotLike(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionGreater($varchar) {
		return $this->orGreater(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionLess($varchar) {
		return $this->orLess(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionLessEqual($varchar) {
		return $this->orLessEqual(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionNull() {
		return $this->orNull(Siembra::DESCRIPCION);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionNotNull() {
		return $this->orNotNull(Siembra::DESCRIPCION);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionBetween($varchar, $from, $to) {
		return $this->orBetween(Siembra::DESCRIPCION, $varchar, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionBeginsWith($varchar) {
		return $this->orBeginsWith(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionEndsWith($varchar) {
		return $this->orEndsWith(Siembra::DESCRIPCION, $varchar);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDescripcionContains($varchar) {
		return $this->orContains(Siembra::DESCRIPCION, $varchar);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderByDescripcionAsc() {
		return $this->orderBy(Siembra::DESCRIPCION, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderByDescripcionDesc() {
		return $this->orderBy(Siembra::DESCRIPCION, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupByDescripcion() {
		return $this->groupBy(Siembra::DESCRIPCION);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Siembra::CREATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Siembra::CREATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Siembra::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Siembra::CREATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Siembra::CREATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Siembra::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Siembra::CREATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Siembra::CREATED_AT, $timestamp);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Siembra::CREATED_AT, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Siembra::CREATED_AT, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Siembra::CREATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Siembra::UPDATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Siembra::UPDATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Siembra::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Siembra::UPDATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Siembra::UPDATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Siembra::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Siembra::UPDATED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Siembra::UPDATED_AT, $timestamp);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Siembra::UPDATED_AT, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Siembra::UPDATED_AT, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Siembra::UPDATED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Siembra::DELETED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Siembra::DELETED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Siembra::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Siembra::DELETED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Siembra::DELETED_AT);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Siembra::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Siembra::DELETED_AT, $timestamp);
	}

	/**
	 * @return SiembraQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Siembra::DELETED_AT, $timestamp);
	}


	/**
	 * @return SiembraQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Siembra::DELETED_AT, self::ASC);
	}

	/**
	 * @return SiembraQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Siembra::DELETED_AT, self::DESC);
	}

	/**
	 * @return SiembraQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Siembra::DELETED_AT);
	}


}