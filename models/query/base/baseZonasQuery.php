<?php

use Dabl\Query\Query;

abstract class baseZonasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Zonas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ZonasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ZonasQuery($table_name, $alias);
	}

	/**
	 * @return Zonas[]
	 */
	function select() {
		return Zonas::doSelect($this);
	}

	/**
	 * @return Zonas
	 */
	function selectOne() {
		return Zonas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Zonas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Zonas::doCount($this);
	}

	/**
	 * @return ZonasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Zonas::isTemporalType($type)) {
			$value = Zonas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Zonas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ZonasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Zonas::isTemporalType($type)) {
			$value = Zonas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Zonas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ZonasQuery
	 */
	function andId($integer) {
		return $this->addAnd(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdNull() {
		return $this->andNull(Zonas::ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Zonas::ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orId($integer) {
		return $this->or(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdNull() {
		return $this->orNull(Zonas::ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Zonas::ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Zonas::ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Zonas::ID, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Zonas::ID, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Zonas::ID, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupById() {
		return $this->groupBy(Zonas::ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionId($integer) {
		return $this->addAnd(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdNot($integer) {
		return $this->andNot(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdLike($integer) {
		return $this->andLike(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdNotLike($integer) {
		return $this->andNotLike(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdGreater($integer) {
		return $this->andGreater(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdLess($integer) {
		return $this->andLess(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdLessEqual($integer) {
		return $this->andLessEqual(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdNull() {
		return $this->andNull(Zonas::ZONA_UBICACION_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdNotNull() {
		return $this->andNotNull(Zonas::ZONA_UBICACION_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::ZONA_UBICACION_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdEndsWith($integer) {
		return $this->andEndsWith(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andZonaUbicacionIdContains($integer) {
		return $this->andContains(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionId($integer) {
		return $this->or(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdNot($integer) {
		return $this->orNot(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdLike($integer) {
		return $this->orLike(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdNotLike($integer) {
		return $this->orNotLike(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdGreater($integer) {
		return $this->orGreater(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdLess($integer) {
		return $this->orLess(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdLessEqual($integer) {
		return $this->orLessEqual(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdNull() {
		return $this->orNull(Zonas::ZONA_UBICACION_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdNotNull() {
		return $this->orNotNull(Zonas::ZONA_UBICACION_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::ZONA_UBICACION_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdEndsWith($integer) {
		return $this->orEndsWith(Zonas::ZONA_UBICACION_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orZonaUbicacionIdContains($integer) {
		return $this->orContains(Zonas::ZONA_UBICACION_ID, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByZonaUbicacionIdAsc() {
		return $this->orderBy(Zonas::ZONA_UBICACION_ID, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByZonaUbicacionIdDesc() {
		return $this->orderBy(Zonas::ZONA_UBICACION_ID, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByZonaUbicacionId() {
		return $this->groupBy(Zonas::ZONA_UBICACION_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioId($integer) {
		return $this->addAnd(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdNot($integer) {
		return $this->andNot(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdLike($integer) {
		return $this->andLike(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdNotLike($integer) {
		return $this->andNotLike(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdGreater($integer) {
		return $this->andGreater(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdLess($integer) {
		return $this->andLess(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdLessEqual($integer) {
		return $this->andLessEqual(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdNull() {
		return $this->andNull(Zonas::PREDIO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdNotNull() {
		return $this->andNotNull(Zonas::PREDIO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::PREDIO_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdEndsWith($integer) {
		return $this->andEndsWith(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andPredioIdContains($integer) {
		return $this->andContains(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioId($integer) {
		return $this->or(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdNot($integer) {
		return $this->orNot(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdLike($integer) {
		return $this->orLike(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdNotLike($integer) {
		return $this->orNotLike(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdGreater($integer) {
		return $this->orGreater(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdLess($integer) {
		return $this->orLess(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdLessEqual($integer) {
		return $this->orLessEqual(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdNull() {
		return $this->orNull(Zonas::PREDIO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdNotNull() {
		return $this->orNotNull(Zonas::PREDIO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::PREDIO_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdEndsWith($integer) {
		return $this->orEndsWith(Zonas::PREDIO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orPredioIdContains($integer) {
		return $this->orContains(Zonas::PREDIO_ID, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByPredioIdAsc() {
		return $this->orderBy(Zonas::PREDIO_ID, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByPredioIdDesc() {
		return $this->orderBy(Zonas::PREDIO_ID, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByPredioId() {
		return $this->groupBy(Zonas::PREDIO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZona($integer) {
		return $this->addAnd(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaNot($integer) {
		return $this->andNot(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaLike($integer) {
		return $this->andLike(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaNotLike($integer) {
		return $this->andNotLike(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaGreater($integer) {
		return $this->andGreater(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaLess($integer) {
		return $this->andLess(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaLessEqual($integer) {
		return $this->andLessEqual(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaNull() {
		return $this->andNull(Zonas::TIPO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaNotNull() {
		return $this->andNotNull(Zonas::TIPO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::TIPO_ZONA, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaEndsWith($integer) {
		return $this->andEndsWith(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andTipoZonaContains($integer) {
		return $this->andContains(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZona($integer) {
		return $this->or(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaNot($integer) {
		return $this->orNot(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaLike($integer) {
		return $this->orLike(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaNotLike($integer) {
		return $this->orNotLike(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaGreater($integer) {
		return $this->orGreater(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaLess($integer) {
		return $this->orLess(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaLessEqual($integer) {
		return $this->orLessEqual(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaNull() {
		return $this->orNull(Zonas::TIPO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaNotNull() {
		return $this->orNotNull(Zonas::TIPO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::TIPO_ZONA, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaEndsWith($integer) {
		return $this->orEndsWith(Zonas::TIPO_ZONA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orTipoZonaContains($integer) {
		return $this->orContains(Zonas::TIPO_ZONA, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByTipoZonaAsc() {
		return $this->orderBy(Zonas::TIPO_ZONA, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByTipoZonaDesc() {
		return $this->orderBy(Zonas::TIPO_ZONA, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByTipoZona() {
		return $this->groupBy(Zonas::TIPO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Zonas::CREATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Zonas::CREATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Zonas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Zonas::CREATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Zonas::CREATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Zonas::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Zonas::CREATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Zonas::CREATED_AT, $timestamp);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Zonas::CREATED_AT, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Zonas::CREATED_AT, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Zonas::CREATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Zonas::UPDATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Zonas::UPDATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Zonas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Zonas::UPDATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Zonas::UPDATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Zonas::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Zonas::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Zonas::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Zonas::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Zonas::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Zonas::UPDATED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Zonas::DELETED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Zonas::DELETED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Zonas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Zonas::DELETED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Zonas::DELETED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Zonas::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Zonas::DELETED_AT, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Zonas::DELETED_AT, $timestamp);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Zonas::DELETED_AT, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Zonas::DELETED_AT, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Zonas::DELETED_AT);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroId($integer) {
		return $this->addAnd(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdNot($integer) {
		return $this->andNot(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdLike($integer) {
		return $this->andLike(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdNotLike($integer) {
		return $this->andNotLike(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdGreater($integer) {
		return $this->andGreater(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdLess($integer) {
		return $this->andLess(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdLessEqual($integer) {
		return $this->andLessEqual(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdNull() {
		return $this->andNull(Zonas::MIEMBRO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdNotNull() {
		return $this->andNotNull(Zonas::MIEMBRO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::MIEMBRO_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdEndsWith($integer) {
		return $this->andEndsWith(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andMiembroIdContains($integer) {
		return $this->andContains(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroId($integer) {
		return $this->or(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdNot($integer) {
		return $this->orNot(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdLike($integer) {
		return $this->orLike(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdNotLike($integer) {
		return $this->orNotLike(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdGreater($integer) {
		return $this->orGreater(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdLess($integer) {
		return $this->orLess(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdLessEqual($integer) {
		return $this->orLessEqual(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdNull() {
		return $this->orNull(Zonas::MIEMBRO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdNotNull() {
		return $this->orNotNull(Zonas::MIEMBRO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::MIEMBRO_ID, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdEndsWith($integer) {
		return $this->orEndsWith(Zonas::MIEMBRO_ID, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orMiembroIdContains($integer) {
		return $this->orContains(Zonas::MIEMBRO_ID, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByMiembroIdAsc() {
		return $this->orderBy(Zonas::MIEMBRO_ID, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByMiembroIdDesc() {
		return $this->orderBy(Zonas::MIEMBRO_ID, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByMiembroId() {
		return $this->groupBy(Zonas::MIEMBRO_ID);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZona($varchar) {
		return $this->addAnd(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaNot($varchar) {
		return $this->andNot(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaLike($varchar) {
		return $this->andLike(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaNotLike($varchar) {
		return $this->andNotLike(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaGreater($varchar) {
		return $this->andGreater(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaLess($varchar) {
		return $this->andLess(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaLessEqual($varchar) {
		return $this->andLessEqual(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaNull() {
		return $this->andNull(Zonas::NUMERO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaNotNull() {
		return $this->andNotNull(Zonas::NUMERO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaBetween($varchar, $from, $to) {
		return $this->andBetween(Zonas::NUMERO_ZONA, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaBeginsWith($varchar) {
		return $this->andBeginsWith(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaEndsWith($varchar) {
		return $this->andEndsWith(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andNumeroZonaContains($varchar) {
		return $this->andContains(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZona($varchar) {
		return $this->or(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaNot($varchar) {
		return $this->orNot(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaLike($varchar) {
		return $this->orLike(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaNotLike($varchar) {
		return $this->orNotLike(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaGreater($varchar) {
		return $this->orGreater(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaLess($varchar) {
		return $this->orLess(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaLessEqual($varchar) {
		return $this->orLessEqual(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaNull() {
		return $this->orNull(Zonas::NUMERO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaNotNull() {
		return $this->orNotNull(Zonas::NUMERO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaBetween($varchar, $from, $to) {
		return $this->orBetween(Zonas::NUMERO_ZONA, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaBeginsWith($varchar) {
		return $this->orBeginsWith(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaEndsWith($varchar) {
		return $this->orEndsWith(Zonas::NUMERO_ZONA, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orNumeroZonaContains($varchar) {
		return $this->orContains(Zonas::NUMERO_ZONA, $varchar);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByNumeroZonaAsc() {
		return $this->orderBy(Zonas::NUMERO_ZONA, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByNumeroZonaDesc() {
		return $this->orderBy(Zonas::NUMERO_ZONA, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByNumeroZona() {
		return $this->groupBy(Zonas::NUMERO_ZONA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andArea($integer) {
		return $this->addAnd(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaNot($integer) {
		return $this->andNot(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaLike($integer) {
		return $this->andLike(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaNotLike($integer) {
		return $this->andNotLike(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaGreater($integer) {
		return $this->andGreater(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaGreaterEqual($integer) {
		return $this->andGreaterEqual(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaLess($integer) {
		return $this->andLess(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaLessEqual($integer) {
		return $this->andLessEqual(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaNull() {
		return $this->andNull(Zonas::AREA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaNotNull() {
		return $this->andNotNull(Zonas::AREA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaBetween($integer, $from, $to) {
		return $this->andBetween(Zonas::AREA, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaBeginsWith($integer) {
		return $this->andBeginsWith(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaEndsWith($integer) {
		return $this->andEndsWith(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function andAreaContains($integer) {
		return $this->andContains(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orArea($integer) {
		return $this->or(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaNot($integer) {
		return $this->orNot(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaLike($integer) {
		return $this->orLike(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaNotLike($integer) {
		return $this->orNotLike(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaGreater($integer) {
		return $this->orGreater(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaGreaterEqual($integer) {
		return $this->orGreaterEqual(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaLess($integer) {
		return $this->orLess(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaLessEqual($integer) {
		return $this->orLessEqual(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaNull() {
		return $this->orNull(Zonas::AREA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaNotNull() {
		return $this->orNotNull(Zonas::AREA);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaBetween($integer, $from, $to) {
		return $this->orBetween(Zonas::AREA, $integer, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaBeginsWith($integer) {
		return $this->orBeginsWith(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaEndsWith($integer) {
		return $this->orEndsWith(Zonas::AREA, $integer);
	}

	/**
	 * @return ZonasQuery
	 */
	function orAreaContains($integer) {
		return $this->orContains(Zonas::AREA, $integer);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByAreaAsc() {
		return $this->orderBy(Zonas::AREA, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByAreaDesc() {
		return $this->orderBy(Zonas::AREA, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByArea() {
		return $this->groupBy(Zonas::AREA);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistro($timestamp) {
		return $this->addAnd(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroNot($timestamp) {
		return $this->andNot(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroLike($timestamp) {
		return $this->andLike(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroNotLike($timestamp) {
		return $this->andNotLike(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroGreater($timestamp) {
		return $this->andGreater(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroLess($timestamp) {
		return $this->andLess(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroLessEqual($timestamp) {
		return $this->andLessEqual(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroNull() {
		return $this->andNull(Zonas::FECHA_REGISTRO);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroNotNull() {
		return $this->andNotNull(Zonas::FECHA_REGISTRO);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroBetween($timestamp, $from, $to) {
		return $this->andBetween(Zonas::FECHA_REGISTRO, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroBeginsWith($timestamp) {
		return $this->andBeginsWith(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroEndsWith($timestamp) {
		return $this->andEndsWith(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function andFechaRegistroContains($timestamp) {
		return $this->andContains(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistro($timestamp) {
		return $this->or(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroNot($timestamp) {
		return $this->orNot(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroLike($timestamp) {
		return $this->orLike(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroNotLike($timestamp) {
		return $this->orNotLike(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroGreater($timestamp) {
		return $this->orGreater(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroLess($timestamp) {
		return $this->orLess(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroLessEqual($timestamp) {
		return $this->orLessEqual(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroNull() {
		return $this->orNull(Zonas::FECHA_REGISTRO);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroNotNull() {
		return $this->orNotNull(Zonas::FECHA_REGISTRO);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroBetween($timestamp, $from, $to) {
		return $this->orBetween(Zonas::FECHA_REGISTRO, $timestamp, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroBeginsWith($timestamp) {
		return $this->orBeginsWith(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroEndsWith($timestamp) {
		return $this->orEndsWith(Zonas::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return ZonasQuery
	 */
	function orFechaRegistroContains($timestamp) {
		return $this->orContains(Zonas::FECHA_REGISTRO, $timestamp);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByFechaRegistroAsc() {
		return $this->orderBy(Zonas::FECHA_REGISTRO, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByFechaRegistroDesc() {
		return $this->orderBy(Zonas::FECHA_REGISTRO, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByFechaRegistro() {
		return $this->groupBy(Zonas::FECHA_REGISTRO);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadas($varchar) {
		return $this->addAnd(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasNot($varchar) {
		return $this->andNot(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasLike($varchar) {
		return $this->andLike(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasNotLike($varchar) {
		return $this->andNotLike(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasGreater($varchar) {
		return $this->andGreater(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasGreaterEqual($varchar) {
		return $this->andGreaterEqual(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasLess($varchar) {
		return $this->andLess(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasLessEqual($varchar) {
		return $this->andLessEqual(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasNull() {
		return $this->andNull(Zonas::COORDENADAS);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasNotNull() {
		return $this->andNotNull(Zonas::COORDENADAS);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasBetween($varchar, $from, $to) {
		return $this->andBetween(Zonas::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasBeginsWith($varchar) {
		return $this->andBeginsWith(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasEndsWith($varchar) {
		return $this->andEndsWith(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andCoordenadasContains($varchar) {
		return $this->andContains(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadas($varchar) {
		return $this->or(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasNot($varchar) {
		return $this->orNot(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasLike($varchar) {
		return $this->orLike(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasNotLike($varchar) {
		return $this->orNotLike(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasGreater($varchar) {
		return $this->orGreater(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasGreaterEqual($varchar) {
		return $this->orGreaterEqual(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasLess($varchar) {
		return $this->orLess(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasLessEqual($varchar) {
		return $this->orLessEqual(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasNull() {
		return $this->orNull(Zonas::COORDENADAS);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasNotNull() {
		return $this->orNotNull(Zonas::COORDENADAS);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasBetween($varchar, $from, $to) {
		return $this->orBetween(Zonas::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasBeginsWith($varchar) {
		return $this->orBeginsWith(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasEndsWith($varchar) {
		return $this->orEndsWith(Zonas::COORDENADAS, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orCoordenadasContains($varchar) {
		return $this->orContains(Zonas::COORDENADAS, $varchar);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByCoordenadasAsc() {
		return $this->orderBy(Zonas::COORDENADAS, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByCoordenadasDesc() {
		return $this->orderBy(Zonas::COORDENADAS, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByCoordenadas() {
		return $this->groupBy(Zonas::COORDENADAS);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Zonas::DIRECCION);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Zonas::DIRECCION);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Zonas::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Zonas::DIRECCION);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Zonas::DIRECCION);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Zonas::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Zonas::DIRECCION, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Zonas::DIRECCION, $varchar);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Zonas::DIRECCION, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Zonas::DIRECCION, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Zonas::DIRECCION);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColor($varchar) {
		return $this->addAnd(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorNot($varchar) {
		return $this->andNot(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorLike($varchar) {
		return $this->andLike(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorNotLike($varchar) {
		return $this->andNotLike(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorGreater($varchar) {
		return $this->andGreater(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorGreaterEqual($varchar) {
		return $this->andGreaterEqual(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorLess($varchar) {
		return $this->andLess(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorLessEqual($varchar) {
		return $this->andLessEqual(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorNull() {
		return $this->andNull(Zonas::COLOR);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorNotNull() {
		return $this->andNotNull(Zonas::COLOR);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorBetween($varchar, $from, $to) {
		return $this->andBetween(Zonas::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorBeginsWith($varchar) {
		return $this->andBeginsWith(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorEndsWith($varchar) {
		return $this->andEndsWith(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function andColorContains($varchar) {
		return $this->andContains(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColor($varchar) {
		return $this->or(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorNot($varchar) {
		return $this->orNot(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorLike($varchar) {
		return $this->orLike(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorNotLike($varchar) {
		return $this->orNotLike(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorGreater($varchar) {
		return $this->orGreater(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorGreaterEqual($varchar) {
		return $this->orGreaterEqual(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorLess($varchar) {
		return $this->orLess(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorLessEqual($varchar) {
		return $this->orLessEqual(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorNull() {
		return $this->orNull(Zonas::COLOR);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorNotNull() {
		return $this->orNotNull(Zonas::COLOR);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorBetween($varchar, $from, $to) {
		return $this->orBetween(Zonas::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorBeginsWith($varchar) {
		return $this->orBeginsWith(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorEndsWith($varchar) {
		return $this->orEndsWith(Zonas::COLOR, $varchar);
	}

	/**
	 * @return ZonasQuery
	 */
	function orColorContains($varchar) {
		return $this->orContains(Zonas::COLOR, $varchar);
	}


	/**
	 * @return ZonasQuery
	 */
	function orderByColorAsc() {
		return $this->orderBy(Zonas::COLOR, self::ASC);
	}

	/**
	 * @return ZonasQuery
	 */
	function orderByColorDesc() {
		return $this->orderBy(Zonas::COLOR, self::DESC);
	}

	/**
	 * @return ZonasQuery
	 */
	function groupByColor() {
		return $this->groupBy(Zonas::COLOR);
	}


}