<?php

use Dabl\Query\Query;

abstract class baseTipoZonaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = TipoZona::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return TipoZonaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new TipoZonaQuery($table_name, $alias);
	}

	/**
	 * @return TipoZona[]
	 */
	function select() {
		return TipoZona::doSelect($this);
	}

	/**
	 * @return TipoZona
	 */
	function selectOne() {
		return TipoZona::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return TipoZona::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return TipoZona::doCount($this);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TipoZona::isTemporalType($type)) {
			$value = TipoZona::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TipoZona::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && TipoZona::isTemporalType($type)) {
			$value = TipoZona::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = TipoZona::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andId($integer) {
		return $this->addAnd(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdNull() {
		return $this->andNull(TipoZona::ID);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(TipoZona::ID);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(TipoZona::ID, $integer, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orId($integer) {
		return $this->or(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdNull() {
		return $this->orNull(TipoZona::ID);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(TipoZona::ID);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(TipoZona::ID, $integer, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(TipoZona::ID, $integer);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(TipoZona::ID, $integer);
	}


	/**
	 * @return TipoZonaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(TipoZona::ID, self::ASC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(TipoZona::ID, self::DESC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function groupById() {
		return $this->groupBy(TipoZona::ID);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZona($varchar) {
		return $this->addAnd(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaNot($varchar) {
		return $this->andNot(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaLike($varchar) {
		return $this->andLike(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaNotLike($varchar) {
		return $this->andNotLike(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaGreater($varchar) {
		return $this->andGreater(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaGreaterEqual($varchar) {
		return $this->andGreaterEqual(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaLess($varchar) {
		return $this->andLess(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaLessEqual($varchar) {
		return $this->andLessEqual(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaNull() {
		return $this->andNull(TipoZona::TIPO_ZONA);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaNotNull() {
		return $this->andNotNull(TipoZona::TIPO_ZONA);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaBetween($varchar, $from, $to) {
		return $this->andBetween(TipoZona::TIPO_ZONA, $varchar, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaBeginsWith($varchar) {
		return $this->andBeginsWith(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaEndsWith($varchar) {
		return $this->andEndsWith(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andTipoZonaContains($varchar) {
		return $this->andContains(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZona($varchar) {
		return $this->or(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaNot($varchar) {
		return $this->orNot(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaLike($varchar) {
		return $this->orLike(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaNotLike($varchar) {
		return $this->orNotLike(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaGreater($varchar) {
		return $this->orGreater(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaGreaterEqual($varchar) {
		return $this->orGreaterEqual(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaLess($varchar) {
		return $this->orLess(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaLessEqual($varchar) {
		return $this->orLessEqual(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaNull() {
		return $this->orNull(TipoZona::TIPO_ZONA);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaNotNull() {
		return $this->orNotNull(TipoZona::TIPO_ZONA);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaBetween($varchar, $from, $to) {
		return $this->orBetween(TipoZona::TIPO_ZONA, $varchar, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaBeginsWith($varchar) {
		return $this->orBeginsWith(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaEndsWith($varchar) {
		return $this->orEndsWith(TipoZona::TIPO_ZONA, $varchar);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orTipoZonaContains($varchar) {
		return $this->orContains(TipoZona::TIPO_ZONA, $varchar);
	}


	/**
	 * @return TipoZonaQuery
	 */
	function orderByTipoZonaAsc() {
		return $this->orderBy(TipoZona::TIPO_ZONA, self::ASC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orderByTipoZonaDesc() {
		return $this->orderBy(TipoZona::TIPO_ZONA, self::DESC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function groupByTipoZona() {
		return $this->groupBy(TipoZona::TIPO_ZONA);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(TipoZona::CREATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(TipoZona::CREATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TipoZona::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(TipoZona::CREATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(TipoZona::CREATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TipoZona::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TipoZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(TipoZona::CREATED_AT, $timestamp);
	}


	/**
	 * @return TipoZonaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(TipoZona::CREATED_AT, self::ASC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(TipoZona::CREATED_AT, self::DESC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(TipoZona::CREATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(TipoZona::UPDATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(TipoZona::UPDATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TipoZona::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(TipoZona::UPDATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(TipoZona::UPDATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TipoZona::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(TipoZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(TipoZona::UPDATED_AT, $timestamp);
	}


	/**
	 * @return TipoZonaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(TipoZona::UPDATED_AT, self::ASC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(TipoZona::UPDATED_AT, self::DESC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(TipoZona::UPDATED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(TipoZona::DELETED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(TipoZona::DELETED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(TipoZona::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(TipoZona::DELETED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(TipoZona::DELETED_AT);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(TipoZona::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(TipoZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(TipoZona::DELETED_AT, $timestamp);
	}


	/**
	 * @return TipoZonaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(TipoZona::DELETED_AT, self::ASC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(TipoZona::DELETED_AT, self::DESC);
	}

	/**
	 * @return TipoZonaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(TipoZona::DELETED_AT);
	}


}