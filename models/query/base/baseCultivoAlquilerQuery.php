<?php

use Dabl\Query\Query;

abstract class baseCultivoAlquilerQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = CultivoAlquiler::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return CultivoAlquilerQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new CultivoAlquilerQuery($table_name, $alias);
	}

	/**
	 * @return CultivoAlquiler[]
	 */
	function select() {
		return CultivoAlquiler::doSelect($this);
	}

	/**
	 * @return CultivoAlquiler
	 */
	function selectOne() {
		return CultivoAlquiler::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return CultivoAlquiler::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return CultivoAlquiler::doCount($this);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && CultivoAlquiler::isTemporalType($type)) {
			$value = CultivoAlquiler::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = CultivoAlquiler::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && CultivoAlquiler::isTemporalType($type)) {
			$value = CultivoAlquiler::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = CultivoAlquiler::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andId($integer) {
		return $this->addAnd(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdNull() {
		return $this->andNull(CultivoAlquiler::ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(CultivoAlquiler::ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(CultivoAlquiler::ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orId($integer) {
		return $this->or(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdNull() {
		return $this->orNull(CultivoAlquiler::ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(CultivoAlquiler::ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(CultivoAlquiler::ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(CultivoAlquiler::ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(CultivoAlquiler::ID, $integer);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(CultivoAlquiler::ID, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(CultivoAlquiler::ID, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupById() {
		return $this->groupBy(CultivoAlquiler::ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoId($integer) {
		return $this->addAnd(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdNot($integer) {
		return $this->andNot(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdLike($integer) {
		return $this->andLike(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdNotLike($integer) {
		return $this->andNotLike(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdGreater($integer) {
		return $this->andGreater(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdLess($integer) {
		return $this->andLess(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdLessEqual($integer) {
		return $this->andLessEqual(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdNull() {
		return $this->andNull(CultivoAlquiler::CULTIVO_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdNotNull() {
		return $this->andNotNull(CultivoAlquiler::CULTIVO_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdBetween($integer, $from, $to) {
		return $this->andBetween(CultivoAlquiler::CULTIVO_ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdBeginsWith($integer) {
		return $this->andBeginsWith(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdEndsWith($integer) {
		return $this->andEndsWith(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCultivoIdContains($integer) {
		return $this->andContains(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoId($integer) {
		return $this->or(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdNot($integer) {
		return $this->orNot(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdLike($integer) {
		return $this->orLike(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdNotLike($integer) {
		return $this->orNotLike(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdGreater($integer) {
		return $this->orGreater(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdLess($integer) {
		return $this->orLess(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdLessEqual($integer) {
		return $this->orLessEqual(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdNull() {
		return $this->orNull(CultivoAlquiler::CULTIVO_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdNotNull() {
		return $this->orNotNull(CultivoAlquiler::CULTIVO_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdBetween($integer, $from, $to) {
		return $this->orBetween(CultivoAlquiler::CULTIVO_ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdBeginsWith($integer) {
		return $this->orBeginsWith(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdEndsWith($integer) {
		return $this->orEndsWith(CultivoAlquiler::CULTIVO_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCultivoIdContains($integer) {
		return $this->orContains(CultivoAlquiler::CULTIVO_ID, $integer);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByCultivoIdAsc() {
		return $this->orderBy(CultivoAlquiler::CULTIVO_ID, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByCultivoIdDesc() {
		return $this->orderBy(CultivoAlquiler::CULTIVO_ID, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupByCultivoId() {
		return $this->groupBy(CultivoAlquiler::CULTIVO_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerId($integer) {
		return $this->addAnd(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdNot($integer) {
		return $this->andNot(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdLike($integer) {
		return $this->andLike(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdNotLike($integer) {
		return $this->andNotLike(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdGreater($integer) {
		return $this->andGreater(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdGreaterEqual($integer) {
		return $this->andGreaterEqual(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdLess($integer) {
		return $this->andLess(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdLessEqual($integer) {
		return $this->andLessEqual(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdNull() {
		return $this->andNull(CultivoAlquiler::ALQUILER_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdNotNull() {
		return $this->andNotNull(CultivoAlquiler::ALQUILER_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdBetween($integer, $from, $to) {
		return $this->andBetween(CultivoAlquiler::ALQUILER_ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdBeginsWith($integer) {
		return $this->andBeginsWith(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdEndsWith($integer) {
		return $this->andEndsWith(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andAlquilerIdContains($integer) {
		return $this->andContains(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerId($integer) {
		return $this->or(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdNot($integer) {
		return $this->orNot(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdLike($integer) {
		return $this->orLike(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdNotLike($integer) {
		return $this->orNotLike(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdGreater($integer) {
		return $this->orGreater(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdGreaterEqual($integer) {
		return $this->orGreaterEqual(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdLess($integer) {
		return $this->orLess(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdLessEqual($integer) {
		return $this->orLessEqual(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdNull() {
		return $this->orNull(CultivoAlquiler::ALQUILER_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdNotNull() {
		return $this->orNotNull(CultivoAlquiler::ALQUILER_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdBetween($integer, $from, $to) {
		return $this->orBetween(CultivoAlquiler::ALQUILER_ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdBeginsWith($integer) {
		return $this->orBeginsWith(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdEndsWith($integer) {
		return $this->orEndsWith(CultivoAlquiler::ALQUILER_ID, $integer);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orAlquilerIdContains($integer) {
		return $this->orContains(CultivoAlquiler::ALQUILER_ID, $integer);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByAlquilerIdAsc() {
		return $this->orderBy(CultivoAlquiler::ALQUILER_ID, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByAlquilerIdDesc() {
		return $this->orderBy(CultivoAlquiler::ALQUILER_ID, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupByAlquilerId() {
		return $this->groupBy(CultivoAlquiler::ALQUILER_ID);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(CultivoAlquiler::CREATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(CultivoAlquiler::CREATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(CultivoAlquiler::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(CultivoAlquiler::CREATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(CultivoAlquiler::CREATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(CultivoAlquiler::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(CultivoAlquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(CultivoAlquiler::CREATED_AT, $timestamp);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(CultivoAlquiler::CREATED_AT, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(CultivoAlquiler::CREATED_AT, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(CultivoAlquiler::CREATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(CultivoAlquiler::UPDATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(CultivoAlquiler::UPDATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(CultivoAlquiler::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(CultivoAlquiler::UPDATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(CultivoAlquiler::UPDATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(CultivoAlquiler::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(CultivoAlquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(CultivoAlquiler::UPDATED_AT, $timestamp);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(CultivoAlquiler::UPDATED_AT, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(CultivoAlquiler::UPDATED_AT, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(CultivoAlquiler::UPDATED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(CultivoAlquiler::DELETED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(CultivoAlquiler::DELETED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(CultivoAlquiler::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(CultivoAlquiler::DELETED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(CultivoAlquiler::DELETED_AT);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(CultivoAlquiler::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(CultivoAlquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(CultivoAlquiler::DELETED_AT, $timestamp);
	}


	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(CultivoAlquiler::DELETED_AT, self::ASC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(CultivoAlquiler::DELETED_AT, self::DESC);
	}

	/**
	 * @return CultivoAlquilerQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(CultivoAlquiler::DELETED_AT);
	}


}