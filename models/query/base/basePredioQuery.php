<?php

use Dabl\Query\Query;

abstract class basePredioQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Predio::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return PredioQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new PredioQuery($table_name, $alias);
	}

	/**
	 * @return Predio[]
	 */
	function select() {
		return Predio::doSelect($this);
	}

	/**
	 * @return Predio
	 */
	function selectOne() {
		return Predio::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Predio::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Predio::doCount($this);
	}

	/**
	 * @return PredioQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Predio::isTemporalType($type)) {
			$value = Predio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Predio::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return PredioQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Predio::isTemporalType($type)) {
			$value = Predio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Predio::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return PredioQuery
	 */
	function andId($integer) {
		return $this->addAnd(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdNull() {
		return $this->andNull(Predio::ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Predio::ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Predio::ID, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orId($integer) {
		return $this->or(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdNull() {
		return $this->orNull(Predio::ID);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Predio::ID);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Predio::ID, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Predio::ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Predio::ID, $integer);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Predio::ID, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Predio::ID, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupById() {
		return $this->groupBy(Predio::ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredio($varchar) {
		return $this->addAnd(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioNot($varchar) {
		return $this->andNot(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioLike($varchar) {
		return $this->andLike(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioNotLike($varchar) {
		return $this->andNotLike(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioGreater($varchar) {
		return $this->andGreater(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioGreaterEqual($varchar) {
		return $this->andGreaterEqual(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioLess($varchar) {
		return $this->andLess(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioLessEqual($varchar) {
		return $this->andLessEqual(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioNull() {
		return $this->andNull(Predio::NUMERO_PREDIO);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioNotNull() {
		return $this->andNotNull(Predio::NUMERO_PREDIO);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioBetween($varchar, $from, $to) {
		return $this->andBetween(Predio::NUMERO_PREDIO, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioBeginsWith($varchar) {
		return $this->andBeginsWith(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioEndsWith($varchar) {
		return $this->andEndsWith(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andNumeroPredioContains($varchar) {
		return $this->andContains(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredio($varchar) {
		return $this->or(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioNot($varchar) {
		return $this->orNot(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioLike($varchar) {
		return $this->orLike(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioNotLike($varchar) {
		return $this->orNotLike(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioGreater($varchar) {
		return $this->orGreater(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioGreaterEqual($varchar) {
		return $this->orGreaterEqual(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioLess($varchar) {
		return $this->orLess(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioLessEqual($varchar) {
		return $this->orLessEqual(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioNull() {
		return $this->orNull(Predio::NUMERO_PREDIO);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioNotNull() {
		return $this->orNotNull(Predio::NUMERO_PREDIO);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioBetween($varchar, $from, $to) {
		return $this->orBetween(Predio::NUMERO_PREDIO, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioBeginsWith($varchar) {
		return $this->orBeginsWith(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioEndsWith($varchar) {
		return $this->orEndsWith(Predio::NUMERO_PREDIO, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orNumeroPredioContains($varchar) {
		return $this->orContains(Predio::NUMERO_PREDIO, $varchar);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByNumeroPredioAsc() {
		return $this->orderBy(Predio::NUMERO_PREDIO, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByNumeroPredioDesc() {
		return $this->orderBy(Predio::NUMERO_PREDIO, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByNumeroPredio() {
		return $this->groupBy(Predio::NUMERO_PREDIO);
	}

	/**
	 * @return PredioQuery
	 */
	function andArea($integer) {
		return $this->addAnd(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaNot($integer) {
		return $this->andNot(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaLike($integer) {
		return $this->andLike(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaNotLike($integer) {
		return $this->andNotLike(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaGreater($integer) {
		return $this->andGreater(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaGreaterEqual($integer) {
		return $this->andGreaterEqual(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaLess($integer) {
		return $this->andLess(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaLessEqual($integer) {
		return $this->andLessEqual(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaNull() {
		return $this->andNull(Predio::AREA);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaNotNull() {
		return $this->andNotNull(Predio::AREA);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaBetween($integer, $from, $to) {
		return $this->andBetween(Predio::AREA, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaBeginsWith($integer) {
		return $this->andBeginsWith(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaEndsWith($integer) {
		return $this->andEndsWith(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andAreaContains($integer) {
		return $this->andContains(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orArea($integer) {
		return $this->or(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaNot($integer) {
		return $this->orNot(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaLike($integer) {
		return $this->orLike(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaNotLike($integer) {
		return $this->orNotLike(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaGreater($integer) {
		return $this->orGreater(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaGreaterEqual($integer) {
		return $this->orGreaterEqual(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaLess($integer) {
		return $this->orLess(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaLessEqual($integer) {
		return $this->orLessEqual(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaNull() {
		return $this->orNull(Predio::AREA);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaNotNull() {
		return $this->orNotNull(Predio::AREA);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaBetween($integer, $from, $to) {
		return $this->orBetween(Predio::AREA, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaBeginsWith($integer) {
		return $this->orBeginsWith(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaEndsWith($integer) {
		return $this->orEndsWith(Predio::AREA, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orAreaContains($integer) {
		return $this->orContains(Predio::AREA, $integer);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByAreaAsc() {
		return $this->orderBy(Predio::AREA, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByAreaDesc() {
		return $this->orderBy(Predio::AREA, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByArea() {
		return $this->groupBy(Predio::AREA);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistro($timestamp) {
		return $this->addAnd(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroNot($timestamp) {
		return $this->andNot(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroLike($timestamp) {
		return $this->andLike(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroNotLike($timestamp) {
		return $this->andNotLike(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroGreater($timestamp) {
		return $this->andGreater(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroLess($timestamp) {
		return $this->andLess(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroLessEqual($timestamp) {
		return $this->andLessEqual(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroNull() {
		return $this->andNull(Predio::FECHA_REGISTRO);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroNotNull() {
		return $this->andNotNull(Predio::FECHA_REGISTRO);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroBetween($timestamp, $from, $to) {
		return $this->andBetween(Predio::FECHA_REGISTRO, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroBeginsWith($timestamp) {
		return $this->andBeginsWith(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroEndsWith($timestamp) {
		return $this->andEndsWith(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andFechaRegistroContains($timestamp) {
		return $this->andContains(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistro($timestamp) {
		return $this->or(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroNot($timestamp) {
		return $this->orNot(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroLike($timestamp) {
		return $this->orLike(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroNotLike($timestamp) {
		return $this->orNotLike(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroGreater($timestamp) {
		return $this->orGreater(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroLess($timestamp) {
		return $this->orLess(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroLessEqual($timestamp) {
		return $this->orLessEqual(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroNull() {
		return $this->orNull(Predio::FECHA_REGISTRO);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroNotNull() {
		return $this->orNotNull(Predio::FECHA_REGISTRO);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroBetween($timestamp, $from, $to) {
		return $this->orBetween(Predio::FECHA_REGISTRO, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroBeginsWith($timestamp) {
		return $this->orBeginsWith(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroEndsWith($timestamp) {
		return $this->orEndsWith(Predio::FECHA_REGISTRO, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orFechaRegistroContains($timestamp) {
		return $this->orContains(Predio::FECHA_REGISTRO, $timestamp);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByFechaRegistroAsc() {
		return $this->orderBy(Predio::FECHA_REGISTRO, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByFechaRegistroDesc() {
		return $this->orderBy(Predio::FECHA_REGISTRO, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByFechaRegistro() {
		return $this->groupBy(Predio::FECHA_REGISTRO);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaId($integer) {
		return $this->addAnd(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdNot($integer) {
		return $this->andNot(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdLike($integer) {
		return $this->andLike(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdNotLike($integer) {
		return $this->andNotLike(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdGreater($integer) {
		return $this->andGreater(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdLess($integer) {
		return $this->andLess(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdLessEqual($integer) {
		return $this->andLessEqual(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdNull() {
		return $this->andNull(Predio::FAMILIA_ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdNotNull() {
		return $this->andNotNull(Predio::FAMILIA_ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdBetween($integer, $from, $to) {
		return $this->andBetween(Predio::FAMILIA_ID, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdBeginsWith($integer) {
		return $this->andBeginsWith(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdEndsWith($integer) {
		return $this->andEndsWith(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function andFamiliaIdContains($integer) {
		return $this->andContains(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaId($integer) {
		return $this->or(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdNot($integer) {
		return $this->orNot(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdLike($integer) {
		return $this->orLike(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdNotLike($integer) {
		return $this->orNotLike(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdGreater($integer) {
		return $this->orGreater(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdLess($integer) {
		return $this->orLess(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdLessEqual($integer) {
		return $this->orLessEqual(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdNull() {
		return $this->orNull(Predio::FAMILIA_ID);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdNotNull() {
		return $this->orNotNull(Predio::FAMILIA_ID);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdBetween($integer, $from, $to) {
		return $this->orBetween(Predio::FAMILIA_ID, $integer, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdBeginsWith($integer) {
		return $this->orBeginsWith(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdEndsWith($integer) {
		return $this->orEndsWith(Predio::FAMILIA_ID, $integer);
	}

	/**
	 * @return PredioQuery
	 */
	function orFamiliaIdContains($integer) {
		return $this->orContains(Predio::FAMILIA_ID, $integer);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByFamiliaIdAsc() {
		return $this->orderBy(Predio::FAMILIA_ID, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByFamiliaIdDesc() {
		return $this->orderBy(Predio::FAMILIA_ID, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByFamiliaId() {
		return $this->groupBy(Predio::FAMILIA_ID);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Predio::CREATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Predio::CREATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Predio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Predio::CREATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Predio::CREATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Predio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Predio::CREATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Predio::CREATED_AT, $timestamp);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Predio::CREATED_AT, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Predio::CREATED_AT, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Predio::CREATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Predio::UPDATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Predio::UPDATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Predio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Predio::UPDATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Predio::UPDATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Predio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Predio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Predio::UPDATED_AT, $timestamp);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Predio::UPDATED_AT, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Predio::UPDATED_AT, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Predio::UPDATED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Predio::DELETED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Predio::DELETED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Predio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Predio::DELETED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Predio::DELETED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Predio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Predio::DELETED_AT, $timestamp);
	}

	/**
	 * @return PredioQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Predio::DELETED_AT, $timestamp);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Predio::DELETED_AT, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Predio::DELETED_AT, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Predio::DELETED_AT);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadas($varchar) {
		return $this->addAnd(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasNot($varchar) {
		return $this->andNot(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasLike($varchar) {
		return $this->andLike(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasNotLike($varchar) {
		return $this->andNotLike(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasGreater($varchar) {
		return $this->andGreater(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasGreaterEqual($varchar) {
		return $this->andGreaterEqual(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasLess($varchar) {
		return $this->andLess(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasLessEqual($varchar) {
		return $this->andLessEqual(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasNull() {
		return $this->andNull(Predio::COORDENADAS);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasNotNull() {
		return $this->andNotNull(Predio::COORDENADAS);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasBetween($varchar, $from, $to) {
		return $this->andBetween(Predio::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasBeginsWith($varchar) {
		return $this->andBeginsWith(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasEndsWith($varchar) {
		return $this->andEndsWith(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andCoordenadasContains($varchar) {
		return $this->andContains(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadas($varchar) {
		return $this->or(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasNot($varchar) {
		return $this->orNot(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasLike($varchar) {
		return $this->orLike(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasNotLike($varchar) {
		return $this->orNotLike(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasGreater($varchar) {
		return $this->orGreater(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasGreaterEqual($varchar) {
		return $this->orGreaterEqual(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasLess($varchar) {
		return $this->orLess(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasLessEqual($varchar) {
		return $this->orLessEqual(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasNull() {
		return $this->orNull(Predio::COORDENADAS);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasNotNull() {
		return $this->orNotNull(Predio::COORDENADAS);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasBetween($varchar, $from, $to) {
		return $this->orBetween(Predio::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasBeginsWith($varchar) {
		return $this->orBeginsWith(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasEndsWith($varchar) {
		return $this->orEndsWith(Predio::COORDENADAS, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orCoordenadasContains($varchar) {
		return $this->orContains(Predio::COORDENADAS, $varchar);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByCoordenadasAsc() {
		return $this->orderBy(Predio::COORDENADAS, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByCoordenadasDesc() {
		return $this->orderBy(Predio::COORDENADAS, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByCoordenadas() {
		return $this->groupBy(Predio::COORDENADAS);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccion($varchar) {
		return $this->addAnd(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionNot($varchar) {
		return $this->andNot(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionLike($varchar) {
		return $this->andLike(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionNotLike($varchar) {
		return $this->andNotLike(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionGreater($varchar) {
		return $this->andGreater(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionGreaterEqual($varchar) {
		return $this->andGreaterEqual(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionLess($varchar) {
		return $this->andLess(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionLessEqual($varchar) {
		return $this->andLessEqual(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionNull() {
		return $this->andNull(Predio::DIRECCION);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionNotNull() {
		return $this->andNotNull(Predio::DIRECCION);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionBetween($varchar, $from, $to) {
		return $this->andBetween(Predio::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionBeginsWith($varchar) {
		return $this->andBeginsWith(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionEndsWith($varchar) {
		return $this->andEndsWith(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andDireccionContains($varchar) {
		return $this->andContains(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccion($varchar) {
		return $this->or(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionNot($varchar) {
		return $this->orNot(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionLike($varchar) {
		return $this->orLike(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionNotLike($varchar) {
		return $this->orNotLike(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionGreater($varchar) {
		return $this->orGreater(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionGreaterEqual($varchar) {
		return $this->orGreaterEqual(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionLess($varchar) {
		return $this->orLess(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionLessEqual($varchar) {
		return $this->orLessEqual(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionNull() {
		return $this->orNull(Predio::DIRECCION);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionNotNull() {
		return $this->orNotNull(Predio::DIRECCION);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionBetween($varchar, $from, $to) {
		return $this->orBetween(Predio::DIRECCION, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionBeginsWith($varchar) {
		return $this->orBeginsWith(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionEndsWith($varchar) {
		return $this->orEndsWith(Predio::DIRECCION, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orDireccionContains($varchar) {
		return $this->orContains(Predio::DIRECCION, $varchar);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByDireccionAsc() {
		return $this->orderBy(Predio::DIRECCION, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByDireccionDesc() {
		return $this->orderBy(Predio::DIRECCION, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByDireccion() {
		return $this->groupBy(Predio::DIRECCION);
	}

	/**
	 * @return PredioQuery
	 */
	function andColor($varchar) {
		return $this->addAnd(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorNot($varchar) {
		return $this->andNot(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorLike($varchar) {
		return $this->andLike(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorNotLike($varchar) {
		return $this->andNotLike(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorGreater($varchar) {
		return $this->andGreater(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorGreaterEqual($varchar) {
		return $this->andGreaterEqual(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorLess($varchar) {
		return $this->andLess(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorLessEqual($varchar) {
		return $this->andLessEqual(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorNull() {
		return $this->andNull(Predio::COLOR);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorNotNull() {
		return $this->andNotNull(Predio::COLOR);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorBetween($varchar, $from, $to) {
		return $this->andBetween(Predio::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorBeginsWith($varchar) {
		return $this->andBeginsWith(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorEndsWith($varchar) {
		return $this->andEndsWith(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function andColorContains($varchar) {
		return $this->andContains(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColor($varchar) {
		return $this->or(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorNot($varchar) {
		return $this->orNot(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorLike($varchar) {
		return $this->orLike(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorNotLike($varchar) {
		return $this->orNotLike(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorGreater($varchar) {
		return $this->orGreater(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorGreaterEqual($varchar) {
		return $this->orGreaterEqual(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorLess($varchar) {
		return $this->orLess(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorLessEqual($varchar) {
		return $this->orLessEqual(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorNull() {
		return $this->orNull(Predio::COLOR);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorNotNull() {
		return $this->orNotNull(Predio::COLOR);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorBetween($varchar, $from, $to) {
		return $this->orBetween(Predio::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorBeginsWith($varchar) {
		return $this->orBeginsWith(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorEndsWith($varchar) {
		return $this->orEndsWith(Predio::COLOR, $varchar);
	}

	/**
	 * @return PredioQuery
	 */
	function orColorContains($varchar) {
		return $this->orContains(Predio::COLOR, $varchar);
	}


	/**
	 * @return PredioQuery
	 */
	function orderByColorAsc() {
		return $this->orderBy(Predio::COLOR, self::ASC);
	}

	/**
	 * @return PredioQuery
	 */
	function orderByColorDesc() {
		return $this->orderBy(Predio::COLOR, self::DESC);
	}

	/**
	 * @return PredioQuery
	 */
	function groupByColor() {
		return $this->groupBy(Predio::COLOR);
	}


}