<?php

use Dabl\Query\Query;

abstract class baseAlquilerQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Alquiler::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return AlquilerQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new AlquilerQuery($table_name, $alias);
	}

	/**
	 * @return Alquiler[]
	 */
	function select() {
		return Alquiler::doSelect($this);
	}

	/**
	 * @return Alquiler
	 */
	function selectOne() {
		return Alquiler::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Alquiler::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Alquiler::doCount($this);
	}

	/**
	 * @return AlquilerQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Alquiler::isTemporalType($type)) {
			$value = Alquiler::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Alquiler::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return AlquilerQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Alquiler::isTemporalType($type)) {
			$value = Alquiler::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Alquiler::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andId($integer) {
		return $this->addAnd(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdNull() {
		return $this->andNull(Alquiler::ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Alquiler::ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Alquiler::ID, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orId($integer) {
		return $this->or(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdNull() {
		return $this->orNull(Alquiler::ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Alquiler::ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Alquiler::ID, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Alquiler::ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Alquiler::ID, $integer);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Alquiler::ID, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Alquiler::ID, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupById() {
		return $this->groupBy(Alquiler::ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatario($integer) {
		return $this->addAnd(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioNot($integer) {
		return $this->andNot(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioLike($integer) {
		return $this->andLike(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioNotLike($integer) {
		return $this->andNotLike(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioGreater($integer) {
		return $this->andGreater(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioGreaterEqual($integer) {
		return $this->andGreaterEqual(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioLess($integer) {
		return $this->andLess(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioLessEqual($integer) {
		return $this->andLessEqual(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioNull() {
		return $this->andNull(Alquiler::ARRENDATARIO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioNotNull() {
		return $this->andNotNull(Alquiler::ARRENDATARIO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioBetween($integer, $from, $to) {
		return $this->andBetween(Alquiler::ARRENDATARIO, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioBeginsWith($integer) {
		return $this->andBeginsWith(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioEndsWith($integer) {
		return $this->andEndsWith(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andArrendatarioContains($integer) {
		return $this->andContains(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatario($integer) {
		return $this->or(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioNot($integer) {
		return $this->orNot(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioLike($integer) {
		return $this->orLike(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioNotLike($integer) {
		return $this->orNotLike(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioGreater($integer) {
		return $this->orGreater(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioGreaterEqual($integer) {
		return $this->orGreaterEqual(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioLess($integer) {
		return $this->orLess(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioLessEqual($integer) {
		return $this->orLessEqual(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioNull() {
		return $this->orNull(Alquiler::ARRENDATARIO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioNotNull() {
		return $this->orNotNull(Alquiler::ARRENDATARIO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioBetween($integer, $from, $to) {
		return $this->orBetween(Alquiler::ARRENDATARIO, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioBeginsWith($integer) {
		return $this->orBeginsWith(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioEndsWith($integer) {
		return $this->orEndsWith(Alquiler::ARRENDATARIO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orArrendatarioContains($integer) {
		return $this->orContains(Alquiler::ARRENDATARIO, $integer);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByArrendatarioAsc() {
		return $this->orderBy(Alquiler::ARRENDATARIO, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByArrendatarioDesc() {
		return $this->orderBy(Alquiler::ARRENDATARIO, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByArrendatario() {
		return $this->groupBy(Alquiler::ARRENDATARIO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContrato($integer) {
		return $this->addAnd(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoNot($integer) {
		return $this->andNot(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoLike($integer) {
		return $this->andLike(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoNotLike($integer) {
		return $this->andNotLike(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoGreater($integer) {
		return $this->andGreater(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoGreaterEqual($integer) {
		return $this->andGreaterEqual(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoLess($integer) {
		return $this->andLess(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoLessEqual($integer) {
		return $this->andLessEqual(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoNull() {
		return $this->andNull(Alquiler::CONTRATO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoNotNull() {
		return $this->andNotNull(Alquiler::CONTRATO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoBetween($integer, $from, $to) {
		return $this->andBetween(Alquiler::CONTRATO, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoBeginsWith($integer) {
		return $this->andBeginsWith(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoEndsWith($integer) {
		return $this->andEndsWith(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andContratoContains($integer) {
		return $this->andContains(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContrato($integer) {
		return $this->or(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoNot($integer) {
		return $this->orNot(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoLike($integer) {
		return $this->orLike(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoNotLike($integer) {
		return $this->orNotLike(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoGreater($integer) {
		return $this->orGreater(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoGreaterEqual($integer) {
		return $this->orGreaterEqual(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoLess($integer) {
		return $this->orLess(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoLessEqual($integer) {
		return $this->orLessEqual(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoNull() {
		return $this->orNull(Alquiler::CONTRATO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoNotNull() {
		return $this->orNotNull(Alquiler::CONTRATO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoBetween($integer, $from, $to) {
		return $this->orBetween(Alquiler::CONTRATO, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoBeginsWith($integer) {
		return $this->orBeginsWith(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoEndsWith($integer) {
		return $this->orEndsWith(Alquiler::CONTRATO, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orContratoContains($integer) {
		return $this->orContains(Alquiler::CONTRATO, $integer);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByContratoAsc() {
		return $this->orderBy(Alquiler::CONTRATO, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByContratoDesc() {
		return $this->orderBy(Alquiler::CONTRATO, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByContrato() {
		return $this->groupBy(Alquiler::CONTRATO);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroId($integer) {
		return $this->addAnd(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdNot($integer) {
		return $this->andNot(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdLike($integer) {
		return $this->andLike(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdNotLike($integer) {
		return $this->andNotLike(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdGreater($integer) {
		return $this->andGreater(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdLess($integer) {
		return $this->andLess(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdLessEqual($integer) {
		return $this->andLessEqual(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdNull() {
		return $this->andNull(Alquiler::MIEMBRO_ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdNotNull() {
		return $this->andNotNull(Alquiler::MIEMBRO_ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdBetween($integer, $from, $to) {
		return $this->andBetween(Alquiler::MIEMBRO_ID, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdBeginsWith($integer) {
		return $this->andBeginsWith(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdEndsWith($integer) {
		return $this->andEndsWith(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andMiembroIdContains($integer) {
		return $this->andContains(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroId($integer) {
		return $this->or(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdNot($integer) {
		return $this->orNot(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdLike($integer) {
		return $this->orLike(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdNotLike($integer) {
		return $this->orNotLike(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdGreater($integer) {
		return $this->orGreater(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdLess($integer) {
		return $this->orLess(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdLessEqual($integer) {
		return $this->orLessEqual(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdNull() {
		return $this->orNull(Alquiler::MIEMBRO_ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdNotNull() {
		return $this->orNotNull(Alquiler::MIEMBRO_ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdBetween($integer, $from, $to) {
		return $this->orBetween(Alquiler::MIEMBRO_ID, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdBeginsWith($integer) {
		return $this->orBeginsWith(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdEndsWith($integer) {
		return $this->orEndsWith(Alquiler::MIEMBRO_ID, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orMiembroIdContains($integer) {
		return $this->orContains(Alquiler::MIEMBRO_ID, $integer);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByMiembroIdAsc() {
		return $this->orderBy(Alquiler::MIEMBRO_ID, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByMiembroIdDesc() {
		return $this->orderBy(Alquiler::MIEMBRO_ID, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByMiembroId() {
		return $this->groupBy(Alquiler::MIEMBRO_ID);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquiler($timestamp) {
		return $this->addAnd(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerNot($timestamp) {
		return $this->andNot(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerLike($timestamp) {
		return $this->andLike(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerNotLike($timestamp) {
		return $this->andNotLike(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerGreater($timestamp) {
		return $this->andGreater(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerLess($timestamp) {
		return $this->andLess(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerLessEqual($timestamp) {
		return $this->andLessEqual(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerNull() {
		return $this->andNull(Alquiler::FECHA_ALQUILER);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerNotNull() {
		return $this->andNotNull(Alquiler::FECHA_ALQUILER);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerBetween($timestamp, $from, $to) {
		return $this->andBetween(Alquiler::FECHA_ALQUILER, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerBeginsWith($timestamp) {
		return $this->andBeginsWith(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerEndsWith($timestamp) {
		return $this->andEndsWith(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andFechaAlquilerContains($timestamp) {
		return $this->andContains(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquiler($timestamp) {
		return $this->or(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerNot($timestamp) {
		return $this->orNot(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerLike($timestamp) {
		return $this->orLike(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerNotLike($timestamp) {
		return $this->orNotLike(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerGreater($timestamp) {
		return $this->orGreater(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerLess($timestamp) {
		return $this->orLess(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerLessEqual($timestamp) {
		return $this->orLessEqual(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerNull() {
		return $this->orNull(Alquiler::FECHA_ALQUILER);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerNotNull() {
		return $this->orNotNull(Alquiler::FECHA_ALQUILER);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerBetween($timestamp, $from, $to) {
		return $this->orBetween(Alquiler::FECHA_ALQUILER, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerBeginsWith($timestamp) {
		return $this->orBeginsWith(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerEndsWith($timestamp) {
		return $this->orEndsWith(Alquiler::FECHA_ALQUILER, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orFechaAlquilerContains($timestamp) {
		return $this->orContains(Alquiler::FECHA_ALQUILER, $timestamp);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByFechaAlquilerAsc() {
		return $this->orderBy(Alquiler::FECHA_ALQUILER, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByFechaAlquilerDesc() {
		return $this->orderBy(Alquiler::FECHA_ALQUILER, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByFechaAlquiler() {
		return $this->groupBy(Alquiler::FECHA_ALQUILER);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Alquiler::CREATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Alquiler::CREATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Alquiler::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Alquiler::CREATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Alquiler::CREATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Alquiler::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Alquiler::CREATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Alquiler::CREATED_AT, $timestamp);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Alquiler::CREATED_AT, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Alquiler::CREATED_AT, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Alquiler::CREATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Alquiler::UPDATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Alquiler::UPDATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Alquiler::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Alquiler::UPDATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Alquiler::UPDATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Alquiler::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Alquiler::UPDATED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Alquiler::UPDATED_AT, $timestamp);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Alquiler::UPDATED_AT, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Alquiler::UPDATED_AT, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Alquiler::UPDATED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Alquiler::DELETED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Alquiler::DELETED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Alquiler::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Alquiler::DELETED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Alquiler::DELETED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Alquiler::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Alquiler::DELETED_AT, $timestamp);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Alquiler::DELETED_AT, $timestamp);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Alquiler::DELETED_AT, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Alquiler::DELETED_AT, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Alquiler::DELETED_AT);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistrador($integer) {
		return $this->addAnd(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorNot($integer) {
		return $this->andNot(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorLike($integer) {
		return $this->andLike(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorNotLike($integer) {
		return $this->andNotLike(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorGreater($integer) {
		return $this->andGreater(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorGreaterEqual($integer) {
		return $this->andGreaterEqual(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorLess($integer) {
		return $this->andLess(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorLessEqual($integer) {
		return $this->andLessEqual(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorNull() {
		return $this->andNull(Alquiler::REGISTRADOR);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorNotNull() {
		return $this->andNotNull(Alquiler::REGISTRADOR);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorBetween($integer, $from, $to) {
		return $this->andBetween(Alquiler::REGISTRADOR, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorBeginsWith($integer) {
		return $this->andBeginsWith(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorEndsWith($integer) {
		return $this->andEndsWith(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function andRegistradorContains($integer) {
		return $this->andContains(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistrador($integer) {
		return $this->or(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorNot($integer) {
		return $this->orNot(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorLike($integer) {
		return $this->orLike(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorNotLike($integer) {
		return $this->orNotLike(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorGreater($integer) {
		return $this->orGreater(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorGreaterEqual($integer) {
		return $this->orGreaterEqual(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorLess($integer) {
		return $this->orLess(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorLessEqual($integer) {
		return $this->orLessEqual(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorNull() {
		return $this->orNull(Alquiler::REGISTRADOR);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorNotNull() {
		return $this->orNotNull(Alquiler::REGISTRADOR);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorBetween($integer, $from, $to) {
		return $this->orBetween(Alquiler::REGISTRADOR, $integer, $from, $to);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorBeginsWith($integer) {
		return $this->orBeginsWith(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorEndsWith($integer) {
		return $this->orEndsWith(Alquiler::REGISTRADOR, $integer);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orRegistradorContains($integer) {
		return $this->orContains(Alquiler::REGISTRADOR, $integer);
	}


	/**
	 * @return AlquilerQuery
	 */
	function orderByRegistradorAsc() {
		return $this->orderBy(Alquiler::REGISTRADOR, self::ASC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function orderByRegistradorDesc() {
		return $this->orderBy(Alquiler::REGISTRADOR, self::DESC);
	}

	/**
	 * @return AlquilerQuery
	 */
	function groupByRegistrador() {
		return $this->groupBy(Alquiler::REGISTRADOR);
	}


}