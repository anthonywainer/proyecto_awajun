<?php

use Dabl\Query\Query;

abstract class baseFolioQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Folio::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return FolioQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new FolioQuery($table_name, $alias);
	}

	/**
	 * @return Folio[]
	 */
	function select() {
		return Folio::doSelect($this);
	}

	/**
	 * @return Folio
	 */
	function selectOne() {
		return Folio::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Folio::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Folio::doCount($this);
	}

	/**
	 * @return FolioQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Folio::isTemporalType($type)) {
			$value = Folio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Folio::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return FolioQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Folio::isTemporalType($type)) {
			$value = Folio::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Folio::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return FolioQuery
	 */
	function andId($integer) {
		return $this->addAnd(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdNull() {
		return $this->andNull(Folio::ID);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Folio::ID);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Folio::ID, $integer, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orId($integer) {
		return $this->or(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdNull() {
		return $this->orNull(Folio::ID);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Folio::ID);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Folio::ID, $integer, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Folio::ID, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Folio::ID, $integer);
	}


	/**
	 * @return FolioQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Folio::ID, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Folio::ID, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupById() {
		return $this->groupBy(Folio::ID);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerie($varchar) {
		return $this->addAnd(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieNot($varchar) {
		return $this->andNot(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieLike($varchar) {
		return $this->andLike(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieNotLike($varchar) {
		return $this->andNotLike(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieGreater($varchar) {
		return $this->andGreater(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieGreaterEqual($varchar) {
		return $this->andGreaterEqual(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieLess($varchar) {
		return $this->andLess(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieLessEqual($varchar) {
		return $this->andLessEqual(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieNull() {
		return $this->andNull(Folio::SERIE);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieNotNull() {
		return $this->andNotNull(Folio::SERIE);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieBetween($varchar, $from, $to) {
		return $this->andBetween(Folio::SERIE, $varchar, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieBeginsWith($varchar) {
		return $this->andBeginsWith(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieEndsWith($varchar) {
		return $this->andEndsWith(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function andSerieContains($varchar) {
		return $this->andContains(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerie($varchar) {
		return $this->or(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieNot($varchar) {
		return $this->orNot(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieLike($varchar) {
		return $this->orLike(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieNotLike($varchar) {
		return $this->orNotLike(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieGreater($varchar) {
		return $this->orGreater(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieGreaterEqual($varchar) {
		return $this->orGreaterEqual(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieLess($varchar) {
		return $this->orLess(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieLessEqual($varchar) {
		return $this->orLessEqual(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieNull() {
		return $this->orNull(Folio::SERIE);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieNotNull() {
		return $this->orNotNull(Folio::SERIE);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieBetween($varchar, $from, $to) {
		return $this->orBetween(Folio::SERIE, $varchar, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieBeginsWith($varchar) {
		return $this->orBeginsWith(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieEndsWith($varchar) {
		return $this->orEndsWith(Folio::SERIE, $varchar);
	}

	/**
	 * @return FolioQuery
	 */
	function orSerieContains($varchar) {
		return $this->orContains(Folio::SERIE, $varchar);
	}


	/**
	 * @return FolioQuery
	 */
	function orderBySerieAsc() {
		return $this->orderBy(Folio::SERIE, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderBySerieDesc() {
		return $this->orderBy(Folio::SERIE, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupBySerie() {
		return $this->groupBy(Folio::SERIE);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojas($integer) {
		return $this->addAnd(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasNot($integer) {
		return $this->andNot(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasLike($integer) {
		return $this->andLike(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasNotLike($integer) {
		return $this->andNotLike(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasGreater($integer) {
		return $this->andGreater(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasGreaterEqual($integer) {
		return $this->andGreaterEqual(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasLess($integer) {
		return $this->andLess(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasLessEqual($integer) {
		return $this->andLessEqual(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasNull() {
		return $this->andNull(Folio::HOJAS);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasNotNull() {
		return $this->andNotNull(Folio::HOJAS);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasBetween($integer, $from, $to) {
		return $this->andBetween(Folio::HOJAS, $integer, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasBeginsWith($integer) {
		return $this->andBeginsWith(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasEndsWith($integer) {
		return $this->andEndsWith(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function andHojasContains($integer) {
		return $this->andContains(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojas($integer) {
		return $this->or(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasNot($integer) {
		return $this->orNot(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasLike($integer) {
		return $this->orLike(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasNotLike($integer) {
		return $this->orNotLike(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasGreater($integer) {
		return $this->orGreater(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasGreaterEqual($integer) {
		return $this->orGreaterEqual(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasLess($integer) {
		return $this->orLess(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasLessEqual($integer) {
		return $this->orLessEqual(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasNull() {
		return $this->orNull(Folio::HOJAS);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasNotNull() {
		return $this->orNotNull(Folio::HOJAS);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasBetween($integer, $from, $to) {
		return $this->orBetween(Folio::HOJAS, $integer, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasBeginsWith($integer) {
		return $this->orBeginsWith(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasEndsWith($integer) {
		return $this->orEndsWith(Folio::HOJAS, $integer);
	}

	/**
	 * @return FolioQuery
	 */
	function orHojasContains($integer) {
		return $this->orContains(Folio::HOJAS, $integer);
	}


	/**
	 * @return FolioQuery
	 */
	function orderByHojasAsc() {
		return $this->orderBy(Folio::HOJAS, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderByHojasDesc() {
		return $this->orderBy(Folio::HOJAS, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupByHojas() {
		return $this->groupBy(Folio::HOJAS);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Folio::CREATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Folio::CREATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Folio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Folio::CREATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Folio::CREATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Folio::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Folio::CREATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Folio::CREATED_AT, $timestamp);
	}


	/**
	 * @return FolioQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Folio::CREATED_AT, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Folio::CREATED_AT, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Folio::CREATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Folio::UPDATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Folio::UPDATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Folio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Folio::UPDATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Folio::UPDATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Folio::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Folio::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Folio::UPDATED_AT, $timestamp);
	}


	/**
	 * @return FolioQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Folio::UPDATED_AT, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Folio::UPDATED_AT, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Folio::UPDATED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Folio::DELETED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Folio::DELETED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Folio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Folio::DELETED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Folio::DELETED_AT);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Folio::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Folio::DELETED_AT, $timestamp);
	}

	/**
	 * @return FolioQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Folio::DELETED_AT, $timestamp);
	}


	/**
	 * @return FolioQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Folio::DELETED_AT, self::ASC);
	}

	/**
	 * @return FolioQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Folio::DELETED_AT, self::DESC);
	}

	/**
	 * @return FolioQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Folio::DELETED_AT);
	}


}