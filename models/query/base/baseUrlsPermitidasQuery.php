<?php

use Dabl\Query\Query;

abstract class baseUrlsPermitidasQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = UrlsPermitidas::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UrlsPermitidasQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UrlsPermitidasQuery($table_name, $alias);
	}

	/**
	 * @return UrlsPermitidas[]
	 */
	function select() {
		return UrlsPermitidas::doSelect($this);
	}

	/**
	 * @return UrlsPermitidas
	 */
	function selectOne() {
		return UrlsPermitidas::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return UrlsPermitidas::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return UrlsPermitidas::doCount($this);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UrlsPermitidas::isTemporalType($type)) {
			$value = UrlsPermitidas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UrlsPermitidas::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UrlsPermitidas::isTemporalType($type)) {
			$value = UrlsPermitidas::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UrlsPermitidas::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andId($integer) {
		return $this->addAnd(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdNull() {
		return $this->andNull(UrlsPermitidas::ID);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(UrlsPermitidas::ID);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(UrlsPermitidas::ID, $integer, $from, $to);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orId($integer) {
		return $this->or(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdNull() {
		return $this->orNull(UrlsPermitidas::ID);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(UrlsPermitidas::ID);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(UrlsPermitidas::ID, $integer, $from, $to);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(UrlsPermitidas::ID, $integer);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(UrlsPermitidas::ID, $integer);
	}


	/**
	 * @return UrlsPermitidasQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(UrlsPermitidas::ID, self::ASC);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(UrlsPermitidas::ID, self::DESC);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function groupById() {
		return $this->groupBy(UrlsPermitidas::ID);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrl($varchar) {
		return $this->addAnd(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlNot($varchar) {
		return $this->andNot(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlLike($varchar) {
		return $this->andLike(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlNotLike($varchar) {
		return $this->andNotLike(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlGreater($varchar) {
		return $this->andGreater(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlGreaterEqual($varchar) {
		return $this->andGreaterEqual(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlLess($varchar) {
		return $this->andLess(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlLessEqual($varchar) {
		return $this->andLessEqual(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlNull() {
		return $this->andNull(UrlsPermitidas::URL);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlNotNull() {
		return $this->andNotNull(UrlsPermitidas::URL);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlBetween($varchar, $from, $to) {
		return $this->andBetween(UrlsPermitidas::URL, $varchar, $from, $to);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlBeginsWith($varchar) {
		return $this->andBeginsWith(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlEndsWith($varchar) {
		return $this->andEndsWith(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function andUrlContains($varchar) {
		return $this->andContains(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrl($varchar) {
		return $this->or(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlNot($varchar) {
		return $this->orNot(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlLike($varchar) {
		return $this->orLike(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlNotLike($varchar) {
		return $this->orNotLike(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlGreater($varchar) {
		return $this->orGreater(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlGreaterEqual($varchar) {
		return $this->orGreaterEqual(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlLess($varchar) {
		return $this->orLess(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlLessEqual($varchar) {
		return $this->orLessEqual(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlNull() {
		return $this->orNull(UrlsPermitidas::URL);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlNotNull() {
		return $this->orNotNull(UrlsPermitidas::URL);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlBetween($varchar, $from, $to) {
		return $this->orBetween(UrlsPermitidas::URL, $varchar, $from, $to);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlBeginsWith($varchar) {
		return $this->orBeginsWith(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlEndsWith($varchar) {
		return $this->orEndsWith(UrlsPermitidas::URL, $varchar);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orUrlContains($varchar) {
		return $this->orContains(UrlsPermitidas::URL, $varchar);
	}


	/**
	 * @return UrlsPermitidasQuery
	 */
	function orderByUrlAsc() {
		return $this->orderBy(UrlsPermitidas::URL, self::ASC);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function orderByUrlDesc() {
		return $this->orderBy(UrlsPermitidas::URL, self::DESC);
	}

	/**
	 * @return UrlsPermitidasQuery
	 */
	function groupByUrl() {
		return $this->groupBy(UrlsPermitidas::URL);
	}


}