<?php

use Dabl\Query\Query;

abstract class baseUsuariosGrupoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = UsuariosGrupo::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UsuariosGrupoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UsuariosGrupoQuery($table_name, $alias);
	}

	/**
	 * @return UsuariosGrupo[]
	 */
	function select() {
		return UsuariosGrupo::doSelect($this);
	}

	/**
	 * @return UsuariosGrupo
	 */
	function selectOne() {
		return UsuariosGrupo::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return UsuariosGrupo::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return UsuariosGrupo::doCount($this);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UsuariosGrupo::isTemporalType($type)) {
			$value = UsuariosGrupo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UsuariosGrupo::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UsuariosGrupo::isTemporalType($type)) {
			$value = UsuariosGrupo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UsuariosGrupo::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andId($integer) {
		return $this->addAnd(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdNull() {
		return $this->andNull(UsuariosGrupo::ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(UsuariosGrupo::ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(UsuariosGrupo::ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orId($integer) {
		return $this->or(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdNull() {
		return $this->orNull(UsuariosGrupo::ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(UsuariosGrupo::ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(UsuariosGrupo::ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(UsuariosGrupo::ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(UsuariosGrupo::ID, $integer);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(UsuariosGrupo::ID, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(UsuariosGrupo::ID, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupById() {
		return $this->groupBy(UsuariosGrupo::ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioId($integer) {
		return $this->addAnd(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdNot($integer) {
		return $this->andNot(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdLike($integer) {
		return $this->andLike(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdNotLike($integer) {
		return $this->andNotLike(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdGreater($integer) {
		return $this->andGreater(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdLess($integer) {
		return $this->andLess(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdLessEqual($integer) {
		return $this->andLessEqual(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdNull() {
		return $this->andNull(UsuariosGrupo::USUARIO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdNotNull() {
		return $this->andNotNull(UsuariosGrupo::USUARIO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdBetween($integer, $from, $to) {
		return $this->andBetween(UsuariosGrupo::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdBeginsWith($integer) {
		return $this->andBeginsWith(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdEndsWith($integer) {
		return $this->andEndsWith(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUsuarioIdContains($integer) {
		return $this->andContains(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioId($integer) {
		return $this->or(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdNot($integer) {
		return $this->orNot(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdLike($integer) {
		return $this->orLike(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdNotLike($integer) {
		return $this->orNotLike(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdGreater($integer) {
		return $this->orGreater(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdLess($integer) {
		return $this->orLess(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdLessEqual($integer) {
		return $this->orLessEqual(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdNull() {
		return $this->orNull(UsuariosGrupo::USUARIO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdNotNull() {
		return $this->orNotNull(UsuariosGrupo::USUARIO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdBetween($integer, $from, $to) {
		return $this->orBetween(UsuariosGrupo::USUARIO_ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdBeginsWith($integer) {
		return $this->orBeginsWith(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdEndsWith($integer) {
		return $this->orEndsWith(UsuariosGrupo::USUARIO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUsuarioIdContains($integer) {
		return $this->orContains(UsuariosGrupo::USUARIO_ID, $integer);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByUsuarioIdAsc() {
		return $this->orderBy(UsuariosGrupo::USUARIO_ID, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByUsuarioIdDesc() {
		return $this->orderBy(UsuariosGrupo::USUARIO_ID, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupByUsuarioId() {
		return $this->groupBy(UsuariosGrupo::USUARIO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoId($integer) {
		return $this->addAnd(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdNot($integer) {
		return $this->andNot(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdLike($integer) {
		return $this->andLike(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdNotLike($integer) {
		return $this->andNotLike(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdGreater($integer) {
		return $this->andGreater(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdLess($integer) {
		return $this->andLess(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdLessEqual($integer) {
		return $this->andLessEqual(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdNull() {
		return $this->andNull(UsuariosGrupo::GRUPO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdNotNull() {
		return $this->andNotNull(UsuariosGrupo::GRUPO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdBetween($integer, $from, $to) {
		return $this->andBetween(UsuariosGrupo::GRUPO_ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdBeginsWith($integer) {
		return $this->andBeginsWith(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdEndsWith($integer) {
		return $this->andEndsWith(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andGrupoIdContains($integer) {
		return $this->andContains(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoId($integer) {
		return $this->or(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdNot($integer) {
		return $this->orNot(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdLike($integer) {
		return $this->orLike(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdNotLike($integer) {
		return $this->orNotLike(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdGreater($integer) {
		return $this->orGreater(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdLess($integer) {
		return $this->orLess(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdLessEqual($integer) {
		return $this->orLessEqual(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdNull() {
		return $this->orNull(UsuariosGrupo::GRUPO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdNotNull() {
		return $this->orNotNull(UsuariosGrupo::GRUPO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdBetween($integer, $from, $to) {
		return $this->orBetween(UsuariosGrupo::GRUPO_ID, $integer, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdBeginsWith($integer) {
		return $this->orBeginsWith(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdEndsWith($integer) {
		return $this->orEndsWith(UsuariosGrupo::GRUPO_ID, $integer);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orGrupoIdContains($integer) {
		return $this->orContains(UsuariosGrupo::GRUPO_ID, $integer);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByGrupoIdAsc() {
		return $this->orderBy(UsuariosGrupo::GRUPO_ID, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByGrupoIdDesc() {
		return $this->orderBy(UsuariosGrupo::GRUPO_ID, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupByGrupoId() {
		return $this->groupBy(UsuariosGrupo::GRUPO_ID);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(UsuariosGrupo::CREATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(UsuariosGrupo::CREATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UsuariosGrupo::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(UsuariosGrupo::CREATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(UsuariosGrupo::CREATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UsuariosGrupo::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UsuariosGrupo::CREATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(UsuariosGrupo::CREATED_AT, $timestamp);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(UsuariosGrupo::CREATED_AT, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(UsuariosGrupo::CREATED_AT, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(UsuariosGrupo::CREATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(UsuariosGrupo::UPDATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(UsuariosGrupo::UPDATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UsuariosGrupo::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(UsuariosGrupo::UPDATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(UsuariosGrupo::UPDATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UsuariosGrupo::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UsuariosGrupo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(UsuariosGrupo::UPDATED_AT, $timestamp);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(UsuariosGrupo::UPDATED_AT, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(UsuariosGrupo::UPDATED_AT, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(UsuariosGrupo::UPDATED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(UsuariosGrupo::DELETED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(UsuariosGrupo::DELETED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UsuariosGrupo::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(UsuariosGrupo::DELETED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(UsuariosGrupo::DELETED_AT);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UsuariosGrupo::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(UsuariosGrupo::DELETED_AT, $timestamp);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(UsuariosGrupo::DELETED_AT, $timestamp);
	}


	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(UsuariosGrupo::DELETED_AT, self::ASC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(UsuariosGrupo::DELETED_AT, self::DESC);
	}

	/**
	 * @return UsuariosGrupoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(UsuariosGrupo::DELETED_AT);
	}


}