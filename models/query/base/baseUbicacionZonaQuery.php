<?php

use Dabl\Query\Query;

abstract class baseUbicacionZonaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = UbicacionZona::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return UbicacionZonaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new UbicacionZonaQuery($table_name, $alias);
	}

	/**
	 * @return UbicacionZona[]
	 */
	function select() {
		return UbicacionZona::doSelect($this);
	}

	/**
	 * @return UbicacionZona
	 */
	function selectOne() {
		return UbicacionZona::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return UbicacionZona::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return UbicacionZona::doCount($this);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UbicacionZona::isTemporalType($type)) {
			$value = UbicacionZona::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UbicacionZona::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && UbicacionZona::isTemporalType($type)) {
			$value = UbicacionZona::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = UbicacionZona::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andId($integer) {
		return $this->addAnd(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdNull() {
		return $this->andNull(UbicacionZona::ID);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(UbicacionZona::ID);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(UbicacionZona::ID, $integer, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orId($integer) {
		return $this->or(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdNull() {
		return $this->orNull(UbicacionZona::ID);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(UbicacionZona::ID);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(UbicacionZona::ID, $integer, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(UbicacionZona::ID, $integer);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(UbicacionZona::ID, $integer);
	}


	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(UbicacionZona::ID, self::ASC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(UbicacionZona::ID, self::DESC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function groupById() {
		return $this->groupBy(UbicacionZona::ID);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZona($varchar) {
		return $this->addAnd(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaNot($varchar) {
		return $this->andNot(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaLike($varchar) {
		return $this->andLike(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaNotLike($varchar) {
		return $this->andNotLike(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaGreater($varchar) {
		return $this->andGreater(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaGreaterEqual($varchar) {
		return $this->andGreaterEqual(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaLess($varchar) {
		return $this->andLess(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaLessEqual($varchar) {
		return $this->andLessEqual(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaNull() {
		return $this->andNull(UbicacionZona::ZONA);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaNotNull() {
		return $this->andNotNull(UbicacionZona::ZONA);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaBetween($varchar, $from, $to) {
		return $this->andBetween(UbicacionZona::ZONA, $varchar, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaBeginsWith($varchar) {
		return $this->andBeginsWith(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaEndsWith($varchar) {
		return $this->andEndsWith(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andZonaContains($varchar) {
		return $this->andContains(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZona($varchar) {
		return $this->or(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaNot($varchar) {
		return $this->orNot(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaLike($varchar) {
		return $this->orLike(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaNotLike($varchar) {
		return $this->orNotLike(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaGreater($varchar) {
		return $this->orGreater(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaGreaterEqual($varchar) {
		return $this->orGreaterEqual(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaLess($varchar) {
		return $this->orLess(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaLessEqual($varchar) {
		return $this->orLessEqual(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaNull() {
		return $this->orNull(UbicacionZona::ZONA);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaNotNull() {
		return $this->orNotNull(UbicacionZona::ZONA);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaBetween($varchar, $from, $to) {
		return $this->orBetween(UbicacionZona::ZONA, $varchar, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaBeginsWith($varchar) {
		return $this->orBeginsWith(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaEndsWith($varchar) {
		return $this->orEndsWith(UbicacionZona::ZONA, $varchar);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orZonaContains($varchar) {
		return $this->orContains(UbicacionZona::ZONA, $varchar);
	}


	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByZonaAsc() {
		return $this->orderBy(UbicacionZona::ZONA, self::ASC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByZonaDesc() {
		return $this->orderBy(UbicacionZona::ZONA, self::DESC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function groupByZona() {
		return $this->groupBy(UbicacionZona::ZONA);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(UbicacionZona::CREATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(UbicacionZona::CREATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UbicacionZona::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(UbicacionZona::CREATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(UbicacionZona::CREATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UbicacionZona::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UbicacionZona::CREATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(UbicacionZona::CREATED_AT, $timestamp);
	}


	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(UbicacionZona::CREATED_AT, self::ASC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(UbicacionZona::CREATED_AT, self::DESC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(UbicacionZona::CREATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(UbicacionZona::UPDATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(UbicacionZona::UPDATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UbicacionZona::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(UbicacionZona::UPDATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(UbicacionZona::UPDATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UbicacionZona::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(UbicacionZona::UPDATED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(UbicacionZona::UPDATED_AT, $timestamp);
	}


	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(UbicacionZona::UPDATED_AT, self::ASC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(UbicacionZona::UPDATED_AT, self::DESC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(UbicacionZona::UPDATED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(UbicacionZona::DELETED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(UbicacionZona::DELETED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(UbicacionZona::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(UbicacionZona::DELETED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(UbicacionZona::DELETED_AT);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(UbicacionZona::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(UbicacionZona::DELETED_AT, $timestamp);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(UbicacionZona::DELETED_AT, $timestamp);
	}


	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(UbicacionZona::DELETED_AT, self::ASC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(UbicacionZona::DELETED_AT, self::DESC);
	}

	/**
	 * @return UbicacionZonaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(UbicacionZona::DELETED_AT);
	}


}