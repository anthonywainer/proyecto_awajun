<?php

use Dabl\Query\Query;

abstract class baseFamiliaQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Familia::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return FamiliaQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new FamiliaQuery($table_name, $alias);
	}

	/**
	 * @return Familia[]
	 */
	function select() {
		return Familia::doSelect($this);
	}

	/**
	 * @return Familia
	 */
	function selectOne() {
		return Familia::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Familia::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Familia::doCount($this);
	}

	/**
	 * @return FamiliaQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Familia::isTemporalType($type)) {
			$value = Familia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Familia::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return FamiliaQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Familia::isTemporalType($type)) {
			$value = Familia::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Familia::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andId($integer) {
		return $this->addAnd(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdNull() {
		return $this->andNull(Familia::ID);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Familia::ID);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Familia::ID, $integer, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orId($integer) {
		return $this->or(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdNull() {
		return $this->orNull(Familia::ID);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Familia::ID);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Familia::ID, $integer, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Familia::ID, $integer);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Familia::ID, $integer);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Familia::ID, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Familia::ID, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupById() {
		return $this->groupBy(Familia::ID);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamilia($varchar) {
		return $this->addAnd(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaNot($varchar) {
		return $this->andNot(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaLike($varchar) {
		return $this->andLike(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaNotLike($varchar) {
		return $this->andNotLike(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaGreater($varchar) {
		return $this->andGreater(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaGreaterEqual($varchar) {
		return $this->andGreaterEqual(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaLess($varchar) {
		return $this->andLess(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaLessEqual($varchar) {
		return $this->andLessEqual(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaNull() {
		return $this->andNull(Familia::NOMBRE_FAMILIA);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaNotNull() {
		return $this->andNotNull(Familia::NOMBRE_FAMILIA);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaBetween($varchar, $from, $to) {
		return $this->andBetween(Familia::NOMBRE_FAMILIA, $varchar, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaBeginsWith($varchar) {
		return $this->andBeginsWith(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaEndsWith($varchar) {
		return $this->andEndsWith(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andNombreFamiliaContains($varchar) {
		return $this->andContains(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamilia($varchar) {
		return $this->or(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaNot($varchar) {
		return $this->orNot(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaLike($varchar) {
		return $this->orLike(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaNotLike($varchar) {
		return $this->orNotLike(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaGreater($varchar) {
		return $this->orGreater(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaGreaterEqual($varchar) {
		return $this->orGreaterEqual(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaLess($varchar) {
		return $this->orLess(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaLessEqual($varchar) {
		return $this->orLessEqual(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaNull() {
		return $this->orNull(Familia::NOMBRE_FAMILIA);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaNotNull() {
		return $this->orNotNull(Familia::NOMBRE_FAMILIA);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaBetween($varchar, $from, $to) {
		return $this->orBetween(Familia::NOMBRE_FAMILIA, $varchar, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaBeginsWith($varchar) {
		return $this->orBeginsWith(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaEndsWith($varchar) {
		return $this->orEndsWith(Familia::NOMBRE_FAMILIA, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orNombreFamiliaContains($varchar) {
		return $this->orContains(Familia::NOMBRE_FAMILIA, $varchar);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByNombreFamiliaAsc() {
		return $this->orderBy(Familia::NOMBRE_FAMILIA, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByNombreFamiliaDesc() {
		return $this->orderBy(Familia::NOMBRE_FAMILIA, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupByNombreFamilia() {
		return $this->groupBy(Familia::NOMBRE_FAMILIA);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigo($varchar) {
		return $this->addAnd(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoNot($varchar) {
		return $this->andNot(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoLike($varchar) {
		return $this->andLike(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoNotLike($varchar) {
		return $this->andNotLike(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoGreater($varchar) {
		return $this->andGreater(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoLess($varchar) {
		return $this->andLess(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoLessEqual($varchar) {
		return $this->andLessEqual(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoNull() {
		return $this->andNull(Familia::CODIGO);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoNotNull() {
		return $this->andNotNull(Familia::CODIGO);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoBetween($varchar, $from, $to) {
		return $this->andBetween(Familia::CODIGO, $varchar, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoBeginsWith($varchar) {
		return $this->andBeginsWith(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoEndsWith($varchar) {
		return $this->andEndsWith(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCodigoContains($varchar) {
		return $this->andContains(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigo($varchar) {
		return $this->or(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoNot($varchar) {
		return $this->orNot(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoLike($varchar) {
		return $this->orLike(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoNotLike($varchar) {
		return $this->orNotLike(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoGreater($varchar) {
		return $this->orGreater(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoLess($varchar) {
		return $this->orLess(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoLessEqual($varchar) {
		return $this->orLessEqual(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoNull() {
		return $this->orNull(Familia::CODIGO);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoNotNull() {
		return $this->orNotNull(Familia::CODIGO);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoBetween($varchar, $from, $to) {
		return $this->orBetween(Familia::CODIGO, $varchar, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoBeginsWith($varchar) {
		return $this->orBeginsWith(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoEndsWith($varchar) {
		return $this->orEndsWith(Familia::CODIGO, $varchar);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCodigoContains($varchar) {
		return $this->orContains(Familia::CODIGO, $varchar);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByCodigoAsc() {
		return $this->orderBy(Familia::CODIGO, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByCodigoDesc() {
		return $this->orderBy(Familia::CODIGO, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupByCodigo() {
		return $this->groupBy(Familia::CODIGO);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Familia::CREATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Familia::CREATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Familia::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Familia::CREATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Familia::CREATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Familia::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Familia::CREATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Familia::CREATED_AT, $timestamp);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Familia::CREATED_AT, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Familia::CREATED_AT, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Familia::CREATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Familia::UPDATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Familia::UPDATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Familia::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Familia::UPDATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Familia::UPDATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Familia::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Familia::UPDATED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Familia::UPDATED_AT, $timestamp);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Familia::UPDATED_AT, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Familia::UPDATED_AT, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Familia::UPDATED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Familia::DELETED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Familia::DELETED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Familia::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Familia::DELETED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Familia::DELETED_AT);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Familia::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Familia::DELETED_AT, $timestamp);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Familia::DELETED_AT, $timestamp);
	}


	/**
	 * @return FamiliaQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Familia::DELETED_AT, self::ASC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Familia::DELETED_AT, self::DESC);
	}

	/**
	 * @return FamiliaQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Familia::DELETED_AT);
	}


}