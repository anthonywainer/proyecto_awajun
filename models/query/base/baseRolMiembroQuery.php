<?php

use Dabl\Query\Query;

abstract class baseRolMiembroQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = RolMiembro::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return RolMiembroQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new RolMiembroQuery($table_name, $alias);
	}

	/**
	 * @return RolMiembro[]
	 */
	function select() {
		return RolMiembro::doSelect($this);
	}

	/**
	 * @return RolMiembro
	 */
	function selectOne() {
		return RolMiembro::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return RolMiembro::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return RolMiembro::doCount($this);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && RolMiembro::isTemporalType($type)) {
			$value = RolMiembro::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = RolMiembro::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && RolMiembro::isTemporalType($type)) {
			$value = RolMiembro::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = RolMiembro::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andId($integer) {
		return $this->addAnd(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdNull() {
		return $this->andNull(RolMiembro::ID);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(RolMiembro::ID);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(RolMiembro::ID, $integer, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orId($integer) {
		return $this->or(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdNull() {
		return $this->orNull(RolMiembro::ID);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(RolMiembro::ID);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(RolMiembro::ID, $integer, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(RolMiembro::ID, $integer);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(RolMiembro::ID, $integer);
	}


	/**
	 * @return RolMiembroQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(RolMiembro::ID, self::ASC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(RolMiembro::ID, self::DESC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function groupById() {
		return $this->groupBy(RolMiembro::ID);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembro($varchar) {
		return $this->addAnd(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroNot($varchar) {
		return $this->andNot(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroLike($varchar) {
		return $this->andLike(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroNotLike($varchar) {
		return $this->andNotLike(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroGreater($varchar) {
		return $this->andGreater(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroGreaterEqual($varchar) {
		return $this->andGreaterEqual(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroLess($varchar) {
		return $this->andLess(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroLessEqual($varchar) {
		return $this->andLessEqual(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroNull() {
		return $this->andNull(RolMiembro::NOMBRE_MIEMBRO);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroNotNull() {
		return $this->andNotNull(RolMiembro::NOMBRE_MIEMBRO);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroBetween($varchar, $from, $to) {
		return $this->andBetween(RolMiembro::NOMBRE_MIEMBRO, $varchar, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroBeginsWith($varchar) {
		return $this->andBeginsWith(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroEndsWith($varchar) {
		return $this->andEndsWith(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andNombreMiembroContains($varchar) {
		return $this->andContains(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembro($varchar) {
		return $this->or(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroNot($varchar) {
		return $this->orNot(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroLike($varchar) {
		return $this->orLike(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroNotLike($varchar) {
		return $this->orNotLike(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroGreater($varchar) {
		return $this->orGreater(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroGreaterEqual($varchar) {
		return $this->orGreaterEqual(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroLess($varchar) {
		return $this->orLess(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroLessEqual($varchar) {
		return $this->orLessEqual(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroNull() {
		return $this->orNull(RolMiembro::NOMBRE_MIEMBRO);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroNotNull() {
		return $this->orNotNull(RolMiembro::NOMBRE_MIEMBRO);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroBetween($varchar, $from, $to) {
		return $this->orBetween(RolMiembro::NOMBRE_MIEMBRO, $varchar, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroBeginsWith($varchar) {
		return $this->orBeginsWith(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroEndsWith($varchar) {
		return $this->orEndsWith(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orNombreMiembroContains($varchar) {
		return $this->orContains(RolMiembro::NOMBRE_MIEMBRO, $varchar);
	}


	/**
	 * @return RolMiembroQuery
	 */
	function orderByNombreMiembroAsc() {
		return $this->orderBy(RolMiembro::NOMBRE_MIEMBRO, self::ASC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orderByNombreMiembroDesc() {
		return $this->orderBy(RolMiembro::NOMBRE_MIEMBRO, self::DESC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function groupByNombreMiembro() {
		return $this->groupBy(RolMiembro::NOMBRE_MIEMBRO);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(RolMiembro::CREATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(RolMiembro::CREATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(RolMiembro::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(RolMiembro::CREATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(RolMiembro::CREATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(RolMiembro::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(RolMiembro::CREATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(RolMiembro::CREATED_AT, $timestamp);
	}


	/**
	 * @return RolMiembroQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(RolMiembro::CREATED_AT, self::ASC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(RolMiembro::CREATED_AT, self::DESC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(RolMiembro::CREATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(RolMiembro::UPDATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(RolMiembro::UPDATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(RolMiembro::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(RolMiembro::UPDATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(RolMiembro::UPDATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(RolMiembro::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(RolMiembro::UPDATED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(RolMiembro::UPDATED_AT, $timestamp);
	}


	/**
	 * @return RolMiembroQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(RolMiembro::UPDATED_AT, self::ASC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(RolMiembro::UPDATED_AT, self::DESC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(RolMiembro::UPDATED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(RolMiembro::DELETED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(RolMiembro::DELETED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(RolMiembro::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(RolMiembro::DELETED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(RolMiembro::DELETED_AT);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(RolMiembro::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(RolMiembro::DELETED_AT, $timestamp);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(RolMiembro::DELETED_AT, $timestamp);
	}


	/**
	 * @return RolMiembroQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(RolMiembro::DELETED_AT, self::ASC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(RolMiembro::DELETED_AT, self::DESC);
	}

	/**
	 * @return RolMiembroQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(RolMiembro::DELETED_AT);
	}


}