<?php

use Dabl\Query\Query;

abstract class baseContratoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Contrato::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ContratoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ContratoQuery($table_name, $alias);
	}

	/**
	 * @return Contrato[]
	 */
	function select() {
		return Contrato::doSelect($this);
	}

	/**
	 * @return Contrato
	 */
	function selectOne() {
		return Contrato::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Contrato::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Contrato::doCount($this);
	}

	/**
	 * @return ContratoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Contrato::isTemporalType($type)) {
			$value = Contrato::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Contrato::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ContratoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Contrato::isTemporalType($type)) {
			$value = Contrato::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Contrato::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ContratoQuery
	 */
	function andId($integer) {
		return $this->addAnd(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdNull() {
		return $this->andNull(Contrato::ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Contrato::ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Contrato::ID, $integer, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orId($integer) {
		return $this->or(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdNull() {
		return $this->orNull(Contrato::ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Contrato::ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Contrato::ID, $integer, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Contrato::ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Contrato::ID, $integer);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Contrato::ID, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Contrato::ID, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupById() {
		return $this->groupBy(Contrato::ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContrato($varchar) {
		return $this->addAnd(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoNot($varchar) {
		return $this->andNot(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoLike($varchar) {
		return $this->andLike(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoNotLike($varchar) {
		return $this->andNotLike(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoGreater($varchar) {
		return $this->andGreater(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoGreaterEqual($varchar) {
		return $this->andGreaterEqual(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoLess($varchar) {
		return $this->andLess(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoLessEqual($varchar) {
		return $this->andLessEqual(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoNull() {
		return $this->andNull(Contrato::NUMERO_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoNotNull() {
		return $this->andNotNull(Contrato::NUMERO_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoBetween($varchar, $from, $to) {
		return $this->andBetween(Contrato::NUMERO_CONTRATO, $varchar, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoBeginsWith($varchar) {
		return $this->andBeginsWith(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoEndsWith($varchar) {
		return $this->andEndsWith(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function andNumeroContratoContains($varchar) {
		return $this->andContains(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContrato($varchar) {
		return $this->or(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoNot($varchar) {
		return $this->orNot(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoLike($varchar) {
		return $this->orLike(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoNotLike($varchar) {
		return $this->orNotLike(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoGreater($varchar) {
		return $this->orGreater(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoGreaterEqual($varchar) {
		return $this->orGreaterEqual(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoLess($varchar) {
		return $this->orLess(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoLessEqual($varchar) {
		return $this->orLessEqual(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoNull() {
		return $this->orNull(Contrato::NUMERO_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoNotNull() {
		return $this->orNotNull(Contrato::NUMERO_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoBetween($varchar, $from, $to) {
		return $this->orBetween(Contrato::NUMERO_CONTRATO, $varchar, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoBeginsWith($varchar) {
		return $this->orBeginsWith(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoEndsWith($varchar) {
		return $this->orEndsWith(Contrato::NUMERO_CONTRATO, $varchar);
	}

	/**
	 * @return ContratoQuery
	 */
	function orNumeroContratoContains($varchar) {
		return $this->orContains(Contrato::NUMERO_CONTRATO, $varchar);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByNumeroContratoAsc() {
		return $this->orderBy(Contrato::NUMERO_CONTRATO, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByNumeroContratoDesc() {
		return $this->orderBy(Contrato::NUMERO_CONTRATO, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByNumeroContrato() {
		return $this->groupBy(Contrato::NUMERO_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioId($integer) {
		return $this->addAnd(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdNot($integer) {
		return $this->andNot(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdLike($integer) {
		return $this->andLike(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdNotLike($integer) {
		return $this->andNotLike(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdGreater($integer) {
		return $this->andGreater(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdLess($integer) {
		return $this->andLess(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdLessEqual($integer) {
		return $this->andLessEqual(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdNull() {
		return $this->andNull(Contrato::FOLIO_ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdNotNull() {
		return $this->andNotNull(Contrato::FOLIO_ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdBetween($integer, $from, $to) {
		return $this->andBetween(Contrato::FOLIO_ID, $integer, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdBeginsWith($integer) {
		return $this->andBeginsWith(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdEndsWith($integer) {
		return $this->andEndsWith(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFolioIdContains($integer) {
		return $this->andContains(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioId($integer) {
		return $this->or(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdNot($integer) {
		return $this->orNot(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdLike($integer) {
		return $this->orLike(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdNotLike($integer) {
		return $this->orNotLike(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdGreater($integer) {
		return $this->orGreater(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdLess($integer) {
		return $this->orLess(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdLessEqual($integer) {
		return $this->orLessEqual(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdNull() {
		return $this->orNull(Contrato::FOLIO_ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdNotNull() {
		return $this->orNotNull(Contrato::FOLIO_ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdBetween($integer, $from, $to) {
		return $this->orBetween(Contrato::FOLIO_ID, $integer, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdBeginsWith($integer) {
		return $this->orBeginsWith(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdEndsWith($integer) {
		return $this->orEndsWith(Contrato::FOLIO_ID, $integer);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFolioIdContains($integer) {
		return $this->orContains(Contrato::FOLIO_ID, $integer);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByFolioIdAsc() {
		return $this->orderBy(Contrato::FOLIO_ID, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByFolioIdDesc() {
		return $this->orderBy(Contrato::FOLIO_ID, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByFolioId() {
		return $this->groupBy(Contrato::FOLIO_ID);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContrato($timestamp) {
		return $this->addAnd(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoNot($timestamp) {
		return $this->andNot(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoLike($timestamp) {
		return $this->andLike(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoNotLike($timestamp) {
		return $this->andNotLike(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoGreater($timestamp) {
		return $this->andGreater(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoLess($timestamp) {
		return $this->andLess(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoNull() {
		return $this->andNull(Contrato::FECHA_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoNotNull() {
		return $this->andNotNull(Contrato::FECHA_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::FECHA_CONTRATO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaContratoContains($timestamp) {
		return $this->andContains(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContrato($timestamp) {
		return $this->or(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoNot($timestamp) {
		return $this->orNot(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoLike($timestamp) {
		return $this->orLike(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoNotLike($timestamp) {
		return $this->orNotLike(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoGreater($timestamp) {
		return $this->orGreater(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoLess($timestamp) {
		return $this->orLess(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoNull() {
		return $this->orNull(Contrato::FECHA_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoNotNull() {
		return $this->orNotNull(Contrato::FECHA_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::FECHA_CONTRATO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::FECHA_CONTRATO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaContratoContains($timestamp) {
		return $this->orContains(Contrato::FECHA_CONTRATO, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByFechaContratoAsc() {
		return $this->orderBy(Contrato::FECHA_CONTRATO, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByFechaContratoDesc() {
		return $this->orderBy(Contrato::FECHA_CONTRATO, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByFechaContrato() {
		return $this->groupBy(Contrato::FECHA_CONTRATO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicio($timestamp) {
		return $this->addAnd(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioNot($timestamp) {
		return $this->andNot(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioLike($timestamp) {
		return $this->andLike(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioNotLike($timestamp) {
		return $this->andNotLike(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioGreater($timestamp) {
		return $this->andGreater(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioLess($timestamp) {
		return $this->andLess(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioNull() {
		return $this->andNull(Contrato::FECHA_INICIO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioNotNull() {
		return $this->andNotNull(Contrato::FECHA_INICIO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::FECHA_INICIO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaInicioContains($timestamp) {
		return $this->andContains(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicio($timestamp) {
		return $this->or(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioNot($timestamp) {
		return $this->orNot(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioLike($timestamp) {
		return $this->orLike(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioNotLike($timestamp) {
		return $this->orNotLike(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioGreater($timestamp) {
		return $this->orGreater(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioLess($timestamp) {
		return $this->orLess(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioNull() {
		return $this->orNull(Contrato::FECHA_INICIO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioNotNull() {
		return $this->orNotNull(Contrato::FECHA_INICIO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::FECHA_INICIO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::FECHA_INICIO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaInicioContains($timestamp) {
		return $this->orContains(Contrato::FECHA_INICIO, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByFechaInicioAsc() {
		return $this->orderBy(Contrato::FECHA_INICIO, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByFechaInicioDesc() {
		return $this->orderBy(Contrato::FECHA_INICIO, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByFechaInicio() {
		return $this->groupBy(Contrato::FECHA_INICIO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTermino($timestamp) {
		return $this->addAnd(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoNot($timestamp) {
		return $this->andNot(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoLike($timestamp) {
		return $this->andLike(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoNotLike($timestamp) {
		return $this->andNotLike(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoGreater($timestamp) {
		return $this->andGreater(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoLess($timestamp) {
		return $this->andLess(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoNull() {
		return $this->andNull(Contrato::FECHA_TERMINO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoNotNull() {
		return $this->andNotNull(Contrato::FECHA_TERMINO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::FECHA_TERMINO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andFechaTerminoContains($timestamp) {
		return $this->andContains(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTermino($timestamp) {
		return $this->or(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoNot($timestamp) {
		return $this->orNot(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoLike($timestamp) {
		return $this->orLike(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoNotLike($timestamp) {
		return $this->orNotLike(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoGreater($timestamp) {
		return $this->orGreater(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoLess($timestamp) {
		return $this->orLess(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoNull() {
		return $this->orNull(Contrato::FECHA_TERMINO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoNotNull() {
		return $this->orNotNull(Contrato::FECHA_TERMINO);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::FECHA_TERMINO, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::FECHA_TERMINO, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orFechaTerminoContains($timestamp) {
		return $this->orContains(Contrato::FECHA_TERMINO, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByFechaTerminoAsc() {
		return $this->orderBy(Contrato::FECHA_TERMINO, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByFechaTerminoDesc() {
		return $this->orderBy(Contrato::FECHA_TERMINO, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByFechaTermino() {
		return $this->groupBy(Contrato::FECHA_TERMINO);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Contrato::CREATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Contrato::CREATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Contrato::CREATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Contrato::CREATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::CREATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Contrato::CREATED_AT, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Contrato::CREATED_AT, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Contrato::CREATED_AT, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Contrato::CREATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Contrato::UPDATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Contrato::UPDATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Contrato::UPDATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Contrato::UPDATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::UPDATED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Contrato::UPDATED_AT, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Contrato::UPDATED_AT, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Contrato::UPDATED_AT, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Contrato::UPDATED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Contrato::DELETED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Contrato::DELETED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Contrato::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Contrato::DELETED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Contrato::DELETED_AT);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Contrato::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Contrato::DELETED_AT, $timestamp);
	}

	/**
	 * @return ContratoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Contrato::DELETED_AT, $timestamp);
	}


	/**
	 * @return ContratoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Contrato::DELETED_AT, self::ASC);
	}

	/**
	 * @return ContratoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Contrato::DELETED_AT, self::DESC);
	}

	/**
	 * @return ContratoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Contrato::DELETED_AT);
	}


}