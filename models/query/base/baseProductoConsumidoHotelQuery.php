<?php

use Dabl\Query\Query;

abstract class baseProductoConsumidoHotelQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = ProductoConsumidoHotel::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return ProductoConsumidoHotelQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new ProductoConsumidoHotelQuery($table_name, $alias);
	}

	/**
	 * @return ProductoConsumidoHotel[]
	 */
	function select() {
		return ProductoConsumidoHotel::doSelect($this);
	}

	/**
	 * @return ProductoConsumidoHotel
	 */
	function selectOne() {
		return ProductoConsumidoHotel::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return ProductoConsumidoHotel::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return ProductoConsumidoHotel::doCount($this);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoConsumidoHotel::isTemporalType($type)) {
			$value = ProductoConsumidoHotel::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoConsumidoHotel::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && ProductoConsumidoHotel::isTemporalType($type)) {
			$value = ProductoConsumidoHotel::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = ProductoConsumidoHotel::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andId($integer) {
		return $this->addAnd(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdNull() {
		return $this->andNull(ProductoConsumidoHotel::ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(ProductoConsumidoHotel::ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumidoHotel::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orId($integer) {
		return $this->or(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdNull() {
		return $this->orNull(ProductoConsumidoHotel::ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(ProductoConsumidoHotel::ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumidoHotel::ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumidoHotel::ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(ProductoConsumidoHotel::ID, $integer);
	}


	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(ProductoConsumidoHotel::ID, self::ASC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(ProductoConsumidoHotel::ID, self::DESC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function groupById() {
		return $this->groupBy(ProductoConsumidoHotel::ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoId($integer) {
		return $this->addAnd(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdNot($integer) {
		return $this->andNot(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdLike($integer) {
		return $this->andLike(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdNotLike($integer) {
		return $this->andNotLike(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdGreater($integer) {
		return $this->andGreater(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdLess($integer) {
		return $this->andLess(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdNull() {
		return $this->andNull(ProductoConsumidoHotel::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdNotNull() {
		return $this->andNotNull(ProductoConsumidoHotel::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumidoHotel::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andProductoIdContains($integer) {
		return $this->andContains(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoId($integer) {
		return $this->or(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdNot($integer) {
		return $this->orNot(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdLike($integer) {
		return $this->orLike(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdNotLike($integer) {
		return $this->orNotLike(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdGreater($integer) {
		return $this->orGreater(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdLess($integer) {
		return $this->orLess(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdNull() {
		return $this->orNull(ProductoConsumidoHotel::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdNotNull() {
		return $this->orNotNull(ProductoConsumidoHotel::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumidoHotel::PRODUCTO_ID, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orProductoIdContains($integer) {
		return $this->orContains(ProductoConsumidoHotel::PRODUCTO_ID, $integer);
	}


	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByProductoIdAsc() {
		return $this->orderBy(ProductoConsumidoHotel::PRODUCTO_ID, self::ASC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByProductoIdDesc() {
		return $this->orderBy(ProductoConsumidoHotel::PRODUCTO_ID, self::DESC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function groupByProductoId() {
		return $this->groupBy(ProductoConsumidoHotel::PRODUCTO_ID);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotel($integer) {
		return $this->addAnd(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelNot($integer) {
		return $this->andNot(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelLike($integer) {
		return $this->andLike(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelNotLike($integer) {
		return $this->andNotLike(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelGreater($integer) {
		return $this->andGreater(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelLess($integer) {
		return $this->andLess(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelNull() {
		return $this->andNull(ProductoConsumidoHotel::HOTEL);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelNotNull() {
		return $this->andNotNull(ProductoConsumidoHotel::HOTEL);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumidoHotel::HOTEL, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andHotelContains($integer) {
		return $this->andContains(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotel($integer) {
		return $this->or(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelNot($integer) {
		return $this->orNot(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelLike($integer) {
		return $this->orLike(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelNotLike($integer) {
		return $this->orNotLike(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelGreater($integer) {
		return $this->orGreater(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelLess($integer) {
		return $this->orLess(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelNull() {
		return $this->orNull(ProductoConsumidoHotel::HOTEL);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelNotNull() {
		return $this->orNotNull(ProductoConsumidoHotel::HOTEL);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumidoHotel::HOTEL, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumidoHotel::HOTEL, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orHotelContains($integer) {
		return $this->orContains(ProductoConsumidoHotel::HOTEL, $integer);
	}


	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByHotelAsc() {
		return $this->orderBy(ProductoConsumidoHotel::HOTEL, self::ASC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByHotelDesc() {
		return $this->orderBy(ProductoConsumidoHotel::HOTEL, self::DESC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function groupByHotel() {
		return $this->groupBy(ProductoConsumidoHotel::HOTEL);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidad($integer) {
		return $this->addAnd(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadNot($integer) {
		return $this->andNot(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadLike($integer) {
		return $this->andLike(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadNotLike($integer) {
		return $this->andNotLike(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadGreater($integer) {
		return $this->andGreater(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadGreaterEqual($integer) {
		return $this->andGreaterEqual(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadLess($integer) {
		return $this->andLess(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadLessEqual($integer) {
		return $this->andLessEqual(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadNull() {
		return $this->andNull(ProductoConsumidoHotel::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadNotNull() {
		return $this->andNotNull(ProductoConsumidoHotel::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadBetween($integer, $from, $to) {
		return $this->andBetween(ProductoConsumidoHotel::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadBeginsWith($integer) {
		return $this->andBeginsWith(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadEndsWith($integer) {
		return $this->andEndsWith(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andCantidadContains($integer) {
		return $this->andContains(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidad($integer) {
		return $this->or(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadNot($integer) {
		return $this->orNot(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadLike($integer) {
		return $this->orLike(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadNotLike($integer) {
		return $this->orNotLike(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadGreater($integer) {
		return $this->orGreater(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadGreaterEqual($integer) {
		return $this->orGreaterEqual(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadLess($integer) {
		return $this->orLess(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadLessEqual($integer) {
		return $this->orLessEqual(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadNull() {
		return $this->orNull(ProductoConsumidoHotel::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadNotNull() {
		return $this->orNotNull(ProductoConsumidoHotel::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadBetween($integer, $from, $to) {
		return $this->orBetween(ProductoConsumidoHotel::CANTIDAD, $integer, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadBeginsWith($integer) {
		return $this->orBeginsWith(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadEndsWith($integer) {
		return $this->orEndsWith(ProductoConsumidoHotel::CANTIDAD, $integer);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orCantidadContains($integer) {
		return $this->orContains(ProductoConsumidoHotel::CANTIDAD, $integer);
	}


	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByCantidadAsc() {
		return $this->orderBy(ProductoConsumidoHotel::CANTIDAD, self::ASC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByCantidadDesc() {
		return $this->orderBy(ProductoConsumidoHotel::CANTIDAD, self::DESC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function groupByCantidad() {
		return $this->groupBy(ProductoConsumidoHotel::CANTIDAD);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMonto($float) {
		return $this->addAnd(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoNot($float) {
		return $this->andNot(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoLike($float) {
		return $this->andLike(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoNotLike($float) {
		return $this->andNotLike(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoGreater($float) {
		return $this->andGreater(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoGreaterEqual($float) {
		return $this->andGreaterEqual(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoLess($float) {
		return $this->andLess(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoLessEqual($float) {
		return $this->andLessEqual(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoNull() {
		return $this->andNull(ProductoConsumidoHotel::MONTO);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoNotNull() {
		return $this->andNotNull(ProductoConsumidoHotel::MONTO);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoBetween($float, $from, $to) {
		return $this->andBetween(ProductoConsumidoHotel::MONTO, $float, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoBeginsWith($float) {
		return $this->andBeginsWith(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoEndsWith($float) {
		return $this->andEndsWith(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function andMontoContains($float) {
		return $this->andContains(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMonto($float) {
		return $this->or(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoNot($float) {
		return $this->orNot(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoLike($float) {
		return $this->orLike(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoNotLike($float) {
		return $this->orNotLike(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoGreater($float) {
		return $this->orGreater(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoGreaterEqual($float) {
		return $this->orGreaterEqual(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoLess($float) {
		return $this->orLess(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoLessEqual($float) {
		return $this->orLessEqual(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoNull() {
		return $this->orNull(ProductoConsumidoHotel::MONTO);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoNotNull() {
		return $this->orNotNull(ProductoConsumidoHotel::MONTO);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoBetween($float, $from, $to) {
		return $this->orBetween(ProductoConsumidoHotel::MONTO, $float, $from, $to);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoBeginsWith($float) {
		return $this->orBeginsWith(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoEndsWith($float) {
		return $this->orEndsWith(ProductoConsumidoHotel::MONTO, $float);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orMontoContains($float) {
		return $this->orContains(ProductoConsumidoHotel::MONTO, $float);
	}


	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByMontoAsc() {
		return $this->orderBy(ProductoConsumidoHotel::MONTO, self::ASC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function orderByMontoDesc() {
		return $this->orderBy(ProductoConsumidoHotel::MONTO, self::DESC);
	}

	/**
	 * @return ProductoConsumidoHotelQuery
	 */
	function groupByMonto() {
		return $this->groupBy(ProductoConsumidoHotel::MONTO);
	}


}