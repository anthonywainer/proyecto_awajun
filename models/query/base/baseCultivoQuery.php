<?php

use Dabl\Query\Query;

abstract class baseCultivoQuery extends Query {

	function __construct($table_name = null, $alias = null) {
		if (null === $table_name) {
			$table_name = Cultivo::getTableName();
		}
		return parent::__construct($table_name, $alias);
	}

	/**
	 * Returns new instance of self by passing arguments directly to constructor.
	 * @param string $alias
	 * @return CultivoQuery
	 */
	static function create($table_name = null, $alias = null) {
		return new CultivoQuery($table_name, $alias);
	}

	/**
	 * @return Cultivo[]
	 */
	function select() {
		return Cultivo::doSelect($this);
	}

	/**
	 * @return Cultivo
	 */
	function selectOne() {
		return Cultivo::doSelectOne($this);
	}

	/**
	 * @return int
	 */
	function delete(){
		return Cultivo::doDelete($this);
	}

	/**
	 * @return int
	 */
	function count(){
		return Cultivo::doCount($this);
	}

	/**
	 * @return CultivoQuery
	 */
	function addAnd($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Cultivo::isTemporalType($type)) {
			$value = Cultivo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Cultivo::coerceTemporalValue($column, $type);
		}
		return parent::addAnd($column, $value, $operator, $quote);
	}

	/**
	 * @return CultivoQuery
	 */
	function addOr($column, $value=null, $operator=self::EQUAL, $quote = null, $type = null) {
		if (null !== $type && Cultivo::isTemporalType($type)) {
			$value = Cultivo::coerceTemporalValue($value, $type);
		}
		if (null === $value && is_array($column) && Model::isTemporalType($type)) {
			$column = Cultivo::coerceTemporalValue($column, $type);
		}
		return parent::addOr($column, $value, $operator, $quote);
	}

	/**
	 * @return CultivoQuery
	 */
	function andId($integer) {
		return $this->addAnd(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdNot($integer) {
		return $this->andNot(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdLike($integer) {
		return $this->andLike(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdNotLike($integer) {
		return $this->andNotLike(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdGreater($integer) {
		return $this->andGreater(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdLess($integer) {
		return $this->andLess(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdLessEqual($integer) {
		return $this->andLessEqual(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdNull() {
		return $this->andNull(Cultivo::ID);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdNotNull() {
		return $this->andNotNull(Cultivo::ID);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdEndsWith($integer) {
		return $this->andEndsWith(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andIdContains($integer) {
		return $this->andContains(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orId($integer) {
		return $this->or(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdNot($integer) {
		return $this->orNot(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdLike($integer) {
		return $this->orLike(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdNotLike($integer) {
		return $this->orNotLike(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdGreater($integer) {
		return $this->orGreater(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdLess($integer) {
		return $this->orLess(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdLessEqual($integer) {
		return $this->orLessEqual(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdNull() {
		return $this->orNull(Cultivo::ID);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdNotNull() {
		return $this->orNotNull(Cultivo::ID);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::ID, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdEndsWith($integer) {
		return $this->orEndsWith(Cultivo::ID, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orIdContains($integer) {
		return $this->orContains(Cultivo::ID, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByIdAsc() {
		return $this->orderBy(Cultivo::ID, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByIdDesc() {
		return $this->orderBy(Cultivo::ID, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupById() {
		return $this->groupBy(Cultivo::ID);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivo($timestamp) {
		return $this->addAnd(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoNot($timestamp) {
		return $this->andNot(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoLike($timestamp) {
		return $this->andLike(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoNotLike($timestamp) {
		return $this->andNotLike(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoGreater($timestamp) {
		return $this->andGreater(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoLess($timestamp) {
		return $this->andLess(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoLessEqual($timestamp) {
		return $this->andLessEqual(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoNull() {
		return $this->andNull(Cultivo::FECHA_CULTIVO);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoNotNull() {
		return $this->andNotNull(Cultivo::FECHA_CULTIVO);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoBetween($timestamp, $from, $to) {
		return $this->andBetween(Cultivo::FECHA_CULTIVO, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoBeginsWith($timestamp) {
		return $this->andBeginsWith(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoEndsWith($timestamp) {
		return $this->andEndsWith(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaCultivoContains($timestamp) {
		return $this->andContains(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivo($timestamp) {
		return $this->or(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoNot($timestamp) {
		return $this->orNot(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoLike($timestamp) {
		return $this->orLike(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoNotLike($timestamp) {
		return $this->orNotLike(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoGreater($timestamp) {
		return $this->orGreater(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoLess($timestamp) {
		return $this->orLess(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoLessEqual($timestamp) {
		return $this->orLessEqual(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoNull() {
		return $this->orNull(Cultivo::FECHA_CULTIVO);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoNotNull() {
		return $this->orNotNull(Cultivo::FECHA_CULTIVO);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoBetween($timestamp, $from, $to) {
		return $this->orBetween(Cultivo::FECHA_CULTIVO, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoBeginsWith($timestamp) {
		return $this->orBeginsWith(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoEndsWith($timestamp) {
		return $this->orEndsWith(Cultivo::FECHA_CULTIVO, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaCultivoContains($timestamp) {
		return $this->orContains(Cultivo::FECHA_CULTIVO, $timestamp);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByFechaCultivoAsc() {
		return $this->orderBy(Cultivo::FECHA_CULTIVO, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByFechaCultivoDesc() {
		return $this->orderBy(Cultivo::FECHA_CULTIVO, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByFechaCultivo() {
		return $this->groupBy(Cultivo::FECHA_CULTIVO);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembra($integer) {
		return $this->addAnd(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraNot($integer) {
		return $this->andNot(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraLike($integer) {
		return $this->andLike(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraNotLike($integer) {
		return $this->andNotLike(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraGreater($integer) {
		return $this->andGreater(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraLess($integer) {
		return $this->andLess(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraLessEqual($integer) {
		return $this->andLessEqual(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraNull() {
		return $this->andNull(Cultivo::TIPO_SIEMBRA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraNotNull() {
		return $this->andNotNull(Cultivo::TIPO_SIEMBRA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::TIPO_SIEMBRA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraEndsWith($integer) {
		return $this->andEndsWith(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andTipoSiembraContains($integer) {
		return $this->andContains(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembra($integer) {
		return $this->or(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraNot($integer) {
		return $this->orNot(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraLike($integer) {
		return $this->orLike(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraNotLike($integer) {
		return $this->orNotLike(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraGreater($integer) {
		return $this->orGreater(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraLess($integer) {
		return $this->orLess(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraLessEqual($integer) {
		return $this->orLessEqual(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraNull() {
		return $this->orNull(Cultivo::TIPO_SIEMBRA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraNotNull() {
		return $this->orNotNull(Cultivo::TIPO_SIEMBRA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::TIPO_SIEMBRA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraEndsWith($integer) {
		return $this->orEndsWith(Cultivo::TIPO_SIEMBRA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orTipoSiembraContains($integer) {
		return $this->orContains(Cultivo::TIPO_SIEMBRA, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByTipoSiembraAsc() {
		return $this->orderBy(Cultivo::TIPO_SIEMBRA, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByTipoSiembraDesc() {
		return $this->orderBy(Cultivo::TIPO_SIEMBRA, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByTipoSiembra() {
		return $this->groupBy(Cultivo::TIPO_SIEMBRA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZona($integer) {
		return $this->addAnd(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaNot($integer) {
		return $this->andNot(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaLike($integer) {
		return $this->andLike(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaNotLike($integer) {
		return $this->andNotLike(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaGreater($integer) {
		return $this->andGreater(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaLess($integer) {
		return $this->andLess(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaLessEqual($integer) {
		return $this->andLessEqual(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaNull() {
		return $this->andNull(Cultivo::ZONA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaNotNull() {
		return $this->andNotNull(Cultivo::ZONA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::ZONA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaEndsWith($integer) {
		return $this->andEndsWith(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andZonaContains($integer) {
		return $this->andContains(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZona($integer) {
		return $this->or(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaNot($integer) {
		return $this->orNot(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaLike($integer) {
		return $this->orLike(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaNotLike($integer) {
		return $this->orNotLike(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaGreater($integer) {
		return $this->orGreater(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaLess($integer) {
		return $this->orLess(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaLessEqual($integer) {
		return $this->orLessEqual(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaNull() {
		return $this->orNull(Cultivo::ZONA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaNotNull() {
		return $this->orNotNull(Cultivo::ZONA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::ZONA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaEndsWith($integer) {
		return $this->orEndsWith(Cultivo::ZONA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orZonaContains($integer) {
		return $this->orContains(Cultivo::ZONA, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByZonaAsc() {
		return $this->orderBy(Cultivo::ZONA, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByZonaDesc() {
		return $this->orderBy(Cultivo::ZONA, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByZona() {
		return $this->groupBy(Cultivo::ZONA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivador($integer) {
		return $this->addAnd(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorNot($integer) {
		return $this->andNot(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorLike($integer) {
		return $this->andLike(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorNotLike($integer) {
		return $this->andNotLike(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorGreater($integer) {
		return $this->andGreater(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorLess($integer) {
		return $this->andLess(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorLessEqual($integer) {
		return $this->andLessEqual(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorNull() {
		return $this->andNull(Cultivo::CULTIVADOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorNotNull() {
		return $this->andNotNull(Cultivo::CULTIVADOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::CULTIVADOR, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorEndsWith($integer) {
		return $this->andEndsWith(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCultivadorContains($integer) {
		return $this->andContains(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivador($integer) {
		return $this->or(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorNot($integer) {
		return $this->orNot(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorLike($integer) {
		return $this->orLike(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorNotLike($integer) {
		return $this->orNotLike(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorGreater($integer) {
		return $this->orGreater(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorLess($integer) {
		return $this->orLess(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorLessEqual($integer) {
		return $this->orLessEqual(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorNull() {
		return $this->orNull(Cultivo::CULTIVADOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorNotNull() {
		return $this->orNotNull(Cultivo::CULTIVADOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::CULTIVADOR, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorEndsWith($integer) {
		return $this->orEndsWith(Cultivo::CULTIVADOR, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCultivadorContains($integer) {
		return $this->orContains(Cultivo::CULTIVADOR, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByCultivadorAsc() {
		return $this->orderBy(Cultivo::CULTIVADOR, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByCultivadorDesc() {
		return $this->orderBy(Cultivo::CULTIVADOR, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByCultivador() {
		return $this->groupBy(Cultivo::CULTIVADOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAt($timestamp) {
		return $this->addAnd(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtNot($timestamp) {
		return $this->andNot(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtLike($timestamp) {
		return $this->andLike(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtNotLike($timestamp) {
		return $this->andNotLike(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtGreater($timestamp) {
		return $this->andGreater(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtLess($timestamp) {
		return $this->andLess(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtNull() {
		return $this->andNull(Cultivo::CREATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtNotNull() {
		return $this->andNotNull(Cultivo::CREATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cultivo::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCreatedAtContains($timestamp) {
		return $this->andContains(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAt($timestamp) {
		return $this->or(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtNot($timestamp) {
		return $this->orNot(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtLike($timestamp) {
		return $this->orLike(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtNotLike($timestamp) {
		return $this->orNotLike(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtGreater($timestamp) {
		return $this->orGreater(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtLess($timestamp) {
		return $this->orLess(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtNull() {
		return $this->orNull(Cultivo::CREATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtNotNull() {
		return $this->orNotNull(Cultivo::CREATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cultivo::CREATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cultivo::CREATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCreatedAtContains($timestamp) {
		return $this->orContains(Cultivo::CREATED_AT, $timestamp);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByCreatedAtAsc() {
		return $this->orderBy(Cultivo::CREATED_AT, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByCreatedAtDesc() {
		return $this->orderBy(Cultivo::CREATED_AT, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByCreatedAt() {
		return $this->groupBy(Cultivo::CREATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAt($timestamp) {
		return $this->addAnd(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtNot($timestamp) {
		return $this->andNot(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtLike($timestamp) {
		return $this->andLike(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtNotLike($timestamp) {
		return $this->andNotLike(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtGreater($timestamp) {
		return $this->andGreater(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtLess($timestamp) {
		return $this->andLess(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtNull() {
		return $this->andNull(Cultivo::UPDATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtNotNull() {
		return $this->andNotNull(Cultivo::UPDATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cultivo::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andUpdatedAtContains($timestamp) {
		return $this->andContains(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAt($timestamp) {
		return $this->or(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtNot($timestamp) {
		return $this->orNot(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtLike($timestamp) {
		return $this->orLike(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtNotLike($timestamp) {
		return $this->orNotLike(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtGreater($timestamp) {
		return $this->orGreater(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtLess($timestamp) {
		return $this->orLess(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtNull() {
		return $this->orNull(Cultivo::UPDATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtNotNull() {
		return $this->orNotNull(Cultivo::UPDATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cultivo::UPDATED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cultivo::UPDATED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orUpdatedAtContains($timestamp) {
		return $this->orContains(Cultivo::UPDATED_AT, $timestamp);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByUpdatedAtAsc() {
		return $this->orderBy(Cultivo::UPDATED_AT, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByUpdatedAtDesc() {
		return $this->orderBy(Cultivo::UPDATED_AT, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByUpdatedAt() {
		return $this->groupBy(Cultivo::UPDATED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAt($timestamp) {
		return $this->addAnd(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtNot($timestamp) {
		return $this->andNot(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtLike($timestamp) {
		return $this->andLike(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtNotLike($timestamp) {
		return $this->andNotLike(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtGreater($timestamp) {
		return $this->andGreater(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtLess($timestamp) {
		return $this->andLess(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtLessEqual($timestamp) {
		return $this->andLessEqual(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtNull() {
		return $this->andNull(Cultivo::DELETED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtNotNull() {
		return $this->andNotNull(Cultivo::DELETED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtBetween($timestamp, $from, $to) {
		return $this->andBetween(Cultivo::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtBeginsWith($timestamp) {
		return $this->andBeginsWith(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtEndsWith($timestamp) {
		return $this->andEndsWith(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andDeletedAtContains($timestamp) {
		return $this->andContains(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAt($timestamp) {
		return $this->or(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtNot($timestamp) {
		return $this->orNot(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtLike($timestamp) {
		return $this->orLike(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtNotLike($timestamp) {
		return $this->orNotLike(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtGreater($timestamp) {
		return $this->orGreater(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtLess($timestamp) {
		return $this->orLess(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtLessEqual($timestamp) {
		return $this->orLessEqual(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtNull() {
		return $this->orNull(Cultivo::DELETED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtNotNull() {
		return $this->orNotNull(Cultivo::DELETED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtBetween($timestamp, $from, $to) {
		return $this->orBetween(Cultivo::DELETED_AT, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtBeginsWith($timestamp) {
		return $this->orBeginsWith(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtEndsWith($timestamp) {
		return $this->orEndsWith(Cultivo::DELETED_AT, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orDeletedAtContains($timestamp) {
		return $this->orContains(Cultivo::DELETED_AT, $timestamp);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByDeletedAtAsc() {
		return $this->orderBy(Cultivo::DELETED_AT, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByDeletedAtDesc() {
		return $this->orderBy(Cultivo::DELETED_AT, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByDeletedAt() {
		return $this->groupBy(Cultivo::DELETED_AT);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccion($integer) {
		return $this->addAnd(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionNot($integer) {
		return $this->andNot(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionLike($integer) {
		return $this->andLike(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionNotLike($integer) {
		return $this->andNotLike(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionGreater($integer) {
		return $this->andGreater(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionLess($integer) {
		return $this->andLess(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionLessEqual($integer) {
		return $this->andLessEqual(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionNull() {
		return $this->andNull(Cultivo::CANTIDAD_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionNotNull() {
		return $this->andNotNull(Cultivo::CANTIDAD_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::CANTIDAD_PRODUCCION, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionEndsWith($integer) {
		return $this->andEndsWith(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadProduccionContains($integer) {
		return $this->andContains(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccion($integer) {
		return $this->or(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionNot($integer) {
		return $this->orNot(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionLike($integer) {
		return $this->orLike(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionNotLike($integer) {
		return $this->orNotLike(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionGreater($integer) {
		return $this->orGreater(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionLess($integer) {
		return $this->orLess(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionLessEqual($integer) {
		return $this->orLessEqual(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionNull() {
		return $this->orNull(Cultivo::CANTIDAD_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionNotNull() {
		return $this->orNotNull(Cultivo::CANTIDAD_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::CANTIDAD_PRODUCCION, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionEndsWith($integer) {
		return $this->orEndsWith(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadProduccionContains($integer) {
		return $this->orContains(Cultivo::CANTIDAD_PRODUCCION, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByCantidadProduccionAsc() {
		return $this->orderBy(Cultivo::CANTIDAD_PRODUCCION, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByCantidadProduccionDesc() {
		return $this->orderBy(Cultivo::CANTIDAD_PRODUCCION, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByCantidadProduccion() {
		return $this->groupBy(Cultivo::CANTIDAD_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccion($timestamp) {
		return $this->addAnd(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionNot($timestamp) {
		return $this->andNot(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionLike($timestamp) {
		return $this->andLike(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionNotLike($timestamp) {
		return $this->andNotLike(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionGreater($timestamp) {
		return $this->andGreater(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionGreaterEqual($timestamp) {
		return $this->andGreaterEqual(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionLess($timestamp) {
		return $this->andLess(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionLessEqual($timestamp) {
		return $this->andLessEqual(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionNull() {
		return $this->andNull(Cultivo::FECHA_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionNotNull() {
		return $this->andNotNull(Cultivo::FECHA_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionBetween($timestamp, $from, $to) {
		return $this->andBetween(Cultivo::FECHA_PRODUCCION, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionBeginsWith($timestamp) {
		return $this->andBeginsWith(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionEndsWith($timestamp) {
		return $this->andEndsWith(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function andFechaProduccionContains($timestamp) {
		return $this->andContains(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccion($timestamp) {
		return $this->or(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionNot($timestamp) {
		return $this->orNot(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionLike($timestamp) {
		return $this->orLike(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionNotLike($timestamp) {
		return $this->orNotLike(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionGreater($timestamp) {
		return $this->orGreater(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionGreaterEqual($timestamp) {
		return $this->orGreaterEqual(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionLess($timestamp) {
		return $this->orLess(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionLessEqual($timestamp) {
		return $this->orLessEqual(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionNull() {
		return $this->orNull(Cultivo::FECHA_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionNotNull() {
		return $this->orNotNull(Cultivo::FECHA_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionBetween($timestamp, $from, $to) {
		return $this->orBetween(Cultivo::FECHA_PRODUCCION, $timestamp, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionBeginsWith($timestamp) {
		return $this->orBeginsWith(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionEndsWith($timestamp) {
		return $this->orEndsWith(Cultivo::FECHA_PRODUCCION, $timestamp);
	}

	/**
	 * @return CultivoQuery
	 */
	function orFechaProduccionContains($timestamp) {
		return $this->orContains(Cultivo::FECHA_PRODUCCION, $timestamp);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByFechaProduccionAsc() {
		return $this->orderBy(Cultivo::FECHA_PRODUCCION, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByFechaProduccionDesc() {
		return $this->orderBy(Cultivo::FECHA_PRODUCCION, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByFechaProduccion() {
		return $this->groupBy(Cultivo::FECHA_PRODUCCION);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembrada($integer) {
		return $this->addAnd(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaNot($integer) {
		return $this->andNot(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaLike($integer) {
		return $this->andLike(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaNotLike($integer) {
		return $this->andNotLike(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaGreater($integer) {
		return $this->andGreater(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaLess($integer) {
		return $this->andLess(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaLessEqual($integer) {
		return $this->andLessEqual(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaNull() {
		return $this->andNull(Cultivo::CANTIDAD_SEMBRADA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaNotNull() {
		return $this->andNotNull(Cultivo::CANTIDAD_SEMBRADA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::CANTIDAD_SEMBRADA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaEndsWith($integer) {
		return $this->andEndsWith(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCantidadSembradaContains($integer) {
		return $this->andContains(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembrada($integer) {
		return $this->or(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaNot($integer) {
		return $this->orNot(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaLike($integer) {
		return $this->orLike(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaNotLike($integer) {
		return $this->orNotLike(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaGreater($integer) {
		return $this->orGreater(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaLess($integer) {
		return $this->orLess(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaLessEqual($integer) {
		return $this->orLessEqual(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaNull() {
		return $this->orNull(Cultivo::CANTIDAD_SEMBRADA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaNotNull() {
		return $this->orNotNull(Cultivo::CANTIDAD_SEMBRADA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::CANTIDAD_SEMBRADA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaEndsWith($integer) {
		return $this->orEndsWith(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCantidadSembradaContains($integer) {
		return $this->orContains(Cultivo::CANTIDAD_SEMBRADA, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByCantidadSembradaAsc() {
		return $this->orderBy(Cultivo::CANTIDAD_SEMBRADA, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByCantidadSembradaDesc() {
		return $this->orderBy(Cultivo::CANTIDAD_SEMBRADA, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByCantidadSembrada() {
		return $this->groupBy(Cultivo::CANTIDAD_SEMBRADA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andArea($integer) {
		return $this->addAnd(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaNot($integer) {
		return $this->andNot(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaLike($integer) {
		return $this->andLike(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaNotLike($integer) {
		return $this->andNotLike(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaGreater($integer) {
		return $this->andGreater(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaGreaterEqual($integer) {
		return $this->andGreaterEqual(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaLess($integer) {
		return $this->andLess(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaLessEqual($integer) {
		return $this->andLessEqual(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaNull() {
		return $this->andNull(Cultivo::AREA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaNotNull() {
		return $this->andNotNull(Cultivo::AREA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaBetween($integer, $from, $to) {
		return $this->andBetween(Cultivo::AREA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaBeginsWith($integer) {
		return $this->andBeginsWith(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaEndsWith($integer) {
		return $this->andEndsWith(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function andAreaContains($integer) {
		return $this->andContains(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orArea($integer) {
		return $this->or(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaNot($integer) {
		return $this->orNot(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaLike($integer) {
		return $this->orLike(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaNotLike($integer) {
		return $this->orNotLike(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaGreater($integer) {
		return $this->orGreater(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaGreaterEqual($integer) {
		return $this->orGreaterEqual(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaLess($integer) {
		return $this->orLess(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaLessEqual($integer) {
		return $this->orLessEqual(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaNull() {
		return $this->orNull(Cultivo::AREA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaNotNull() {
		return $this->orNotNull(Cultivo::AREA);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaBetween($integer, $from, $to) {
		return $this->orBetween(Cultivo::AREA, $integer, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaBeginsWith($integer) {
		return $this->orBeginsWith(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaEndsWith($integer) {
		return $this->orEndsWith(Cultivo::AREA, $integer);
	}

	/**
	 * @return CultivoQuery
	 */
	function orAreaContains($integer) {
		return $this->orContains(Cultivo::AREA, $integer);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByAreaAsc() {
		return $this->orderBy(Cultivo::AREA, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByAreaDesc() {
		return $this->orderBy(Cultivo::AREA, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByArea() {
		return $this->groupBy(Cultivo::AREA);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadas($varchar) {
		return $this->addAnd(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasNot($varchar) {
		return $this->andNot(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasLike($varchar) {
		return $this->andLike(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasNotLike($varchar) {
		return $this->andNotLike(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasGreater($varchar) {
		return $this->andGreater(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasGreaterEqual($varchar) {
		return $this->andGreaterEqual(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasLess($varchar) {
		return $this->andLess(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasLessEqual($varchar) {
		return $this->andLessEqual(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasNull() {
		return $this->andNull(Cultivo::COORDENADAS);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasNotNull() {
		return $this->andNotNull(Cultivo::COORDENADAS);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasBetween($varchar, $from, $to) {
		return $this->andBetween(Cultivo::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasBeginsWith($varchar) {
		return $this->andBeginsWith(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasEndsWith($varchar) {
		return $this->andEndsWith(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andCoordenadasContains($varchar) {
		return $this->andContains(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadas($varchar) {
		return $this->or(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasNot($varchar) {
		return $this->orNot(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasLike($varchar) {
		return $this->orLike(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasNotLike($varchar) {
		return $this->orNotLike(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasGreater($varchar) {
		return $this->orGreater(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasGreaterEqual($varchar) {
		return $this->orGreaterEqual(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasLess($varchar) {
		return $this->orLess(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasLessEqual($varchar) {
		return $this->orLessEqual(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasNull() {
		return $this->orNull(Cultivo::COORDENADAS);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasNotNull() {
		return $this->orNotNull(Cultivo::COORDENADAS);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasBetween($varchar, $from, $to) {
		return $this->orBetween(Cultivo::COORDENADAS, $varchar, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasBeginsWith($varchar) {
		return $this->orBeginsWith(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasEndsWith($varchar) {
		return $this->orEndsWith(Cultivo::COORDENADAS, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orCoordenadasContains($varchar) {
		return $this->orContains(Cultivo::COORDENADAS, $varchar);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByCoordenadasAsc() {
		return $this->orderBy(Cultivo::COORDENADAS, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByCoordenadasDesc() {
		return $this->orderBy(Cultivo::COORDENADAS, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByCoordenadas() {
		return $this->groupBy(Cultivo::COORDENADAS);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColor($varchar) {
		return $this->addAnd(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorNot($varchar) {
		return $this->andNot(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorLike($varchar) {
		return $this->andLike(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorNotLike($varchar) {
		return $this->andNotLike(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorGreater($varchar) {
		return $this->andGreater(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorGreaterEqual($varchar) {
		return $this->andGreaterEqual(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorLess($varchar) {
		return $this->andLess(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorLessEqual($varchar) {
		return $this->andLessEqual(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorNull() {
		return $this->andNull(Cultivo::COLOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorNotNull() {
		return $this->andNotNull(Cultivo::COLOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorBetween($varchar, $from, $to) {
		return $this->andBetween(Cultivo::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorBeginsWith($varchar) {
		return $this->andBeginsWith(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorEndsWith($varchar) {
		return $this->andEndsWith(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function andColorContains($varchar) {
		return $this->andContains(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColor($varchar) {
		return $this->or(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorNot($varchar) {
		return $this->orNot(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorLike($varchar) {
		return $this->orLike(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorNotLike($varchar) {
		return $this->orNotLike(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorGreater($varchar) {
		return $this->orGreater(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorGreaterEqual($varchar) {
		return $this->orGreaterEqual(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorLess($varchar) {
		return $this->orLess(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorLessEqual($varchar) {
		return $this->orLessEqual(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorNull() {
		return $this->orNull(Cultivo::COLOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorNotNull() {
		return $this->orNotNull(Cultivo::COLOR);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorBetween($varchar, $from, $to) {
		return $this->orBetween(Cultivo::COLOR, $varchar, $from, $to);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorBeginsWith($varchar) {
		return $this->orBeginsWith(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorEndsWith($varchar) {
		return $this->orEndsWith(Cultivo::COLOR, $varchar);
	}

	/**
	 * @return CultivoQuery
	 */
	function orColorContains($varchar) {
		return $this->orContains(Cultivo::COLOR, $varchar);
	}


	/**
	 * @return CultivoQuery
	 */
	function orderByColorAsc() {
		return $this->orderBy(Cultivo::COLOR, self::ASC);
	}

	/**
	 * @return CultivoQuery
	 */
	function orderByColorDesc() {
		return $this->orderBy(Cultivo::COLOR, self::DESC);
	}

	/**
	 * @return CultivoQuery
	 */
	function groupByColor() {
		return $this->groupBy(Cultivo::COLOR);
	}


}