<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseMiembrosFamilia extends ApplicationModel {

	const ID = 'miembros_familia.id';
	const ROL_MIEMBRO = 'miembros_familia.rol_miembro';
	const FAMILIA = 'miembros_familia.familia';
	const PERSONA = 'miembros_familia.persona';
	const CREATED_AT = 'miembros_familia.created_at';
	const UPDATED_AT = 'miembros_familia.updated_at';
	const DELETED_AT = 'miembros_familia.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'miembros_familia';

	/**
	 * Cache of objects retrieved from the database
	 * @var MiembrosFamilia[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'rol_miembro' => Model::COLUMN_TYPE_INTEGER,
		'familia' => Model::COLUMN_TYPE_INTEGER,
		'persona' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `rol_miembro` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $rol_miembro;

	/**
	 * `familia` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $familia;

	/**
	 * `persona` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $persona;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return MiembrosFamilia
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the rol_miembro field
	 */
	function getRolMiembro() {
		return $this->rol_miembro;
	}

	/**
	 * Sets the value of the rol_miembro field
	 * @return MiembrosFamilia
	 */
	function setRolMiembro($value) {
		return $this->setColumnValue('rol_miembro', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for MiembrosFamilia::getRolMiembro
	 * final because getRolMiembro should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::getRolMiembro
	 */
	final function getRol_miembro() {
		return $this->getRolMiembro();
	}

	/**
	 * Convenience function for MiembrosFamilia::setRolMiembro
	 * final because setRolMiembro should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::setRolMiembro
	 * @return MiembrosFamilia
	 */
	final function setRol_miembro($value) {
		return $this->setRolMiembro($value);
	}

	/**
	 * Gets the value of the familia field
	 */
	function getFamilia() {
		return $this->familia;
	}

	/**
	 * Sets the value of the familia field
	 * @return MiembrosFamilia
	 */
	function setFamilia($value) {
		return $this->setColumnValue('familia', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the persona field
	 */
	function getPersona() {
		return $this->persona;
	}

	/**
	 * Sets the value of the persona field
	 * @return MiembrosFamilia
	 */
	function setPersona($value) {
		return $this->setColumnValue('persona', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return MiembrosFamilia
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MiembrosFamilia::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for MiembrosFamilia::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::setCreatedAt
	 * @return MiembrosFamilia
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return MiembrosFamilia
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MiembrosFamilia::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for MiembrosFamilia::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::setUpdatedAt
	 * @return MiembrosFamilia
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return MiembrosFamilia
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for MiembrosFamilia::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for MiembrosFamilia::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see MiembrosFamilia::setDeletedAt
	 * @return MiembrosFamilia
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return MiembrosFamilia
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return MiembrosFamilia
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveById($value) {
		return MiembrosFamilia::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a rol_miembro
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByRolMiembro($value) {
		return static::retrieveByColumn('rol_miembro', $value);
	}

	/**
	 * Searches the database for a row with a familia
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByFamilia($value) {
		return static::retrieveByColumn('familia', $value);
	}

	/**
	 * Searches the database for a row with a persona
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByPersona($value) {
		return static::retrieveByColumn('persona', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return MiembrosFamilia
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return MiembrosFamilia
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->rol_miembro = (null === $this->rol_miembro) ? null : (int) $this->rol_miembro;
		$this->familia = (null === $this->familia) ? null : (int) $this->familia;
		$this->persona = (null === $this->persona) ? null : (int) $this->persona;
		return $this;
	}

	/**
	 * @return MiembrosFamilia
	 */
	function setFamiliaRelatedByFamilia(Familia $familia = null) {
		if (null === $familia) {
			$this->setfamilia(null);
		} else {
			if (!$familia->getid()) {
				throw new Exception('Cannot connect a Familia without a id');
			}
			$this->setfamilia($familia->getid());
		}
		return $this;
	}

	/**
	 * Returns a familia object with a id
	 * that matches $this->familia.
	 * @return Familia
	 */
	function getFamiliaRelatedByFamilia() {
		$fk_value = $this->getfamilia();
		if (null === $fk_value) {
			return null;
		}
		return Familia::retrieveByPK($fk_value);
	}

	/**
	 * @return MiembrosFamilia[]
	 */
	static function doSelectJoinFamiliaRelatedByFamilia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Familia::getTableName();
		$q->join($to_table, $this_table . '.familia = ' . $to_table . '.id', $join_type);
		foreach (Familia::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Familia'));
	}

	/**
	 * @return MiembrosFamilia
	 */
	function setRolMiembroRelatedByRolMiembro(RolMiembro $rolmiembro = null) {
		if (null === $rolmiembro) {
			$this->setrol_miembro(null);
		} else {
			if (!$rolmiembro->getid()) {
				throw new Exception('Cannot connect a RolMiembro without a id');
			}
			$this->setrol_miembro($rolmiembro->getid());
		}
		return $this;
	}

	/**
	 * Returns a rol_miembro object with a id
	 * that matches $this->rol_miembro.
	 * @return RolMiembro
	 */
	function getRolMiembroRelatedByRolMiembro() {
		$fk_value = $this->getrol_miembro();
		if (null === $fk_value) {
			return null;
		}
		return RolMiembro::retrieveByPK($fk_value);
	}

	/**
	 * @return MiembrosFamilia[]
	 */
	static function doSelectJoinRolMiembroRelatedByRolMiembro(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = RolMiembro::getTableName();
		$q->join($to_table, $this_table . '.rol_miembro = ' . $to_table . '.id', $join_type);
		foreach (RolMiembro::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('RolMiembro'));
	}

	/**
	 * @return MiembrosFamilia
	 */
	function setUsuariosRelatedByPersona(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setpersona(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setpersona($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->persona.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByPersona() {
		$fk_value = $this->getpersona();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->persona.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByPersona();
	}

	/**
	 * @return MiembrosFamilia
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByPersona($usuarios);
	}

	/**
	 * @return MiembrosFamilia[]
	 */
	static function doSelectJoinUsuariosRelatedByPersona(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.persona = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return MiembrosFamilia[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Familia::getTableName();
		$q->join($to_table, $this_table . '.familia = ' . $to_table . '.id', $join_type);
		foreach (Familia::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Familia';
	
		$to_table = RolMiembro::getTableName();
		$q->join($to_table, $this_table . '.rol_miembro = ' . $to_table . '.id', $join_type);
		foreach (RolMiembro::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'RolMiembro';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.persona = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting alquiler Objects(rows) from the alquiler table
	 * with a miembro_id that matches $this->id.
	 * @return Query
	 */
	function getAlquilersRelatedByMiembroIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'miembro_id', 'id', $q);
	}

	/**
	 * Returns the count of Alquiler Objects(rows) from the alquiler table
	 * with a miembro_id that matches $this->id.
	 * @return int
	 */
	function countAlquilersRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Alquiler::doCount($this->getAlquilersRelatedByMiembroIdQuery($q));
	}

	/**
	 * Deletes the alquiler Objects(rows) from the alquiler table
	 * with a miembro_id that matches $this->id.
	 * @return int
	 */
	function deleteAlquilersRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AlquilersRelatedByMiembroId_c = array();
		return Alquiler::doDelete($this->getAlquilersRelatedByMiembroIdQuery($q));
	}

	protected $AlquilersRelatedByMiembroId_c = array();

	/**
	 * Returns an array of Alquiler objects with a miembro_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Alquiler[]
	 */
	function getAlquilersRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AlquilersRelatedByMiembroId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AlquilersRelatedByMiembroId_c;
		}

		$result = Alquiler::doSelect($this->getAlquilersRelatedByMiembroIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AlquilersRelatedByMiembroId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting zonas Objects(rows) from the zonas table
	 * with a miembro_id that matches $this->id.
	 * @return Query
	 */
	function getZonassRelatedByMiembroIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'miembro_id', 'id', $q);
	}

	/**
	 * Returns the count of Zonas Objects(rows) from the zonas table
	 * with a miembro_id that matches $this->id.
	 * @return int
	 */
	function countZonassRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Zonas::doCount($this->getZonassRelatedByMiembroIdQuery($q));
	}

	/**
	 * Deletes the zonas Objects(rows) from the zonas table
	 * with a miembro_id that matches $this->id.
	 * @return int
	 */
	function deleteZonassRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ZonassRelatedByMiembroId_c = array();
		return Zonas::doDelete($this->getZonassRelatedByMiembroIdQuery($q));
	}

	protected $ZonassRelatedByMiembroId_c = array();

	/**
	 * Returns an array of Zonas objects with a miembro_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Zonas[]
	 */
	function getZonassRelatedByMiembroId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ZonassRelatedByMiembroId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ZonassRelatedByMiembroId_c;
		}

		$result = Zonas::doSelect($this->getZonassRelatedByMiembroIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ZonassRelatedByMiembroId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for MiembrosFamilia::getAlquilersRelatedBymiembro_id
	 * @return Alquiler[]
	 * @see MiembrosFamilia::getAlquilersRelatedByMiembroId
	 */
	function getAlquilers($extra = null) {
		return $this->getAlquilersRelatedByMiembroId($extra);
	}

	/**
	  * Convenience function for MiembrosFamilia::getAlquilersRelatedBymiembro_idQuery
	  * @return Query
	  * @see MiembrosFamilia::getAlquilersRelatedBymiembro_idQuery
	  */
	function getAlquilersQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'miembro_id','id', $q);
	}

	/**
	  * Convenience function for MiembrosFamilia::deleteAlquilersRelatedBymiembro_id
	  * @return int
	  * @see MiembrosFamilia::deleteAlquilersRelatedBymiembro_id
	  */
	function deleteAlquilers(Query $q = null) {
		return $this->deleteAlquilersRelatedByMiembroId($q);
	}

	/**
	  * Convenience function for MiembrosFamilia::countAlquilersRelatedBymiembro_id
	  * @return int
	  * @see MiembrosFamilia::countAlquilersRelatedByMiembroId
	  */
	function countAlquilers(Query $q = null) {
		return $this->countAlquilersRelatedByMiembroId($q);
	}

	/**
	 * Convenience function for MiembrosFamilia::getZonassRelatedBymiembro_id
	 * @return Zonas[]
	 * @see MiembrosFamilia::getZonassRelatedByMiembroId
	 */
	function getZonass($extra = null) {
		return $this->getZonassRelatedByMiembroId($extra);
	}

	/**
	  * Convenience function for MiembrosFamilia::getZonassRelatedBymiembro_idQuery
	  * @return Query
	  * @see MiembrosFamilia::getZonassRelatedBymiembro_idQuery
	  */
	function getZonassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'miembro_id','id', $q);
	}

	/**
	  * Convenience function for MiembrosFamilia::deleteZonassRelatedBymiembro_id
	  * @return int
	  * @see MiembrosFamilia::deleteZonassRelatedBymiembro_id
	  */
	function deleteZonass(Query $q = null) {
		return $this->deleteZonassRelatedByMiembroId($q);
	}

	/**
	  * Convenience function for MiembrosFamilia::countZonassRelatedBymiembro_id
	  * @return int
	  * @see MiembrosFamilia::countZonassRelatedByMiembroId
	  */
	function countZonass(Query $q = null) {
		return $this->countZonassRelatedByMiembroId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getrol_miembro()) {
			$this->_validationErrors[] = 'rol_miembro must not be null';
		}
		if (null === $this->getfamilia()) {
			$this->_validationErrors[] = 'familia must not be null';
		}
		if (null === $this->getpersona()) {
			$this->_validationErrors[] = 'persona must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}