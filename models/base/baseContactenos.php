<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseContactenos extends ApplicationModel {

	const ID = 'contactenos.id';
	const NOMBRES = 'contactenos.nombres';
	const TELEFONO = 'contactenos.telefono';
	const EMAIL = 'contactenos.email';
	const ASUNTO = 'contactenos.asunto';
	const ESTADO = 'contactenos.estado';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'contactenos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Contactenos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_VARCHAR,
		'email' => Model::COLUMN_TYPE_VARCHAR,
		'asunto' => Model::COLUMN_TYPE_VARCHAR,
		'estado' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `telefono` VARCHAR NOT NULL
	 * @var string
	 */
	protected $telefono;

	/**
	 * `email` VARCHAR NOT NULL
	 * @var string
	 */
	protected $email;

	/**
	 * `asunto` VARCHAR NOT NULL
	 * @var string
	 */
	protected $asunto;

	/**
	 * `estado` VARCHAR NOT NULL
	 * @var string
	 */
	protected $estado;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Contactenos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Contactenos
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Contactenos
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the email field
	 */
	function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the value of the email field
	 * @return Contactenos
	 */
	function setEmail($value) {
		return $this->setColumnValue('email', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the asunto field
	 */
	function getAsunto() {
		return $this->asunto;
	}

	/**
	 * Sets the value of the asunto field
	 * @return Contactenos
	 */
	function setAsunto($value) {
		return $this->setColumnValue('asunto', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the estado field
	 */
	function getEstado() {
		return $this->estado;
	}

	/**
	 * Sets the value of the estado field
	 * @return Contactenos
	 */
	function setEstado($value) {
		return $this->setColumnValue('estado', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Contactenos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Contactenos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveById($value) {
		return Contactenos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a email
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveByEmail($value) {
		return static::retrieveByColumn('email', $value);
	}

	/**
	 * Searches the database for a row with a asunto
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveByAsunto($value) {
		return static::retrieveByColumn('asunto', $value);
	}

	/**
	 * Searches the database for a row with a estado
	 * value that matches the one provided
	 * @return Contactenos
	 */
	static function retrieveByEstado($value) {
		return static::retrieveByColumn('estado', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Contactenos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Contactenos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}
		if (null === $this->gettelefono()) {
			$this->_validationErrors[] = 'telefono must not be null';
		}
		if (null === $this->getemail()) {
			$this->_validationErrors[] = 'email must not be null';
		}
		if (null === $this->getasunto()) {
			$this->_validationErrors[] = 'asunto must not be null';
		}
		if (null === $this->getestado()) {
			$this->_validationErrors[] = 'estado must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}