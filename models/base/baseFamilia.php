<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseFamilia extends ApplicationModel {

	const ID = 'familia.id';
	const NOMBRE_FAMILIA = 'familia.nombre_familia';
	const CODIGO = 'familia.codigo';
	const CREATED_AT = 'familia.created_at';
	const UPDATED_AT = 'familia.updated_at';
	const DELETED_AT = 'familia.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'familia';

	/**
	 * Cache of objects retrieved from the database
	 * @var Familia[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre_familia' => Model::COLUMN_TYPE_VARCHAR,
		'codigo' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre_familia` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre_familia;

	/**
	 * `codigo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $codigo;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Familia
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre_familia field
	 */
	function getNombreFamilia() {
		return $this->nombre_familia;
	}

	/**
	 * Sets the value of the nombre_familia field
	 * @return Familia
	 */
	function setNombreFamilia($value) {
		return $this->setColumnValue('nombre_familia', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Familia::getNombreFamilia
	 * final because getNombreFamilia should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::getNombreFamilia
	 */
	final function getNombre_familia() {
		return $this->getNombreFamilia();
	}

	/**
	 * Convenience function for Familia::setNombreFamilia
	 * final because setNombreFamilia should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::setNombreFamilia
	 * @return Familia
	 */
	final function setNombre_familia($value) {
		return $this->setNombreFamilia($value);
	}

	/**
	 * Gets the value of the codigo field
	 */
	function getCodigo() {
		return $this->codigo;
	}

	/**
	 * Sets the value of the codigo field
	 * @return Familia
	 */
	function setCodigo($value) {
		return $this->setColumnValue('codigo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Familia
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Familia::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Familia::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::setCreatedAt
	 * @return Familia
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Familia
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Familia::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Familia::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::setUpdatedAt
	 * @return Familia
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Familia
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Familia::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Familia::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Familia::setDeletedAt
	 * @return Familia
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Familia
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Familia
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveById($value) {
		return Familia::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre_familia
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveByNombreFamilia($value) {
		return static::retrieveByColumn('nombre_familia', $value);
	}

	/**
	 * Searches the database for a row with a codigo
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveByCodigo($value) {
		return static::retrieveByColumn('codigo', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Familia
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Familia
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Familia[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting miembros_familia Objects(rows) from the miembros_familia table
	 * with a familia that matches $this->id.
	 * @return Query
	 */
	function getMiembrosFamiliasRelatedByFamiliaQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'familia', 'id', $q);
	}

	/**
	 * Returns the count of MiembrosFamilia Objects(rows) from the miembros_familia table
	 * with a familia that matches $this->id.
	 * @return int
	 */
	function countMiembrosFamiliasRelatedByFamilia(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MiembrosFamilia::doCount($this->getMiembrosFamiliasRelatedByFamiliaQuery($q));
	}

	/**
	 * Deletes the miembros_familia Objects(rows) from the miembros_familia table
	 * with a familia that matches $this->id.
	 * @return int
	 */
	function deleteMiembrosFamiliasRelatedByFamilia(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MiembrosFamiliasRelatedByFamilia_c = array();
		return MiembrosFamilia::doDelete($this->getMiembrosFamiliasRelatedByFamiliaQuery($q));
	}

	protected $MiembrosFamiliasRelatedByFamilia_c = array();

	/**
	 * Returns an array of MiembrosFamilia objects with a familia
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MiembrosFamilia[]
	 */
	function getMiembrosFamiliasRelatedByFamilia(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MiembrosFamiliasRelatedByFamilia_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MiembrosFamiliasRelatedByFamilia_c;
		}

		$result = MiembrosFamilia::doSelect($this->getMiembrosFamiliasRelatedByFamiliaQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MiembrosFamiliasRelatedByFamilia_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting predio Objects(rows) from the predio table
	 * with a familia_id that matches $this->id.
	 * @return Query
	 */
	function getPrediosRelatedByFamiliaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('predio', 'familia_id', 'id', $q);
	}

	/**
	 * Returns the count of Predio Objects(rows) from the predio table
	 * with a familia_id that matches $this->id.
	 * @return int
	 */
	function countPrediosRelatedByFamiliaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Predio::doCount($this->getPrediosRelatedByFamiliaIdQuery($q));
	}

	/**
	 * Deletes the predio Objects(rows) from the predio table
	 * with a familia_id that matches $this->id.
	 * @return int
	 */
	function deletePrediosRelatedByFamiliaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PrediosRelatedByFamiliaId_c = array();
		return Predio::doDelete($this->getPrediosRelatedByFamiliaIdQuery($q));
	}

	protected $PrediosRelatedByFamiliaId_c = array();

	/**
	 * Returns an array of Predio objects with a familia_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Predio[]
	 */
	function getPrediosRelatedByFamiliaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PrediosRelatedByFamiliaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PrediosRelatedByFamiliaId_c;
		}

		$result = Predio::doSelect($this->getPrediosRelatedByFamiliaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PrediosRelatedByFamiliaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Familia::getMiembrosFamiliasRelatedByfamilia
	 * @return MiembrosFamilia[]
	 * @see Familia::getMiembrosFamiliasRelatedByFamilia
	 */
	function getMiembrosFamilias($extra = null) {
		return $this->getMiembrosFamiliasRelatedByFamilia($extra);
	}

	/**
	  * Convenience function for Familia::getMiembrosFamiliasRelatedByfamiliaQuery
	  * @return Query
	  * @see Familia::getMiembrosFamiliasRelatedByfamiliaQuery
	  */
	function getMiembrosFamiliasQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'familia','id', $q);
	}

	/**
	  * Convenience function for Familia::deleteMiembrosFamiliasRelatedByfamilia
	  * @return int
	  * @see Familia::deleteMiembrosFamiliasRelatedByfamilia
	  */
	function deleteMiembrosFamilias(Query $q = null) {
		return $this->deleteMiembrosFamiliasRelatedByFamilia($q);
	}

	/**
	  * Convenience function for Familia::countMiembrosFamiliasRelatedByfamilia
	  * @return int
	  * @see Familia::countMiembrosFamiliasRelatedByFamilia
	  */
	function countMiembrosFamilias(Query $q = null) {
		return $this->countMiembrosFamiliasRelatedByFamilia($q);
	}

	/**
	 * Convenience function for Familia::getPrediosRelatedByfamilia_id
	 * @return Predio[]
	 * @see Familia::getPrediosRelatedByFamiliaId
	 */
	function getPredios($extra = null) {
		return $this->getPrediosRelatedByFamiliaId($extra);
	}

	/**
	  * Convenience function for Familia::getPrediosRelatedByfamilia_idQuery
	  * @return Query
	  * @see Familia::getPrediosRelatedByfamilia_idQuery
	  */
	function getPrediosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('predio', 'familia_id','id', $q);
	}

	/**
	  * Convenience function for Familia::deletePrediosRelatedByfamilia_id
	  * @return int
	  * @see Familia::deletePrediosRelatedByfamilia_id
	  */
	function deletePredios(Query $q = null) {
		return $this->deletePrediosRelatedByFamiliaId($q);
	}

	/**
	  * Convenience function for Familia::countPrediosRelatedByfamilia_id
	  * @return int
	  * @see Familia::countPrediosRelatedByFamiliaId
	  */
	function countPredios(Query $q = null) {
		return $this->countPrediosRelatedByFamiliaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre_familia()) {
			$this->_validationErrors[] = 'nombre_familia must not be null';
		}
		if (null === $this->getcodigo()) {
			$this->_validationErrors[] = 'codigo must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}