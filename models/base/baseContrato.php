<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseContrato extends ApplicationModel {

	const ID = 'contrato.id';
	const NUMERO_CONTRATO = 'contrato.numero_contrato';
	const FOLIO_ID = 'contrato.folio_id';
	const FECHA_CONTRATO = 'contrato.fecha_contrato';
	const FECHA_INICIO = 'contrato.fecha_inicio';
	const FECHA_TERMINO = 'contrato.fecha_termino';
	const CREATED_AT = 'contrato.created_at';
	const UPDATED_AT = 'contrato.updated_at';
	const DELETED_AT = 'contrato.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'contrato';

	/**
	 * Cache of objects retrieved from the database
	 * @var Contrato[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero_contrato' => Model::COLUMN_TYPE_VARCHAR,
		'folio_id' => Model::COLUMN_TYPE_INTEGER,
		'fecha_contrato' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_inicio' => Model::COLUMN_TYPE_TIMESTAMP,
		'fecha_termino' => Model::COLUMN_TYPE_TIMESTAMP,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero_contrato` VARCHAR NOT NULL
	 * @var string
	 */
	protected $numero_contrato;

	/**
	 * `folio_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $folio_id;

	/**
	 * `fecha_contrato` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_contrato;

	/**
	 * `fecha_inicio` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_inicio;

	/**
	 * `fecha_termino` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_termino;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Contrato
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero_contrato field
	 */
	function getNumeroContrato() {
		return $this->numero_contrato;
	}

	/**
	 * Sets the value of the numero_contrato field
	 * @return Contrato
	 */
	function setNumeroContrato($value) {
		return $this->setColumnValue('numero_contrato', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Contrato::getNumeroContrato
	 * final because getNumeroContrato should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getNumeroContrato
	 */
	final function getNumero_contrato() {
		return $this->getNumeroContrato();
	}

	/**
	 * Convenience function for Contrato::setNumeroContrato
	 * final because setNumeroContrato should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setNumeroContrato
	 * @return Contrato
	 */
	final function setNumero_contrato($value) {
		return $this->setNumeroContrato($value);
	}

	/**
	 * Gets the value of the folio_id field
	 */
	function getFolioId() {
		return $this->folio_id;
	}

	/**
	 * Sets the value of the folio_id field
	 * @return Contrato
	 */
	function setFolioId($value) {
		return $this->setColumnValue('folio_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Contrato::getFolioId
	 * final because getFolioId should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getFolioId
	 */
	final function getFolio_id() {
		return $this->getFolioId();
	}

	/**
	 * Convenience function for Contrato::setFolioId
	 * final because setFolioId should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setFolioId
	 * @return Contrato
	 */
	final function setFolio_id($value) {
		return $this->setFolioId($value);
	}

	/**
	 * Gets the value of the fecha_contrato field
	 */
	function getFechaContrato($format = null) {
		if (null === $this->fecha_contrato || null === $format) {
			return $this->fecha_contrato;
		}
		if (0 === strpos($this->fecha_contrato, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_contrato));
	}

	/**
	 * Sets the value of the fecha_contrato field
	 * @return Contrato
	 */
	function setFechaContrato($value) {
		return $this->setColumnValue('fecha_contrato', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getFechaContrato
	 * final because getFechaContrato should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getFechaContrato
	 */
	final function getFecha_contrato($format = null) {
		return $this->getFechaContrato($format);
	}

	/**
	 * Convenience function for Contrato::setFechaContrato
	 * final because setFechaContrato should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setFechaContrato
	 * @return Contrato
	 */
	final function setFecha_contrato($value) {
		return $this->setFechaContrato($value);
	}

	/**
	 * Gets the value of the fecha_inicio field
	 */
	function getFechaInicio($format = null) {
		if (null === $this->fecha_inicio || null === $format) {
			return $this->fecha_inicio;
		}
		if (0 === strpos($this->fecha_inicio, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_inicio));
	}

	/**
	 * Sets the value of the fecha_inicio field
	 * @return Contrato
	 */
	function setFechaInicio($value) {
		return $this->setColumnValue('fecha_inicio', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getFechaInicio
	 * final because getFechaInicio should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getFechaInicio
	 */
	final function getFecha_inicio($format = null) {
		return $this->getFechaInicio($format);
	}

	/**
	 * Convenience function for Contrato::setFechaInicio
	 * final because setFechaInicio should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setFechaInicio
	 * @return Contrato
	 */
	final function setFecha_inicio($value) {
		return $this->setFechaInicio($value);
	}

	/**
	 * Gets the value of the fecha_termino field
	 */
	function getFechaTermino($format = null) {
		if (null === $this->fecha_termino || null === $format) {
			return $this->fecha_termino;
		}
		if (0 === strpos($this->fecha_termino, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_termino));
	}

	/**
	 * Sets the value of the fecha_termino field
	 * @return Contrato
	 */
	function setFechaTermino($value) {
		return $this->setColumnValue('fecha_termino', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getFechaTermino
	 * final because getFechaTermino should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getFechaTermino
	 */
	final function getFecha_termino($format = null) {
		return $this->getFechaTermino($format);
	}

	/**
	 * Convenience function for Contrato::setFechaTermino
	 * final because setFechaTermino should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setFechaTermino
	 * @return Contrato
	 */
	final function setFecha_termino($value) {
		return $this->setFechaTermino($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Contrato
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Contrato::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setCreatedAt
	 * @return Contrato
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Contrato
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Contrato::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setUpdatedAt
	 * @return Contrato
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Contrato
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Contrato::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Contrato::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Contrato::setDeletedAt
	 * @return Contrato
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Contrato
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Contrato
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveById($value) {
		return Contrato::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero_contrato
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByNumeroContrato($value) {
		return static::retrieveByColumn('numero_contrato', $value);
	}

	/**
	 * Searches the database for a row with a folio_id
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByFolioId($value) {
		return static::retrieveByColumn('folio_id', $value);
	}

	/**
	 * Searches the database for a row with a fecha_contrato
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByFechaContrato($value) {
		return static::retrieveByColumn('fecha_contrato', $value);
	}

	/**
	 * Searches the database for a row with a fecha_inicio
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByFechaInicio($value) {
		return static::retrieveByColumn('fecha_inicio', $value);
	}

	/**
	 * Searches the database for a row with a fecha_termino
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByFechaTermino($value) {
		return static::retrieveByColumn('fecha_termino', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Contrato
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Contrato
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->folio_id = (null === $this->folio_id) ? null : (int) $this->folio_id;
		return $this;
	}

	/**
	 * @return Contrato
	 */
	function setFolio(Folio $folio = null) {
		return $this->setFolioRelatedByFolioId($folio);
	}

	/**
	 * @return Contrato
	 */
	function setFolioRelatedByFolioId(Folio $folio = null) {
		if (null === $folio) {
			$this->setfolio_id(null);
		} else {
			if (!$folio->getid()) {
				throw new Exception('Cannot connect a Folio without a id');
			}
			$this->setfolio_id($folio->getid());
		}
		return $this;
	}

	/**
	 * Returns a folio object with a id
	 * that matches $this->folio_id.
	 * @return Folio
	 */
	function getFolio() {
		return $this->getFolioRelatedByFolioId();
	}

	/**
	 * Returns a folio object with a id
	 * that matches $this->folio_id.
	 * @return Folio
	 */
	function getFolioRelatedByFolioId() {
		$fk_value = $this->getfolio_id();
		if (null === $fk_value) {
			return null;
		}
		return Folio::retrieveByPK($fk_value);
	}

	static function doSelectJoinFolio(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinFolioRelatedByFolioId($q, $join_type);
	}

	/**
	 * @return Contrato[]
	 */
	static function doSelectJoinFolioRelatedByFolioId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Folio::getTableName();
		$q->join($to_table, $this_table . '.folio_id = ' . $to_table . '.id', $join_type);
		foreach (Folio::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Folio'));
	}

	/**
	 * @return Contrato[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Folio::getTableName();
		$q->join($to_table, $this_table . '.folio_id = ' . $to_table . '.id', $join_type);
		foreach (Folio::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Folio';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting alquiler Objects(rows) from the alquiler table
	 * with a contrato that matches $this->id.
	 * @return Query
	 */
	function getAlquilersRelatedByContratoQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'contrato', 'id', $q);
	}

	/**
	 * Returns the count of Alquiler Objects(rows) from the alquiler table
	 * with a contrato that matches $this->id.
	 * @return int
	 */
	function countAlquilersRelatedByContrato(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Alquiler::doCount($this->getAlquilersRelatedByContratoQuery($q));
	}

	/**
	 * Deletes the alquiler Objects(rows) from the alquiler table
	 * with a contrato that matches $this->id.
	 * @return int
	 */
	function deleteAlquilersRelatedByContrato(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AlquilersRelatedByContrato_c = array();
		return Alquiler::doDelete($this->getAlquilersRelatedByContratoQuery($q));
	}

	protected $AlquilersRelatedByContrato_c = array();

	/**
	 * Returns an array of Alquiler objects with a contrato
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Alquiler[]
	 */
	function getAlquilersRelatedByContrato(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AlquilersRelatedByContrato_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AlquilersRelatedByContrato_c;
		}

		$result = Alquiler::doSelect($this->getAlquilersRelatedByContratoQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AlquilersRelatedByContrato_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Contrato::getAlquilersRelatedBycontrato
	 * @return Alquiler[]
	 * @see Contrato::getAlquilersRelatedByContrato
	 */
	function getAlquilers($extra = null) {
		return $this->getAlquilersRelatedByContrato($extra);
	}

	/**
	  * Convenience function for Contrato::getAlquilersRelatedBycontratoQuery
	  * @return Query
	  * @see Contrato::getAlquilersRelatedBycontratoQuery
	  */
	function getAlquilersQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'contrato','id', $q);
	}

	/**
	  * Convenience function for Contrato::deleteAlquilersRelatedBycontrato
	  * @return int
	  * @see Contrato::deleteAlquilersRelatedBycontrato
	  */
	function deleteAlquilers(Query $q = null) {
		return $this->deleteAlquilersRelatedByContrato($q);
	}

	/**
	  * Convenience function for Contrato::countAlquilersRelatedBycontrato
	  * @return int
	  * @see Contrato::countAlquilersRelatedByContrato
	  */
	function countAlquilers(Query $q = null) {
		return $this->countAlquilersRelatedByContrato($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnumero_contrato()) {
			$this->_validationErrors[] = 'numero_contrato must not be null';
		}
		if (null === $this->getfolio_id()) {
			$this->_validationErrors[] = 'folio_id must not be null';
		}
		if (null === $this->getfecha_contrato()) {
			$this->_validationErrors[] = 'fecha_contrato must not be null';
		}
		if (null === $this->getfecha_inicio()) {
			$this->_validationErrors[] = 'fecha_inicio must not be null';
		}
		if (null === $this->getfecha_termino()) {
			$this->_validationErrors[] = 'fecha_termino must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}