<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseSiembra extends ApplicationModel {

	const ID = 'siembra.id';
	const SIEMBRA = 'siembra.siembra';
	const DESCRIPCION = 'siembra.descripcion';
	const CREATED_AT = 'siembra.created_at';
	const UPDATED_AT = 'siembra.updated_at';
	const DELETED_AT = 'siembra.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'siembra';

	/**
	 * Cache of objects retrieved from the database
	 * @var Siembra[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'siembra' => Model::COLUMN_TYPE_VARCHAR,
		'descripcion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `siembra` VARCHAR
	 * @var string
	 */
	protected $siembra;

	/**
	 * `descripcion` VARCHAR
	 * @var string
	 */
	protected $descripcion;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Siembra
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the siembra field
	 */
	function getSiembra() {
		return $this->siembra;
	}

	/**
	 * Sets the value of the siembra field
	 * @return Siembra
	 */
	function setSiembra($value) {
		return $this->setColumnValue('siembra', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the descripcion field
	 */
	function getDescripcion() {
		return $this->descripcion;
	}

	/**
	 * Sets the value of the descripcion field
	 * @return Siembra
	 */
	function setDescripcion($value) {
		return $this->setColumnValue('descripcion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Siembra
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Siembra::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Siembra::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::setCreatedAt
	 * @return Siembra
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Siembra
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Siembra::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Siembra::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::setUpdatedAt
	 * @return Siembra
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Siembra
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Siembra::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Siembra::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Siembra::setDeletedAt
	 * @return Siembra
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Siembra
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Siembra
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveById($value) {
		return Siembra::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a siembra
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveBySiembra($value) {
		return static::retrieveByColumn('siembra', $value);
	}

	/**
	 * Searches the database for a row with a descripcion
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveByDescripcion($value) {
		return static::retrieveByColumn('descripcion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Siembra
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Siembra
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Siembra[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting cultivo Objects(rows) from the cultivo table
	 * with a tipo_siembra that matches $this->id.
	 * @return Query
	 */
	function getCultivosRelatedByTipoSiembraQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'tipo_siembra', 'id', $q);
	}

	/**
	 * Returns the count of Cultivo Objects(rows) from the cultivo table
	 * with a tipo_siembra that matches $this->id.
	 * @return int
	 */
	function countCultivosRelatedByTipoSiembra(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Cultivo::doCount($this->getCultivosRelatedByTipoSiembraQuery($q));
	}

	/**
	 * Deletes the cultivo Objects(rows) from the cultivo table
	 * with a tipo_siembra that matches $this->id.
	 * @return int
	 */
	function deleteCultivosRelatedByTipoSiembra(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->CultivosRelatedByTipoSiembra_c = array();
		return Cultivo::doDelete($this->getCultivosRelatedByTipoSiembraQuery($q));
	}

	protected $CultivosRelatedByTipoSiembra_c = array();

	/**
	 * Returns an array of Cultivo objects with a tipo_siembra
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Cultivo[]
	 */
	function getCultivosRelatedByTipoSiembra(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->CultivosRelatedByTipoSiembra_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->CultivosRelatedByTipoSiembra_c;
		}

		$result = Cultivo::doSelect($this->getCultivosRelatedByTipoSiembraQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->CultivosRelatedByTipoSiembra_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Siembra::getCultivosRelatedBytipo_siembra
	 * @return Cultivo[]
	 * @see Siembra::getCultivosRelatedByTipoSiembra
	 */
	function getCultivos($extra = null) {
		return $this->getCultivosRelatedByTipoSiembra($extra);
	}

	/**
	  * Convenience function for Siembra::getCultivosRelatedBytipo_siembraQuery
	  * @return Query
	  * @see Siembra::getCultivosRelatedBytipo_siembraQuery
	  */
	function getCultivosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'tipo_siembra','id', $q);
	}

	/**
	  * Convenience function for Siembra::deleteCultivosRelatedBytipo_siembra
	  * @return int
	  * @see Siembra::deleteCultivosRelatedBytipo_siembra
	  */
	function deleteCultivos(Query $q = null) {
		return $this->deleteCultivosRelatedByTipoSiembra($q);
	}

	/**
	  * Convenience function for Siembra::countCultivosRelatedBytipo_siembra
	  * @return int
	  * @see Siembra::countCultivosRelatedByTipoSiembra
	  */
	function countCultivos(Query $q = null) {
		return $this->countCultivosRelatedByTipoSiembra($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}