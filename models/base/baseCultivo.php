<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCultivo extends ApplicationModel {

	const ID = 'cultivo.id';
	const FECHA_CULTIVO = 'cultivo.fecha_cultivo';
	const TIPO_SIEMBRA = 'cultivo.tipo_siembra';
	const ZONA = 'cultivo.zona';
	const CULTIVADOR = 'cultivo.cultivador';
	const CREATED_AT = 'cultivo.created_at';
	const UPDATED_AT = 'cultivo.updated_at';
	const DELETED_AT = 'cultivo.deleted_at';
	const CANTIDAD_PRODUCCION = 'cultivo.cantidad_produccion';
	const FECHA_PRODUCCION = 'cultivo.fecha_produccion';
	const CANTIDAD_SEMBRADA = 'cultivo.cantidad_sembrada';
	const AREA = 'cultivo.area';
	const COORDENADAS = 'cultivo.coordenadas';
	const COLOR = 'cultivo.color';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'cultivo';

	/**
	 * Cache of objects retrieved from the database
	 * @var Cultivo[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'fecha_cultivo' => Model::COLUMN_TYPE_TIMESTAMP,
		'tipo_siembra' => Model::COLUMN_TYPE_INTEGER,
		'zona' => Model::COLUMN_TYPE_INTEGER,
		'cultivador' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'cantidad_produccion' => Model::COLUMN_TYPE_INTEGER,
		'fecha_produccion' => Model::COLUMN_TYPE_TIMESTAMP,
		'cantidad_sembrada' => Model::COLUMN_TYPE_INTEGER,
		'area' => Model::COLUMN_TYPE_INTEGER,
		'coordenadas' => Model::COLUMN_TYPE_VARCHAR,
		'color' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `fecha_cultivo` TIMESTAMP
	 * @var string
	 */
	protected $fecha_cultivo;

	/**
	 * `tipo_siembra` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $tipo_siembra;

	/**
	 * `zona` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $zona;

	/**
	 * `cultivador` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cultivador;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `cantidad_produccion` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cantidad_produccion;

	/**
	 * `fecha_produccion` TIMESTAMP
	 * @var string
	 */
	protected $fecha_produccion;

	/**
	 * `cantidad_sembrada` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $cantidad_sembrada;

	/**
	 * `area` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $area;

	/**
	 * `coordenadas` VARCHAR NOT NULL
	 * @var string
	 */
	protected $coordenadas;

	/**
	 * `color` VARCHAR NOT NULL
	 * @var string
	 */
	protected $color;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Cultivo
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha_cultivo field
	 */
	function getFechaCultivo($format = null) {
		if (null === $this->fecha_cultivo || null === $format) {
			return $this->fecha_cultivo;
		}
		if (0 === strpos($this->fecha_cultivo, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_cultivo));
	}

	/**
	 * Sets the value of the fecha_cultivo field
	 * @return Cultivo
	 */
	function setFechaCultivo($value) {
		return $this->setColumnValue('fecha_cultivo', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cultivo::getFechaCultivo
	 * final because getFechaCultivo should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getFechaCultivo
	 */
	final function getFecha_cultivo($format = null) {
		return $this->getFechaCultivo($format);
	}

	/**
	 * Convenience function for Cultivo::setFechaCultivo
	 * final because setFechaCultivo should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setFechaCultivo
	 * @return Cultivo
	 */
	final function setFecha_cultivo($value) {
		return $this->setFechaCultivo($value);
	}

	/**
	 * Gets the value of the tipo_siembra field
	 */
	function getTipoSiembra() {
		return $this->tipo_siembra;
	}

	/**
	 * Sets the value of the tipo_siembra field
	 * @return Cultivo
	 */
	function setTipoSiembra($value) {
		return $this->setColumnValue('tipo_siembra', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Cultivo::getTipoSiembra
	 * final because getTipoSiembra should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getTipoSiembra
	 */
	final function getTipo_siembra() {
		return $this->getTipoSiembra();
	}

	/**
	 * Convenience function for Cultivo::setTipoSiembra
	 * final because setTipoSiembra should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setTipoSiembra
	 * @return Cultivo
	 */
	final function setTipo_siembra($value) {
		return $this->setTipoSiembra($value);
	}

	/**
	 * Gets the value of the zona field
	 */
	function getZona() {
		return $this->zona;
	}

	/**
	 * Sets the value of the zona field
	 * @return Cultivo
	 */
	function setZona($value) {
		return $this->setColumnValue('zona', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cultivador field
	 */
	function getCultivador() {
		return $this->cultivador;
	}

	/**
	 * Sets the value of the cultivador field
	 * @return Cultivo
	 */
	function setCultivador($value) {
		return $this->setColumnValue('cultivador', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Cultivo
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cultivo::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Cultivo::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setCreatedAt
	 * @return Cultivo
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Cultivo
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cultivo::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Cultivo::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setUpdatedAt
	 * @return Cultivo
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Cultivo
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cultivo::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Cultivo::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setDeletedAt
	 * @return Cultivo
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the cantidad_produccion field
	 */
	function getCantidadProduccion() {
		return $this->cantidad_produccion;
	}

	/**
	 * Sets the value of the cantidad_produccion field
	 * @return Cultivo
	 */
	function setCantidadProduccion($value) {
		return $this->setColumnValue('cantidad_produccion', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Cultivo::getCantidadProduccion
	 * final because getCantidadProduccion should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getCantidadProduccion
	 */
	final function getCantidad_produccion() {
		return $this->getCantidadProduccion();
	}

	/**
	 * Convenience function for Cultivo::setCantidadProduccion
	 * final because setCantidadProduccion should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setCantidadProduccion
	 * @return Cultivo
	 */
	final function setCantidad_produccion($value) {
		return $this->setCantidadProduccion($value);
	}

	/**
	 * Gets the value of the fecha_produccion field
	 */
	function getFechaProduccion($format = null) {
		if (null === $this->fecha_produccion || null === $format) {
			return $this->fecha_produccion;
		}
		if (0 === strpos($this->fecha_produccion, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_produccion));
	}

	/**
	 * Sets the value of the fecha_produccion field
	 * @return Cultivo
	 */
	function setFechaProduccion($value) {
		return $this->setColumnValue('fecha_produccion', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Cultivo::getFechaProduccion
	 * final because getFechaProduccion should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getFechaProduccion
	 */
	final function getFecha_produccion($format = null) {
		return $this->getFechaProduccion($format);
	}

	/**
	 * Convenience function for Cultivo::setFechaProduccion
	 * final because setFechaProduccion should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setFechaProduccion
	 * @return Cultivo
	 */
	final function setFecha_produccion($value) {
		return $this->setFechaProduccion($value);
	}

	/**
	 * Gets the value of the cantidad_sembrada field
	 */
	function getCantidadSembrada() {
		return $this->cantidad_sembrada;
	}

	/**
	 * Sets the value of the cantidad_sembrada field
	 * @return Cultivo
	 */
	function setCantidadSembrada($value) {
		return $this->setColumnValue('cantidad_sembrada', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Cultivo::getCantidadSembrada
	 * final because getCantidadSembrada should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::getCantidadSembrada
	 */
	final function getCantidad_sembrada() {
		return $this->getCantidadSembrada();
	}

	/**
	 * Convenience function for Cultivo::setCantidadSembrada
	 * final because setCantidadSembrada should be extended instead
	 * to ensure consistent behavior
	 * @see Cultivo::setCantidadSembrada
	 * @return Cultivo
	 */
	final function setCantidad_sembrada($value) {
		return $this->setCantidadSembrada($value);
	}

	/**
	 * Gets the value of the area field
	 */
	function getArea() {
		return $this->area;
	}

	/**
	 * Sets the value of the area field
	 * @return Cultivo
	 */
	function setArea($value) {
		return $this->setColumnValue('area', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the coordenadas field
	 */
	function getCoordenadas() {
		return $this->coordenadas;
	}

	/**
	 * Sets the value of the coordenadas field
	 * @return Cultivo
	 */
	function setCoordenadas($value) {
		return $this->setColumnValue('coordenadas', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the color field
	 */
	function getColor() {
		return $this->color;
	}

	/**
	 * Sets the value of the color field
	 * @return Cultivo
	 */
	function setColor($value) {
		return $this->setColumnValue('color', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Cultivo
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Cultivo
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveById($value) {
		return Cultivo::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a fecha_cultivo
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByFechaCultivo($value) {
		return static::retrieveByColumn('fecha_cultivo', $value);
	}

	/**
	 * Searches the database for a row with a tipo_siembra
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByTipoSiembra($value) {
		return static::retrieveByColumn('tipo_siembra', $value);
	}

	/**
	 * Searches the database for a row with a zona
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByZona($value) {
		return static::retrieveByColumn('zona', $value);
	}

	/**
	 * Searches the database for a row with a cultivador
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByCultivador($value) {
		return static::retrieveByColumn('cultivador', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a cantidad_produccion
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByCantidadProduccion($value) {
		return static::retrieveByColumn('cantidad_produccion', $value);
	}

	/**
	 * Searches the database for a row with a fecha_produccion
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByFechaProduccion($value) {
		return static::retrieveByColumn('fecha_produccion', $value);
	}

	/**
	 * Searches the database for a row with a cantidad_sembrada
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByCantidadSembrada($value) {
		return static::retrieveByColumn('cantidad_sembrada', $value);
	}

	/**
	 * Searches the database for a row with a area
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByArea($value) {
		return static::retrieveByColumn('area', $value);
	}

	/**
	 * Searches the database for a row with a coordenadas
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByCoordenadas($value) {
		return static::retrieveByColumn('coordenadas', $value);
	}

	/**
	 * Searches the database for a row with a color
	 * value that matches the one provided
	 * @return Cultivo
	 */
	static function retrieveByColor($value) {
		return static::retrieveByColumn('color', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Cultivo
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->tipo_siembra = (null === $this->tipo_siembra) ? null : (int) $this->tipo_siembra;
		$this->zona = (null === $this->zona) ? null : (int) $this->zona;
		$this->cultivador = (null === $this->cultivador) ? null : (int) $this->cultivador;
		$this->cantidad_produccion = (null === $this->cantidad_produccion) ? null : (int) $this->cantidad_produccion;
		$this->cantidad_sembrada = (null === $this->cantidad_sembrada) ? null : (int) $this->cantidad_sembrada;
		$this->area = (null === $this->area) ? null : (int) $this->area;
		return $this;
	}

	/**
	 * @return Cultivo
	 */
	function setSiembraRelatedByTipoSiembra(Siembra $siembra = null) {
		if (null === $siembra) {
			$this->settipo_siembra(null);
		} else {
			if (!$siembra->getid()) {
				throw new Exception('Cannot connect a Siembra without a id');
			}
			$this->settipo_siembra($siembra->getid());
		}
		return $this;
	}

	/**
	 * Returns a siembra object with a id
	 * that matches $this->tipo_siembra.
	 * @return Siembra
	 */
	function getSiembraRelatedByTipoSiembra() {
		$fk_value = $this->gettipo_siembra();
		if (null === $fk_value) {
			return null;
		}
		return Siembra::retrieveByPK($fk_value);
	}

	/**
	 * Returns a siembra object with a id
	 * that matches $this->tipo_siembra.
	 * @return Siembra
	 */
	function getSiembra() {
		return $this->getSiembraRelatedByTipoSiembra();
	}

	/**
	 * @return Cultivo
	 */
	function setSiembra(Siembra $siembra = null) {
		return $this->setSiembraRelatedByTipoSiembra($siembra);
	}

	/**
	 * @return Cultivo[]
	 */
	static function doSelectJoinSiembraRelatedByTipoSiembra(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Siembra::getTableName();
		$q->join($to_table, $this_table . '.tipo_siembra = ' . $to_table . '.id', $join_type);
		foreach (Siembra::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Siembra'));
	}

	/**
	 * @return Cultivo
	 */
	function setZonasRelatedByZona(Zonas $zonas = null) {
		if (null === $zonas) {
			$this->setzona(null);
		} else {
			if (!$zonas->getid()) {
				throw new Exception('Cannot connect a Zonas without a id');
			}
			$this->setzona($zonas->getid());
		}
		return $this;
	}

	/**
	 * Returns a zonas object with a id
	 * that matches $this->zona.
	 * @return Zonas
	 */
	function getZonasRelatedByZona() {
		$fk_value = $this->getzona();
		if (null === $fk_value) {
			return null;
		}
		return Zonas::retrieveByPK($fk_value);
	}

	/**
	 * Returns a zonas object with a id
	 * that matches $this->zona.
	 * @return Zonas
	 */
	function getZonas() {
		return $this->getZonasRelatedByZona();
	}

	/**
	 * @return Cultivo
	 */
	function setZonas(Zonas $zonas = null) {
		return $this->setZonasRelatedByZona($zonas);
	}

	/**
	 * @return Cultivo[]
	 */
	static function doSelectJoinZonasRelatedByZona(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Zonas::getTableName();
		$q->join($to_table, $this_table . '.zona = ' . $to_table . '.id', $join_type);
		foreach (Zonas::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Zonas'));
	}

	/**
	 * @return Cultivo
	 */
	function setUsuariosRelatedByCultivador(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setcultivador(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setcultivador($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->cultivador.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByCultivador() {
		$fk_value = $this->getcultivador();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->cultivador.
	 * @return Usuarios
	 */
	function getUsuarios() {
		return $this->getUsuariosRelatedByCultivador();
	}

	/**
	 * @return Cultivo
	 */
	function setUsuarios(Usuarios $usuarios = null) {
		return $this->setUsuariosRelatedByCultivador($usuarios);
	}

	/**
	 * @return Cultivo[]
	 */
	static function doSelectJoinUsuariosRelatedByCultivador(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.cultivador = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return Cultivo[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Siembra::getTableName();
		$q->join($to_table, $this_table . '.tipo_siembra = ' . $to_table . '.id', $join_type);
		foreach (Siembra::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Siembra';
	
		$to_table = Zonas::getTableName();
		$q->join($to_table, $this_table . '.zona = ' . $to_table . '.id', $join_type);
		foreach (Zonas::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Zonas';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.cultivador = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getarea()) {
			$this->_validationErrors[] = 'area must not be null';
		}
		if (null === $this->getcoordenadas()) {
			$this->_validationErrors[] = 'coordenadas must not be null';
		}
		if (null === $this->getcolor()) {
			$this->_validationErrors[] = 'color must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}