<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseFolio extends ApplicationModel {

	const ID = 'folio.id';
	const SERIE = 'folio.serie';
	const HOJAS = 'folio.hojas';
	const CREATED_AT = 'folio.created_at';
	const UPDATED_AT = 'folio.updated_at';
	const DELETED_AT = 'folio.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'folio';

	/**
	 * Cache of objects retrieved from the database
	 * @var Folio[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'serie' => Model::COLUMN_TYPE_VARCHAR,
		'hojas' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `serie` VARCHAR
	 * @var string
	 */
	protected $serie;

	/**
	 * `hojas` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $hojas;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Folio
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the serie field
	 */
	function getSerie() {
		return $this->serie;
	}

	/**
	 * Sets the value of the serie field
	 * @return Folio
	 */
	function setSerie($value) {
		return $this->setColumnValue('serie', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the hojas field
	 */
	function getHojas() {
		return $this->hojas;
	}

	/**
	 * Sets the value of the hojas field
	 * @return Folio
	 */
	function setHojas($value) {
		return $this->setColumnValue('hojas', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Folio
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Folio::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Folio::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::setCreatedAt
	 * @return Folio
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Folio
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Folio::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Folio::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::setUpdatedAt
	 * @return Folio
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Folio
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Folio::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Folio::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Folio::setDeletedAt
	 * @return Folio
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Folio
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Folio
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveById($value) {
		return Folio::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a serie
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveBySerie($value) {
		return static::retrieveByColumn('serie', $value);
	}

	/**
	 * Searches the database for a row with a hojas
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveByHojas($value) {
		return static::retrieveByColumn('hojas', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Folio
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Folio
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->hojas = (null === $this->hojas) ? null : (int) $this->hojas;
		return $this;
	}

	/**
	 * @return Folio[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting contrato Objects(rows) from the contrato table
	 * with a folio_id that matches $this->id.
	 * @return Query
	 */
	function getContratosRelatedByFolioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('contrato', 'folio_id', 'id', $q);
	}

	/**
	 * Returns the count of Contrato Objects(rows) from the contrato table
	 * with a folio_id that matches $this->id.
	 * @return int
	 */
	function countContratosRelatedByFolioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Contrato::doCount($this->getContratosRelatedByFolioIdQuery($q));
	}

	/**
	 * Deletes the contrato Objects(rows) from the contrato table
	 * with a folio_id that matches $this->id.
	 * @return int
	 */
	function deleteContratosRelatedByFolioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ContratosRelatedByFolioId_c = array();
		return Contrato::doDelete($this->getContratosRelatedByFolioIdQuery($q));
	}

	protected $ContratosRelatedByFolioId_c = array();

	/**
	 * Returns an array of Contrato objects with a folio_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Contrato[]
	 */
	function getContratosRelatedByFolioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ContratosRelatedByFolioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ContratosRelatedByFolioId_c;
		}

		$result = Contrato::doSelect($this->getContratosRelatedByFolioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ContratosRelatedByFolioId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Folio::getContratosRelatedByfolio_id
	 * @return Contrato[]
	 * @see Folio::getContratosRelatedByFolioId
	 */
	function getContratos($extra = null) {
		return $this->getContratosRelatedByFolioId($extra);
	}

	/**
	  * Convenience function for Folio::getContratosRelatedByfolio_idQuery
	  * @return Query
	  * @see Folio::getContratosRelatedByfolio_idQuery
	  */
	function getContratosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('contrato', 'folio_id','id', $q);
	}

	/**
	  * Convenience function for Folio::deleteContratosRelatedByfolio_id
	  * @return int
	  * @see Folio::deleteContratosRelatedByfolio_id
	  */
	function deleteContratos(Query $q = null) {
		return $this->deleteContratosRelatedByFolioId($q);
	}

	/**
	  * Convenience function for Folio::countContratosRelatedByfolio_id
	  * @return int
	  * @see Folio::countContratosRelatedByFolioId
	  */
	function countContratos(Query $q = null) {
		return $this->countContratosRelatedByFolioId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}