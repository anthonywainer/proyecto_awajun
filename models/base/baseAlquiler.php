<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseAlquiler extends ApplicationModel {

	const ID = 'alquiler.id';
	const ARRENDATARIO = 'alquiler.arrendatario';
	const CONTRATO = 'alquiler.contrato';
	const MIEMBRO_ID = 'alquiler.miembro_id';
	const FECHA_ALQUILER = 'alquiler.fecha_alquiler';
	const CREATED_AT = 'alquiler.created_at';
	const UPDATED_AT = 'alquiler.updated_at';
	const DELETED_AT = 'alquiler.deleted_at';
	const REGISTRADOR = 'alquiler.registrador';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'alquiler';

	/**
	 * Cache of objects retrieved from the database
	 * @var Alquiler[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'arrendatario' => Model::COLUMN_TYPE_INTEGER,
		'contrato' => Model::COLUMN_TYPE_INTEGER,
		'miembro_id' => Model::COLUMN_TYPE_INTEGER,
		'fecha_alquiler' => Model::COLUMN_TYPE_TIMESTAMP,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'registrador' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `arrendatario` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $arrendatario;

	/**
	 * `contrato` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $contrato;

	/**
	 * `miembro_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $miembro_id;

	/**
	 * `fecha_alquiler` TIMESTAMP NOT NULL
	 * @var string
	 */
	protected $fecha_alquiler;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `registrador` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $registrador;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Alquiler
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the arrendatario field
	 */
	function getArrendatario() {
		return $this->arrendatario;
	}

	/**
	 * Sets the value of the arrendatario field
	 * @return Alquiler
	 */
	function setArrendatario($value) {
		return $this->setColumnValue('arrendatario', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the contrato field
	 */
	function getContrato() {
		return $this->contrato;
	}

	/**
	 * Sets the value of the contrato field
	 * @return Alquiler
	 */
	function setContrato($value) {
		return $this->setColumnValue('contrato', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the miembro_id field
	 */
	function getMiembroId() {
		return $this->miembro_id;
	}

	/**
	 * Sets the value of the miembro_id field
	 * @return Alquiler
	 */
	function setMiembroId($value) {
		return $this->setColumnValue('miembro_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Alquiler::getMiembroId
	 * final because getMiembroId should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::getMiembroId
	 */
	final function getMiembro_id() {
		return $this->getMiembroId();
	}

	/**
	 * Convenience function for Alquiler::setMiembroId
	 * final because setMiembroId should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::setMiembroId
	 * @return Alquiler
	 */
	final function setMiembro_id($value) {
		return $this->setMiembroId($value);
	}

	/**
	 * Gets the value of the fecha_alquiler field
	 */
	function getFechaAlquiler($format = null) {
		if (null === $this->fecha_alquiler || null === $format) {
			return $this->fecha_alquiler;
		}
		if (0 === strpos($this->fecha_alquiler, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_alquiler));
	}

	/**
	 * Sets the value of the fecha_alquiler field
	 * @return Alquiler
	 */
	function setFechaAlquiler($value) {
		return $this->setColumnValue('fecha_alquiler', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Alquiler::getFechaAlquiler
	 * final because getFechaAlquiler should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::getFechaAlquiler
	 */
	final function getFecha_alquiler($format = null) {
		return $this->getFechaAlquiler($format);
	}

	/**
	 * Convenience function for Alquiler::setFechaAlquiler
	 * final because setFechaAlquiler should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::setFechaAlquiler
	 * @return Alquiler
	 */
	final function setFecha_alquiler($value) {
		return $this->setFechaAlquiler($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Alquiler
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Alquiler::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Alquiler::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::setCreatedAt
	 * @return Alquiler
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Alquiler
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Alquiler::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Alquiler::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::setUpdatedAt
	 * @return Alquiler
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Alquiler
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Alquiler::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Alquiler::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Alquiler::setDeletedAt
	 * @return Alquiler
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the registrador field
	 */
	function getRegistrador() {
		return $this->registrador;
	}

	/**
	 * Sets the value of the registrador field
	 * @return Alquiler
	 */
	function setRegistrador($value) {
		return $this->setColumnValue('registrador', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Alquiler
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Alquiler
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveById($value) {
		return Alquiler::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a arrendatario
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByArrendatario($value) {
		return static::retrieveByColumn('arrendatario', $value);
	}

	/**
	 * Searches the database for a row with a contrato
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByContrato($value) {
		return static::retrieveByColumn('contrato', $value);
	}

	/**
	 * Searches the database for a row with a miembro_id
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByMiembroId($value) {
		return static::retrieveByColumn('miembro_id', $value);
	}

	/**
	 * Searches the database for a row with a fecha_alquiler
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByFechaAlquiler($value) {
		return static::retrieveByColumn('fecha_alquiler', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a registrador
	 * value that matches the one provided
	 * @return Alquiler
	 */
	static function retrieveByRegistrador($value) {
		return static::retrieveByColumn('registrador', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Alquiler
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->arrendatario = (null === $this->arrendatario) ? null : (int) $this->arrendatario;
		$this->contrato = (null === $this->contrato) ? null : (int) $this->contrato;
		$this->miembro_id = (null === $this->miembro_id) ? null : (int) $this->miembro_id;
		$this->registrador = (null === $this->registrador) ? null : (int) $this->registrador;
		return $this;
	}

	/**
	 * @return Alquiler
	 */
	function setContratoRelatedByContrato(Contrato $contrato = null) {
		if (null === $contrato) {
			$this->setcontrato(null);
		} else {
			if (!$contrato->getid()) {
				throw new Exception('Cannot connect a Contrato without a id');
			}
			$this->setcontrato($contrato->getid());
		}
		return $this;
	}

	/**
	 * Returns a contrato object with a id
	 * that matches $this->contrato.
	 * @return Contrato
	 */
	function getContratoRelatedByContrato() {
		$fk_value = $this->getcontrato();
		if (null === $fk_value) {
			return null;
		}
		return Contrato::retrieveByPK($fk_value);
	}

	/**
	 * @return Alquiler[]
	 */
	static function doSelectJoinContratoRelatedByContrato(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Contrato::getTableName();
		$q->join($to_table, $this_table . '.contrato = ' . $to_table . '.id', $join_type);
		foreach (Contrato::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Contrato'));
	}

	/**
	 * @return Alquiler
	 */
	function setUsuariosRelatedByArrendatario(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setarrendatario(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setarrendatario($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->arrendatario.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByArrendatario() {
		$fk_value = $this->getarrendatario();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	/**
	 * @return Alquiler[]
	 */
	static function doSelectJoinUsuariosRelatedByArrendatario(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.arrendatario = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return Alquiler
	 */
	function setUsuariosRelatedByRegistrador(Usuarios $usuarios = null) {
		if (null === $usuarios) {
			$this->setregistrador(null);
		} else {
			if (!$usuarios->getid()) {
				throw new Exception('Cannot connect a Usuarios without a id');
			}
			$this->setregistrador($usuarios->getid());
		}
		return $this;
	}

	/**
	 * Returns a usuarios object with a id
	 * that matches $this->registrador.
	 * @return Usuarios
	 */
	function getUsuariosRelatedByRegistrador() {
		$fk_value = $this->getregistrador();
		if (null === $fk_value) {
			return null;
		}
		return Usuarios::retrieveByPK($fk_value);
	}

	/**
	 * @return Alquiler[]
	 */
	static function doSelectJoinUsuariosRelatedByRegistrador(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.registrador = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Usuarios'));
	}

	/**
	 * @return Alquiler[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Contrato::getTableName();
		$q->join($to_table, $this_table . '.contrato = ' . $to_table . '.id', $join_type);
		foreach (Contrato::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Contrato';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.arrendatario = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$to_table = Usuarios::getTableName();
		$q->join($to_table, $this_table . '.registrador = ' . $to_table . '.id', $join_type);
		foreach (Usuarios::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Usuarios';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getarrendatario()) {
			$this->_validationErrors[] = 'arrendatario must not be null';
		}
		if (null === $this->getcontrato()) {
			$this->_validationErrors[] = 'contrato must not be null';
		}
		if (null === $this->getfecha_alquiler()) {
			$this->_validationErrors[] = 'fecha_alquiler must not be null';
		}
		if (null === $this->getregistrador()) {
			$this->_validationErrors[] = 'registrador must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}