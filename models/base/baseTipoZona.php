<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseTipoZona extends ApplicationModel {

	const ID = 'tipo_zona.id';
	const TIPO_ZONA = 'tipo_zona.tipo_zona';
	const CREATED_AT = 'tipo_zona.created_at';
	const UPDATED_AT = 'tipo_zona.updated_at';
	const DELETED_AT = 'tipo_zona.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'tipo_zona';

	/**
	 * Cache of objects retrieved from the database
	 * @var TipoZona[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'tipo_zona' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `tipo_zona` VARCHAR NOT NULL
	 * @var string
	 */
	protected $tipo_zona;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return TipoZona
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the tipo_zona field
	 */
	function getTipoZona() {
		return $this->tipo_zona;
	}

	/**
	 * Sets the value of the tipo_zona field
	 * @return TipoZona
	 */
	function setTipoZona($value) {
		return $this->setColumnValue('tipo_zona', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for TipoZona::getTipoZona
	 * final because getTipoZona should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::getTipoZona
	 */
	final function getTipo_zona() {
		return $this->getTipoZona();
	}

	/**
	 * Convenience function for TipoZona::setTipoZona
	 * final because setTipoZona should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::setTipoZona
	 * @return TipoZona
	 */
	final function setTipo_zona($value) {
		return $this->setTipoZona($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return TipoZona
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TipoZona::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for TipoZona::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::setCreatedAt
	 * @return TipoZona
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return TipoZona
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TipoZona::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for TipoZona::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::setUpdatedAt
	 * @return TipoZona
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return TipoZona
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for TipoZona::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for TipoZona::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see TipoZona::setDeletedAt
	 * @return TipoZona
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return TipoZona
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return TipoZona
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return TipoZona
	 */
	static function retrieveById($value) {
		return TipoZona::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a tipo_zona
	 * value that matches the one provided
	 * @return TipoZona
	 */
	static function retrieveByTipoZona($value) {
		return static::retrieveByColumn('tipo_zona', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return TipoZona
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return TipoZona
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return TipoZona
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return TipoZona
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return TipoZona[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting zonas Objects(rows) from the zonas table
	 * with a tipo_zona that matches $this->id.
	 * @return Query
	 */
	function getZonassRelatedByTipoZonaQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'tipo_zona', 'id', $q);
	}

	/**
	 * Returns the count of Zonas Objects(rows) from the zonas table
	 * with a tipo_zona that matches $this->id.
	 * @return int
	 */
	function countZonassRelatedByTipoZona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Zonas::doCount($this->getZonassRelatedByTipoZonaQuery($q));
	}

	/**
	 * Deletes the zonas Objects(rows) from the zonas table
	 * with a tipo_zona that matches $this->id.
	 * @return int
	 */
	function deleteZonassRelatedByTipoZona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ZonassRelatedByTipoZona_c = array();
		return Zonas::doDelete($this->getZonassRelatedByTipoZonaQuery($q));
	}

	protected $ZonassRelatedByTipoZona_c = array();

	/**
	 * Returns an array of Zonas objects with a tipo_zona
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Zonas[]
	 */
	function getZonassRelatedByTipoZona(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ZonassRelatedByTipoZona_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ZonassRelatedByTipoZona_c;
		}

		$result = Zonas::doSelect($this->getZonassRelatedByTipoZonaQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ZonassRelatedByTipoZona_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for TipoZona::getZonassRelatedBytipo_zona
	 * @return Zonas[]
	 * @see TipoZona::getZonassRelatedByTipoZona
	 */
	function getZonass($extra = null) {
		return $this->getZonassRelatedByTipoZona($extra);
	}

	/**
	  * Convenience function for TipoZona::getZonassRelatedBytipo_zonaQuery
	  * @return Query
	  * @see TipoZona::getZonassRelatedBytipo_zonaQuery
	  */
	function getZonassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'tipo_zona','id', $q);
	}

	/**
	  * Convenience function for TipoZona::deleteZonassRelatedBytipo_zona
	  * @return int
	  * @see TipoZona::deleteZonassRelatedBytipo_zona
	  */
	function deleteZonass(Query $q = null) {
		return $this->deleteZonassRelatedByTipoZona($q);
	}

	/**
	  * Convenience function for TipoZona::countZonassRelatedBytipo_zona
	  * @return int
	  * @see TipoZona::countZonassRelatedByTipoZona
	  */
	function countZonass(Query $q = null) {
		return $this->countZonassRelatedByTipoZona($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->gettipo_zona()) {
			$this->_validationErrors[] = 'tipo_zona must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}