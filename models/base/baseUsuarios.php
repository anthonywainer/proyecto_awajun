<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUsuarios extends ApplicationModel {

	const ID = 'usuarios.id';
	const NOMBRES = 'usuarios.nombres';
	const APELLIDOS = 'usuarios.apellidos';
	const CLAVE = 'usuarios.clave';
	const ESTA_ACTIVO = 'usuarios.esta_activo';
	const CORREO = 'usuarios.correo';
	const TELEFONO = 'usuarios.telefono';
	const DIRECCION = 'usuarios.direccion';
	const CREATED_AT = 'usuarios.created_at';
	const UPDATED_AT = 'usuarios.updated_at';
	const DELETED_AT = 'usuarios.deleted_at';
	const DNI = 'usuarios.dni';
	const UBIGEO = 'usuarios.ubigeo';
	const FECHA_NACIMIENTO = 'usuarios.fecha_nacimiento';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'usuarios';

	/**
	 * Cache of objects retrieved from the database
	 * @var Usuarios[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombres' => Model::COLUMN_TYPE_VARCHAR,
		'apellidos' => Model::COLUMN_TYPE_VARCHAR,
		'clave' => Model::COLUMN_TYPE_VARCHAR,
		'esta_activo' => Model::COLUMN_TYPE_BOOLEAN,
		'correo' => Model::COLUMN_TYPE_VARCHAR,
		'telefono' => Model::COLUMN_TYPE_INTEGER,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'dni' => Model::COLUMN_TYPE_VARCHAR,
		'ubigeo' => Model::COLUMN_TYPE_INTEGER,
		'fecha_nacimiento' => Model::COLUMN_TYPE_DATE,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombres` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombres;

	/**
	 * `apellidos` VARCHAR NOT NULL
	 * @var string
	 */
	protected $apellidos;

	/**
	 * `clave` VARCHAR NOT NULL
	 * @var string
	 */
	protected $clave;

	/**
	 * `esta_activo` BOOLEAN NOT NULL DEFAULT 'b\'1\''
	 * @var boolean
	 */
	protected $esta_activo = 'b\'1\'';

	/**
	 * `correo` VARCHAR
	 * @var string
	 */
	protected $correo;

	/**
	 * `telefono` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $telefono;

	/**
	 * `direccion` VARCHAR
	 * @var string
	 */
	protected $direccion;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `dni` VARCHAR NOT NULL
	 * @var string
	 */
	protected $dni;

	/**
	 * `ubigeo` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $ubigeo;

	/**
	 * `fecha_nacimiento` DATE NOT NULL
	 * @var string
	 */
	protected $fecha_nacimiento;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Usuarios
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombres field
	 */
	function getNombres() {
		return $this->nombres;
	}

	/**
	 * Sets the value of the nombres field
	 * @return Usuarios
	 */
	function setNombres($value) {
		return $this->setColumnValue('nombres', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the apellidos field
	 */
	function getApellidos() {
		return $this->apellidos;
	}

	/**
	 * Sets the value of the apellidos field
	 * @return Usuarios
	 */
	function setApellidos($value) {
		return $this->setColumnValue('apellidos', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the clave field
	 */
	function getClave() {
		return $this->clave;
	}

	/**
	 * Sets the value of the clave field
	 * @return Usuarios
	 */
	function setClave($value) {
		return $this->setColumnValue('clave', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the esta_activo field
	 */
	function getEstaActivo() {
		return $this->esta_activo;
	}

	/**
	 * Sets the value of the esta_activo field
	 * @return Usuarios
	 */
	function setEstaActivo($value) {
		return $this->setColumnValue('esta_activo', $value, Model::COLUMN_TYPE_BOOLEAN);
	}

	/**
	 * Convenience function for Usuarios::getEstaActivo
	 * final because getEstaActivo should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getEstaActivo
	 */
	final function getEsta_activo() {
		return $this->getEstaActivo();
	}

	/**
	 * Convenience function for Usuarios::setEstaActivo
	 * final because setEstaActivo should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setEstaActivo
	 * @return Usuarios
	 */
	final function setEsta_activo($value) {
		return $this->setEstaActivo($value);
	}

	/**
	 * Gets the value of the correo field
	 */
	function getCorreo() {
		return $this->correo;
	}

	/**
	 * Sets the value of the correo field
	 * @return Usuarios
	 */
	function setCorreo($value) {
		return $this->setColumnValue('correo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the telefono field
	 */
	function getTelefono() {
		return $this->telefono;
	}

	/**
	 * Sets the value of the telefono field
	 * @return Usuarios
	 */
	function setTelefono($value) {
		return $this->setColumnValue('telefono', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Usuarios
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Usuarios
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setCreatedAt
	 * @return Usuarios
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Usuarios
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setUpdatedAt
	 * @return Usuarios
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Usuarios
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Usuarios::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Usuarios::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setDeletedAt
	 * @return Usuarios
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the dni field
	 */
	function getDni() {
		return $this->dni;
	}

	/**
	 * Sets the value of the dni field
	 * @return Usuarios
	 */
	function setDni($value) {
		return $this->setColumnValue('dni', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the ubigeo field
	 */
	function getUbigeo() {
		return $this->ubigeo;
	}

	/**
	 * Sets the value of the ubigeo field
	 * @return Usuarios
	 */
	function setUbigeo($value) {
		return $this->setColumnValue('ubigeo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha_nacimiento field
	 */
	function getFechaNacimiento($format = null) {
		if (null === $this->fecha_nacimiento || null === $format) {
			return $this->fecha_nacimiento;
		}
		if (0 === strpos($this->fecha_nacimiento, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_nacimiento));
	}

	/**
	 * Sets the value of the fecha_nacimiento field
	 * @return Usuarios
	 */
	function setFechaNacimiento($value) {
		return $this->setColumnValue('fecha_nacimiento', $value, Model::COLUMN_TYPE_DATE);
	}

	/**
	 * Convenience function for Usuarios::getFechaNacimiento
	 * final because getFechaNacimiento should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::getFechaNacimiento
	 */
	final function getFecha_nacimiento($format = null) {
		return $this->getFechaNacimiento($format);
	}

	/**
	 * Convenience function for Usuarios::setFechaNacimiento
	 * final because setFechaNacimiento should be extended instead
	 * to ensure consistent behavior
	 * @see Usuarios::setFechaNacimiento
	 * @return Usuarios
	 */
	final function setFecha_nacimiento($value) {
		return $this->setFechaNacimiento($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Usuarios
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Usuarios
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveById($value) {
		return Usuarios::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombres
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByNombres($value) {
		return static::retrieveByColumn('nombres', $value);
	}

	/**
	 * Searches the database for a row with a apellidos
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByApellidos($value) {
		return static::retrieveByColumn('apellidos', $value);
	}

	/**
	 * Searches the database for a row with a clave
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByClave($value) {
		return static::retrieveByColumn('clave', $value);
	}

	/**
	 * Searches the database for a row with a esta_activo
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByEstaActivo($value) {
		return static::retrieveByColumn('esta_activo', $value);
	}

	/**
	 * Searches the database for a row with a correo
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByCorreo($value) {
		return static::retrieveByColumn('correo', $value);
	}

	/**
	 * Searches the database for a row with a telefono
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByTelefono($value) {
		return static::retrieveByColumn('telefono', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a dni
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByDni($value) {
		return static::retrieveByColumn('dni', $value);
	}

	/**
	 * Searches the database for a row with a ubigeo
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByUbigeo($value) {
		return static::retrieveByColumn('ubigeo', $value);
	}

	/**
	 * Searches the database for a row with a fecha_nacimiento
	 * value that matches the one provided
	 * @return Usuarios
	 */
	static function retrieveByFechaNacimiento($value) {
		return static::retrieveByColumn('fecha_nacimiento', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Usuarios
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->esta_activo = (null === $this->esta_activo) ? null : (int) $this->esta_activo;
		$this->telefono = (null === $this->telefono) ? null : (int) $this->telefono;
		$this->ubigeo = (null === $this->ubigeo) ? null : (int) $this->ubigeo;
		return $this;
	}

	/**
	 * @return Usuarios
	 */
	function setUbigeoRelatedByUbigeo(Ubigeo $ubigeo = null) {
		if (null === $ubigeo) {
			$this->setubigeo(null);
		} else {
			if (!$ubigeo->getid()) {
				throw new Exception('Cannot connect a Ubigeo without a id');
			}
			$this->setubigeo($ubigeo->getid());
		}
		return $this;
	}

	/**
	 * Returns a ubigeo object with a id
	 * that matches $this->ubigeo.
	 * @return Ubigeo
	 */
	function getUbigeoRelatedByUbigeo() {
		$fk_value = $this->getubigeo();
		if (null === $fk_value) {
			return null;
		}
		return Ubigeo::retrieveByPK($fk_value);
	}

	/**
	 * @return Usuarios[]
	 */
	static function doSelectJoinUbigeoRelatedByUbigeo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ubigeo::getTableName();
		$q->join($to_table, $this_table . '.ubigeo = ' . $to_table . '.id', $join_type);
		foreach (Ubigeo::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Ubigeo'));
	}

	/**
	 * @return Usuarios[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Ubigeo::getTableName();
		$q->join($to_table, $this_table . '.ubigeo = ' . $to_table . '.id', $join_type);
		foreach (Ubigeo::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Ubigeo';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting alquiler Objects(rows) from the alquiler table
	 * with a arrendatario that matches $this->id.
	 * @return Query
	 */
	function getAlquilersRelatedByArrendatarioQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'arrendatario', 'id', $q);
	}

	/**
	 * Returns the count of Alquiler Objects(rows) from the alquiler table
	 * with a arrendatario that matches $this->id.
	 * @return int
	 */
	function countAlquilersRelatedByArrendatario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Alquiler::doCount($this->getAlquilersRelatedByArrendatarioQuery($q));
	}

	/**
	 * Deletes the alquiler Objects(rows) from the alquiler table
	 * with a arrendatario that matches $this->id.
	 * @return int
	 */
	function deleteAlquilersRelatedByArrendatario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AlquilersRelatedByArrendatario_c = array();
		return Alquiler::doDelete($this->getAlquilersRelatedByArrendatarioQuery($q));
	}

	protected $AlquilersRelatedByArrendatario_c = array();

	/**
	 * Returns an array of Alquiler objects with a arrendatario
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Alquiler[]
	 */
	function getAlquilersRelatedByArrendatario(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AlquilersRelatedByArrendatario_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AlquilersRelatedByArrendatario_c;
		}

		$result = Alquiler::doSelect($this->getAlquilersRelatedByArrendatarioQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AlquilersRelatedByArrendatario_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting alquiler Objects(rows) from the alquiler table
	 * with a registrador that matches $this->id.
	 * @return Query
	 */
	function getAlquilersRelatedByRegistradorQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'registrador', 'id', $q);
	}

	/**
	 * Returns the count of Alquiler Objects(rows) from the alquiler table
	 * with a registrador that matches $this->id.
	 * @return int
	 */
	function countAlquilersRelatedByRegistrador(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Alquiler::doCount($this->getAlquilersRelatedByRegistradorQuery($q));
	}

	/**
	 * Deletes the alquiler Objects(rows) from the alquiler table
	 * with a registrador that matches $this->id.
	 * @return int
	 */
	function deleteAlquilersRelatedByRegistrador(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AlquilersRelatedByRegistrador_c = array();
		return Alquiler::doDelete($this->getAlquilersRelatedByRegistradorQuery($q));
	}

	protected $AlquilersRelatedByRegistrador_c = array();

	/**
	 * Returns an array of Alquiler objects with a registrador
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Alquiler[]
	 */
	function getAlquilersRelatedByRegistrador(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AlquilersRelatedByRegistrador_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AlquilersRelatedByRegistrador_c;
		}

		$result = Alquiler::doSelect($this->getAlquilersRelatedByRegistradorQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AlquilersRelatedByRegistrador_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting cultivo Objects(rows) from the cultivo table
	 * with a cultivador that matches $this->id.
	 * @return Query
	 */
	function getCultivosRelatedByCultivadorQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'cultivador', 'id', $q);
	}

	/**
	 * Returns the count of Cultivo Objects(rows) from the cultivo table
	 * with a cultivador that matches $this->id.
	 * @return int
	 */
	function countCultivosRelatedByCultivador(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Cultivo::doCount($this->getCultivosRelatedByCultivadorQuery($q));
	}

	/**
	 * Deletes the cultivo Objects(rows) from the cultivo table
	 * with a cultivador that matches $this->id.
	 * @return int
	 */
	function deleteCultivosRelatedByCultivador(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->CultivosRelatedByCultivador_c = array();
		return Cultivo::doDelete($this->getCultivosRelatedByCultivadorQuery($q));
	}

	protected $CultivosRelatedByCultivador_c = array();

	/**
	 * Returns an array of Cultivo objects with a cultivador
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Cultivo[]
	 */
	function getCultivosRelatedByCultivador(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->CultivosRelatedByCultivador_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->CultivosRelatedByCultivador_c;
		}

		$result = Cultivo::doSelect($this->getCultivosRelatedByCultivadorQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->CultivosRelatedByCultivador_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting miembros_familia Objects(rows) from the miembros_familia table
	 * with a persona that matches $this->id.
	 * @return Query
	 */
	function getMiembrosFamiliasRelatedByPersonaQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'persona', 'id', $q);
	}

	/**
	 * Returns the count of MiembrosFamilia Objects(rows) from the miembros_familia table
	 * with a persona that matches $this->id.
	 * @return int
	 */
	function countMiembrosFamiliasRelatedByPersona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MiembrosFamilia::doCount($this->getMiembrosFamiliasRelatedByPersonaQuery($q));
	}

	/**
	 * Deletes the miembros_familia Objects(rows) from the miembros_familia table
	 * with a persona that matches $this->id.
	 * @return int
	 */
	function deleteMiembrosFamiliasRelatedByPersona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MiembrosFamiliasRelatedByPersona_c = array();
		return MiembrosFamilia::doDelete($this->getMiembrosFamiliasRelatedByPersonaQuery($q));
	}

	protected $MiembrosFamiliasRelatedByPersona_c = array();

	/**
	 * Returns an array of MiembrosFamilia objects with a persona
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MiembrosFamilia[]
	 */
	function getMiembrosFamiliasRelatedByPersona(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MiembrosFamiliasRelatedByPersona_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MiembrosFamiliasRelatedByPersona_c;
		}

		$result = MiembrosFamilia::doSelect($this->getMiembrosFamiliasRelatedByPersonaQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MiembrosFamiliasRelatedByPersona_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return Query
	 */
	function getPermisosUsuariosRelatedByIdusuarioQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idusuario', 'id', $q);
	}

	/**
	 * Returns the count of PermisosUsuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return int
	 */
	function countPermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PermisosUsuario::doCount($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));
	}

	/**
	 * Deletes the permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idusuario that matches $this->id.
	 * @return int
	 */
	function deletePermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisosUsuariosRelatedByIdusuario_c = array();
		return PermisosUsuario::doDelete($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));
	}

	protected $PermisosUsuariosRelatedByIdusuario_c = array();

	/**
	 * Returns an array of PermisosUsuario objects with a idusuario
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PermisosUsuario[]
	 */
	function getPermisosUsuariosRelatedByIdusuario(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisosUsuariosRelatedByIdusuario_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisosUsuariosRelatedByIdusuario_c;
		}

		$result = PermisosUsuario::doSelect($this->getPermisosUsuariosRelatedByIdusuarioQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisosUsuariosRelatedByIdusuario_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return Query
	 */
	function getUsuariosGruposRelatedByUsuarioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'usuario_id', 'id', $q);
	}

	/**
	 * Returns the count of UsuariosGrupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return int
	 */
	function countUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return UsuariosGrupo::doCount($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));
	}

	/**
	 * Deletes the usuarios_grupo Objects(rows) from the usuarios_grupo table
	 * with a usuario_id that matches $this->id.
	 * @return int
	 */
	function deleteUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->UsuariosGruposRelatedByUsuarioId_c = array();
		return UsuariosGrupo::doDelete($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));
	}

	protected $UsuariosGruposRelatedByUsuarioId_c = array();

	/**
	 * Returns an array of UsuariosGrupo objects with a usuario_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return UsuariosGrupo[]
	 */
	function getUsuariosGruposRelatedByUsuarioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->UsuariosGruposRelatedByUsuarioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->UsuariosGruposRelatedByUsuarioId_c;
		}

		$result = UsuariosGrupo::doSelect($this->getUsuariosGruposRelatedByUsuarioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->UsuariosGruposRelatedByUsuarioId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Usuarios::getCultivosRelatedBycultivador
	 * @return Cultivo[]
	 * @see Usuarios::getCultivosRelatedByCultivador
	 */
	function getCultivos($extra = null) {
		return $this->getCultivosRelatedByCultivador($extra);
	}

	/**
	  * Convenience function for Usuarios::getCultivosRelatedBycultivadorQuery
	  * @return Query
	  * @see Usuarios::getCultivosRelatedBycultivadorQuery
	  */
	function getCultivosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'cultivador','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deleteCultivosRelatedBycultivador
	  * @return int
	  * @see Usuarios::deleteCultivosRelatedBycultivador
	  */
	function deleteCultivos(Query $q = null) {
		return $this->deleteCultivosRelatedByCultivador($q);
	}

	/**
	  * Convenience function for Usuarios::countCultivosRelatedBycultivador
	  * @return int
	  * @see Usuarios::countCultivosRelatedByCultivador
	  */
	function countCultivos(Query $q = null) {
		return $this->countCultivosRelatedByCultivador($q);
	}

	/**
	 * Convenience function for Usuarios::getMiembrosFamiliasRelatedBypersona
	 * @return MiembrosFamilia[]
	 * @see Usuarios::getMiembrosFamiliasRelatedByPersona
	 */
	function getMiembrosFamilias($extra = null) {
		return $this->getMiembrosFamiliasRelatedByPersona($extra);
	}

	/**
	  * Convenience function for Usuarios::getMiembrosFamiliasRelatedBypersonaQuery
	  * @return Query
	  * @see Usuarios::getMiembrosFamiliasRelatedBypersonaQuery
	  */
	function getMiembrosFamiliasQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'persona','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deleteMiembrosFamiliasRelatedBypersona
	  * @return int
	  * @see Usuarios::deleteMiembrosFamiliasRelatedBypersona
	  */
	function deleteMiembrosFamilias(Query $q = null) {
		return $this->deleteMiembrosFamiliasRelatedByPersona($q);
	}

	/**
	  * Convenience function for Usuarios::countMiembrosFamiliasRelatedBypersona
	  * @return int
	  * @see Usuarios::countMiembrosFamiliasRelatedByPersona
	  */
	function countMiembrosFamilias(Query $q = null) {
		return $this->countMiembrosFamiliasRelatedByPersona($q);
	}

	/**
	 * Convenience function for Usuarios::getPermisosUsuariosRelatedByidusuario
	 * @return PermisosUsuario[]
	 * @see Usuarios::getPermisosUsuariosRelatedByIdusuario
	 */
	function getPermisosUsuarios($extra = null) {
		return $this->getPermisosUsuariosRelatedByIdusuario($extra);
	}

	/**
	  * Convenience function for Usuarios::getPermisosUsuariosRelatedByidusuarioQuery
	  * @return Query
	  * @see Usuarios::getPermisosUsuariosRelatedByidusuarioQuery
	  */
	function getPermisosUsuariosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idusuario','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deletePermisosUsuariosRelatedByidusuario
	  * @return int
	  * @see Usuarios::deletePermisosUsuariosRelatedByidusuario
	  */
	function deletePermisosUsuarios(Query $q = null) {
		return $this->deletePermisosUsuariosRelatedByIdusuario($q);
	}

	/**
	  * Convenience function for Usuarios::countPermisosUsuariosRelatedByidusuario
	  * @return int
	  * @see Usuarios::countPermisosUsuariosRelatedByIdusuario
	  */
	function countPermisosUsuarios(Query $q = null) {
		return $this->countPermisosUsuariosRelatedByIdusuario($q);
	}

	/**
	 * Convenience function for Usuarios::getUsuariosGruposRelatedByusuario_id
	 * @return UsuariosGrupo[]
	 * @see Usuarios::getUsuariosGruposRelatedByUsuarioId
	 */
	function getUsuariosGrupos($extra = null) {
		return $this->getUsuariosGruposRelatedByUsuarioId($extra);
	}

	/**
	  * Convenience function for Usuarios::getUsuariosGruposRelatedByusuario_idQuery
	  * @return Query
	  * @see Usuarios::getUsuariosGruposRelatedByusuario_idQuery
	  */
	function getUsuariosGruposQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios_grupo', 'usuario_id','id', $q);
	}

	/**
	  * Convenience function for Usuarios::deleteUsuariosGruposRelatedByusuario_id
	  * @return int
	  * @see Usuarios::deleteUsuariosGruposRelatedByusuario_id
	  */
	function deleteUsuariosGrupos(Query $q = null) {
		return $this->deleteUsuariosGruposRelatedByUsuarioId($q);
	}

	/**
	  * Convenience function for Usuarios::countUsuariosGruposRelatedByusuario_id
	  * @return int
	  * @see Usuarios::countUsuariosGruposRelatedByUsuarioId
	  */
	function countUsuariosGrupos(Query $q = null) {
		return $this->countUsuariosGruposRelatedByUsuarioId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombres()) {
			$this->_validationErrors[] = 'nombres must not be null';
		}
		if (null === $this->getapellidos()) {
			$this->_validationErrors[] = 'apellidos must not be null';
		}
		if (null === $this->getclave()) {
			$this->_validationErrors[] = 'clave must not be null';
		}
		if (null === $this->getdni()) {
			$this->_validationErrors[] = 'dni must not be null';
		}
		if (null === $this->getubigeo()) {
			$this->_validationErrors[] = 'ubigeo must not be null';
		}
		if (null === $this->getfecha_nacimiento()) {
			$this->_validationErrors[] = 'fecha_nacimiento must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}