<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePermisos extends ApplicationModel {

	const ID = 'permisos.id';
	const IDACCION = 'permisos.idaccion';
	const IDSUBMODULO = 'permisos.idsubmodulo';
	const NOMBRE = 'permisos.nombre';
	const IDMODULO = 'permisos.idmodulo';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'permisos';

	/**
	 * Cache of objects retrieved from the database
	 * @var Permisos[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'idaccion' => Model::COLUMN_TYPE_INTEGER,
		'idsubmodulo' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'idmodulo' => Model::COLUMN_TYPE_INTEGER,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `idaccion` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $idaccion;

	/**
	 * `idsubmodulo` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $idsubmodulo;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `idmodulo` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $idmodulo;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Permisos
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idaccion field
	 */
	function getIdaccion() {
		return $this->idaccion;
	}

	/**
	 * Sets the value of the idaccion field
	 * @return Permisos
	 */
	function setIdaccion($value) {
		return $this->setColumnValue('idaccion', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the idsubmodulo field
	 */
	function getIdsubmodulo() {
		return $this->idsubmodulo;
	}

	/**
	 * Sets the value of the idsubmodulo field
	 * @return Permisos
	 */
	function setIdsubmodulo($value) {
		return $this->setColumnValue('idsubmodulo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return Permisos
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the idmodulo field
	 */
	function getIdmodulo() {
		return $this->idmodulo;
	}

	/**
	 * Sets the value of the idmodulo field
	 * @return Permisos
	 */
	function setIdmodulo($value) {
		return $this->setColumnValue('idmodulo', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Permisos
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Permisos
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveById($value) {
		return Permisos::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a idaccion
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByIdaccion($value) {
		return static::retrieveByColumn('idaccion', $value);
	}

	/**
	 * Searches the database for a row with a idsubmodulo
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByIdsubmodulo($value) {
		return static::retrieveByColumn('idsubmodulo', $value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a idmodulo
	 * value that matches the one provided
	 * @return Permisos
	 */
	static function retrieveByIdmodulo($value) {
		return static::retrieveByColumn('idmodulo', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Permisos
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->idaccion = (null === $this->idaccion) ? null : (int) $this->idaccion;
		$this->idsubmodulo = (null === $this->idsubmodulo) ? null : (int) $this->idsubmodulo;
		$this->idmodulo = (null === $this->idmodulo) ? null : (int) $this->idmodulo;
		return $this;
	}

	/**
	 * @return Permisos
	 */
	function setAccionesRelatedByIdaccion(Acciones $acciones = null) {
		if (null === $acciones) {
			$this->setidaccion(null);
		} else {
			if (!$acciones->getid()) {
				throw new Exception('Cannot connect a Acciones without a id');
			}
			$this->setidaccion($acciones->getid());
		}
		return $this;
	}

	/**
	 * Returns a acciones object with a id
	 * that matches $this->idaccion.
	 * @return Acciones
	 */
	function getAccionesRelatedByIdaccion() {
		$fk_value = $this->getidaccion();
		if (null === $fk_value) {
			return null;
		}
		return Acciones::retrieveByPK($fk_value);
	}

	/**
	 * Returns a acciones object with a id
	 * that matches $this->idaccion.
	 * @return Acciones
	 */
	function getAcciones() {
		return $this->getAccionesRelatedByIdaccion();
	}

	/**
	 * @return Permisos
	 */
	function setAcciones(Acciones $acciones = null) {
		return $this->setAccionesRelatedByIdaccion($acciones);
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinAccionesRelatedByIdaccion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Acciones::getTableName();
		$q->join($to_table, $this_table . '.idaccion = ' . $to_table . '.id', $join_type);
		foreach (Acciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Acciones'));
	}

	/**
	 * @return Permisos
	 */
	function setModulosRelatedByIdmodulo(Modulos $modulos = null) {
		if (null === $modulos) {
			$this->setidmodulo(null);
		} else {
			if (!$modulos->getid()) {
				throw new Exception('Cannot connect a Modulos without a id');
			}
			$this->setidmodulo($modulos->getid());
		}
		return $this;
	}

	/**
	 * Returns a modulos object with a id
	 * that matches $this->idmodulo.
	 * @return Modulos
	 */
	function getModulosRelatedByIdmodulo() {
		$fk_value = $this->getidmodulo();
		if (null === $fk_value) {
			return null;
		}
		return Modulos::retrieveByPK($fk_value);
	}

	/**
	 * Returns a modulos object with a id
	 * that matches $this->idmodulo.
	 * @return Modulos
	 */
	function getModulos() {
		return $this->getModulosRelatedByIdmodulo();
	}

	/**
	 * @return Permisos
	 */
	function setModulos(Modulos $modulos = null) {
		return $this->setModulosRelatedByIdmodulo($modulos);
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinModulosRelatedByIdmodulo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Modulos::getTableName();
		$q->join($to_table, $this_table . '.idmodulo = ' . $to_table . '.id', $join_type);
		foreach (Modulos::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Modulos'));
	}

	/**
	 * @return Permisos
	 */
	function setSubmoduloRelatedByIdsubmodulo(Submodulo $submodulo = null) {
		if (null === $submodulo) {
			$this->setidsubmodulo(null);
		} else {
			if (!$submodulo->getid()) {
				throw new Exception('Cannot connect a Submodulo without a id');
			}
			$this->setidsubmodulo($submodulo->getid());
		}
		return $this;
	}

	/**
	 * Returns a submodulo object with a id
	 * that matches $this->idsubmodulo.
	 * @return Submodulo
	 */
	function getSubmoduloRelatedByIdsubmodulo() {
		$fk_value = $this->getidsubmodulo();
		if (null === $fk_value) {
			return null;
		}
		return Submodulo::retrieveByPK($fk_value);
	}

	/**
	 * Returns a submodulo object with a id
	 * that matches $this->idsubmodulo.
	 * @return Submodulo
	 */
	function getSubmodulo() {
		return $this->getSubmoduloRelatedByIdsubmodulo();
	}

	/**
	 * @return Permisos
	 */
	function setSubmodulo(Submodulo $submodulo = null) {
		return $this->setSubmoduloRelatedByIdsubmodulo($submodulo);
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinSubmoduloRelatedByIdsubmodulo(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Submodulo::getTableName();
		$q->join($to_table, $this_table . '.idsubmodulo = ' . $to_table . '.id', $join_type);
		foreach (Submodulo::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Submodulo'));
	}

	/**
	 * @return Permisos[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Acciones::getTableName();
		$q->join($to_table, $this_table . '.idaccion = ' . $to_table . '.id', $join_type);
		foreach (Acciones::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Acciones';
	
		$to_table = Modulos::getTableName();
		$q->join($to_table, $this_table . '.idmodulo = ' . $to_table . '.id', $join_type);
		foreach (Modulos::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Modulos';
	
		$to_table = Submodulo::getTableName();
		$q->join($to_table, $this_table . '.idsubmodulo = ' . $to_table . '.id', $join_type);
		foreach (Submodulo::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Submodulo';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting permisos_grupo Objects(rows) from the permisos_grupo table
	 * with a idpermiso that matches $this->id.
	 * @return Query
	 */
	function getPermisosGruposRelatedByIdpermisoQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_grupo', 'idpermiso', 'id', $q);
	}

	/**
	 * Returns the count of PermisosGrupo Objects(rows) from the permisos_grupo table
	 * with a idpermiso that matches $this->id.
	 * @return int
	 */
	function countPermisosGruposRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PermisosGrupo::doCount($this->getPermisosGruposRelatedByIdpermisoQuery($q));
	}

	/**
	 * Deletes the permisos_grupo Objects(rows) from the permisos_grupo table
	 * with a idpermiso that matches $this->id.
	 * @return int
	 */
	function deletePermisosGruposRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisosGruposRelatedByIdpermiso_c = array();
		return PermisosGrupo::doDelete($this->getPermisosGruposRelatedByIdpermisoQuery($q));
	}

	protected $PermisosGruposRelatedByIdpermiso_c = array();

	/**
	 * Returns an array of PermisosGrupo objects with a idpermiso
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PermisosGrupo[]
	 */
	function getPermisosGruposRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisosGruposRelatedByIdpermiso_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisosGruposRelatedByIdpermiso_c;
		}

		$result = PermisosGrupo::doSelect($this->getPermisosGruposRelatedByIdpermisoQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisosGruposRelatedByIdpermiso_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idpermiso that matches $this->id.
	 * @return Query
	 */
	function getPermisosUsuariosRelatedByIdpermisoQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idpermiso', 'id', $q);
	}

	/**
	 * Returns the count of PermisosUsuario Objects(rows) from the permisos_usuario table
	 * with a idpermiso that matches $this->id.
	 * @return int
	 */
	function countPermisosUsuariosRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return PermisosUsuario::doCount($this->getPermisosUsuariosRelatedByIdpermisoQuery($q));
	}

	/**
	 * Deletes the permisos_usuario Objects(rows) from the permisos_usuario table
	 * with a idpermiso that matches $this->id.
	 * @return int
	 */
	function deletePermisosUsuariosRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->PermisosUsuariosRelatedByIdpermiso_c = array();
		return PermisosUsuario::doDelete($this->getPermisosUsuariosRelatedByIdpermisoQuery($q));
	}

	protected $PermisosUsuariosRelatedByIdpermiso_c = array();

	/**
	 * Returns an array of PermisosUsuario objects with a idpermiso
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return PermisosUsuario[]
	 */
	function getPermisosUsuariosRelatedByIdpermiso(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->PermisosUsuariosRelatedByIdpermiso_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->PermisosUsuariosRelatedByIdpermiso_c;
		}

		$result = PermisosUsuario::doSelect($this->getPermisosUsuariosRelatedByIdpermisoQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->PermisosUsuariosRelatedByIdpermiso_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Permisos::getPermisosGruposRelatedByidpermiso
	 * @return PermisosGrupo[]
	 * @see Permisos::getPermisosGruposRelatedByIdpermiso
	 */
	function getPermisosGrupos($extra = null) {
		return $this->getPermisosGruposRelatedByIdpermiso($extra);
	}

	/**
	  * Convenience function for Permisos::getPermisosGruposRelatedByidpermisoQuery
	  * @return Query
	  * @see Permisos::getPermisosGruposRelatedByidpermisoQuery
	  */
	function getPermisosGruposQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_grupo', 'idpermiso','id', $q);
	}

	/**
	  * Convenience function for Permisos::deletePermisosGruposRelatedByidpermiso
	  * @return int
	  * @see Permisos::deletePermisosGruposRelatedByidpermiso
	  */
	function deletePermisosGrupos(Query $q = null) {
		return $this->deletePermisosGruposRelatedByIdpermiso($q);
	}

	/**
	  * Convenience function for Permisos::countPermisosGruposRelatedByidpermiso
	  * @return int
	  * @see Permisos::countPermisosGruposRelatedByIdpermiso
	  */
	function countPermisosGrupos(Query $q = null) {
		return $this->countPermisosGruposRelatedByIdpermiso($q);
	}

	/**
	 * Convenience function for Permisos::getPermisosUsuariosRelatedByidpermiso
	 * @return PermisosUsuario[]
	 * @see Permisos::getPermisosUsuariosRelatedByIdpermiso
	 */
	function getPermisosUsuarios($extra = null) {
		return $this->getPermisosUsuariosRelatedByIdpermiso($extra);
	}

	/**
	  * Convenience function for Permisos::getPermisosUsuariosRelatedByidpermisoQuery
	  * @return Query
	  * @see Permisos::getPermisosUsuariosRelatedByidpermisoQuery
	  */
	function getPermisosUsuariosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('permisos_usuario', 'idpermiso','id', $q);
	}

	/**
	  * Convenience function for Permisos::deletePermisosUsuariosRelatedByidpermiso
	  * @return int
	  * @see Permisos::deletePermisosUsuariosRelatedByidpermiso
	  */
	function deletePermisosUsuarios(Query $q = null) {
		return $this->deletePermisosUsuariosRelatedByIdpermiso($q);
	}

	/**
	  * Convenience function for Permisos::countPermisosUsuariosRelatedByidpermiso
	  * @return int
	  * @see Permisos::countPermisosUsuariosRelatedByIdpermiso
	  */
	function countPermisosUsuarios(Query $q = null) {
		return $this->countPermisosUsuariosRelatedByIdpermiso($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getidaccion()) {
			$this->_validationErrors[] = 'idaccion must not be null';
		}
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}