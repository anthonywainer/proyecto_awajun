<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUbigeo extends ApplicationModel {

	const ID = 'ubigeo.id';
	const DEPARTAMENTO = 'ubigeo.departamento';
	const PROVINCIA = 'ubigeo.provincia';
	const DISTRITO = 'ubigeo.distrito';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ubigeo';

	/**
	 * Cache of objects retrieved from the database
	 * @var Ubigeo[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'departamento' => Model::COLUMN_TYPE_VARCHAR,
		'provincia' => Model::COLUMN_TYPE_VARCHAR,
		'distrito' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `departamento` VARCHAR
	 * @var string
	 */
	protected $departamento;

	/**
	 * `provincia` VARCHAR
	 * @var string
	 */
	protected $provincia;

	/**
	 * `distrito` VARCHAR
	 * @var string
	 */
	protected $distrito;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Ubigeo
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the departamento field
	 */
	function getDepartamento() {
		return $this->departamento;
	}

	/**
	 * Sets the value of the departamento field
	 * @return Ubigeo
	 */
	function setDepartamento($value) {
		return $this->setColumnValue('departamento', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the provincia field
	 */
	function getProvincia() {
		return $this->provincia;
	}

	/**
	 * Sets the value of the provincia field
	 * @return Ubigeo
	 */
	function setProvincia($value) {
		return $this->setColumnValue('provincia', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the distrito field
	 */
	function getDistrito() {
		return $this->distrito;
	}

	/**
	 * Sets the value of the distrito field
	 * @return Ubigeo
	 */
	function setDistrito($value) {
		return $this->setColumnValue('distrito', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Ubigeo
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Ubigeo
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Ubigeo
	 */
	static function retrieveById($value) {
		return Ubigeo::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a departamento
	 * value that matches the one provided
	 * @return Ubigeo
	 */
	static function retrieveByDepartamento($value) {
		return static::retrieveByColumn('departamento', $value);
	}

	/**
	 * Searches the database for a row with a provincia
	 * value that matches the one provided
	 * @return Ubigeo
	 */
	static function retrieveByProvincia($value) {
		return static::retrieveByColumn('provincia', $value);
	}

	/**
	 * Searches the database for a row with a distrito
	 * value that matches the one provided
	 * @return Ubigeo
	 */
	static function retrieveByDistrito($value) {
		return static::retrieveByColumn('distrito', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Ubigeo
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return Ubigeo[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting usuarios Objects(rows) from the usuarios table
	 * with a ubigeo that matches $this->id.
	 * @return Query
	 */
	function getUsuariossRelatedByUbigeoQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios', 'ubigeo', 'id', $q);
	}

	/**
	 * Returns the count of Usuarios Objects(rows) from the usuarios table
	 * with a ubigeo that matches $this->id.
	 * @return int
	 */
	function countUsuariossRelatedByUbigeo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Usuarios::doCount($this->getUsuariossRelatedByUbigeoQuery($q));
	}

	/**
	 * Deletes the usuarios Objects(rows) from the usuarios table
	 * with a ubigeo that matches $this->id.
	 * @return int
	 */
	function deleteUsuariossRelatedByUbigeo(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->UsuariossRelatedByUbigeo_c = array();
		return Usuarios::doDelete($this->getUsuariossRelatedByUbigeoQuery($q));
	}

	protected $UsuariossRelatedByUbigeo_c = array();

	/**
	 * Returns an array of Usuarios objects with a ubigeo
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Usuarios[]
	 */
	function getUsuariossRelatedByUbigeo(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->UsuariossRelatedByUbigeo_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->UsuariossRelatedByUbigeo_c;
		}

		$result = Usuarios::doSelect($this->getUsuariossRelatedByUbigeoQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->UsuariossRelatedByUbigeo_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Ubigeo::getUsuariossRelatedByubigeo
	 * @return Usuarios[]
	 * @see Ubigeo::getUsuariossRelatedByUbigeo
	 */
	function getUsuarioss($extra = null) {
		return $this->getUsuariossRelatedByUbigeo($extra);
	}

	/**
	  * Convenience function for Ubigeo::getUsuariossRelatedByubigeoQuery
	  * @return Query
	  * @see Ubigeo::getUsuariossRelatedByubigeoQuery
	  */
	function getUsuariossQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('usuarios', 'ubigeo','id', $q);
	}

	/**
	  * Convenience function for Ubigeo::deleteUsuariossRelatedByubigeo
	  * @return int
	  * @see Ubigeo::deleteUsuariossRelatedByubigeo
	  */
	function deleteUsuarioss(Query $q = null) {
		return $this->deleteUsuariossRelatedByUbigeo($q);
	}

	/**
	  * Convenience function for Ubigeo::countUsuariossRelatedByubigeo
	  * @return int
	  * @see Ubigeo::countUsuariossRelatedByUbigeo
	  */
	function countUsuarioss(Query $q = null) {
		return $this->countUsuariossRelatedByUbigeo($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}