<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseRolMiembro extends ApplicationModel {

	const ID = 'rol_miembro.id';
	const NOMBRE_MIEMBRO = 'rol_miembro.nombre_miembro';
	const CREATED_AT = 'rol_miembro.created_at';
	const UPDATED_AT = 'rol_miembro.updated_at';
	const DELETED_AT = 'rol_miembro.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'rol_miembro';

	/**
	 * Cache of objects retrieved from the database
	 * @var RolMiembro[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre_miembro' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre_miembro` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre_miembro;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return RolMiembro
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre_miembro field
	 */
	function getNombreMiembro() {
		return $this->nombre_miembro;
	}

	/**
	 * Sets the value of the nombre_miembro field
	 * @return RolMiembro
	 */
	function setNombreMiembro($value) {
		return $this->setColumnValue('nombre_miembro', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for RolMiembro::getNombreMiembro
	 * final because getNombreMiembro should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::getNombreMiembro
	 */
	final function getNombre_miembro() {
		return $this->getNombreMiembro();
	}

	/**
	 * Convenience function for RolMiembro::setNombreMiembro
	 * final because setNombreMiembro should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::setNombreMiembro
	 * @return RolMiembro
	 */
	final function setNombre_miembro($value) {
		return $this->setNombreMiembro($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return RolMiembro
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for RolMiembro::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for RolMiembro::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::setCreatedAt
	 * @return RolMiembro
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return RolMiembro
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for RolMiembro::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for RolMiembro::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::setUpdatedAt
	 * @return RolMiembro
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return RolMiembro
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for RolMiembro::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for RolMiembro::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see RolMiembro::setDeletedAt
	 * @return RolMiembro
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return RolMiembro
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return RolMiembro
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return RolMiembro
	 */
	static function retrieveById($value) {
		return RolMiembro::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre_miembro
	 * value that matches the one provided
	 * @return RolMiembro
	 */
	static function retrieveByNombreMiembro($value) {
		return static::retrieveByColumn('nombre_miembro', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return RolMiembro
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return RolMiembro
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return RolMiembro
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return RolMiembro
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return RolMiembro[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting miembros_familia Objects(rows) from the miembros_familia table
	 * with a rol_miembro that matches $this->id.
	 * @return Query
	 */
	function getMiembrosFamiliasRelatedByRolMiembroQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'rol_miembro', 'id', $q);
	}

	/**
	 * Returns the count of MiembrosFamilia Objects(rows) from the miembros_familia table
	 * with a rol_miembro that matches $this->id.
	 * @return int
	 */
	function countMiembrosFamiliasRelatedByRolMiembro(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return MiembrosFamilia::doCount($this->getMiembrosFamiliasRelatedByRolMiembroQuery($q));
	}

	/**
	 * Deletes the miembros_familia Objects(rows) from the miembros_familia table
	 * with a rol_miembro that matches $this->id.
	 * @return int
	 */
	function deleteMiembrosFamiliasRelatedByRolMiembro(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->MiembrosFamiliasRelatedByRolMiembro_c = array();
		return MiembrosFamilia::doDelete($this->getMiembrosFamiliasRelatedByRolMiembroQuery($q));
	}

	protected $MiembrosFamiliasRelatedByRolMiembro_c = array();

	/**
	 * Returns an array of MiembrosFamilia objects with a rol_miembro
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return MiembrosFamilia[]
	 */
	function getMiembrosFamiliasRelatedByRolMiembro(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->MiembrosFamiliasRelatedByRolMiembro_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->MiembrosFamiliasRelatedByRolMiembro_c;
		}

		$result = MiembrosFamilia::doSelect($this->getMiembrosFamiliasRelatedByRolMiembroQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->MiembrosFamiliasRelatedByRolMiembro_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for RolMiembro::getMiembrosFamiliasRelatedByrol_miembro
	 * @return MiembrosFamilia[]
	 * @see RolMiembro::getMiembrosFamiliasRelatedByRolMiembro
	 */
	function getMiembrosFamilias($extra = null) {
		return $this->getMiembrosFamiliasRelatedByRolMiembro($extra);
	}

	/**
	  * Convenience function for RolMiembro::getMiembrosFamiliasRelatedByrol_miembroQuery
	  * @return Query
	  * @see RolMiembro::getMiembrosFamiliasRelatedByrol_miembroQuery
	  */
	function getMiembrosFamiliasQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('miembros_familia', 'rol_miembro','id', $q);
	}

	/**
	  * Convenience function for RolMiembro::deleteMiembrosFamiliasRelatedByrol_miembro
	  * @return int
	  * @see RolMiembro::deleteMiembrosFamiliasRelatedByrol_miembro
	  */
	function deleteMiembrosFamilias(Query $q = null) {
		return $this->deleteMiembrosFamiliasRelatedByRolMiembro($q);
	}

	/**
	  * Convenience function for RolMiembro::countMiembrosFamiliasRelatedByrol_miembro
	  * @return int
	  * @see RolMiembro::countMiembrosFamiliasRelatedByRolMiembro
	  */
	function countMiembrosFamilias(Query $q = null) {
		return $this->countMiembrosFamiliasRelatedByRolMiembro($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre_miembro()) {
			$this->_validationErrors[] = 'nombre_miembro must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}