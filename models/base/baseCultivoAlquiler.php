<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseCultivoAlquiler extends ApplicationModel {

	const ID = 'cultivo_alquiler.id';
	const CULTIVO_ID = 'cultivo_alquiler.cultivo_id';
	const ALQUILER_ID = 'cultivo_alquiler.alquiler_id';
	const CREATED_AT = 'cultivo_alquiler.created_at';
	const UPDATED_AT = 'cultivo_alquiler.updated_at';
	const DELETED_AT = 'cultivo_alquiler.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'cultivo_alquiler';

	/**
	 * Cache of objects retrieved from the database
	 * @var CultivoAlquiler[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'cultivo_id' => Model::COLUMN_TYPE_INTEGER,
		'alquiler_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `cultivo_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $cultivo_id;

	/**
	 * `alquiler_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $alquiler_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return CultivoAlquiler
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the cultivo_id field
	 */
	function getCultivoId() {
		return $this->cultivo_id;
	}

	/**
	 * Sets the value of the cultivo_id field
	 * @return CultivoAlquiler
	 */
	function setCultivoId($value) {
		return $this->setColumnValue('cultivo_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for CultivoAlquiler::getCultivoId
	 * final because getCultivoId should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::getCultivoId
	 */
	final function getCultivo_id() {
		return $this->getCultivoId();
	}

	/**
	 * Convenience function for CultivoAlquiler::setCultivoId
	 * final because setCultivoId should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::setCultivoId
	 * @return CultivoAlquiler
	 */
	final function setCultivo_id($value) {
		return $this->setCultivoId($value);
	}

	/**
	 * Gets the value of the alquiler_id field
	 */
	function getAlquilerId() {
		return $this->alquiler_id;
	}

	/**
	 * Sets the value of the alquiler_id field
	 * @return CultivoAlquiler
	 */
	function setAlquilerId($value) {
		return $this->setColumnValue('alquiler_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for CultivoAlquiler::getAlquilerId
	 * final because getAlquilerId should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::getAlquilerId
	 */
	final function getAlquiler_id() {
		return $this->getAlquilerId();
	}

	/**
	 * Convenience function for CultivoAlquiler::setAlquilerId
	 * final because setAlquilerId should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::setAlquilerId
	 * @return CultivoAlquiler
	 */
	final function setAlquiler_id($value) {
		return $this->setAlquilerId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return CultivoAlquiler
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CultivoAlquiler::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for CultivoAlquiler::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::setCreatedAt
	 * @return CultivoAlquiler
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return CultivoAlquiler
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CultivoAlquiler::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for CultivoAlquiler::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::setUpdatedAt
	 * @return CultivoAlquiler
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return CultivoAlquiler
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for CultivoAlquiler::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for CultivoAlquiler::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see CultivoAlquiler::setDeletedAt
	 * @return CultivoAlquiler
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return CultivoAlquiler
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return CultivoAlquiler
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveById($value) {
		return CultivoAlquiler::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a cultivo_id
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveByCultivoId($value) {
		return static::retrieveByColumn('cultivo_id', $value);
	}

	/**
	 * Searches the database for a row with a alquiler_id
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveByAlquilerId($value) {
		return static::retrieveByColumn('alquiler_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return CultivoAlquiler
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return CultivoAlquiler
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->cultivo_id = (null === $this->cultivo_id) ? null : (int) $this->cultivo_id;
		$this->alquiler_id = (null === $this->alquiler_id) ? null : (int) $this->alquiler_id;
		return $this;
	}

	/**
	 * @return CultivoAlquiler[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getcultivo_id()) {
			$this->_validationErrors[] = 'cultivo_id must not be null';
		}
		if (null === $this->getalquiler_id()) {
			$this->_validationErrors[] = 'alquiler_id must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}