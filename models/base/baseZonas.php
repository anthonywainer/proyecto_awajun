<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseZonas extends ApplicationModel {

	const ID = 'zonas.id';
	const ZONA_UBICACION_ID = 'zonas.zona_ubicacion_id';
	const PREDIO_ID = 'zonas.predio_id';
	const TIPO_ZONA = 'zonas.tipo_zona';
	const CREATED_AT = 'zonas.created_at';
	const UPDATED_AT = 'zonas.updated_at';
	const DELETED_AT = 'zonas.deleted_at';
	const MIEMBRO_ID = 'zonas.miembro_id';
	const NUMERO_ZONA = 'zonas.numero_zona';
	const AREA = 'zonas.area';
	const FECHA_REGISTRO = 'zonas.fecha_registro';
	const COORDENADAS = 'zonas.coordenadas';
	const DIRECCION = 'zonas.direccion';
	const COLOR = 'zonas.color';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'zonas';

	/**
	 * Cache of objects retrieved from the database
	 * @var Zonas[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'zona_ubicacion_id' => Model::COLUMN_TYPE_INTEGER,
		'predio_id' => Model::COLUMN_TYPE_INTEGER,
		'tipo_zona' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'miembro_id' => Model::COLUMN_TYPE_INTEGER,
		'numero_zona' => Model::COLUMN_TYPE_VARCHAR,
		'area' => Model::COLUMN_TYPE_INTEGER,
		'fecha_registro' => Model::COLUMN_TYPE_TIMESTAMP,
		'coordenadas' => Model::COLUMN_TYPE_VARCHAR,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'color' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `zona_ubicacion_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $zona_ubicacion_id;

	/**
	 * `predio_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $predio_id;

	/**
	 * `tipo_zona` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $tipo_zona;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `miembro_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $miembro_id;

	/**
	 * `numero_zona` VARCHAR NOT NULL
	 * @var string
	 */
	protected $numero_zona;

	/**
	 * `area` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $area;

	/**
	 * `fecha_registro` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $fecha_registro = 'CURRENT_TIMESTAMP';

	/**
	 * `coordenadas` VARCHAR NOT NULL
	 * @var string
	 */
	protected $coordenadas;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `color` VARCHAR NOT NULL
	 * @var string
	 */
	protected $color;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Zonas
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the zona_ubicacion_id field
	 */
	function getZonaUbicacionId() {
		return $this->zona_ubicacion_id;
	}

	/**
	 * Sets the value of the zona_ubicacion_id field
	 * @return Zonas
	 */
	function setZonaUbicacionId($value) {
		return $this->setColumnValue('zona_ubicacion_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Zonas::getZonaUbicacionId
	 * final because getZonaUbicacionId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getZonaUbicacionId
	 */
	final function getZona_ubicacion_id() {
		return $this->getZonaUbicacionId();
	}

	/**
	 * Convenience function for Zonas::setZonaUbicacionId
	 * final because setZonaUbicacionId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setZonaUbicacionId
	 * @return Zonas
	 */
	final function setZona_ubicacion_id($value) {
		return $this->setZonaUbicacionId($value);
	}

	/**
	 * Gets the value of the predio_id field
	 */
	function getPredioId() {
		return $this->predio_id;
	}

	/**
	 * Sets the value of the predio_id field
	 * @return Zonas
	 */
	function setPredioId($value) {
		return $this->setColumnValue('predio_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Zonas::getPredioId
	 * final because getPredioId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getPredioId
	 */
	final function getPredio_id() {
		return $this->getPredioId();
	}

	/**
	 * Convenience function for Zonas::setPredioId
	 * final because setPredioId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setPredioId
	 * @return Zonas
	 */
	final function setPredio_id($value) {
		return $this->setPredioId($value);
	}

	/**
	 * Gets the value of the tipo_zona field
	 */
	function getTipoZona() {
		return $this->tipo_zona;
	}

	/**
	 * Sets the value of the tipo_zona field
	 * @return Zonas
	 */
	function setTipoZona($value) {
		return $this->setColumnValue('tipo_zona', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Zonas::getTipoZona
	 * final because getTipoZona should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getTipoZona
	 */
	final function getTipo_zona() {
		return $this->getTipoZona();
	}

	/**
	 * Convenience function for Zonas::setTipoZona
	 * final because setTipoZona should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setTipoZona
	 * @return Zonas
	 */
	final function setTipo_zona($value) {
		return $this->setTipoZona($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Zonas
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Zonas::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Zonas::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setCreatedAt
	 * @return Zonas
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Zonas
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Zonas::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Zonas::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setUpdatedAt
	 * @return Zonas
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Zonas
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Zonas::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Zonas::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setDeletedAt
	 * @return Zonas
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the miembro_id field
	 */
	function getMiembroId() {
		return $this->miembro_id;
	}

	/**
	 * Sets the value of the miembro_id field
	 * @return Zonas
	 */
	function setMiembroId($value) {
		return $this->setColumnValue('miembro_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Zonas::getMiembroId
	 * final because getMiembroId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getMiembroId
	 */
	final function getMiembro_id() {
		return $this->getMiembroId();
	}

	/**
	 * Convenience function for Zonas::setMiembroId
	 * final because setMiembroId should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setMiembroId
	 * @return Zonas
	 */
	final function setMiembro_id($value) {
		return $this->setMiembroId($value);
	}

	/**
	 * Gets the value of the numero_zona field
	 */
	function getNumeroZona() {
		return $this->numero_zona;
	}

	/**
	 * Sets the value of the numero_zona field
	 * @return Zonas
	 */
	function setNumeroZona($value) {
		return $this->setColumnValue('numero_zona', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Zonas::getNumeroZona
	 * final because getNumeroZona should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getNumeroZona
	 */
	final function getNumero_zona() {
		return $this->getNumeroZona();
	}

	/**
	 * Convenience function for Zonas::setNumeroZona
	 * final because setNumeroZona should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setNumeroZona
	 * @return Zonas
	 */
	final function setNumero_zona($value) {
		return $this->setNumeroZona($value);
	}

	/**
	 * Gets the value of the area field
	 */
	function getArea() {
		return $this->area;
	}

	/**
	 * Sets the value of the area field
	 * @return Zonas
	 */
	function setArea($value) {
		return $this->setColumnValue('area', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha_registro field
	 */
	function getFechaRegistro($format = null) {
		if (null === $this->fecha_registro || null === $format) {
			return $this->fecha_registro;
		}
		if (0 === strpos($this->fecha_registro, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_registro));
	}

	/**
	 * Sets the value of the fecha_registro field
	 * @return Zonas
	 */
	function setFechaRegistro($value) {
		return $this->setColumnValue('fecha_registro', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Zonas::getFechaRegistro
	 * final because getFechaRegistro should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::getFechaRegistro
	 */
	final function getFecha_registro($format = null) {
		return $this->getFechaRegistro($format);
	}

	/**
	 * Convenience function for Zonas::setFechaRegistro
	 * final because setFechaRegistro should be extended instead
	 * to ensure consistent behavior
	 * @see Zonas::setFechaRegistro
	 * @return Zonas
	 */
	final function setFecha_registro($value) {
		return $this->setFechaRegistro($value);
	}

	/**
	 * Gets the value of the coordenadas field
	 */
	function getCoordenadas() {
		return $this->coordenadas;
	}

	/**
	 * Sets the value of the coordenadas field
	 * @return Zonas
	 */
	function setCoordenadas($value) {
		return $this->setColumnValue('coordenadas', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Zonas
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the color field
	 */
	function getColor() {
		return $this->color;
	}

	/**
	 * Sets the value of the color field
	 * @return Zonas
	 */
	function setColor($value) {
		return $this->setColumnValue('color', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Zonas
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Zonas
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveById($value) {
		return Zonas::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a zona_ubicacion_id
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByZonaUbicacionId($value) {
		return static::retrieveByColumn('zona_ubicacion_id', $value);
	}

	/**
	 * Searches the database for a row with a predio_id
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByPredioId($value) {
		return static::retrieveByColumn('predio_id', $value);
	}

	/**
	 * Searches the database for a row with a tipo_zona
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByTipoZona($value) {
		return static::retrieveByColumn('tipo_zona', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a miembro_id
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByMiembroId($value) {
		return static::retrieveByColumn('miembro_id', $value);
	}

	/**
	 * Searches the database for a row with a numero_zona
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByNumeroZona($value) {
		return static::retrieveByColumn('numero_zona', $value);
	}

	/**
	 * Searches the database for a row with a area
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByArea($value) {
		return static::retrieveByColumn('area', $value);
	}

	/**
	 * Searches the database for a row with a fecha_registro
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByFechaRegistro($value) {
		return static::retrieveByColumn('fecha_registro', $value);
	}

	/**
	 * Searches the database for a row with a coordenadas
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByCoordenadas($value) {
		return static::retrieveByColumn('coordenadas', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a color
	 * value that matches the one provided
	 * @return Zonas
	 */
	static function retrieveByColor($value) {
		return static::retrieveByColumn('color', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Zonas
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->zona_ubicacion_id = (null === $this->zona_ubicacion_id) ? null : (int) $this->zona_ubicacion_id;
		$this->predio_id = (null === $this->predio_id) ? null : (int) $this->predio_id;
		$this->tipo_zona = (null === $this->tipo_zona) ? null : (int) $this->tipo_zona;
		$this->miembro_id = (null === $this->miembro_id) ? null : (int) $this->miembro_id;
		$this->area = (null === $this->area) ? null : (int) $this->area;
		return $this;
	}

	/**
	 * @return Zonas
	 */
	function setTipoZonaRelatedByTipoZona(TipoZona $tipozona = null) {
		if (null === $tipozona) {
			$this->settipo_zona(null);
		} else {
			if (!$tipozona->getid()) {
				throw new Exception('Cannot connect a TipoZona without a id');
			}
			$this->settipo_zona($tipozona->getid());
		}
		return $this;
	}

	/**
	 * Returns a tipo_zona object with a id
	 * that matches $this->tipo_zona.
	 * @return TipoZona
	 */
	function getTipoZonaRelatedByTipoZona() {
		$fk_value = $this->gettipo_zona();
		if (null === $fk_value) {
			return null;
		}
		return TipoZona::retrieveByPK($fk_value);
	}

	/**
	 * @return Zonas[]
	 */
	static function doSelectJoinTipoZonaRelatedByTipoZona(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TipoZona::getTableName();
		$q->join($to_table, $this_table . '.tipo_zona = ' . $to_table . '.id', $join_type);
		foreach (TipoZona::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('TipoZona'));
	}

	/**
	 * @return Zonas
	 */
	function setZonaUbicacion(UbicacionZona $ubicacionzona = null) {
		return $this->setUbicacionZonaRelatedByZonaUbicacionId($ubicacionzona);
	}

	/**
	 * @return Zonas
	 */
	function setUbicacionZonaRelatedByZonaUbicacionId(UbicacionZona $ubicacionzona = null) {
		if (null === $ubicacionzona) {
			$this->setzona_ubicacion_id(null);
		} else {
			if (!$ubicacionzona->getid()) {
				throw new Exception('Cannot connect a UbicacionZona without a id');
			}
			$this->setzona_ubicacion_id($ubicacionzona->getid());
		}
		return $this;
	}

	/**
	 * Returns a ubicacion_zona object with a id
	 * that matches $this->zona_ubicacion_id.
	 * @return UbicacionZona
	 */
	function getZonaUbicacion() {
		return $this->getUbicacionZonaRelatedByZonaUbicacionId();
	}

	/**
	 * Returns a ubicacion_zona object with a id
	 * that matches $this->zona_ubicacion_id.
	 * @return UbicacionZona
	 */
	function getUbicacionZonaRelatedByZonaUbicacionId() {
		$fk_value = $this->getzona_ubicacion_id();
		if (null === $fk_value) {
			return null;
		}
		return UbicacionZona::retrieveByPK($fk_value);
	}

	static function doSelectJoinZonaUbicacion(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinUbicacionZonaRelatedByZonaUbicacionId($q, $join_type);
	}

	/**
	 * Returns a ubicacion_zona object with a id
	 * that matches $this->zona_ubicacion_id.
	 * @return UbicacionZona
	 */
	function getUbicacionZona() {
		return $this->getUbicacionZonaRelatedByZonaUbicacionId();
	}

	/**
	 * @return Zonas
	 */
	function setUbicacionZona(UbicacionZona $ubicacionzona = null) {
		return $this->setUbicacionZonaRelatedByZonaUbicacionId($ubicacionzona);
	}

	/**
	 * @return Zonas[]
	 */
	static function doSelectJoinUbicacionZonaRelatedByZonaUbicacionId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = UbicacionZona::getTableName();
		$q->join($to_table, $this_table . '.zona_ubicacion_id = ' . $to_table . '.id', $join_type);
		foreach (UbicacionZona::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('UbicacionZona'));
	}

	/**
	 * @return Zonas
	 */
	function setMiembro(MiembrosFamilia $miembrosfamilia = null) {
		return $this->setMiembrosFamiliaRelatedByMiembroId($miembrosfamilia);
	}

	/**
	 * @return Zonas
	 */
	function setMiembrosFamiliaRelatedByMiembroId(MiembrosFamilia $miembrosfamilia = null) {
		if (null === $miembrosfamilia) {
			$this->setmiembro_id(null);
		} else {
			if (!$miembrosfamilia->getid()) {
				throw new Exception('Cannot connect a MiembrosFamilia without a id');
			}
			$this->setmiembro_id($miembrosfamilia->getid());
		}
		return $this;
	}

	/**
	 * Returns a miembros_familia object with a id
	 * that matches $this->miembro_id.
	 * @return MiembrosFamilia
	 */
	function getMiembro() {
		return $this->getMiembrosFamiliaRelatedByMiembroId();
	}

	/**
	 * Returns a miembros_familia object with a id
	 * that matches $this->miembro_id.
	 * @return MiembrosFamilia
	 */
	function getMiembrosFamiliaRelatedByMiembroId() {
		$fk_value = $this->getmiembro_id();
		if (null === $fk_value) {
			return null;
		}
		return MiembrosFamilia::retrieveByPK($fk_value);
	}

	static function doSelectJoinMiembro(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinMiembrosFamiliaRelatedByMiembroId($q, $join_type);
	}

	/**
	 * Returns a miembros_familia object with a id
	 * that matches $this->miembro_id.
	 * @return MiembrosFamilia
	 */
	function getMiembrosFamilia() {
		return $this->getMiembrosFamiliaRelatedByMiembroId();
	}

	/**
	 * @return Zonas
	 */
	function setMiembrosFamilia(MiembrosFamilia $miembrosfamilia = null) {
		return $this->setMiembrosFamiliaRelatedByMiembroId($miembrosfamilia);
	}

	/**
	 * @return Zonas[]
	 */
	static function doSelectJoinMiembrosFamiliaRelatedByMiembroId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = MiembrosFamilia::getTableName();
		$q->join($to_table, $this_table . '.miembro_id = ' . $to_table . '.id', $join_type);
		foreach (MiembrosFamilia::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('MiembrosFamilia'));
	}

	/**
	 * @return Zonas
	 */
	function setPredio(Predio $predio = null) {
		return $this->setPredioRelatedByPredioId($predio);
	}

	/**
	 * @return Zonas
	 */
	function setPredioRelatedByPredioId(Predio $predio = null) {
		if (null === $predio) {
			$this->setpredio_id(null);
		} else {
			if (!$predio->getid()) {
				throw new Exception('Cannot connect a Predio without a id');
			}
			$this->setpredio_id($predio->getid());
		}
		return $this;
	}

	/**
	 * Returns a predio object with a id
	 * that matches $this->predio_id.
	 * @return Predio
	 */
	function getPredio() {
		return $this->getPredioRelatedByPredioId();
	}

	/**
	 * Returns a predio object with a id
	 * that matches $this->predio_id.
	 * @return Predio
	 */
	function getPredioRelatedByPredioId() {
		$fk_value = $this->getpredio_id();
		if (null === $fk_value) {
			return null;
		}
		return Predio::retrieveByPK($fk_value);
	}

	static function doSelectJoinPredio(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinPredioRelatedByPredioId($q, $join_type);
	}

	/**
	 * @return Zonas[]
	 */
	static function doSelectJoinPredioRelatedByPredioId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Predio::getTableName();
		$q->join($to_table, $this_table . '.predio_id = ' . $to_table . '.id', $join_type);
		foreach (Predio::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Predio'));
	}

	/**
	 * @return Zonas[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = TipoZona::getTableName();
		$q->join($to_table, $this_table . '.tipo_zona = ' . $to_table . '.id', $join_type);
		foreach (TipoZona::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'TipoZona';
	
		$to_table = UbicacionZona::getTableName();
		$q->join($to_table, $this_table . '.zona_ubicacion_id = ' . $to_table . '.id', $join_type);
		foreach (UbicacionZona::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'UbicacionZona';
	
		$to_table = MiembrosFamilia::getTableName();
		$q->join($to_table, $this_table . '.miembro_id = ' . $to_table . '.id', $join_type);
		foreach (MiembrosFamilia::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'MiembrosFamilia';
	
		$to_table = Predio::getTableName();
		$q->join($to_table, $this_table . '.predio_id = ' . $to_table . '.id', $join_type);
		foreach (Predio::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Predio';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting cultivo Objects(rows) from the cultivo table
	 * with a zona that matches $this->id.
	 * @return Query
	 */
	function getCultivosRelatedByZonaQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'zona', 'id', $q);
	}

	/**
	 * Returns the count of Cultivo Objects(rows) from the cultivo table
	 * with a zona that matches $this->id.
	 * @return int
	 */
	function countCultivosRelatedByZona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Cultivo::doCount($this->getCultivosRelatedByZonaQuery($q));
	}

	/**
	 * Deletes the cultivo Objects(rows) from the cultivo table
	 * with a zona that matches $this->id.
	 * @return int
	 */
	function deleteCultivosRelatedByZona(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->CultivosRelatedByZona_c = array();
		return Cultivo::doDelete($this->getCultivosRelatedByZonaQuery($q));
	}

	protected $CultivosRelatedByZona_c = array();

	/**
	 * Returns an array of Cultivo objects with a zona
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Cultivo[]
	 */
	function getCultivosRelatedByZona(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->CultivosRelatedByZona_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->CultivosRelatedByZona_c;
		}

		$result = Cultivo::doSelect($this->getCultivosRelatedByZonaQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->CultivosRelatedByZona_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Zonas::getCultivosRelatedByzona
	 * @return Cultivo[]
	 * @see Zonas::getCultivosRelatedByZona
	 */
	function getCultivos($extra = null) {
		return $this->getCultivosRelatedByZona($extra);
	}

	/**
	  * Convenience function for Zonas::getCultivosRelatedByzonaQuery
	  * @return Query
	  * @see Zonas::getCultivosRelatedByzonaQuery
	  */
	function getCultivosQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('cultivo', 'zona','id', $q);
	}

	/**
	  * Convenience function for Zonas::deleteCultivosRelatedByzona
	  * @return int
	  * @see Zonas::deleteCultivosRelatedByzona
	  */
	function deleteCultivos(Query $q = null) {
		return $this->deleteCultivosRelatedByZona($q);
	}

	/**
	  * Convenience function for Zonas::countCultivosRelatedByzona
	  * @return int
	  * @see Zonas::countCultivosRelatedByZona
	  */
	function countCultivos(Query $q = null) {
		return $this->countCultivosRelatedByZona($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getzona_ubicacion_id()) {
			$this->_validationErrors[] = 'zona_ubicacion_id must not be null';
		}
		if (null === $this->getpredio_id()) {
			$this->_validationErrors[] = 'predio_id must not be null';
		}
		if (null === $this->gettipo_zona()) {
			$this->_validationErrors[] = 'tipo_zona must not be null';
		}
		if (null === $this->getmiembro_id()) {
			$this->_validationErrors[] = 'miembro_id must not be null';
		}

		if (null === $this->getarea()) {
			$this->_validationErrors[] = 'area must not be null';
		}

		return 0 === count($this->_validationErrors);
	}

}