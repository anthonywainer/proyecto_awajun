<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUbicacionZona extends ApplicationModel {

	const ID = 'ubicacion_zona.id';
	const ZONA = 'ubicacion_zona.zona';
	const CREATED_AT = 'ubicacion_zona.created_at';
	const UPDATED_AT = 'ubicacion_zona.updated_at';
	const DELETED_AT = 'ubicacion_zona.deleted_at';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'ubicacion_zona';

	/**
	 * Cache of objects retrieved from the database
	 * @var UbicacionZona[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'zona' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `zona` VARCHAR
	 * @var string
	 */
	protected $zona;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return UbicacionZona
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the zona field
	 */
	function getZona() {
		return $this->zona;
	}

	/**
	 * Sets the value of the zona field
	 * @return UbicacionZona
	 */
	function setZona($value) {
		return $this->setColumnValue('zona', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return UbicacionZona
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UbicacionZona::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for UbicacionZona::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::setCreatedAt
	 * @return UbicacionZona
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return UbicacionZona
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UbicacionZona::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for UbicacionZona::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::setUpdatedAt
	 * @return UbicacionZona
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return UbicacionZona
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UbicacionZona::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for UbicacionZona::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UbicacionZona::setDeletedAt
	 * @return UbicacionZona
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return UbicacionZona
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return UbicacionZona
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return UbicacionZona
	 */
	static function retrieveById($value) {
		return UbicacionZona::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a zona
	 * value that matches the one provided
	 * @return UbicacionZona
	 */
	static function retrieveByZona($value) {
		return static::retrieveByColumn('zona', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return UbicacionZona
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return UbicacionZona
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return UbicacionZona
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return UbicacionZona
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return UbicacionZona[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting zonas Objects(rows) from the zonas table
	 * with a zona_ubicacion_id that matches $this->id.
	 * @return Query
	 */
	function getZonassRelatedByZonaUbicacionIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'zona_ubicacion_id', 'id', $q);
	}

	/**
	 * Returns the count of Zonas Objects(rows) from the zonas table
	 * with a zona_ubicacion_id that matches $this->id.
	 * @return int
	 */
	function countZonassRelatedByZonaUbicacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Zonas::doCount($this->getZonassRelatedByZonaUbicacionIdQuery($q));
	}

	/**
	 * Deletes the zonas Objects(rows) from the zonas table
	 * with a zona_ubicacion_id that matches $this->id.
	 * @return int
	 */
	function deleteZonassRelatedByZonaUbicacionId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ZonassRelatedByZonaUbicacionId_c = array();
		return Zonas::doDelete($this->getZonassRelatedByZonaUbicacionIdQuery($q));
	}

	protected $ZonassRelatedByZonaUbicacionId_c = array();

	/**
	 * Returns an array of Zonas objects with a zona_ubicacion_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Zonas[]
	 */
	function getZonassRelatedByZonaUbicacionId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ZonassRelatedByZonaUbicacionId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ZonassRelatedByZonaUbicacionId_c;
		}

		$result = Zonas::doSelect($this->getZonassRelatedByZonaUbicacionIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ZonassRelatedByZonaUbicacionId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for UbicacionZona::getZonassRelatedByzona_ubicacion_id
	 * @return Zonas[]
	 * @see UbicacionZona::getZonassRelatedByZonaUbicacionId
	 */
	function getZonass($extra = null) {
		return $this->getZonassRelatedByZonaUbicacionId($extra);
	}

	/**
	  * Convenience function for UbicacionZona::getZonassRelatedByzona_ubicacion_idQuery
	  * @return Query
	  * @see UbicacionZona::getZonassRelatedByzona_ubicacion_idQuery
	  */
	function getZonassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'zona_ubicacion_id','id', $q);
	}

	/**
	  * Convenience function for UbicacionZona::deleteZonassRelatedByzona_ubicacion_id
	  * @return int
	  * @see UbicacionZona::deleteZonassRelatedByzona_ubicacion_id
	  */
	function deleteZonass(Query $q = null) {
		return $this->deleteZonassRelatedByZonaUbicacionId($q);
	}

	/**
	  * Convenience function for UbicacionZona::countZonassRelatedByzona_ubicacion_id
	  * @return int
	  * @see UbicacionZona::countZonassRelatedByZonaUbicacionId
	  */
	function countZonass(Query $q = null) {
		return $this->countZonassRelatedByZonaUbicacionId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		return 0 === count($this->_validationErrors);
	}

}