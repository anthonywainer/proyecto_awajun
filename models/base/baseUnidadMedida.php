<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class baseUnidadMedida extends ApplicationModel {

	const ID = 'unidad_medida.id';
	const NOMBRE = 'unidad_medida.nombre';
	const CREATED_AT = 'unidad_medida.created_at';
	const UPDATED_AT = 'unidad_medida.updated_at';
	const DELETED_AT = 'unidad_medida.deleted_at';
	const SIMBOLO = 'unidad_medida.simbolo';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'unidad_medida';

	/**
	 * Cache of objects retrieved from the database
	 * @var UnidadMedida[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'nombre' => Model::COLUMN_TYPE_VARCHAR,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'simbolo' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `nombre` VARCHAR NOT NULL
	 * @var string
	 */
	protected $nombre;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `simbolo` VARCHAR NOT NULL
	 * @var string
	 */
	protected $simbolo;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return UnidadMedida
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the nombre field
	 */
	function getNombre() {
		return $this->nombre;
	}

	/**
	 * Sets the value of the nombre field
	 * @return UnidadMedida
	 */
	function setNombre($value) {
		return $this->setColumnValue('nombre', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return UnidadMedida
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UnidadMedida::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for UnidadMedida::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::setCreatedAt
	 * @return UnidadMedida
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return UnidadMedida
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UnidadMedida::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for UnidadMedida::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::setUpdatedAt
	 * @return UnidadMedida
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return UnidadMedida
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for UnidadMedida::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for UnidadMedida::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see UnidadMedida::setDeletedAt
	 * @return UnidadMedida
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the simbolo field
	 */
	function getSimbolo() {
		return $this->simbolo;
	}

	/**
	 * Sets the value of the simbolo field
	 * @return UnidadMedida
	 */
	function setSimbolo($value) {
		return $this->setColumnValue('simbolo', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return UnidadMedida
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return UnidadMedida
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveById($value) {
		return UnidadMedida::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a nombre
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveByNombre($value) {
		return static::retrieveByColumn('nombre', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a simbolo
	 * value that matches the one provided
	 * @return UnidadMedida
	 */
	static function retrieveBySimbolo($value) {
		return static::retrieveByColumn('simbolo', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return UnidadMedida
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		return $this;
	}

	/**
	 * @return UnidadMedida[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting zonas Objects(rows) from the zonas table
	 * with a unidad_medida_id that matches $this->id.
	 * @return Query
	 */
	function getZonassRelatedByUnidadMedidaIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'unidad_medida_id', 'id', $q);
	}

	/**
	 * Returns the count of Zonas Objects(rows) from the zonas table
	 * with a unidad_medida_id that matches $this->id.
	 * @return int
	 */
	function countZonassRelatedByUnidadMedidaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Zonas::doCount($this->getZonassRelatedByUnidadMedidaIdQuery($q));
	}

	/**
	 * Deletes the zonas Objects(rows) from the zonas table
	 * with a unidad_medida_id that matches $this->id.
	 * @return int
	 */
	function deleteZonassRelatedByUnidadMedidaId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ZonassRelatedByUnidadMedidaId_c = array();
		return Zonas::doDelete($this->getZonassRelatedByUnidadMedidaIdQuery($q));
	}

	protected $ZonassRelatedByUnidadMedidaId_c = array();

	/**
	 * Returns an array of Zonas objects with a unidad_medida_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Zonas[]
	 */
	function getZonassRelatedByUnidadMedidaId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ZonassRelatedByUnidadMedidaId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ZonassRelatedByUnidadMedidaId_c;
		}

		$result = Zonas::doSelect($this->getZonassRelatedByUnidadMedidaIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ZonassRelatedByUnidadMedidaId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for UnidadMedida::getZonassRelatedByunidad_medida_id
	 * @return Zonas[]
	 * @see UnidadMedida::getZonassRelatedByUnidadMedidaId
	 */
	function getZonass($extra = null) {
		return $this->getZonassRelatedByUnidadMedidaId($extra);
	}

	/**
	  * Convenience function for UnidadMedida::getZonassRelatedByunidad_medida_idQuery
	  * @return Query
	  * @see UnidadMedida::getZonassRelatedByunidad_medida_idQuery
	  */
	function getZonassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'unidad_medida_id','id', $q);
	}

	/**
	  * Convenience function for UnidadMedida::deleteZonassRelatedByunidad_medida_id
	  * @return int
	  * @see UnidadMedida::deleteZonassRelatedByunidad_medida_id
	  */
	function deleteZonass(Query $q = null) {
		return $this->deleteZonassRelatedByUnidadMedidaId($q);
	}

	/**
	  * Convenience function for UnidadMedida::countZonassRelatedByunidad_medida_id
	  * @return int
	  * @see UnidadMedida::countZonassRelatedByUnidadMedidaId
	  */
	function countZonass(Query $q = null) {
		return $this->countZonassRelatedByUnidadMedidaId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnombre()) {
			$this->_validationErrors[] = 'nombre must not be null';
		}
		if (null === $this->getsimbolo()) {
			$this->_validationErrors[] = 'simbolo must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}