<?php

use Dabl\Orm\Model;
use Dabl\Query\DBManager;
use Dabl\Query\Query;
use Dabl\Adapter\DABLPDO;

/**
 *		Created by Dan Blaisdell's DABL
 *		Do not alter base files, as they will be overwritten.
 *		To alter the objects, alter the extended classes in
 *		the 'models' folder.
 *
 */
abstract class basePredio extends ApplicationModel {

	const ID = 'predio.id';
	const NUMERO_PREDIO = 'predio.numero_predio';
	const AREA = 'predio.area';
	const FECHA_REGISTRO = 'predio.fecha_registro';
	const FAMILIA_ID = 'predio.familia_id';
	const CREATED_AT = 'predio.created_at';
	const UPDATED_AT = 'predio.updated_at';
	const DELETED_AT = 'predio.deleted_at';
	const COORDENADAS = 'predio.coordenadas';
	const DIRECCION = 'predio.direccion';
	const COLOR = 'predio.color';

	/**
	 * Name of the table
	 * @var string
	 */
	protected static $_tableName = 'predio';

	/**
	 * Cache of objects retrieved from the database
	 * @var Predio[]
	 */
	protected static $_instancePool = array();

	protected static $_instancePoolCount = 0;

	protected static $_poolEnabled = true;

	/**
	 * Array of objects to batch insert
	 */
	protected static $_insertBatch = array();

	/**
	 * Maximum size of the insert batch
	 */
	protected static $_insertBatchSize = 500;

	/**
	 * Array of all primary keys
	 * @var string[]
	 */
	protected static $_primaryKeys = array(
		'id',
	);

	/**
	 * true if primary key is an auto-increment column
	 * @var bool
	 */
	protected static $_isAutoIncrement = true;

	/**
	 * array of all column types
	 * @var string[]
	 */
	protected static $_columns = array(
		'id' => Model::COLUMN_TYPE_INTEGER,
		'numero_predio' => Model::COLUMN_TYPE_VARCHAR,
		'area' => Model::COLUMN_TYPE_INTEGER,
		'fecha_registro' => Model::COLUMN_TYPE_TIMESTAMP,
		'familia_id' => Model::COLUMN_TYPE_INTEGER,
		'created_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'updated_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'deleted_at' => Model::COLUMN_TYPE_TIMESTAMP,
		'coordenadas' => Model::COLUMN_TYPE_VARCHAR,
		'direccion' => Model::COLUMN_TYPE_VARCHAR,
		'color' => Model::COLUMN_TYPE_VARCHAR,
	);

	/**
	 * `id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $id;

	/**
	 * `numero_predio` VARCHAR NOT NULL
	 * @var string
	 */
	protected $numero_predio;

	/**
	 * `area` INTEGER DEFAULT ''
	 * @var int
	 */
	protected $area;

	/**
	 * `fecha_registro` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $fecha_registro = 'CURRENT_TIMESTAMP';

	/**
	 * `familia_id` INTEGER NOT NULL DEFAULT ''
	 * @var int
	 */
	protected $familia_id;

	/**
	 * `created_at` TIMESTAMP NOT NULL DEFAULT 'CURRENT_TIMESTAMP'
	 * @var string
	 */
	protected $created_at = 'CURRENT_TIMESTAMP';

	/**
	 * `updated_at` TIMESTAMP
	 * @var string
	 */
	protected $updated_at;

	/**
	 * `deleted_at` TIMESTAMP
	 * @var string
	 */
	protected $deleted_at;

	/**
	 * `coordenadas` VARCHAR
	 * @var string
	 */
	protected $coordenadas;

	/**
	 * `direccion` VARCHAR NOT NULL
	 * @var string
	 */
	protected $direccion;

	/**
	 * `color` VARCHAR
	 * @var string
	 */
	protected $color;

	/**
	 * Gets the value of the id field
	 */
	function getId() {
		return $this->id;
	}

	/**
	 * Sets the value of the id field
	 * @return Predio
	 */
	function setId($value) {
		return $this->setColumnValue('id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the numero_predio field
	 */
	function getNumeroPredio() {
		return $this->numero_predio;
	}

	/**
	 * Sets the value of the numero_predio field
	 * @return Predio
	 */
	function setNumeroPredio($value) {
		return $this->setColumnValue('numero_predio', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Convenience function for Predio::getNumeroPredio
	 * final because getNumeroPredio should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getNumeroPredio
	 */
	final function getNumero_predio() {
		return $this->getNumeroPredio();
	}

	/**
	 * Convenience function for Predio::setNumeroPredio
	 * final because setNumeroPredio should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setNumeroPredio
	 * @return Predio
	 */
	final function setNumero_predio($value) {
		return $this->setNumeroPredio($value);
	}

	/**
	 * Gets the value of the area field
	 */
	function getArea() {
		return $this->area;
	}

	/**
	 * Sets the value of the area field
	 * @return Predio
	 */
	function setArea($value) {
		return $this->setColumnValue('area', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Gets the value of the fecha_registro field
	 */
	function getFechaRegistro($format = null) {
		if (null === $this->fecha_registro || null === $format) {
			return $this->fecha_registro;
		}
		if (0 === strpos($this->fecha_registro, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->fecha_registro));
	}

	/**
	 * Sets the value of the fecha_registro field
	 * @return Predio
	 */
	function setFechaRegistro($value) {
		return $this->setColumnValue('fecha_registro', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Predio::getFechaRegistro
	 * final because getFechaRegistro should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getFechaRegistro
	 */
	final function getFecha_registro($format = null) {
		return $this->getFechaRegistro($format);
	}

	/**
	 * Convenience function for Predio::setFechaRegistro
	 * final because setFechaRegistro should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setFechaRegistro
	 * @return Predio
	 */
	final function setFecha_registro($value) {
		return $this->setFechaRegistro($value);
	}

	/**
	 * Gets the value of the familia_id field
	 */
	function getFamiliaId() {
		return $this->familia_id;
	}

	/**
	 * Sets the value of the familia_id field
	 * @return Predio
	 */
	function setFamiliaId($value) {
		return $this->setColumnValue('familia_id', $value, Model::COLUMN_TYPE_INTEGER);
	}

	/**
	 * Convenience function for Predio::getFamiliaId
	 * final because getFamiliaId should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getFamiliaId
	 */
	final function getFamilia_id() {
		return $this->getFamiliaId();
	}

	/**
	 * Convenience function for Predio::setFamiliaId
	 * final because setFamiliaId should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setFamiliaId
	 * @return Predio
	 */
	final function setFamilia_id($value) {
		return $this->setFamiliaId($value);
	}

	/**
	 * Gets the value of the created_at field
	 */
	function getCreatedAt($format = null) {
		if (null === $this->created_at || null === $format) {
			return $this->created_at;
		}
		if (0 === strpos($this->created_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->created_at));
	}

	/**
	 * Sets the value of the created_at field
	 * @return Predio
	 */
	function setCreatedAt($value) {
		return $this->setColumnValue('created_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Predio::getCreatedAt
	 * final because getCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getCreatedAt
	 */
	final function getCreated_at($format = null) {
		return $this->getCreatedAt($format);
	}

	/**
	 * Convenience function for Predio::setCreatedAt
	 * final because setCreatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setCreatedAt
	 * @return Predio
	 */
	final function setCreated_at($value) {
		return $this->setCreatedAt($value);
	}

	/**
	 * Gets the value of the updated_at field
	 */
	function getUpdatedAt($format = null) {
		if (null === $this->updated_at || null === $format) {
			return $this->updated_at;
		}
		if (0 === strpos($this->updated_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->updated_at));
	}

	/**
	 * Sets the value of the updated_at field
	 * @return Predio
	 */
	function setUpdatedAt($value) {
		return $this->setColumnValue('updated_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Predio::getUpdatedAt
	 * final because getUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getUpdatedAt
	 */
	final function getUpdated_at($format = null) {
		return $this->getUpdatedAt($format);
	}

	/**
	 * Convenience function for Predio::setUpdatedAt
	 * final because setUpdatedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setUpdatedAt
	 * @return Predio
	 */
	final function setUpdated_at($value) {
		return $this->setUpdatedAt($value);
	}

	/**
	 * Gets the value of the deleted_at field
	 */
	function getDeletedAt($format = null) {
		if (null === $this->deleted_at || null === $format) {
			return $this->deleted_at;
		}
		if (0 === strpos($this->deleted_at, '0000-00-00')) {
			return null;
		}
		return date($format, strtotime($this->deleted_at));
	}

	/**
	 * Sets the value of the deleted_at field
	 * @return Predio
	 */
	function setDeletedAt($value) {
		return $this->setColumnValue('deleted_at', $value, Model::COLUMN_TYPE_TIMESTAMP);
	}

	/**
	 * Convenience function for Predio::getDeletedAt
	 * final because getDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::getDeletedAt
	 */
	final function getDeleted_at($format = null) {
		return $this->getDeletedAt($format);
	}

	/**
	 * Convenience function for Predio::setDeletedAt
	 * final because setDeletedAt should be extended instead
	 * to ensure consistent behavior
	 * @see Predio::setDeletedAt
	 * @return Predio
	 */
	final function setDeleted_at($value) {
		return $this->setDeletedAt($value);
	}

	/**
	 * Gets the value of the coordenadas field
	 */
	function getCoordenadas() {
		return $this->coordenadas;
	}

	/**
	 * Sets the value of the coordenadas field
	 * @return Predio
	 */
	function setCoordenadas($value) {
		return $this->setColumnValue('coordenadas', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the direccion field
	 */
	function getDireccion() {
		return $this->direccion;
	}

	/**
	 * Sets the value of the direccion field
	 * @return Predio
	 */
	function setDireccion($value) {
		return $this->setColumnValue('direccion', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * Gets the value of the color field
	 */
	function getColor() {
		return $this->color;
	}

	/**
	 * Sets the value of the color field
	 * @return Predio
	 */
	function setColor($value) {
		return $this->setColumnValue('color', $value, Model::COLUMN_TYPE_VARCHAR);
	}

	/**
	 * @return DABLPDO
	 */
	static function getConnection() {
		return DBManager::getConnection('default_connection');
	}

	/**
	 * Searches the database for a row with the ID(primary key) that matches
	 * the one input.
	 * @return Predio
	 */
	static function retrieveByPK($id) {
		return static::retrieveByPKs($id);
	}

	/**
	 * Searches the database for a row with the primary keys that match
	 * the ones input.
	 * @return Predio
	 */
	static function retrieveByPKs($id) {
		if (null === $id) {
			return null;
		}
		if (static::$_poolEnabled) {
			$pool_instance = static::retrieveFromPool($id);
			if (null !== $pool_instance) {
				return $pool_instance;
			}
		}
		$q = new Query;
		$q->add('id', $id);
		return static::doSelectOne($q);
	}

	/**
	 * Searches the database for a row with a id
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveById($value) {
		return Predio::retrieveByPK($value);
	}

	/**
	 * Searches the database for a row with a numero_predio
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByNumeroPredio($value) {
		return static::retrieveByColumn('numero_predio', $value);
	}

	/**
	 * Searches the database for a row with a area
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByArea($value) {
		return static::retrieveByColumn('area', $value);
	}

	/**
	 * Searches the database for a row with a fecha_registro
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByFechaRegistro($value) {
		return static::retrieveByColumn('fecha_registro', $value);
	}

	/**
	 * Searches the database for a row with a familia_id
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByFamiliaId($value) {
		return static::retrieveByColumn('familia_id', $value);
	}

	/**
	 * Searches the database for a row with a created_at
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByCreatedAt($value) {
		return static::retrieveByColumn('created_at', $value);
	}

	/**
	 * Searches the database for a row with a updated_at
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByUpdatedAt($value) {
		return static::retrieveByColumn('updated_at', $value);
	}

	/**
	 * Searches the database for a row with a deleted_at
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByDeletedAt($value) {
		return static::retrieveByColumn('deleted_at', $value);
	}

	/**
	 * Searches the database for a row with a coordenadas
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByCoordenadas($value) {
		return static::retrieveByColumn('coordenadas', $value);
	}

	/**
	 * Searches the database for a row with a direccion
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByDireccion($value) {
		return static::retrieveByColumn('direccion', $value);
	}

	/**
	 * Searches the database for a row with a color
	 * value that matches the one provided
	 * @return Predio
	 */
	static function retrieveByColor($value) {
		return static::retrieveByColumn('color', $value);
	}


	/**
	 * Casts values of int fields to (int)
	 * @return Predio
	 */
	function castInts() {
		$this->id = (null === $this->id) ? null : (int) $this->id;
		$this->area = (null === $this->area) ? null : (int) $this->area;
		$this->familia_id = (null === $this->familia_id) ? null : (int) $this->familia_id;
		return $this;
	}

	/**
	 * @return Predio
	 */
	function setFamilia(Familia $familia = null) {
		return $this->setFamiliaRelatedByFamiliaId($familia);
	}

	/**
	 * @return Predio
	 */
	function setFamiliaRelatedByFamiliaId(Familia $familia = null) {
		if (null === $familia) {
			$this->setfamilia_id(null);
		} else {
			if (!$familia->getid()) {
				throw new Exception('Cannot connect a Familia without a id');
			}
			$this->setfamilia_id($familia->getid());
		}
		return $this;
	}

	/**
	 * Returns a familia object with a id
	 * that matches $this->familia_id.
	 * @return Familia
	 */
	function getFamilia() {
		return $this->getFamiliaRelatedByFamiliaId();
	}

	/**
	 * Returns a familia object with a id
	 * that matches $this->familia_id.
	 * @return Familia
	 */
	function getFamiliaRelatedByFamiliaId() {
		$fk_value = $this->getfamilia_id();
		if (null === $fk_value) {
			return null;
		}
		return Familia::retrieveByPK($fk_value);
	}

	static function doSelectJoinFamilia(Query $q = null, $join_type = Query::LEFT_JOIN) {
		return static::doSelectJoinFamiliaRelatedByFamiliaId($q, $join_type);
	}

	/**
	 * @return Predio[]
	 */
	static function doSelectJoinFamiliaRelatedByFamiliaId(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Familia::getTableName();
		$q->join($to_table, $this_table . '.familia_id = ' . $to_table . '.id', $join_type);
		foreach (Familia::getColumns() as $column) {
			$columns[] = $column;
		}
		$q->setColumns($columns);

		return static::doSelect($q, array('Familia'));
	}

	/**
	 * @return Predio[]
	 */
	static function doSelectJoinAll(Query $q = null, $join_type = Query::LEFT_JOIN) {
		$q = $q ? clone $q : new Query;
		$columns = $q->getColumns();
		$classes = array();
		$alias = $q->getAlias();
		$this_table = $alias ? $alias : static::getTableName();
		if (!$columns) {
			if ($alias) {
				foreach (static::getColumns() as $column_name) {
					$columns[] = $alias . '.' . $column_name;
				}
			} else {
				$columns = static::getColumns();
			}
		}

		$to_table = Familia::getTableName();
		$q->join($to_table, $this_table . '.familia_id = ' . $to_table . '.id', $join_type);
		foreach (Familia::getColumns() as $column) {
			$columns[] = $column;
		}
		$classes[] = 'Familia';
	
		$q->setColumns($columns);
		return static::doSelect($q, $classes);
	}

	/**
	 * Returns a Query for selecting alquiler Objects(rows) from the alquiler table
	 * with a predio_id that matches $this->id.
	 * @return Query
	 */
	function getAlquilersRelatedByPredioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'predio_id', 'id', $q);
	}

	/**
	 * Returns the count of Alquiler Objects(rows) from the alquiler table
	 * with a predio_id that matches $this->id.
	 * @return int
	 */
	function countAlquilersRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Alquiler::doCount($this->getAlquilersRelatedByPredioIdQuery($q));
	}

	/**
	 * Deletes the alquiler Objects(rows) from the alquiler table
	 * with a predio_id that matches $this->id.
	 * @return int
	 */
	function deleteAlquilersRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->AlquilersRelatedByPredioId_c = array();
		return Alquiler::doDelete($this->getAlquilersRelatedByPredioIdQuery($q));
	}

	protected $AlquilersRelatedByPredioId_c = array();

	/**
	 * Returns an array of Alquiler objects with a predio_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Alquiler[]
	 */
	function getAlquilersRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->AlquilersRelatedByPredioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->AlquilersRelatedByPredioId_c;
		}

		$result = Alquiler::doSelect($this->getAlquilersRelatedByPredioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->AlquilersRelatedByPredioId_c = $result;
		}
		return $result;
	}

	/**
	 * Returns a Query for selecting zonas Objects(rows) from the zonas table
	 * with a predio_id that matches $this->id.
	 * @return Query
	 */
	function getZonassRelatedByPredioIdQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'predio_id', 'id', $q);
	}

	/**
	 * Returns the count of Zonas Objects(rows) from the zonas table
	 * with a predio_id that matches $this->id.
	 * @return int
	 */
	function countZonassRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		return Zonas::doCount($this->getZonassRelatedByPredioIdQuery($q));
	}

	/**
	 * Deletes the zonas Objects(rows) from the zonas table
	 * with a predio_id that matches $this->id.
	 * @return int
	 */
	function deleteZonassRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return 0;
		}
		$this->ZonassRelatedByPredioId_c = array();
		return Zonas::doDelete($this->getZonassRelatedByPredioIdQuery($q));
	}

	protected $ZonassRelatedByPredioId_c = array();

	/**
	 * Returns an array of Zonas objects with a predio_id
	 * that matches $this->id.
	 * When first called, this method will cache the result.
	 * After that, if $this->id is not modified, the
	 * method will return the cached result instead of querying the database
	 * a second time(for performance purposes).
	 * @return Zonas[]
	 */
	function getZonassRelatedByPredioId(Query $q = null) {
		if (null === $this->getid()) {
			return array();
		}

		if (
			null === $q
			&& $this->getCacheResults()
			&& !empty($this->ZonassRelatedByPredioId_c)
			&& !$this->isColumnModified('id')
		) {
			return $this->ZonassRelatedByPredioId_c;
		}

		$result = Zonas::doSelect($this->getZonassRelatedByPredioIdQuery($q));

		if ($q !== null) {
			return $result;
		}

		if ($this->getCacheResults()) {
			$this->ZonassRelatedByPredioId_c = $result;
		}
		return $result;
	}

	/**
	 * Convenience function for Predio::getAlquilersRelatedBypredio_id
	 * @return Alquiler[]
	 * @see Predio::getAlquilersRelatedByPredioId
	 */
	function getAlquilers($extra = null) {
		return $this->getAlquilersRelatedByPredioId($extra);
	}

	/**
	  * Convenience function for Predio::getAlquilersRelatedBypredio_idQuery
	  * @return Query
	  * @see Predio::getAlquilersRelatedBypredio_idQuery
	  */
	function getAlquilersQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('alquiler', 'predio_id','id', $q);
	}

	/**
	  * Convenience function for Predio::deleteAlquilersRelatedBypredio_id
	  * @return int
	  * @see Predio::deleteAlquilersRelatedBypredio_id
	  */
	function deleteAlquilers(Query $q = null) {
		return $this->deleteAlquilersRelatedByPredioId($q);
	}

	/**
	  * Convenience function for Predio::countAlquilersRelatedBypredio_id
	  * @return int
	  * @see Predio::countAlquilersRelatedByPredioId
	  */
	function countAlquilers(Query $q = null) {
		return $this->countAlquilersRelatedByPredioId($q);
	}

	/**
	 * Convenience function for Predio::getZonassRelatedBypredio_id
	 * @return Zonas[]
	 * @see Predio::getZonassRelatedByPredioId
	 */
	function getZonass($extra = null) {
		return $this->getZonassRelatedByPredioId($extra);
	}

	/**
	  * Convenience function for Predio::getZonassRelatedBypredio_idQuery
	  * @return Query
	  * @see Predio::getZonassRelatedBypredio_idQuery
	  */
	function getZonassQuery(Query $q = null) {
		return $this->getForeignObjectsQuery('zonas', 'predio_id','id', $q);
	}

	/**
	  * Convenience function for Predio::deleteZonassRelatedBypredio_id
	  * @return int
	  * @see Predio::deleteZonassRelatedBypredio_id
	  */
	function deleteZonass(Query $q = null) {
		return $this->deleteZonassRelatedByPredioId($q);
	}

	/**
	  * Convenience function for Predio::countZonassRelatedBypredio_id
	  * @return int
	  * @see Predio::countZonassRelatedByPredioId
	  */
	function countZonass(Query $q = null) {
		return $this->countZonassRelatedByPredioId($q);
	}

	/**
	 * Returns true if the column values validate.
	 * @return bool
	 */
	function validate() {
		$this->_validationErrors = array();
		if (null === $this->getnumero_predio()) {
			$this->_validationErrors[] = 'numero_predio must not be null';
		}
		if (null === $this->getfamilia_id()) {
			$this->_validationErrors[] = 'familia_id must not be null';
		}
		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}
		if (null === $this->getdireccion()) {
			$this->_validationErrors[] = 'direccion must not be null';
		}
		return 0 === count($this->_validationErrors);
	}

}