/*****
 * CONFIGURATION
 * hotel Mansion @v1
 */
//Main navigation

$.navigation = $('nav > ul.nav');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary =  '#20a8d8';
$.brandSuccess =  '#4dbd74';
$.brandInfo =     '#63c2de';
$.brandWarning =  '#f8cb00';
$.brandDanger =   '#f86c6b';

$.grayDark =      '#2a2c36';
$.gray =          '#55595c';
$.grayLight =     '#818a91';
$.grayLighter =   '#d1d4d7';
$.grayLightest =  '#f8f9fa';

'use strict';

/****
 * MAIN NAVIGATION
 */

$(document).ready(function($){

    $('[data-toggle="tooltip"]').tooltip();
    /*calcular_precio();*/
    var separator = ' - ', dateFormat = "DD/MM/YYYY";
    var options = {
        language: 'en',
        autoUpdateInput: false,
        autoApply: true,
        locale: {
            format: dateFormat,
            separator: separator
        },
        minDate: moment(),
        maxDate: moment().add(359, 'days'),
        opens: "left",
        disableTouchKeyboard: true
    };
    $("input[type=number]").keypress(function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });
    $("#btn_show_search_result").click(function() {
        // display code address
        codeAddress();
        return false;
    });

    $('.datetimepicker').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 1,
        autoApply: true,
        locale: {
            format: 'DD/MM/YYYY h:mm:ss A',
            separator: " - ",
            fromLabel: "Desde",
            toLabel: "A",
            customRangeLabel: "Custom",
            daysOfWeek: [
                "Dom",
                "Lun",
                "Mar",
                "Mié",
                "Jue",
                "Vie",
                "Sáb"
            ],
            monthNames: [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            firstDay: 1
        },
        maxDate: moment().add(359, 'days'),
        disableTouchKeyboard: true
    });
    $("select").select2();
    $('.datepicker').daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            //startDate: '01/01/2013',
            //endDate: '12/11/2013',
            locale: {
                format: "DD/MM/YYYY",
                separator: " - ",
                fromLabel: "Desde",
                toLabel: "A",
                customRangeLabel: "Custom",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mié",
                    "Jue",
                    "Vie",
                    "Sáb"
                ],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                firstDay: 1
            }
        });




    // Add class .active to current link
    $.navigation.find('a').each(function(){

        var cUrl = String(window.location).split('?')[0];

        if (cUrl.substr(cUrl.length - 1) == '#') {
            cUrl = cUrl.slice(0,-1);
        }

        if ($($(this))[0].href==cUrl) {
            $(this).addClass('active');

            $(this).parents('ul').add(this).each(function(){
                $(this).parent().addClass('open');
            });
        }
    });

    // Dropdown Menu
    $.navigation.on('click', 'a', function(e){

        if ($.ajaxLoad) {
            e.preventDefault();
        }

        if ($(this).hasClass('nav-dropdown-toggle')) {
            $(this).parent().toggleClass('open');
            resizeBroadcast();
        }

    });

    function resizeBroadcast() {

        var timesRun = 0;
        var interval = setInterval(function(){
            timesRun += 1;
            if(timesRun === 5){
                clearInterval(interval);
            }
            window.dispatchEvent(new Event('resize'));
        }, 62.5);
    }

    /* ---------- Main Menu Open/Close, Min/Full ---------- */
    $('.sidebar-toggler').click(function(){
        $('body').toggleClass('sidebar-hidden');
        resizeBroadcast();
    });

    $('.sidebar-minimizer').click(function(){
        $('body').toggleClass('sidebar-minimized');
        resizeBroadcast();
    });

    $('.brand-minimizer').click(function(){
        $('body').toggleClass('brand-minimized');
    });

    $('.aside-menu-toggler').click(function(){
        $('body').toggleClass('aside-menu-hidden');
        resizeBroadcast();
    });

    $('.mobile-sidebar-toggler').click(function(){
        $('body').toggleClass('sidebar-mobile-show');
        resizeBroadcast();
    });

    $('.sidebar-close').click(function(){
        $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
    });

    /* ---------- Disable moving to top ---------- */
    $('a[href="#"][data-top!=true]').click(function(e){
        e.preventDefault();
    });

    if(document.getElementById('map_canvas') != null){
        initialize();
    }

});

function readUrl(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#predio_plano').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

    /* ---------- Tooltip ---------- */
    $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

    /* ---------- Popover ---------- */
    $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}

function buscar_tabla_html(t) {
    var rex = new RegExp($(t).val(), 'i');
    $('tbody tr').hide();
    $('tbody tr').filter(function () {
        return rex.test($(this).text());
    }).show();
}

function buscar_tabla_ajax(t) {
    var data = $(t).val();
    var url = $(t).attr('href');
    history.pushState('data', '', url+data);
    $('#grilla').load(url+data+" #grilla");
}

function openmodal(t) {
    u = $(t).attr('href');
    $('#primaryModal').modal('show');
    $(".modal-body").load(u+" #usa",function () {
        var aa = $("#primaryModal h4").clone();
        $("#primaryModal h4").remove();
        $(".modal-header").prepend(aa);
        $("select").select2();
        $('.datepicker').daterangepicker({
            autoApply: true,
            singleDatePicker: true,
            //startDate: '01/01/2013',
            //endDate: '12/11/2013',
            locale: {
                format: "DD/MM/YYYY",
                separator: " - ",
                fromLabel: "Desde",
                toLabel: "A",
                customRangeLabel: "Custom",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mié",
                    "Jue",
                    "Vie",
                    "Sáb"
                ],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                firstDay: 1
            }
        });
        $('.datetimepicker').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePickerIncrement: 1,
            autoApply: true,
            locale: {
                format: 'DD/MM/YYYY h:mm:ss A',
                separator: " - ",
                fromLabel: "Desde",
                toLabel: "A",
                customRangeLabel: "Custom",
                daysOfWeek: [
                    "Dom",
                    "Lun",
                    "Mar",
                    "Mié",
                    "Jue",
                    "Vie",
                    "Sáb"
                ],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                firstDay: 1
            },
            maxDate: moment().add(359, 'days'),
            disableTouchKeyboard: true
        });
    });

}

//creado por Gianela
function validarusuario(){
    $('#usuarios_claves').keyup(function(){
        var pass_1 = $('#usuarios_clave').val();
        var pass_2 = $('#usuarios_claves').val();
        var _this = $('#usuarios_claves');
        _this.attr('style', 'background:white');
        if(pass_1 != pass_2 && pass_2 != ''){
            _this.attr('style', 'background:red');
            $("#mensaje").show();
            $("#mensaje").html("Las contraseñas no coinciden").css("color","red");
            $('#btnEnviar').attr('disabled', true);
        }else if(pass_1 == pass_2){
            $("#mensaje").html("Las contraseñas correctas").css("color","green");
            $('#btnEnviar').attr('disabled',false);
        }else {
            $("#mensaje").hide();
        }
    });
}
//creado por Gianela
function soloLetras(e){
    var key = e.keyCode || e.which;
    var tecla = String.fromCharCode(key).toLowerCase();
    var letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    var especiales = "8-37-39-46";

    var tecla_especial = false
    for(var i in especiales){
        if(key == especiales[i]){
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla)==-1 && !tecla_especial){
        return false;
    }
}
//creado por Gianela
function numeros(e) {
   var key = e.keyCode || e.which;
   var tecla = String.fromCharCode(key).toLowerCase();
  var  letras = "*#0123456789";
   var especiales = [8, 37, 39, 46];

   var tecla_especial = false
    for(var i in especiales) {
        if(key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if(letras.indexOf(tecla) == -1 && !tecla_especial)
        return false;
}
//creado por Gianela
function tiposclientes(t) {

    if ($(t).val()==1){

        $('.juridico').prop('disabled',false).removeClass('hidden');
        $("#id1").html("<p> DNI:</p>");
        $("#dni").focusout(function () {
            if($(this).val().length>0 && $(this).val().length<=7) {
                $("#mensaje").html("<span style='color:red'> DNI INCOMPLETO</span>");
                $('#btnEnviar').attr('disabled',true);
            }
            else if($(this).val().length==8) {
                $("#mensaje").html("<span style='color:blue'>DNI VALIDO</span>");
                $('#btnEnviar').attr('disabled',false);
            }
            else{
                $("#mensaje").html("<span style='color:red'> Error! maximo 8 caracteres</span>");
                $('#btnEnviar').attr('disabled',true);
            }
        });

    }else{
        $('.juridico').prop('disabled',true).addClass('hidden');
        $("#id1").html("<p> Ruc:</p>");
        $("#dni").focusout(function () {
            if ($(this).val().length>0 && $(this).val().length<=10) {
                $("#mensaje").html("<span style='color:red; display: inline' >RUC INCOMPLETO</span>");
                $('#btnEnviar').attr('disabled', true);
            } else if ($(this).val().length==11) {
                $("#mensaje").html("<span style='color:blue'> RUC Valido</span>")
                $('#btnEnviar').attr('disabled',false);
            }
            else {
                $("#mensaje").html("<span style='color:red'> Error! maximo 11 caracteres</span>")
                $('#btnEnviar').attr('disabled', true);
            }
        });
    }


}

function conf_eliminar(tit,preg,url) {
    $.confirm({
        title: tit,
        content: preg,
        type: 'red',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: 'Eliminar',
                btnClass: 'btn-red',
                action: function(){
                    window.location.href = url;
                }
            },
            cerrar: function () {
            }
        }
    });
}
//creado por Domi
function  validar_dni() {
    $("#dni").keyup(function () {
        if($(this).val().length>0 && $(this).val().length<=7) {
            $("#mensaje").html("<span style='color:red'> DNI INCOMPLETO</span>");
        }
        else if($(this).val().length==8) {
            $("#mensaje").html("<span style='color:blue'>DNI VALIDO</span>")
        }
        else{
            $("#mensaje").html("<span style='color:green'> Error! maximo 8 caracteres</span>")
        }
    });
}

