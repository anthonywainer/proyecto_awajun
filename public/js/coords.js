function autocomplete(){
    var input = document.getElementById('search_address');
    var autocomplete = new google.maps.places.Autocomplete(input);
}
google.maps.event.addDomListener(window, 'load', autocomplete);


var map = null;

var default_zoom = 18;
var map_div = "map_canvas";
var measure;
var terreno = [];
var predioPath;

function initialize() {
    boo= false;
    if ($("#idpredio").val()==""){
        var default_lat = -5.814462088065862;
        var default_lng = -77.38361813176726;
    }else {
        boo=true;
        coordenadas_predio = eval($("input[name=coordenadas]").val());
        default_lat = coordenadas_predio[0].lat;
        default_lng = coordenadas_predio[0].lng;
    }


    measure = {
        mvcLine: new google.maps.MVCArray(),
        mvcPolygon: new google.maps.MVCArray(),
        mvcMarkers: new google.maps.MVCArray(),
        line: null,
        polygon: null
    };

    var latlng = new google.maps.LatLng(default_lat, default_lng);


    var mapOptions = {
        scaleControl: true,
        zoom: default_zoom,
        zoomControl: true,
        zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
        panControl: false,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        draggableCursor: 'crosshair'
    };


    map = new google.maps.Map(document.getElementById(map_div), mapOptions);

    map.setTilt(0);

    if (boo) {
        predioPath = new google.maps.Polygon({
            path: coordenadas_predio,
            geodesic: true,
            strokeColor: $("#colorp").val(),
            strokeOpacity: 1.0,
            strokeWeight: 2,
            fillColor: $("#colorp").val(),
            fillOpacity: 0.8
        });

        predioPath.setMap(map);
    }

    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);
    });

}

function codeAddress() {
    var geocoder = new google.maps.Geocoder();
    var address = document.getElementById('search_address').value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            placeMarker(results[0].geometry.location);
        } else {
            alert('Address not found , reason: ' + status);
        }
    });
}

function placeMarker(latLng) {
    terreno.push("{'lat':"+latLng.lat()+",'lng':"+latLng.lng()+"}");
    var marker = new google.maps.Marker({map: map, position: latLng, draggable: true});
    measure.mvcLine.push(latLng);
    measure.mvcPolygon.push(latLng);
    measure.mvcMarkers.push(marker);
    var latLngIndex = measure.mvcLine.getLength() - 1;
    google.maps.event.addListener(marker, "drag", function(evt) {
        measure.mvcLine.setAt(latLngIndex, evt.latLng);
        measure.mvcPolygon.setAt(latLngIndex, evt.latLng);
    });
    google.maps.event.addListener(marker, "dragend", function() {
        if (measure.mvcLine.getLength() > 1) {
            compute_length();
        }
    });

    display_polygon_line();
}

function display_polygon_line() {
    color = ($("#colorz").val()? $("#colorz").val(): $("#colorp").val());
    if (measure.mvcLine.getLength() > 1) {
        if (!measure.line) {
            measure.line = new google.maps.Polyline({
                map: map,
                clickable: false,
                strokeColor:color,
                strokeOpacity: 1,
                strokeWeight: 3,
                path: measure.mvcLine
            });
        }

        if (measure.mvcPolygon.getLength() > 2) {

            if (measure.polygon != null)
            {
                measure.polygon.setMap(null);
            }

            measure.polygon = new google.maps.Polygon({
                clickable: false,
                map: map,
                fillOpacity: 0.35,
                strokeOpacity: 0,
                paths: measure.mvcPolygon
            });

        }
    }
    if (measure.mvcLine.getLength() > 1) {
        compute_length();
    }
}

function compute_length() {
    var area = 0;
    if (measure.mvcPolygon.getLength() > 2) {
        area = google.maps.geometry.spherical.computeArea(measure.polygon.getPath());
    }
    var km = area / 10000;
    var unit = " m&sup2;";
    var unit2 = " Hectárea;";
    document.getElementById('polyArea').innerHTML =  area.toFixed(0) + unit + "<br/>" + km.toFixed(3) + unit2;
    $("input[name=area]").val(area.toFixed(0));
    $("#plano-coordenadas").val("["+terreno.toString()+"]");
    $("#plano-coordenadas-p").html("["+terreno.toString()+"]");

}

function removeLastMarker() {
    var test = measure.mvcLine.pop();
    terreno.pop();
    if (test)
    {
        measure.mvcPolygon.pop();
        var marker = measure.mvcMarkers.pop();
        marker.setMap(null);
        compute_length();
    }
}

function clearMarkers() {
    terreno.length=0;
    if (measure.polygon) {
        measure.polygon.setMap(null);
        measure.polygon = null;
    }
    if (measure.line) {
        measure.line.setMap(null);
        measure.line = null
    }
    measure.mvcLine.clear();
    measure.mvcPolygon.clear();
    measure.mvcMarkers.forEach(function(elem, index) {
        elem.setMap(null);
    });
    measure.mvcMarkers.clear();
    document.getElementById('polyArea').innerHTML = '';
    predioPath.setMap(null);
}
