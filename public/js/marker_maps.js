function geoMap(t){
    input = $(t).val();
    var latlngStr = input.split(',', 2);
    lat= parseFloat(latlngStr[0]);
    lng= parseFloat(latlngStr[1]);
    getAddressByCoordinates(lat,lng)
}
function getCoordinatesByAddress(t){
    address = $(t).val();
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var geolocate = new google.maps.LatLng(latitude, longitude);
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                mapTypeId: 'satellite',
                center: {
                    lat: latitude,
                    lng: longitude
                }
            });
            placeMarker(geolocate,geocoder,map);
        }
    });
}


function getAddressByCoordinates(latitude,longitude){
    var geocoder = new google.maps.Geocoder;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: {
            lat: latitude,
            lng: longitude
        }
    });

    var geolocate = new google.maps.LatLng(latitude, longitude);
    placeMarker(geolocate,geocoder,map);
}

function placeMarker(location,geocoder,map) {
    document.getElementById("coords").value = location.lat()+","+ location.lng();
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP
    });
    marker.addListener('click', toggleBounce);

    marker.addListener( 'dragend', function (event)
    {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
        document.getElementById("coords").value = this.getPosition().lat()+","+ this.getPosition().lng();
        getAddressByCoordinates(this.getPosition().lat(),this.getPosition().lng())
    });

    var address="No resolved address";
    geocoder.geocode({'location': location},
        function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    console.log(results);
                    address = results[0].formatted_address;
                } else {
                    // error; do nothing
                }
            } else {
                // error; do nothing

            }
            infowindow = new google.maps.InfoWindow({
                map: map,
                position: location,
                content:
                '<p><strong>Geolocation:</strong> '+address +'</p>' +
                '<p><strong>Latitude:</strong> ' +  location.lat()+ '</p>' +
                '<p><strong>Longitude:</strong> ' + location.lng() +'</p>'
            });
            infowindow.open(map, marker);
        }
    );
}

//callback al hacer clic en el marcador lo que hace es quitar y poner la animacion BOUNCE
function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}

// Carga de la libreria de google maps