<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class RolMiembrosController extends ApplicationController {

	/**
	 * Returns all RolMiembro records matching the query. Examples:
	 * GET /rol-miembros?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/rol-miembros.json&limit=5
	 *
	 * @return RolMiembro[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = RolMiembro::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'RolMiembro';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['rol_miembros'] = $this['pager']->fetchPage();
        $this['u'] = 'rol-miembros/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a RolMiembro. Example:
	 * GET /rol-miembros/edit/1
	 *
	 * @return RolMiembro
	 */
	function editar($id = null) {
		$this->getRolMiembro($id)->fromArray(@$_GET);
        $this['u'] = 'rol-miembros/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a RolMiembro. Examples:
	 * POST /rol-miembros/save/1
	 * POST /rest/rol-miembros/.json
	 * PUT /rest/rol-miembros/1.json
	 */
	function guardar($id = null) {
		$rol_miembro = $this->getRolMiembro($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$rol_miembro->fromArray($_REQUEST);
			if ($rol_miembro->validate()) {
				$rol_miembro->save();
				$this->flash['messages'][] = 'Rol Miembro guardado';
				$this->redirect('rol-miembros');
			}
			$this->flash['errors'] = $rol_miembro->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('rol-miembros/editar/' . $rol_miembro->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the RolMiembro with the id. Examples:
	 * GET /rol-miembros/show/1
	 * GET /rest/rol-miembros/1.json
	 *
	 * @return RolMiembro
	 */
	function mostrar($id = null) {
		$this->getRolMiembro($id);
        $this['u'] = 'rol-miembros/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the RolMiembro with the id. Examples:
	 * GET /rol-miembros/delete/1
	 * DELETE /rest/rol-miembros/1.json
	 */
	function eliminar($id = null) {
        $rol_miembro = $this->getRolMiembro($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $rol_miembro->fromArray($_REQUEST);

		try {
			if (null !== $rol_miembro && $rol_miembro->save()) {
				$this['messages'][] = 'Rol Miembro eliminado';
			} else {
				$this['errors'][] = 'Rol Miembro no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('rol-miembros');
		}
	}

	/**
	 * @return RolMiembro
	 */
	private function getRolMiembro($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[RolMiembro::getPrimaryKey()])) {
			$id = $_REQUEST[RolMiembro::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new RolMiembro
			$this['rol_miembro'] = new RolMiembro;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['rol_miembro'] = RolMiembro::retrieveByPK($id);
		}
		return $this['rol_miembro'];
	}

}