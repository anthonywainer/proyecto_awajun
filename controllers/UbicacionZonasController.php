<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class UbicacionZonasController extends ApplicationController {

	/**
	 * Returns all UbicacionZona records matching the query. Examples:
	 * GET /ubicacion-zonas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ubicacion-zonas.json&limit=5
	 *
	 * @return UbicacionZona[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = UbicacionZona::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'UbicacionZona';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['ubicacion_zonas'] = $this['pager']->fetchPage();
        $this['u'] = 'ubicacion-zonas/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a UbicacionZona. Example:
	 * GET /ubicacion-zonas/edit/1
	 *
	 * @return UbicacionZona
	 */
	function editar($id = null) {
		$this->getUbicacionZona($id)->fromArray(@$_GET);
        $this['u'] = 'ubicacion-zonas/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a UbicacionZona. Examples:
	 * POST /ubicacion-zonas/save/1
	 * POST /rest/ubicacion-zonas/.json
	 * PUT /rest/ubicacion-zonas/1.json
	 */
	function guardar($id = null) {
		$ubicacion_zona = $this->getUbicacionZona($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$ubicacion_zona->fromArray($_REQUEST);
			if ($ubicacion_zona->validate()) {
				$ubicacion_zona->save();
				$this->flash['messages'][] = 'Ubicacion Zona guardado';
				$this->redirect('ubicacion-zonas');
			}
			$this->flash['errors'] = $ubicacion_zona->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('ubicacion-zonas/editar/' . $ubicacion_zona->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the UbicacionZona with the id. Examples:
	 * GET /ubicacion-zonas/show/1
	 * GET /rest/ubicacion-zonas/1.json
	 *
	 * @return UbicacionZona
	 */
	function mostrar($id = null) {
		$this->getUbicacionZona($id);
        $this['u'] = 'ubicacion-zonas/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the UbicacionZona with the id. Examples:
	 * GET /ubicacion-zonas/delete/1
	 * DELETE /rest/ubicacion-zonas/1.json
	 */
	function eliminar($id = null) {
        $ubicacion_zona = $this->getUbicacionZona($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $ubicacion_zona->fromArray($_REQUEST);

		try {
			if (null !== $ubicacion_zona && $ubicacion_zona->save()) {
				$this['messages'][] = 'Ubicacion Zona eliminado';
			} else {
				$this['errors'][] = 'Ubicacion Zona no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ubicacion-zonas');
		}
	}

	/**
	 * @return UbicacionZona
	 */
	private function getUbicacionZona($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[UbicacionZona::getPrimaryKey()])) {
			$id = $_REQUEST[UbicacionZona::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new UbicacionZona
			$this['ubicacion_zona'] = new UbicacionZona;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ubicacion_zona'] = UbicacionZona::retrieveByPK($id);
		}
		return $this['ubicacion_zona'];
	}

}