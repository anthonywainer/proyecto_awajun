<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class FamiliasController extends ApplicationController {

	/**
	 * Returns all Familia records matching the query. Examples:
	 * GET /familias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/familias.json&limit=5
	 *
	 * @return Familia[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Familia::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Familia';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['familias'] = $this['pager']->fetchPage();
        $this['u'] = 'familias/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Familia. Example:
	 * GET /familias/edit/1
	 *
	 * @return Familia
	 */
	function editar($id = null) {
		$this->getFamilia($id)->fromArray(@$_GET);
        $this['u'] = 'familias/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Familia. Examples:
	 * POST /familias/save/1
	 * POST /rest/familias/.json
	 * PUT /rest/familias/1.json
	 */
	function guardar($id = null) {
		$familia = $this->getFamilia($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$familia->fromArray($_REQUEST);
			if ($familia->validate()) {
				$familia->save();
				$this->flash['messages'][] = 'Familia guardado';
				$this->redirect('familias');
			}
			$this->flash['errors'] = $familia->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('familias/editar/' . $familia->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Familia with the id. Examples:
	 * GET /familias/show/1
	 * GET /rest/familias/1.json
	 *
	 * @return Familia
	 */
	function mostrar($id = null) {
		$this->getFamilia($id);
        $this['u'] = 'familias/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Familia with the id. Examples:
	 * GET /familias/delete/1
	 * DELETE /rest/familias/1.json
	 */
	function eliminar($id = null) {
        $familia = $this->getFamilia($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $familia->fromArray($_REQUEST);

		try {
			if (null !== $familia && $familia->save()) {
				$this['messages'][] = 'Familia eliminado';
			} else {
				$this['errors'][] = 'Familia no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('familias');
		}
	}

	/**
	 * @return Familia
	 */
	private function getFamilia($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Familia::getPrimaryKey()])) {
			$id = $_REQUEST[Familia::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Familia
			$this['familia'] = new Familia;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['familia'] = Familia::retrieveByPK($id);
		}
		return $this['familia'];
	}

}