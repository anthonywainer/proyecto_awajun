<?php

class IndexController extends ApplicationController {
	function index() {
        $this->layout = 'layouts/main-web';
        #$this->redirect('/login'); #comentar si hay pagina web
	}
	function admin(){
        if ($_SESSION) {
            return $this['u'] = 'dashboard/index';
        }else{
            $this->redirect('/');
        }
    }
    function logout(){
        session_destroy();
        $this->redirect('/');
    }

    function nosotros(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/nosotros",$this);
    }
    function servicios(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/servicios",$this);
    }
    function galeria(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/galeria",$this);
    }
    function contactenos(){
        $this->layout = 'layouts/main-web';
        $this->loadView("paginaweb/contactenos",$this);

    }
}