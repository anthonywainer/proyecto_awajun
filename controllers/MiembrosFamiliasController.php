<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class MiembrosFamiliasController extends ApplicationController {

	/**
	 * Returns all MiembrosFamilia records matching the query. Examples:
	 * GET /miembros-familias?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/miembros-familias.json&limit=5
	 *
	 * @return MiembrosFamilia[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = MiembrosFamilia::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'MiembrosFamilia';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['miembros_familias'] = $this['pager']->fetchPage();
        $this['u'] = 'miembros-familias/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a MiembrosFamilia. Example:
	 * GET /miembros-familias/edit/1
	 *
	 * @return MiembrosFamilia
	 */
	function editar($id = null) {
		$this->getMiembrosFamilia($id)->fromArray(@$_GET);
        $this['u'] = 'miembros-familias/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a MiembrosFamilia. Examples:
	 * POST /miembros-familias/save/1
	 * POST /rest/miembros-familias/.json
	 * PUT /rest/miembros-familias/1.json
	 */
	function guardar($id = null) {
		$miembros_familia = $this->getMiembrosFamilia($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$miembros_familia->fromArray($_REQUEST);
			if ($miembros_familia->validate()) {
				$miembros_familia->save();
				$this->flash['messages'][] = 'Miembros Familia guardado';
				$this->redirect('miembros-familias');
			}
			$this->flash['errors'] = $miembros_familia->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('miembros-familias/editar/' . $miembros_familia->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the MiembrosFamilia with the id. Examples:
	 * GET /miembros-familias/show/1
	 * GET /rest/miembros-familias/1.json
	 *
	 * @return MiembrosFamilia
	 */
	function mostrar($id = null) {
		$this->getMiembrosFamilia($id);
        $this['u'] = 'miembros-familias/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the MiembrosFamilia with the id. Examples:
	 * GET /miembros-familias/delete/1
	 * DELETE /rest/miembros-familias/1.json
	 */
	function eliminar($id = null) {
        $miembros_familia = $this->getMiembrosFamilia($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $miembros_familia->fromArray($_REQUEST);

		try {
			if (null !== $miembros_familia && $miembros_familia->save()) {
				$this['messages'][] = 'Miembros Familia eliminado';
			} else {
				$this['errors'][] = 'Miembros Familia no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('miembros-familias');
		}
	}

	/**
	 * @return MiembrosFamilia
	 */
	private function getMiembrosFamilia($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[MiembrosFamilia::getPrimaryKey()])) {
			$id = $_REQUEST[MiembrosFamilia::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new MiembrosFamilia
			$this['miembros_familia'] = new MiembrosFamilia;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['miembros_familia'] = MiembrosFamilia::retrieveByPK($id);
		}
		return $this['miembros_familia'];
	}

}