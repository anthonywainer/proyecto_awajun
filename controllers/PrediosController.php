<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class PrediosController extends ApplicationController {

	/**
	 * Returns all Predio records matching the query. Examples:
	 * GET /predios?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/predios.json&limit=5
	 *
	 * @return Predio[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Predio::getQuery(@$_GET)->andNull('deleted_at')
            ->andLike('predio.numero_predio','%'.$_GET['search'].'%');
		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Predio';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['predios'] = $this['pager']->fetchPage();
        $this['u'] = 'predios/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Predio. Example:
	 * GET /predios/edit/1
	 *
	 * @return Predio
	 */
	function editar($id = null) {
		$this->getPredio($id)->fromArray(@$_GET);
        $this['u'] = 'predios/edit';
        return $this->loadView('admin/index',$this);
	}


	function zonas($id = null){
        $this->getPredio($id)->fromArray(@$_GET);
        $this['u'] = 'predios/zonas';
        return $this->loadView('admin/index',$this);
    }

	/**
	 * Saves a Predio. Examples:
	 * POST /predios/save/1
	 * POST /rest/predios/.json
	 * PUT /rest/predios/1.json
	 */
	function guardar($id = null) {
		$predio = $this->getPredio($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
            $value = $_REQUEST['fecha_registro'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_registro'] = $value;
			$predio->fromArray($_REQUEST);
			if ($predio->validate()) {
				$predio->save();
				$this->flash['messages'][] = 'Predio guardado';
				$this->redirect('predios');
			}
			$this->flash['errors'] = $predio->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('predios/editar/' . $predio->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Predio with the id. Examples:
	 * GET /predios/show/1
	 * GET /rest/predios/1.json
	 *
	 * @return Predio
	 */
	function mostrar($id = null) {
		$this->getPredio($id);
        $this['u'] = 'predios/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Predio with the id. Examples:
	 * GET /predios/delete/1
	 * DELETE /rest/predios/1.json
	 */
	function eliminar($id = null) {
        $predio = $this->getPredio($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $predio->fromArray($_REQUEST);

		try {
			if (null !== $predio && $predio->save()) {
				$this['messages'][] = 'Predio eliminado';
			} else {
				$this['errors'][] = 'Predio no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('predios');
		}
	}

	/**
	 * @return Predio
	 */
	private function getPredio($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Predio::getPrimaryKey()])) {
			$id = $_REQUEST[Predio::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Predio
			$this['predio'] = new Predio;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['predio'] = Predio::retrieveByPK($id);
		}
		return $this['predio'];
	}

}