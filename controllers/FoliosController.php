<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class FoliosController extends ApplicationController {

	/**
	 * Returns all Folio records matching the query. Examples:
	 * GET /folios?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/folios.json&limit=5
	 *
	 * @return Folio[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Folio::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Folio';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['folios'] = $this['pager']->fetchPage();
        $this['u'] = 'folios/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Folio. Example:
	 * GET /folios/edit/1
	 *
	 * @return Folio
	 */
	function editar($id = null) {
		$this->getFolio($id)->fromArray(@$_GET);
        $this['u'] = 'folios/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Folio. Examples:
	 * POST /folios/save/1
	 * POST /rest/folios/.json
	 * PUT /rest/folios/1.json
	 */
	function guardar($id = null) {
		$folio = $this->getFolio($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$folio->fromArray($_REQUEST);
			if ($folio->validate()) {
				$folio->save();
				$this->flash['messages'][] = 'Folio guardado';
				$this->redirect('folios');
			}
			$this->flash['errors'] = $folio->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('folios/editar/' . $folio->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Folio with the id. Examples:
	 * GET /folios/show/1
	 * GET /rest/folios/1.json
	 *
	 * @return Folio
	 */
	function mostrar($id = null) {
		$this->getFolio($id);
        $this['u'] = 'folios/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Folio with the id. Examples:
	 * GET /folios/delete/1
	 * DELETE /rest/folios/1.json
	 */
	function eliminar($id = null) {
        $folio = $this->getFolio($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $folio->fromArray($_REQUEST);

		try {
			if (null !== $folio && $folio->save()) {
				$this['messages'][] = 'Folio eliminado';
			} else {
				$this['errors'][] = 'Folio no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('folios');
		}
	}

	/**
	 * @return Folio
	 */
	private function getFolio($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Folio::getPrimaryKey()])) {
			$id = $_REQUEST[Folio::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Folio
			$this['folio'] = new Folio;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['folio'] = Folio::retrieveByPK($id);
		}
		return $this['folio'];
	}

}