<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class UbigeosController extends ApplicationController {

	/**
	 * Returns all Ubigeo records matching the query. Examples:
	 * GET /ubigeos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/ubigeos.json&limit=5
	 *
	 * @return Ubigeo[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Ubigeo::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Ubigeo';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['ubigeos'] = $this['pager']->fetchPage();
        $this['u'] = 'ubigeos/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Ubigeo. Example:
	 * GET /ubigeos/edit/1
	 *
	 * @return Ubigeo
	 */
	function editar($id = null) {
		$this->getUbigeo($id)->fromArray(@$_GET);
        $this['u'] = 'ubigeos/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Ubigeo. Examples:
	 * POST /ubigeos/save/1
	 * POST /rest/ubigeos/.json
	 * PUT /rest/ubigeos/1.json
	 */
	function guardar($id = null) {
		$ubigeo = $this->getUbigeo($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$ubigeo->fromArray($_REQUEST);
			if ($ubigeo->validate()) {
				$ubigeo->save();
				$this->flash['messages'][] = 'Ubigeo guardado';
				$this->redirect('ubigeos');
			}
			$this->flash['errors'] = $ubigeo->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('ubigeos/editar/' . $ubigeo->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Ubigeo with the id. Examples:
	 * GET /ubigeos/show/1
	 * GET /rest/ubigeos/1.json
	 *
	 * @return Ubigeo
	 */
	function mostrar($id = null) {
		$this->getUbigeo($id);
        $this['u'] = 'ubigeos/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Ubigeo with the id. Examples:
	 * GET /ubigeos/delete/1
	 * DELETE /rest/ubigeos/1.json
	 */
	function eliminar($id = null) {
        $ubigeo = $this->getUbigeo($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $ubigeo->fromArray($_REQUEST);

		try {
			if (null !== $ubigeo && $ubigeo->save()) {
				$this['messages'][] = 'Ubigeo eliminado';
			} else {
				$this['errors'][] = 'Ubigeo no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('ubigeos');
		}
	}

	/**
	 * @return Ubigeo
	 */
	private function getUbigeo($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Ubigeo::getPrimaryKey()])) {
			$id = $_REQUEST[Ubigeo::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Ubigeo
			$this['ubigeo'] = new Ubigeo;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['ubigeo'] = Ubigeo::retrieveByPK($id);
		}
		return $this['ubigeo'];
	}

}