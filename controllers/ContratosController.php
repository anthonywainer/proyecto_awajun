<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ContratosController extends ApplicationController {

	/**
	 * Returns all Contrato records matching the query. Examples:
	 * GET /contratos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/contratos.json&limit=5
	 *
	 * @return Contrato[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Contrato::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Contrato';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['contratos'] = $this['pager']->fetchPage();
        $this['u'] = 'contratos/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Contrato. Example:
	 * GET /contratos/edit/1
	 *
	 * @return Contrato
	 */
	function editar($id = null) {
		$this->getContrato($id)->fromArray(@$_GET);
        $this['u'] = 'contratos/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Contrato. Examples:
	 * POST /contratos/save/1
	 * POST /rest/contratos/.json
	 * PUT /rest/contratos/1.json
	 */
	function guardar($id = null) {
		$contrato = $this->getContrato($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');
            $value = $_REQUEST['fecha_contrato'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_contrato'] = $value;

            $value = $_REQUEST['fecha_inicio'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_inicio'] = $value;

            $value = $_REQUEST['fecha_termino'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_termino'] = $value;

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$contrato->fromArray($_REQUEST);
			if ($contrato->validate()) {
				$contrato->save();
                if ($_REQUEST['json']){
                    print_r($contrato->getId()); exit();
                }
				$this->flash['messages'][] = 'Contrato guardado';
				$this->redirect('contratos');
			}
			$this->flash['errors'] = $contrato->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('contratos/editar/' . $contrato->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Contrato with the id. Examples:
	 * GET /contratos/show/1
	 * GET /rest/contratos/1.json
	 *
	 * @return Contrato
	 */
	function mostrar($id = null) {
		$this->getContrato($id);
        $this['u'] = 'contratos/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Contrato with the id. Examples:
	 * GET /contratos/delete/1
	 * DELETE /rest/contratos/1.json
	 */
	function eliminar($id = null) {
        $contrato = $this->getContrato($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $contrato->fromArray($_REQUEST);

		try {
			if (null !== $contrato && $contrato->save()) {
				$this['messages'][] = 'Contrato eliminado';
			} else {
				$this['errors'][] = 'Contrato no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('contratos');
		}
	}

	/**
	 * @return Contrato
	 */
	private function getContrato($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Contrato::getPrimaryKey()])) {
			$id = $_REQUEST[Contrato::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Contrato
			$this['contrato'] = new Contrato;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['contrato'] = Contrato::retrieveByPK($id);
		}
		return $this['contrato'];
	}

}