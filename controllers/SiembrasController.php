<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class SiembrasController extends ApplicationController {

	/**
	 * Returns all Siembra records matching the query. Examples:
	 * GET /siembras?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/siembras.json&limit=5
	 *
	 * @return Siembra[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Siembra::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Siembra';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['siembras'] = $this['pager']->fetchPage();
        $this['u'] = 'siembras/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Siembra. Example:
	 * GET /siembras/edit/1
	 *
	 * @return Siembra
	 */
	function editar($id = null) {
		$this->getSiembra($id)->fromArray(@$_GET);
        $this['u'] = 'siembras/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Siembra. Examples:
	 * POST /siembras/save/1
	 * POST /rest/siembras/.json
	 * PUT /rest/siembras/1.json
	 */
	function guardar($id = null) {
		$siembra = $this->getSiembra($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$siembra->fromArray($_REQUEST);
			if ($siembra->validate()) {
				$siembra->save();
				$this->flash['messages'][] = 'Siembra guardado';
				$this->redirect('siembras');
			}
			$this->flash['errors'] = $siembra->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('siembras/editar/' . $siembra->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Siembra with the id. Examples:
	 * GET /siembras/show/1
	 * GET /rest/siembras/1.json
	 *
	 * @return Siembra
	 */
	function mostrar($id = null) {
		$this->getSiembra($id);
        $this['u'] = 'siembras/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Siembra with the id. Examples:
	 * GET /siembras/delete/1
	 * DELETE /rest/siembras/1.json
	 */
	function eliminar($id = null) {
        $siembra = $this->getSiembra($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $siembra->fromArray($_REQUEST);

		try {
			if (null !== $siembra && $siembra->save()) {
				$this['messages'][] = 'Siembra eliminado';
			} else {
				$this['errors'][] = 'Siembra no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('siembras');
		}
	}

	/**
	 * @return Siembra
	 */
	private function getSiembra($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Siembra::getPrimaryKey()])) {
			$id = $_REQUEST[Siembra::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Siembra
			$this['siembra'] = new Siembra;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['siembra'] = Siembra::retrieveByPK($id);
		}
		return $this['siembra'];
	}

}