<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class TipoZonasController extends ApplicationController {

	/**
	 * Returns all TipoZona records matching the query. Examples:
	 * GET /tipo-zonas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/tipo-zonas.json&limit=5
	 *
	 * @return TipoZona[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = TipoZona::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'TipoZona';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['tipo_zonas'] = $this['pager']->fetchPage();
        $this['u'] = 'tipo-zonas/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a TipoZona. Example:
	 * GET /tipo-zonas/edit/1
	 *
	 * @return TipoZona
	 */
	function editar($id = null) {
		$this->getTipoZona($id)->fromArray(@$_GET);
        $this['u'] = 'tipo-zonas/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a TipoZona. Examples:
	 * POST /tipo-zonas/save/1
	 * POST /rest/tipo-zonas/.json
	 * PUT /rest/tipo-zonas/1.json
	 */
	function guardar($id = null) {
		$tipo_zona = $this->getTipoZona($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$tipo_zona->fromArray($_REQUEST);
			if ($tipo_zona->validate()) {
				$tipo_zona->save();
				$this->flash['messages'][] = 'Tipo Zona guardado';
				$this->redirect('tipo-zonas');
			}
			$this->flash['errors'] = $tipo_zona->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('tipo-zonas/editar/' . $tipo_zona->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the TipoZona with the id. Examples:
	 * GET /tipo-zonas/show/1
	 * GET /rest/tipo-zonas/1.json
	 *
	 * @return TipoZona
	 */
	function mostrar($id = null) {
		$this->getTipoZona($id);
        $this['u'] = 'tipo-zonas/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the TipoZona with the id. Examples:
	 * GET /tipo-zonas/delete/1
	 * DELETE /rest/tipo-zonas/1.json
	 */
	function eliminar($id = null) {
        $tipo_zona = $this->getTipoZona($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $tipo_zona->fromArray($_REQUEST);

		try {
			if (null !== $tipo_zona && $tipo_zona->save()) {
				$this['messages'][] = 'Tipo Zona eliminado';
			} else {
				$this['errors'][] = 'Tipo Zona no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('tipo-zonas');
		}
	}

	/**
	 * @return TipoZona
	 */
	private function getTipoZona($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[TipoZona::getPrimaryKey()])) {
			$id = $_REQUEST[TipoZona::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new TipoZona
			$this['tipo_zona'] = new TipoZona;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['tipo_zona'] = TipoZona::retrieveByPK($id);
		}
		return $this['tipo_zona'];
	}

}