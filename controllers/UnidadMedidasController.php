<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class UnidadMedidasController extends ApplicationController {

	/**
	 * Returns all UnidadMedida records matching the query. Examples:
	 * GET /unidad-medidas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/unidad-medidas.json&limit=5
	 *
	 * @return UnidadMedida[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = UnidadMedida::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'UnidadMedida';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['unidad_medidas'] = $this['pager']->fetchPage();
        $this['u'] = 'unidad-medidas/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a UnidadMedida. Example:
	 * GET /unidad-medidas/edit/1
	 *
	 * @return UnidadMedida
	 */
	function editar($id = null) {
		$this->getUnidadMedida($id)->fromArray(@$_GET);
        $this['u'] = 'unidad-medidas/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a UnidadMedida. Examples:
	 * POST /unidad-medidas/save/1
	 * POST /rest/unidad-medidas/.json
	 * PUT /rest/unidad-medidas/1.json
	 */
	function guardar($id = null) {
		$unidad_medida = $this->getUnidadMedida($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$unidad_medida->fromArray($_REQUEST);
			if ($unidad_medida->validate()) {
				$unidad_medida->save();
				$this->flash['messages'][] = 'Unidad Medida guardado';
				$this->redirect('unidad-medidas');
			}
			$this->flash['errors'] = $unidad_medida->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('unidad-medidas/editar/' . $unidad_medida->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the UnidadMedida with the id. Examples:
	 * GET /unidad-medidas/show/1
	 * GET /rest/unidad-medidas/1.json
	 *
	 * @return UnidadMedida
	 */
	function mostrar($id = null) {
		$this->getUnidadMedida($id);
        $this['u'] = 'unidad-medidas/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the UnidadMedida with the id. Examples:
	 * GET /unidad-medidas/delete/1
	 * DELETE /rest/unidad-medidas/1.json
	 */
	function eliminar($id = null) {
        $unidad_medida = $this->getUnidadMedida($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $unidad_medida->fromArray($_REQUEST);

		try {
			if (null !== $unidad_medida && $unidad_medida->save()) {
				$this['messages'][] = 'Unidad Medida eliminado';
			} else {
				$this['errors'][] = 'Unidad Medida no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('unidad-medidas');
		}
	}

	/**
	 * @return UnidadMedida
	 */
	private function getUnidadMedida($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[UnidadMedida::getPrimaryKey()])) {
			$id = $_REQUEST[UnidadMedida::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new UnidadMedida
			$this['unidad_medida'] = new UnidadMedida;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['unidad_medida'] = UnidadMedida::retrieveByPK($id);
		}
		return $this['unidad_medida'];
	}

}