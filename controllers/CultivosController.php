<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class CultivosController extends ApplicationController {

	/**
	 * Returns all Cultivo records matching the query. Examples:
	 * GET /cultivos?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/cultivos.json&limit=5
	 *
	 * @return Cultivo[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Cultivo::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Cultivo';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['cultivos'] = $this['pager']->fetchPage();
        $this['u'] = 'cultivos/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Cultivo. Example:
	 * GET /cultivos/edit/1
	 *
	 * @return Cultivo
	 */
	function editar($id = null) {
		$this->getCultivo($id)->fromArray(@$_GET);
        $this['u'] = 'cultivos/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Cultivo. Examples:
	 * POST /cultivos/save/1
	 * POST /rest/cultivos/.json
	 * PUT /rest/cultivos/1.json
	 */
	function guardar($id = null) {
		$cultivo = $this->getCultivo($id);

		try {

            $fecha_actual = date('Y-m-d H:m:s');
            $value = $_REQUEST['fecha_cultivo'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_cultivo'] = $value;

            $value1 = $_REQUEST['fecha_produccion'];
            if (strpos($value1, '/')){
                $value1 =  DateTime::createFromFormat('d/m/Y h:i:s A', $value1)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_produccion'] = $value1;

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$cultivo->fromArray($_REQUEST);
			if ($cultivo->validate()) {
				$cultivo->save();
				$this->flash['messages'][] = 'Cultivo guardado';
				$this->redirect('zonas/cultivos/'.$cultivo->getZona());
			}
			$this->flash['errors'] = $cultivo->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('cultivos/editar/' . $cultivo->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Cultivo with the id. Examples:
	 * GET /cultivos/show/1
	 * GET /rest/cultivos/1.json
	 *
	 * @return Cultivo
	 */
	function mostrar($id = null) {
		$this->getCultivo($id);
        $this['u'] = 'cultivos/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Cultivo with the id. Examples:
	 * GET /cultivos/delete/1
	 * DELETE /rest/cultivos/1.json
	 */
	function eliminar($id = null) {
        $cultivo = $this->getCultivo($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $cultivo->fromArray($_REQUEST);

		try {
			if (null !== $cultivo && $cultivo->save()) {
				$this['messages'][] = 'Cultivo eliminado';
			} else {
				$this['errors'][] = 'Cultivo no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('cultivos');
		}
	}

	/**
	 * @return Cultivo
	 */
	private function getCultivo($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Cultivo::getPrimaryKey()])) {
			$id = $_REQUEST[Cultivo::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Cultivo
			$this['cultivo'] = new Cultivo;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['cultivo'] = Cultivo::retrieveByPK($id);
		}
		return $this['cultivo'];
	}

}