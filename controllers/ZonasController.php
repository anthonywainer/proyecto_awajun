<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class ZonasController extends ApplicationController {

	/**
	 * Returns all Zonas records matching the query. Examples:
	 * GET /zonas?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/zonas.json&limit=5
	 *
	 * @return Zonas[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Zonas::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Zonas';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['zonas'] = $this['pager']->fetchPage();
        $this['u'] = 'zonas/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Zonas. Example:
	 * GET /zonas/edit/1
	 *
	 * @return Zonas
	 */
	function editar($id = null) {
		$this->getZonas($id)->fromArray(@$_GET);
        $this['u'] = 'zonas/edit';
        return $this->loadView('admin/index',$this);
	}

    function cultivos($id = null){
        $this->getZonas($id)->fromArray(@$_GET);
        $this['u'] = 'zonas/cultivos';
        return $this->loadView('admin/index',$this);
    }
	/**
	 * Saves a Zonas. Examples:
	 * POST /zonas/save/1
	 * POST /rest/zonas/.json
	 * PUT /rest/zonas/1.json
	 */
	function guardar($id = null) {
		$zonas = $this->getZonas($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');
            $value = $_REQUEST['fecha_registro'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_registro'] = $value;

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$zonas->fromArray($_REQUEST);
			if ($zonas->validate()) {
				$zonas->save();
				$this->flash['messages'][] = 'Zona guardada';
				$this->redirect('predios/zonas/'.$_REQUEST['predio_id']);
			}
			$this->flash['errors'] = $zonas->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('zonas/editar/' . $zonas->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Zonas with the id. Examples:
	 * GET /zonas/show/1
	 * GET /rest/zonas/1.json
	 *
	 * @return Zonas
	 */
	function mostrar($id = null) {
		$this->getZonas($id);
        $this['u'] = 'zonas/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Zonas with the id. Examples:
	 * GET /zonas/delete/1
	 * DELETE /rest/zonas/1.json
	 */
	function eliminar($id = null) {
        $zonas = $this->getZonas($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $zonas->fromArray($_REQUEST);

		try {
			if (null !== $zonas && $zonas->save()) {
				$this['messages'][] = 'Zonas eliminado';
			} else {
				$this['errors'][] = 'Zonas no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('zonas');
		}
	}

	/**
	 * @return Zonas
	 */
	private function getZonas($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Zonas::getPrimaryKey()])) {
			$id = $_REQUEST[Zonas::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Zonas
			$this['zonas'] = new Zonas;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['zonas'] = Zonas::retrieveByPK($id);
		}
		return $this['zonas'];
	}

}