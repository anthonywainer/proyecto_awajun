<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class CultivoAlquilersController extends ApplicationController {

	/**
	 * Returns all CultivoAlquiler records matching the query. Examples:
	 * GET /cultivo-alquilers?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/cultivo-alquilers.json&limit=5
	 *
	 * @return CultivoAlquiler[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = CultivoAlquiler::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'CultivoAlquiler';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['cultivo_alquilers'] = $this['pager']->fetchPage();
        $this['u'] = 'cultivo-alquilers/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a CultivoAlquiler. Example:
	 * GET /cultivo-alquilers/edit/1
	 *
	 * @return CultivoAlquiler
	 */
	function editar($id = null) {
		$this->getCultivoAlquiler($id)->fromArray(@$_GET);
        $this['u'] = 'cultivo-alquilers/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a CultivoAlquiler. Examples:
	 * POST /cultivo-alquilers/save/1
	 * POST /rest/cultivo-alquilers/.json
	 * PUT /rest/cultivo-alquilers/1.json
	 */
	function guardar($id = null) {
		$cultivo_alquiler = $this->getCultivoAlquiler($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                $_REQUEST['updated_at'] = $fecha_actual;
            }
			$cultivo_alquiler->fromArray($_REQUEST);
			if ($cultivo_alquiler->validate()) {
				$cultivo_alquiler->save();
				$this->flash['messages'][] = 'Cultivo Alquiler guardado';
				$this->redirect('cultivo-alquilers');
			}
			$this->flash['errors'] = $cultivo_alquiler->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('cultivo-alquilers/editar/' . $cultivo_alquiler->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the CultivoAlquiler with the id. Examples:
	 * GET /cultivo-alquilers/show/1
	 * GET /rest/cultivo-alquilers/1.json
	 *
	 * @return CultivoAlquiler
	 */
	function mostrar($id = null) {
		$this->getCultivoAlquiler($id);
        $this['u'] = 'cultivo-alquilers/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the CultivoAlquiler with the id. Examples:
	 * GET /cultivo-alquilers/delete/1
	 * DELETE /rest/cultivo-alquilers/1.json
	 */
	function eliminar($id = null) {
        $cultivo_alquiler = $this->getCultivoAlquiler($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $cultivo_alquiler->fromArray($_REQUEST);

		try {
			if (null !== $cultivo_alquiler && $cultivo_alquiler->save()) {
				$this['messages'][] = 'Cultivo Alquiler eliminado';
			} else {
				$this['errors'][] = 'Cultivo Alquiler no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('cultivo-alquilers');
		}
	}

	/**
	 * @return CultivoAlquiler
	 */
	private function getCultivoAlquiler($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[CultivoAlquiler::getPrimaryKey()])) {
			$id = $_REQUEST[CultivoAlquiler::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new CultivoAlquiler
			$this['cultivo_alquiler'] = new CultivoAlquiler;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['cultivo_alquiler'] = CultivoAlquiler::retrieveByPK($id);
		}
		return $this['cultivo_alquiler'];
	}

}