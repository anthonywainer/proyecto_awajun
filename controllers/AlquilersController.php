<?php
use Dabl\Controller\ControllerRoute;
use Dabl\Query\QueryPager;

class AlquilersController extends ApplicationController {

	/**
	 * Returns all Alquiler records matching the query. Examples:
	 * GET /alquilers?column=value&order_by=column&dir=DESC&limit=20&page=2&count_only
	 * GET /rest/alquilers.json&limit=5
	 *
	 * @return Alquiler[]
	 */
    function __construct(ControllerRoute $route = null){
        parent::__construct($route);
        if (!$_SESSION){
            return redirect('/login');
        }
    }
	function index() {
        if (!isset($_GET['search'])){
            $_GET['search'] = '';
        }
        #add query search  ->andLike('model.name','%'.$_GET['search'].'%')
		$q = Alquiler::getQuery(@$_GET)->andNull('deleted_at');

		// paginate
		$limit = empty($_REQUEST['limit']) ? 10 : $_REQUEST['limit'];
		$page = empty($_REQUEST['page']) ? 1 : $_REQUEST['page'];
		$class = 'Alquiler';
		$method = 'doSelectIterator';
		$this['pager'] = new QueryPager($q, $limit, $page, $class, $method);

		if (isset($_GET['count_only'])) {
			return $this['pager'];
		}
		$this['alquilers'] = $this['pager']->fetchPage();
        $this['u'] = 'alquilers/index';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Form to create or edit a Alquiler. Example:
	 * GET /alquilers/edit/1
	 *
	 * @return Alquiler
	 */
	function editar($id = null) {
		$this->getAlquiler($id)->fromArray(@$_GET);
        $this['u'] = 'alquilers/edit';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Saves a Alquiler. Examples:
	 * POST /alquilers/save/1
	 * POST /rest/alquilers/.json
	 * PUT /rest/alquilers/1.json
	 */
	function guardar($id = null) {
		$alquiler = $this->getAlquiler($id);

		try {
            $fecha_actual = date('Y-m-d H:m:s');

            $value = $_REQUEST['fecha_alquiler'];
            if (strpos($value, '/')){
                $value =  DateTime::createFromFormat('d/m/Y h:i:s A', $value)->format('Y-m-d H:i:s');
            }
            $_REQUEST['fecha_alquiler'] = $value;

            if (!$_REQUEST['id']) {
                $_REQUEST['created_at'] = $fecha_actual;
            }else {
                foreach (CultivoAlquiler::getAll('where alquiler_id='.$_REQUEST['id']) as $ca) {
                    $cultivo_alquiler = CultivoAlquiler::retrieveByPK($ca->getId());
                    $cultivo_alquiler->fromArray(['deleted_at'=>$fecha_actual]);
                    $cultivo_alquiler->save();
                }
                $_REQUEST['updated_at'] = $fecha_actual;
            }
            $_REQUEST['registrador'] = $_SESSION['user']->id;
			$alquiler->fromArray($_REQUEST);
			if ($alquiler->validate()) {
				$alquiler->save();
                $uid = $alquiler->getId();
                foreach ($_REQUEST['cultivos'] as $per) {
                    $arr = [];
                    if (!$_REQUEST['id']) {
                        $arr['created_at'] = $fecha_actual;
                    }else {
                        $arr['updated_at'] = $fecha_actual;
                    }
                    $arr['cultivo_id'] = $per;
                    $arr['alquiler_id'] = $uid; #ultimo id ingresado
                    $cultivo_alquiler = new CultivoAlquiler();
                    $cultivo_alquiler->fromArray($arr);
                    $cultivo_alquiler->save();
                }

                $this->flash['messages'][] = 'Alquiler guardado';
				$this->redirect('alquilers');
			}
			$this->flash['errors'] = $alquiler->getValidationErrors();
		} catch (Exception $e) {
			$this->flash['errors'][] = $e->getMessage();
		}

		$this->redirect('alquilers/editar/' . $alquiler->getId() . '?' . http_build_query($_REQUEST));
	}

	/**
	 * Returns the Alquiler with the id. Examples:
	 * GET /alquilers/show/1
	 * GET /rest/alquilers/1.json
	 *
	 * @return Alquiler
	 */
	function mostrar($id = null) {
		$this->getAlquiler($id);
        $this['u'] = 'alquilers/show';
        return $this->loadView('admin/index',$this);
	}

	/**
	 * Deletes the Alquiler with the id. Examples:
	 * GET /alquilers/delete/1
	 * DELETE /rest/alquilers/1.json
	 */
	function eliminar($id = null) {
        $alquiler = $this->getAlquiler($id);
        $fecha_actual = date('Y-m-d H:m:s');
        $_REQUEST['deleted_at'] = $fecha_actual;
        $alquiler->fromArray($_REQUEST);

		try {
			if (null !== $alquiler && $alquiler->save()) {
				$this['messages'][] = 'Alquiler eliminado';
			} else {
				$this['errors'][] = 'Alquiler no puede ser eliminado';
			}
		} catch (Exception $e) {
			$this['errors'][] = $e->getMessage();
		}

		if ($this->outputFormat === 'html') {
			$this->flash['errors'] = @$this['errors'];
			$this->flash['messages'] = @$this['messages'];
			$this->redirect('alquilers');
		}
	}

	/**
	 * @return Alquiler
	 */
	private function getAlquiler($id = null) {
		// look for id in param or in $_REQUEST array
		if (null === $id && isset($_REQUEST[Alquiler::getPrimaryKey()])) {
			$id = $_REQUEST[Alquiler::getPrimaryKey()];
		}

		if ('' === $id || null === $id) {
			// if no primary key provided, create new Alquiler
			$this['alquiler'] = new Alquiler;
		} else {
			// if primary key provided, retrieve the record from the db
			$this['alquiler'] = Alquiler::retrieveByPK($id);
		}
		return $this['alquiler'];
	}

}