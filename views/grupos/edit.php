<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/permisosestilos.css', true) ?>"
<div id="usa">
    <h5  align="center">
        <?php echo $grupos->isNew() ? "NUEVO" : "EDITAR" ?> GRUPO</h5>
    <form method="post" action="<?php echo site_url('grupos/guardar') ?>">
        <div class="ui-widget-content ui-corner-all ui-helper-clearfix">
            <input type="hidden" name="id" value="<?php echo h($grupos->getId()) ?>" />
            <div class="form-inline">
                <label class="form-field-label" for="grupos_descripcion">Descripción :</label>
                <input required class="form-control"  id="grupos_descripcion"
                       type="text" name="descripcion"
                       value="<?php echo h($grupos->getDescripcion()) ?>"
                />
            </div>
            <br>
        </div>
        <h3>Permisos</h3>

        <?php foreach ($modulos as $m ):?> <!-- recorremos todos los módulos -->
            <ul>
                <div class="accordion-container">
                    <a href="#" class="accordion-titulo">
                        <?= $m->nombre ?>
                        <span class="toggle-icon"></span></a>
                    <div class="accordion-content">
                        <label style="text-align: center; font-weight: bold; " > <?= $m->nombre ?></label>
                        <?php $perM= Permisos::getAll("WHERE idmodulo= ".$m->id);
                        if($perM){   ?>
                        <label>
                            <input type="checkbox"
                                <?php if (isset($permisos)) if(in_array($perM[0]->id,$permisos)==1)
                                {echo "checked";}?>
                                   name="permisos[]"
                                   value="<?= $perM[0]->id ?>"
                            />
                        </label>
                        <?php } ?><!-- imprimimos los permisos de los módulos -->

                        <?php foreach (Submodulo::getAll('WHERE idmodulo= '.$m->id) as $sm){?>
                            <!-- recorremos todos los submódulos filtrando por id módulo-->
                            <li class="lista">
                                <p class="parrafo-permisos"><?= $sm->nombre ?></p>
                                <?php $persM= Permisos::getAll("WHERE idsubmodulo= ".$sm->id);
                                if(isset($persM)){
                                    foreach ($persM as $pm){
                                        ?>
                                        <label for=""><?= $pm->nombre ?></label>
                                        <input type="checkbox" class="option-input checkbox"
                                            <?php if (isset($permisos)) if(in_array($pm->id,$permisos)==1){echo "checked"; }?>
                                               name="permisos[]"
                                               value="<?= $pm->id ?>"
                                        />
                                    <?php } }?>
                                <!-- imprimimos los permisos de los submódulos -->
                            </li>
                        <?php } ?>
                    </div>
                </div>

            </ul>
        <?php endforeach; ?>
        <div class="form-action-buttons ui-helper-clearfix" align="right">
             <span class="button" data-icon="disk">
                <input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $grupos->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
            </span>

            <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
                <a class="btn btn-danger"  data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                    Cancelar
                </a>
            <?php endif ?>

        </div>

    </form>
</div>