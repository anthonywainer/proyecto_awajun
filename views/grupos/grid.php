<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid grupos-grid  table table-condensed table-bordered table-striped" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				ID
            </th>
			<th class="ui-widget-header ">
                DESCRIPCIÓN

			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($grupos as $key => $grupos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($grupos->getId()) ?>&nbsp;</td>
			<td><?php echo h($grupos->getDescripcion()) ?>&nbsp;</td>
			<td>
                <a   class="btn btn-outline-primary"  href="<?php echo site_url('grupos/editar/' . $grupos->getId()) ?>">
                    <i class="fa fa-edit fa-lg "></i>
                </a>
                <a  class="btn btn-outline-danger" href="#"
                    onclick="conf_eliminar('Eliminar Grupos','¿Está seguro de Eliminar?','<?php echo site_url('grupos/eliminar/' . $grupos->getId()) ?>')">
                    <i  class="fa fa-trash-o fa-lg "></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>