<div class="container-fluid">
<h4 align="center">
    LISTA DE GRUPOS
</h4>
<br>
<div class="row">
    <div class="form-group col-md-12">

        <div class="input-group">
            <a href="<?php echo site_url('grupos/editar') ?>"
               class="button"
               data-icon="plusthick"
               title="Nuevo Grupo">
                <button class="btn btn-default" style="background: #3C8DBC; color: white">
                    Registrar
                </button>
            </a>
            <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                   href="<?= site_url('grupos/index?search=') ?>"
                   value="<?= $_GET['search'] ?>"
                   class="form-control" placeholder="Buscar Grupo" style="margin-left: 40%"/>
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>
<div id="grilla">
<div class="ui-widget-content ui-corner-all">
	<?php View::load('grupos/grid', $params) ?>
</div>
</div>

<?php View::load('pager', compact('pager')) ?>
</div>
</div>
<div class="modal fade" id="primaryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-primary" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background: #024C51">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</div>