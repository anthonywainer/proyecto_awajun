<h3 align="center">Mostrar Ubicacion Zona</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('ubicacion-zonas/editar/' . $ubicacion_zona->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Ubicacion_zona">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Ubicacion_zona"		onclick="conf_eliminar('Eliminar Ubicacion Zona','¿Está seguro de Eliminar?','<?php echo site_url('ubicacion-zonas/eliminar/' . $ubicacion_zona->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('zonas?zona_ubicacion_id=' . $ubicacion_zona->getId()) ?>"
		class="btn btn-outline-primary" title="Zonas Ubicacion_zona">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Zona</strong>:
		<?php echo h($ubicacion_zona->getZona()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($ubicacion_zona->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($ubicacion_zona->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($ubicacion_zona->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>