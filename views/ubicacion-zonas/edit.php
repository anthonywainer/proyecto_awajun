<div id="usa">
<h4 align="center"><?php echo $ubicacion_zona->isNew() ? "Nuevo" : "Editar" ?>    Ubicacion Zona</h4>
<form method="post" action="<?php echo site_url('ubicacion-zonas/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ubicacion_zona->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ubicacion_zona_zona">Zona</label>
			<input id="ubicacion_zona_zona"
                   class="form-control"
                   type="text" name="zona" value="<?php echo h($ubicacion_zona->getZona()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $ubicacion_zona->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>