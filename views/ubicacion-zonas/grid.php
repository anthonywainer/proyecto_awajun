<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm ubicacion_zona-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UbicacionZona::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UbicacionZona::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UbicacionZona::ZONA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UbicacionZona::ZONA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Zona
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ubicacion_zonas as $key => $ubicacion_zona): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ubicacion_zona->getId()) ?>&nbsp;</td>
			<td><?php echo h($ubicacion_zona->getZona()) ?>&nbsp;</td>
			<td><?php echo h($ubicacion_zona->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($ubicacion_zona->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($ubicacion_zona->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Ubicacion_zona"
					href="<?php echo site_url('ubicacion-zonas/mostrar/' . $ubicacion_zona->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Ubicacion_zona"
					href="<?php echo site_url('ubicacion-zonas/editar/' . $ubicacion_zona->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Ubicacion_zona"
					href="#"
					onclick="conf_eliminar('Eliminar Ubicacion Zona','¿Está seguro de Eliminar?','<?php echo site_url('ubicacion-zonas/eliminar/' . $ubicacion_zona->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>