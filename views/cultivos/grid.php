<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm cultivo-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Cultivo::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::TIPO_SIEMBRA))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Cultivo::TIPO_SIEMBRA): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Tipo Siembra
                </a>
            </th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::FECHA_CULTIVO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Cultivo::FECHA_CULTIVO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha de Siembra
				</a>
			</th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::CANTIDAD_SEMBRADA))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Cultivo::CANTIDAD_SEMBRADA): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Cantidad Sembrada
                </a>
            </th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::CANTIDAD_PRODUCCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Cultivo::CANTIDAD_PRODUCCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cantidad de Producción
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::FECHA_PRODUCCION))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Cultivo::FECHA_PRODUCCION): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha Producción
				</a>
			</th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::ZONA))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Cultivo::ZONA): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Zona
                </a>
            </th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::CULTIVADOR))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Cultivo::CULTIVADOR): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Cultivador
                </a>
            </th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Cultivo::AREA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Cultivo::AREA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Área
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($cultivos as $key => $cultivo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($cultivo->getId()) ?>&nbsp;</td>
            <td><?php echo h($cultivo->getSiembraRelatedByTipoSiembra()->siembra) ?>&nbsp;</td>
            <td><?php echo h($cultivo->getFechaCultivo(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
            <td><?php echo h($cultivo->getCantidadSembrada()) ?>&nbsp;</td>
			<td><?php echo h($cultivo->getCantidadProduccion()) ?>&nbsp;</td>
			<td><?php echo h($cultivo->getFechaProduccion(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
            <td><?php echo h($cultivo->getZonasRelatedByZona()->getTipoZonaRelatedByTipoZona()->tipo_zona) ?>&nbsp;</td>
            <td><?php echo h($cultivo->getUsuariosRelatedByCultivador()->nombres) ?>&nbsp;</td>
			<td><?php echo h($cultivo->getArea()).' m&sup2' ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Cultivo"
					href="<?php echo site_url('cultivos/mostrar/' . $cultivo->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Cultivo"
					href="<?php echo site_url('cultivos/editar/' . $cultivo->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Cultivo"
					href="#"
					onclick="conf_eliminar('Eliminar Cultivo','¿Está seguro de Eliminar?','<?php echo site_url('cultivos/eliminar/' . $cultivo->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>