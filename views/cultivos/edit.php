<style>
    #map_canvas_container{
        border: 1px solid #aaaaaa;
        padding:15px;
        border-radius: 10px;
    }
    #map_canvas{
        height:300px;width:100%;
    }
    .maps_divs{width:100%;height:330px;}
    #plano-coordenadas-p{
        font-size: 0.7em;
    }
</style>
<div id="usa">
<h4 align="left"><?php echo $cultivo->isNew() ? "Nuevo" : "Editar" ?>    Cultivo</h4>
<form method="post" action="<?php echo site_url('cultivos/guardar') ?>">
    <div class="row container">
        <div class="col-md-4 ">
            <div class="row">
                <input type="hidden" id="idcultivo" name="id" value="<?php echo h($cultivo->getId()) ?>" />
                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_fecha_cultivo">Fecha de Registro</label>
                    <input required id="cultivo_fecha_cultivo"
                           class="form-control datetimepicker"
                           type="text" name="fecha_cultivo" value="<?php echo h($cultivo->getFechaCultivo(VIEW_TIMESTAMP_FORMAT)) ?>" />
                </div>
                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_tipo_siembra">Siembra</label>
                    <select required id="cultivo_tipo_siembra" class="form-control" name="tipo_siembra">
                    <?php foreach (Siembra::doSelect() as $siembra): ?>
                        <option <?php if ($cultivo->getTipoSiembra() === $siembra->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $siembra->getId() ?>"><?php echo $siembra->siembra?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <?php $zonas_id =$cultivo->zona?$cultivo->zona:$_REQUEST['zi']?>
                <input type="hidden" name="zona" value="<?= $zonas_id?>">
                <?php $zonas=Zonas::getAll('where id='.$zonas_id)[0] ?>
                <?php $coor=  "[{'coor':".$zonas->getCoordenadas().",'color':'".$zonas->getColor()."'}" ?>

                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_cultivador">Cultivador</label>
                    <select required id="cultivo_cultivador" class="form-control" name="cultivador">
                    <?php foreach (Usuarios::doSelect() as $usuarios): ?>
                        <option <?php if ($cultivo->getCultivador() === $usuarios->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $usuarios->getId() ?>">
                            <?= $usuarios->getNombres().' '.$usuarios->getApellidos()?>
                        </option>
                    <?php endforeach ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_cantidad_sembrada">Cantidad Sembrada</label>
                    <input required id="cultivo_cantidad_sembrada"
                           class="form-control"
                           type="text" name="cantidad_sembrada" value="<?php echo h($cultivo->getCantidadSembrada()) ?>" />
                </div>
                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_fecha_produccion">Fecha de Produccion</label>
                    <input required id="cultivo_fecha_produccion"
                           class="form-control datetimepicker"
                           type="text" name="fecha_produccion" value="<?php echo h($cultivo->getFechaProduccion(VIEW_TIMESTAMP_FORMAT)) ?>" />
                </div>
                <div class="col-md-6">
                    <label class="form-field-label" for="cultivo_cantidad_produccion">Cantidad Produccion</label>
                    <input required id="cultivo_cantidad_produccion"
                           class="form-control"
                           type="text" name="cantidad_produccion" value="<?php echo h($cultivo->getCantidadProduccion()) ?>" />
                </div>

                <div class="col-md-12">
                    <label for="">Plano Coordenadas:</label>
                    <p id="plano-coordenadas-p"><?= $cultivo->getCoordenadas() ?></p>
                    <input type="hidden" name="coordenadas"
                           value="<?= $cultivo->getCoordenadas() ?>"
                           id="plano-coordenadas">
                    <?php $zz= Cultivo::getAll('where zona='.$zonas->getId()) ?>
                    <?php if($zz) {
                        foreach ($zz as $nz){
                            if ($nz->getId() == $cultivo->getId()){
                                continue;
                            }
                            $coor .= ",{'coor':" . $nz->getCoordenadas() . ",'color':'" . $nz->getColor() . "'}";
                        }

                    }
                    ?>
                    <input type="hidden" id="zona" value="<?=$coor.']'?>">

                </div>
                <div class="col-md-12">
                    <strong>Área:</strong><br>
                    <div id="map_calculation_result_container">
                        <div id="polyArea">
                            <?= $cultivo->getArea().' m&sup2' ?><br>
                            <?= ($cultivo->getArea()/10000).' Hectárea' ?>
                        </div>
                        <input type="hidden" name="area" value="<?= $cultivo->getArea() ?>">
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-8" >
            <section class="container clearfix">
                <div id="map_container" class="row">

                    <div id="search_street_container" class="col-md-12">
                        <div id="map_controls_options" >
                            <div class="clearfix row" >
                                <div class="col-md-12 form-inline">
                                    <p style="text-align: right">
                                        <input type="color" required value="<?= $cultivo->getColor()? $cultivo->getColor():'#ffffff' ?>" name="color" id="colorz" >
                                        <input type="hidden" value="<?= $zonas->getColor() ?>" id="colorp" >
                                        <input class="btn btn-info" type="button" onclick="clearMarkers();" value="Borrar Marcadores">
                                        <input class="btn btn-danger" type="button" onclick="removeLastMarker();" value="Borrar último Marcador">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="map_canvas_container" class="container maps_divs col-md-12">
                        <div id="map_canvas"></div>
                    </div>
                </div>
            </section>

        </div>
        <div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $cultivo->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
            <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
                <a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                    Cancelar
                </a>
            <?php endif ?>
        </div>
    </div>
    <hr>

</form>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACJapLIVhm-uVWitwICh24232jYdkP1SQ&language=es&libraries=places"></script>
<script src="<?= site_url('js/coords.js') ?>"></script>
<script>
    function initialize() {
        boo= false;
        coorp = eval($("#zona").val());
        if ($("#idcultivo").val()==""){
            var default_lat = coorp[0].coor[0].lat;
            var default_lng = coorp[0].coor[0].lng;
        }else {
            boo=true;
            coordenadas_predio = eval($("input[name=coordenadas]").val());
            default_lat = coordenadas_predio[0].lat;
            default_lng = coordenadas_predio[0].lng;
        }

        measure = {
            mvcLine: new google.maps.MVCArray(),
            mvcPolygon: new google.maps.MVCArray(),
            mvcMarkers: new google.maps.MVCArray(),
            line: null,
            polygon: null
        };

        var latlng = new google.maps.LatLng(default_lat, default_lng);


        var mapOptions = {
            scaleControl: true,
            zoom: default_zoom,
            zoomControl: true,
            zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
            panControl: false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            draggableCursor: 'crosshair'
        };


        map = new google.maps.Map(document.getElementById(map_div), mapOptions);

        map.setTilt(0);

        if (boo) {
            predioPath = new google.maps.Polygon({
                path: coordenadas_predio,
                geodesic: true,
                strokeColor: $("#colorz").val(),
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: $("#colorz").val(),
                fillOpacity: 0.8
            });

            predioPath.setMap(map);
        }
        $.each(coorp,function (i,v) {
            new google.maps.Polygon({
                clickable: false,
                map: map,
                path: v.coor,
                geodesic: true,
                strokeColor: v.color,
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: v.color,
                fillOpacity: 0.2
            });
        });

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

    }
</script>