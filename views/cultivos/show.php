<h3 align="center">Mostrar Cultivo</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('cultivos/editar/' . $cultivo->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Cultivo">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Cultivo"		onclick="conf_eliminar('Eliminar Cultivo','¿Está seguro de Eliminar?','<?php echo site_url('cultivos/eliminar/' . $cultivo->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Fecha Cultivo</strong>:
		<?php echo h($cultivo->getFechaCultivo(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Tipo Siembra</strong>:
		<?php echo h($cultivo->getSiembraRelatedByTipoSiembra()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Zona</strong>:
		<?php echo h($cultivo->getZonasRelatedByZona()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Cultivador</strong>:
		<?php echo h($cultivo->getUsuariosRelatedByCultivador()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($cultivo->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($cultivo->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($cultivo->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Cantidad Produccion</strong>:
		<?php echo h($cultivo->getCantidadProduccion()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Fecha Produccion</strong>:
		<?php echo h($cultivo->getFechaProduccion(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Cantidad Sembrada</strong>:
		<?php echo h($cultivo->getCantidadSembrada()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Area</strong>:
		<?php echo h($cultivo->getArea()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Coordenadas</strong>:
		<?php echo h($cultivo->getCoordenadas()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Color</strong>:
		<?php echo h($cultivo->getColor()) ?>
	</div>
</div>