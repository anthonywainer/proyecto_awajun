<div id="usa">
<h6 align="center"><?php echo $usuarios->isNew() ? "NUEVO" : "EDITAR" ?> USUARIO</h6>
<form method="post"  action="<?php echo site_url('usuarios/guardar') ?>" onsubmit="registrar(this); return false">
	<div class="row">
		<input type="hidden" name="id" value="<?php echo h($usuarios->getId()) ?>" />
        <div class="col-md-6">
            <label class="form-field-label" for="usuarios_dni">Dni</label>
            <input type="text" class="form-control" id="usuarios_dni"  name="dni"
                   onkeyup="checkDni(this)"
                   value="<?php echo h($usuarios->getDni()) ?>" required/>
            <label id="mensaje"></label>
        </div>
		<div class="col-md-6">
            <label class="form-field-label" for="usuarios_correo">Correo</label>
            <input type="email" class="form-control" id="usuarios_correo"  name="correo" value="<?php echo h($usuarios->getCorreo()) ?>" />
        </div>
		<div class="col-md-6">
			<label class="form-field-label" for="usuarios_nombres">Nombres</label>
			<input required class="form-control" id="usuarios_nombres" type="text" name="nombres" onkeypress="return soloLetras(event)" value="<?php echo h($usuarios->getNombres()) ?>" />
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="usuarios_apellidos">Apellidos</label>
			<input required class="form-control" id="usuarios_apellidos" type="text" name="apellidos" onkeypress="return soloLetras(event)" value="<?php echo h($usuarios->getApellidos()) ?>" />
		</div>
        <div class="col-md-6">
            <label class="form-field-label" for="usuarios_fecha_nacimiento">Fecha Nacimiento</label>
            <input required class="form-control datepicker" id="usuarios_fecha_nacimiento" type="text" name="fecha_nacimiento" value="<?php echo h($usuarios->getFechaNacimiento(VIEW_DATE_FORMAT)) ?>" />
        </div>
        <div class="col-md-6">
            <label class="form-field-label" for="usuarios_ubigeo">Ubigeo</label>
            <select name="ubigeo" class="form-control">
                <?php foreach (Ubigeo::doSelect() as $g):  ?>
                    <option <?php if($usuarios->ubigeo == $g->id){echo "selected"; }   ?>
                            value="<?php echo $g->id ?>">
                        <?= utf8_encode($g->getDepartamento().' - '.$g->getProvincia().' - '.$g->getDistrito()) ?>
                    </option>
                <?php endforeach ?>
            </select>
        </div>
        <?php if (isset($_REQUEST['grupos'])): ?>
            <input type="hidden" name="grupos" value="<?=$_REQUEST['grupos']?>">
        <?php else: ?>
        <div class="col-md-6">
            <label class="form-field-label" for="grupos">Grupo</label>
            <select name="grupos" id="grupo" class="form-control">
                <?php foreach ($grupos as $g):  ?>
                    <option <?php if (isset($idgrupo )){ if ($idgrupo[0]->grupo_id == $g->id){echo "selected"; } }  ?> value="<?php echo $g->id ?>"><?php echo $g->descripcion ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <?php endif; ?>

            <input required class="form-control" id="usuarios_clave" type="hidden" name="clave"
                   value="<?php echo h($usuarios->getClave()) ?>" />

		<div class="col-md-6">
			<label class="form-field-label" for="usuarios_telefono">Teléfono</label>
			<input required class="form-control" id="usuarios_telefono" type="number" name="telefono" value="<?php echo h($usuarios->getTelefono()) ?>" />
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="usuarios_direccion">Dirección</label>
			<input required class="form-control" id="usuarios_direccion" type="text" name="direccion" value="<?php echo h($usuarios->getDireccion()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <span data-icon="disk" >
			<input style="background: #3C8DBC; color: white" id ="btnEnviar" class="btn btn-primary"  type="submit" value="<?php echo $usuarios->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-danger" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>

    </div>

</form>

