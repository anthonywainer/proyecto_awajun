<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid usuarios-grid table table-condensed table-bordered" cellspacing="0" id="table1">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					#
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header ">
					APELLIDO
			</th>

			<th class="ui-widget-header ">
					CORREO
			</th>
			<th class="ui-widget-header ">
					TELÉFONO
			</th>
			<th class="ui-widget-header ">
					DIRECCIÓN
			</th>
            <th>
                    GRUPO
            </th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES </th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($usuarios as $key => $usuarios): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php $id = $usuarios->getId(); echo $key+1;//echo ($usuarios->getId()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getApellidos()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getCorreo()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($usuarios->getDireccion()) ?>&nbsp;</td>
            <td>
                <?php echo Grupos::getAll('where id='.UsuariosGrupo::getAll('where usuario_id='.$id)[0]->getGrupo_id())[0]->getDescripcion();  ?>
            </td>
            <td>
                <a class="btn btn-outline-secondary" title="ver permisos" href="<?php echo site_url('usuarios/permisos/' . $usuarios->getId()) ?>">
                    <i class="fa fa-lock fa-lg "></i>
                </a>
                <i class="btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('usuarios/editar/' . $usuarios->getId()) ?>">
                    <i  class="fa fa-edit fa-lg "></i>
                </i>
                <a   class="btn btn-outline-danger" href="#"
                    onclick="conf_eliminar('Eliminar Usuarios',
                            '¿Está seguro de Eliminar?',
                            '<?php echo site_url('usuarios/eliminar/' . $usuarios->getId()) ?>')">
                    <i   class="fa fa-trash-o fa-lg "></i>
                </a>

			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>