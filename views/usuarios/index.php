<div class="container-fluid">
<h3 align="center">
    LISTA DE USUARIOS
</h3>

<div class="row">
    <div class="form-group col-md-12">

        <div class="input-group">
            <i href="<?php echo site_url('usuarios/editar') ?>" onclick="openmodal(this)"
               class="button"
               data-icon="plusthick"
               title="Nuevo Usuario">
                <button class="btn btn-default" style="background: #3C8DBC; color: white">
                    Registrar
                </button>
            </i>
            <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                   href="<?= site_url('usuarios/index?search=') ?>"
                   value="<?= $_GET['search'] ?>"
                   class="form-control" placeholder="Buscar usuarios" style="margin-left: 40%"/>
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
    </div>
</div>
<div id="grilla">
    <div class="ui-widget-content ui-corner-all">
        <?php View::load('usuarios/grid', $params) ?>
    </div>
</div>
<?php View::load('pager', compact('pager')) ?>
<?php View::load('layouts/modal', $params) ?>
</div>
<script>
    function checkDni(t){
        if($(t).val().length>0 && $(t).val().length<=7) {
            $("#mensaje").html("<span style='color:red'> DNI INCOMPLETO</span>");
            $('#btnEnviar').attr('disabled',true);
        }
        else if($(t).val().length==8) {
            $("#mensaje").html("<span style='color:blue'>DNI VÁLIDO</span>");
            $("#usuarios_clave").val($(t).val());
            $('#btnEnviar').attr('disabled',false);
        }
        else{
            $("#mensaje").html("<span style='color:red'> Error! maximo 8 caracteres</span>");
            $('#btnEnviar').attr('disabled',true);
        }
    }

</script>