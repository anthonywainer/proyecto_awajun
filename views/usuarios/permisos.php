<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/permisosestilos.css', true) ?>"
      xmlns="http://www.w3.org/1999/html">
<h2 align="center">Lista de Permisos</h2>

<form action="<?php echo site_url('usuarios/permisos/'.$idp) ?>" method="POST">

 <!--<?php foreach ($modulos as $m ):?>
        <ul>
            <?= $m->nombre ?>
            <?php $perM= Permisos::getAll("WHERE idmodulo= ".$m->id); if(isset($perM)){   ?>
                <input type="checkbox"
                    <?php if(in_array($perM[0]->id,$permisos_g)==1){echo "checked disabled";}
                elseif(in_array($perM[0]->id,$permisos_u)==1){echo "checked";}
                ?> name="permisos[]" value="<?= $perM[0]->id ?>">
            <?php } ?>

            <?php foreach (Submodulo::getAll('WHERE idmodulo= '.$m->id) as $sm){?>
                <li><?= $sm->nombre ?> ====>
                    <?php $persM= Permisos::getAll("WHERE idsubmodulo= ".$sm->id); if(isset($persM)){
                        foreach ($persM as $pm){?>
                            <label for=""><?= $pm->nombre ?></label>
                            <input type="checkbox"
                            <?php if(in_array($pm->id,$permisos_g)==1){echo "checked disabled";}
                                elseif (in_array($pm->id,$permisos_u)==1){echo "checked";}
                            ?> name="permisos[]" value="<?= $pm->id ?>">
                        <?php }  } ?>
                </li>
            <?php }?>

        </ul>
    <?php endforeach; ?>-->

   <?php foreach ($modulos as $m ):?>
        <ul>
            <div class="accordion-container">

                <a href="#" class="accordion-titulo">
                    <?= $m->nombre ?>
                   <span class="toggle-icon"></span></a>

                <div class="accordion-content">

                    <label style="text-align: center; font-weight: bold; " ><?= $m->nombre ?></label>

                    <?php $perM= Permisos::getAll("WHERE idmodulo= ".$m->id); if(isset($perM)){   ?>
                       <label> <input type="checkbox" class="check"
                            <?php if(in_array($perM[0]->id,$permisos_g)==1){echo "checked disabled";}
                            elseif(in_array($perM[0]->id,$permisos_u)==1){echo "checked";}
                            ?> name="permisos[]" value="<?= $perM[0]->id ?>"></label>
                    <?php } ?>
            <?php foreach (Submodulo::getAll('WHERE idmodulo= '.$m->id) as $sm){?>

                <li class="lista">
                   <p  class="parrafo-permisos" ><?= $sm->nombre ?></p>

                    <?php $persM= Permisos::getAll("WHERE idsubmodulo= ".$sm->id); if(isset($persM)){
                        foreach ($persM as $pm){?>
                            <label for=""><?= $pm->nombre ?></label>
                            <input type="checkbox" class="option-input checkbox"
                                <?php if(in_array($pm->id,$permisos_g)==1){echo "checked disabled";}
                                elseif (in_array($pm->id,$permisos_u)==1){echo "checked";}
                                ?> name="permisos[]" value="<?= $pm->id ?>">
                        <?php }  } ?>
                </li>
            <?php }?>
                </div>
            </div>
        </ul>
    <?php endforeach; ?>




<div class="form-action-buttons ui-helper-clearfix" align="right">
     <span data-icon="disk" >
			<input  style="background: #3C8DBC; color: white"  class="btn btn-primary"  type="submit" value="Guardar" />
    </span>

    <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
        <a class="btn btn-danger" data-icon="cancel" data-dismiss="modal" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
            Cancelar
        </a>
    <?php endif ?>


</div>
</form>

