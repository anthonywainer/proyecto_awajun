<style>
    #map_canvas_container{
        border: 1px solid #aaaaaa;
        padding:15px;
        border-radius: 10px;
    }
    #map_canvas{
        height:300px;width:100%;
    }
    .maps_divs{width:100%;height:330px;}
    #plano-coordenadas-p{
        font-size: 0.7em;
    }
</style>
<div id="usa">
<h4 align="left"><?php echo $zonas->isNew() ? "Nueva" : "Editar" ?>    Zona</h4>
<form method="post" action="<?php echo site_url('zonas/guardar') ?>">
    <div class="row container">
        <div class="col-md-4 row">
		    <input type="hidden" id="idzona" name="id" value="<?php echo h($zonas->getId()) ?>" />
        		<div class="col-md-6">
                    <label class="form-field-label" for="zonas_zona_ubicacion_id">Ubicación Zona</label>
                    <select required id="zonas_zona_ubicacion_id" class="form-control" name="zona_ubicacion_id">
                    <?php foreach (UbicacionZona::doSelect() as $ubicacion_zona): ?>
                        <option <?php if ($zonas->getZonaUbicacionId() === $ubicacion_zona->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $ubicacion_zona->getId() ?>"><?php echo $ubicacion_zona->getZona()?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <?php $predio_id =$zonas->predio_id?$zonas->predio_id:$_REQUEST['ip']?>
			    <input type="hidden" name="predio_id" value="<?= $predio_id?>">
                 <?php $predio=Predio::getAll('where id='.$predio_id)[0] ?>
                <?php $coor=  "[{'coor':".$predio->getCoordenadas().",'color':'".$predio->getColor()."'}" ?>
		        <div class="col-md-6">
                    <label class="form-field-label" for="zonas_tipo_zona">Tipo Zona</label>
                    <select required id="zonas_tipo_zona" class="form-control" name="tipo_zona">
                    <?php foreach (TipoZona::doSelect() as $tipo_zona): ?>
                        <option <?php if ($zonas->getTipoZona() === $tipo_zona->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $tipo_zona->getId() ?>"><?php echo $tipo_zona->getTipoZona()?></option>
                    <?php endforeach ?>
                    </select>
                </div>
                <input type="hidden" name="miembro_id" value="<?= $zonas->miembro_id?$zonas->miembro_id:$_REQUEST['im']?>">
            <div class="col-md-6">
                <div class="form-field-wrapper">
                    <label class="form-field-label" for="predio_numero_zona">Número Zona:</label>
                    <input required id="predio_numero_zona"
                           class="form-control"
                           type="text" name="numero_zona" value="<?php echo h($zonas->getNumeroZona()) ?>" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-field-label" for="predio_fecha_registro">Fecha Registro:</label>
                    <div class='input-group' >
                        <input required id="predio_fecha_registro"
                               class="form-control datetimepicker"
                               type="text" name="fecha_registro" value="<?php echo h($zonas->getFechaRegistro(VIEW_TIMESTAMP_FORMAT)) ?>" />
                    </div>
                </div>
            </div>
                <form action="#" class="fromAddress printHidden col-md-12 col-sm-12 col-xs-12">
                    <label for="">Dirección:</label>
                    <div class="form-inline">
                        <input required id="search_address" type="search"
                               name="direccion" style="width: 81%;"
                               value="<?= $zonas->getDireccion?$zonas->getDireccion:$predio->getDireccion() ?>"
                               class="textBefore vpsClearOnFocus form-control"
                               placeholder="calle, número de casa, ciudad, etc..." />
                        <button id="btn_show_search_result" type="submit"
                                class="submitOn btn btn-default fa fa-search" >
                        </button>
                    </div>
                    <div class="clear"></div>
                </form>
                <div class="col-md-12">
                    <label for="">Plano Coordenadas:</label>
                    <p id="plano-coordenadas-p"><?= $zonas->getCoordenadas() ?></p>
                    <input type="hidden" name="coordenadas"
                           value="<?= $zonas->getCoordenadas() ?>"
                           id="plano-coordenadas">
                    <?php $zz= Zonas::getAll('where predio_id='.$predio->getId()) ?>
                    <?php if($zz) {
                        foreach ($zz as $nz){
                            if ($nz->getId() == $zonas->getId()){
                                continue;
                            }
                            $coor .= ",{'coor':" . $nz->getCoordenadas() . ",'color':'" . $nz->getColor() . "'}";
                        }

                    }
                    ?>
                        <input type="hidden" id="predio" value="<?=$coor.']'?>">

                </div>
                <div class="col-md-12">
                    <strong>Área:</strong><br>
                    <div id="map_calculation_result_container">
                        <div id="polyArea">
                            <?= $zonas->getArea().' m&sup2' ?><br>
                            <?= ($zonas->getArea()/10000).' Hectárea' ?>
                        </div>
                        <input type="hidden" name="area" value="<?= $zonas->getArea() ?>">
                    </div>
                </div>
        </div>

        <div class="col-md-8" >
        <section class="container clearfix">
            <div id="map_container" class="row">

                <div id="search_street_container" class="col-md-12">
                    <div id="map_controls_options" >
                        <div class="clearfix row" >
                            <div class="col-md-12 form-inline">
                                <p style="text-align: right">
                                    <input type="color" required value="<?= $zonas->getColor()? $zonas->getColor():'#ffffff' ?>" name="color" id="colorz" >
                                    <input type="hidden" value="<?= $predio->getColor() ?>" id="colorp" >
                                    <input class="btn btn-info" type="button" onclick="clearMarkers();" value="Borrar Marcadores">
                                    <input class="btn btn-danger" type="button" onclick="removeLastMarker();" value="Borrar último Marcador">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="map_canvas_container" class="container maps_divs col-md-12">
                    <div id="map_canvas"></div>
                </div>
            </div>
        </section>

        </div>
        <hr>
	    <div class="col-md-12" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $zonas->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
    </div>
</form>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACJapLIVhm-uVWitwICh24232jYdkP1SQ&language=es&libraries=places"></script>
<script src="<?= site_url('js/coords.js') ?>"></script>
<script>
    function initialize() {
        boo= false;
        coorp = eval($("#predio").val());
        if ($("#idzona").val()==""){
            var default_lat = coorp[0].coor[0].lat;
            var default_lng = coorp[0].coor[0].lng;
        }else {
            boo=true;
            coordenadas_predio = eval($("input[name=coordenadas]").val());
            default_lat = coordenadas_predio[0].lat;
            default_lng = coordenadas_predio[0].lng;
        }

        measure = {
            mvcLine: new google.maps.MVCArray(),
            mvcPolygon: new google.maps.MVCArray(),
            mvcMarkers: new google.maps.MVCArray(),
            line: null,
            polygon: null
        };

        var latlng = new google.maps.LatLng(default_lat, default_lng);


        var mapOptions = {
            scaleControl: true,
            zoom: default_zoom,
            zoomControl: true,
            zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
            panControl: false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            draggableCursor: 'crosshair'
        };


        map = new google.maps.Map(document.getElementById(map_div), mapOptions);

        map.setTilt(0);

        if (boo) {
            predioPath = new google.maps.Polygon({
                path: coordenadas_predio,
                geodesic: true,
                strokeColor: $("#colorz").val(),
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: $("#colorz").val(),
                fillOpacity: 0.8
            });

            predioPath.setMap(map);
        }
        $.each(coorp,function (i,v) {
            new google.maps.Polygon({
                clickable: false,
                map: map,
                path: v.coor,
                geodesic: true,
                strokeColor: v.color,
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: v.color,
                fillOpacity: 0.2
            });
        });

        google.maps.event.addListener(map, 'click', function(event) {
            placeMarker(event.latLng);
        });

    }
</script>