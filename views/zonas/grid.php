<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm zonas-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::UNIDAD_MEDIDA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::UNIDAD_MEDIDA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Unidad Medida
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::ZONA_UBICACION_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::ZONA_UBICACION_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Zona Ubicacion
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::PREDIO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::PREDIO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Predio
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::TIPO_ZONA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::TIPO_ZONA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Tipo Zona
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Zonas::MIEMBRO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Zonas::MIEMBRO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Miembro
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($zonas as $key => $zonas): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($zonas->getId()) ?>&nbsp;</td>
			<td><?php echo h($zonas->getUnidadMedidaRelatedByUnidadMedidaId()) ?>&nbsp;</td>
			<td><?php echo h($zonas->getUbicacionZonaRelatedByZonaUbicacionId()) ?>&nbsp;</td>
			<td><?php echo h($zonas->getPredioRelatedByPredioId()) ?>&nbsp;</td>
			<td><?php echo h($zonas->getTipoZonaRelatedByTipoZona()) ?>&nbsp;</td>
			<td><?php echo h($zonas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($zonas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($zonas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($zonas->getMiembrosFamiliaRelatedByMiembroId()) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Zonas"
					href="<?php echo site_url('zonas/mostrar/' . $zonas->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Zonas"
					href="<?php echo site_url('zonas/editar/' . $zonas->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Zonas"
					href="#"
					onclick="conf_eliminar('Eliminar Zonas','¿Está seguro de Eliminar?','<?php echo site_url('zonas/eliminar/' . $zonas->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('cultivos?zona=' . $zonas->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>