<h3 align="center">Mostrar Zonas</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('zonas/editar/' . $zonas->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Zonas">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Zonas"		onclick="conf_eliminar('Eliminar Zonas','¿Está seguro de Eliminar?','<?php echo site_url('zonas/eliminar/' . $zonas->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('cultivos?zona=' . $zonas->getId()) ?>"
		class="btn btn-outline-primary" title="Cultivos Zonas">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Unidad Medida</strong>:
		<?php echo h($zonas->getUnidadMedidaRelatedByUnidadMedidaId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Zona Ubicacion</strong>:
		<?php echo h($zonas->getUbicacionZonaRelatedByZonaUbicacionId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Predio</strong>:
		<?php echo h($zonas->getPredioRelatedByPredioId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Tipo Zona</strong>:
		<?php echo h($zonas->getTipoZonaRelatedByTipoZona()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($zonas->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($zonas->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($zonas->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Miembro</strong>:
		<?php echo h($zonas->getMiembrosFamiliaRelatedByMiembroId()) ?>
	</div>
</div>