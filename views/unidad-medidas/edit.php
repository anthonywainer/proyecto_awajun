<div id="usa">
<h4 align="center"><?php echo $unidad_medida->isNew() ? "Nuevo" : "Editar" ?>    Unidad Medida</h4>
<form method="post" action="<?php echo site_url('unidad-medidas/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($unidad_medida->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="unidad_medida_nombre">Nombre</label>
			<input id="unidad_medida_nombre"
                   class="form-control"
                   type="text" name="nombre" value="<?php echo h($unidad_medida->getNombre()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="unidad_medida_simbolo">Símbolo</label>
			<input id="unidad_medida_simbolo"
                   class="form-control"
                   type="text" name="simbolo" value="<?php echo h($unidad_medida->getSimbolo()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $unidad_medida->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>