<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm unidad_medida-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UnidadMedida::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UnidadMedida::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UnidadMedida::NOMBRE))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UnidadMedida::NOMBRE): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombre
				</a>
			</th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UnidadMedida::SIMBOLO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UnidadMedida::SIMBOLO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Simbolo
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($unidad_medidas as $key => $unidad_medida): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($unidad_medida->getId()) ?>&nbsp;</td>
			<td><?php echo h($unidad_medida->getNombre()) ?>&nbsp;</td>
			<td><?php echo h($unidad_medida->getSimbolo()) ?>&nbsp;</td>
			<td>
				<a class="btn btn-outline-primary"
					data-icon="search"
					title="Mostrar Unidad_medida"
					href="<?php echo site_url('unidad-medidas/mostrar/' . $unidad_medida->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>
				</a>
				<a class="btn btn-outline-primary"
					data-icon="pencil"
					title="Editar Unidad de medida"
					href="<?php echo site_url('unidad-medidas/editar/' . $unidad_medida->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>
				</a>
				<a class="btn btn-outline-danger"
					data-icon="trash"
					title="Eliminar Unidad de medida"
					href="#"
                    onclick="conf_eliminar('Eliminar Unidad de Medida','¿Está seguro de Eliminar?','<?php echo site_url('unidad-medidas/eliminar/' . $unidad_medida->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>