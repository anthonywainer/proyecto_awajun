<h3 align="center">Mostrar Unidad Medida</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('unidad-medidas/editar/' . $unidad_medida->getId()) ?>"
       class="btn btn-outline-primary"  data-icon="pencil" title="Editar Unidad_medida">
        <i class="fa fa-edit fa-lg"></i>
    </a>
	<a href="#"
       class="btn  btn-outline-danger " data-icon="trash" title="Eliminar Unidad_medida"
       onclick="conf_eliminar('Eliminar Folio','¿Está seguro de Eliminar?','<?php echo site_url('unidad-medidas/eliminar/' . $unidad_medida->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	<a href="<?php echo site_url('zonas?unidad_medida_id=' . $unidad_medida->getId()) ?>"
       class="btn btn-outline-primary" data-icon="carat-1-e" title="Zonas Unidad_medida">
        <i class="fa fa-plus fa-lg"></i>
    </a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Nombre:</strong>
		<?php echo h($unidad_medida->getNombre()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At:</strong>
		<?php echo h($unidad_medida->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At:</strong>
		<?php echo h($unidad_medida->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At:</strong>
		<?php echo h($unidad_medida->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Símbolo:</strong>
		<?php echo h($unidad_medida->getSimbolo()) ?>
	</div>
</div>