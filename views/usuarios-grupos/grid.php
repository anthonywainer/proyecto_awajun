<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid usuarios_grupo-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::USUARIO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::USUARIO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Usuario
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::GRUPO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::GRUPO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Grupo
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::CREATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::CREATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Created At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::UPDATED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::UPDATED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Updated At
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => UsuariosGrupo::DELETED_AT))) ?>">
					<?php if ( @$_REQUEST['order_by'] == UsuariosGrupo::DELETED_AT): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Deleted At
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($usuarios_grupos as $key => $usuarios_grupo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($usuarios_grupo->getId()) ?>&nbsp;</td>
			<td><?php echo h($usuarios_grupo->getUsuariosRelatedByUsuarioId()) ?>&nbsp;</td>
			<td><?php echo h($usuarios_grupo->getGruposRelatedByGrupoId()) ?>&nbsp;</td>
			<td><?php echo h($usuarios_grupo->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($usuarios_grupo->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($usuarios_grupo->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="button"
					data-icon="search"
					title="Show Usuarios_grupo"
					href="<?php echo site_url('usuarios-grupos/show/' . $usuarios_grupo->getId()) ?>">
					Show
				</a>
				<a
					class="button"
					data-icon="pencil"
					title="Edit Usuarios_grupo"
					href="<?php echo site_url('usuarios-grupos/edit/' . $usuarios_grupo->getId()) ?>">
					Edit
				</a>
				<a
					class="button"
					data-icon="trash"
					title="Delete Usuarios_grupo"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('usuarios-grupos/delete/' . $usuarios_grupo->getId()) ?>'; } return false">
					Delete
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>