<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid permisos_usuario-grid table table-condensed table-bordered table table-striped" cellspacing="0">
	<thead>
    <thead>
    <tr>
        <th class="ui-widget-header ui-corner-tl">
            ID
        </th>
        <th class="ui-widget-header ">
            USUARIO
        </th>
        <th class="ui-widget-header ">
            PERMISO
        </th>
        <th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
    </tr>
	</thead>
	<tbody>
<?php foreach ($permisos_usuarios as $key => $permisos_usuario): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos_usuario->getId()) ?>&nbsp;</td>
            <td><?php if($permisos_usuario->getIdUsuario()) echo Usuarios::getAll('where id='.$permisos_usuario->getIdUsuario())[0]->nombres ?></td>
            <td><?php if($permisos_usuario->getIdPermiso()) echo $permisos_usuario->getPermisos($permisos_usuario->getIdPermiso())->nombre ?>&nbsp;</td>

            <td>
				<a   title="Show Permisos_usuario" href="<?php echo site_url('permisos-usuarios/' . $permisos_usuario->getId()) ?>">
                    <i class="fa fa-search fa-lg" style="color: green;"></i>
                    </a>
				<i  onclick="openmodal(this)"
					title="Edit Permisos_usuario"
					href="<?php echo site_url('permisos-usuarios/editar/' . $permisos_usuario->getId()) ?>">
                    <i class="fa fa-edit fa-lg" style="color: #0e6498;"></i>
				</i>
				<a

					data-icon="trash"
					title="Delete Permisos_usuario"
					href="#"
					onclick="if (confirm('Are you sure?')) { window.location.href = '<?php echo site_url('permisos-usuarios/eliminar/' . $permisos_usuario->getId()) ?>'; } return false">
                    <i class="fa fa-trash-o fa-lg " style="color: red"></i>
				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>