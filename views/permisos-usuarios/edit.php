<div id="usa">
<h1><?php echo $permisos_usuario->isNew() ? "New" : "Edit" ?> Permisos Usuario</h1>
<form method="post" action="<?php echo site_url('permisos-usuarios/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<div class="form-field-wrapper">
			<input class="form-control" id="permisos_usuario_id" type="hidden" name="id" value="<?php echo h($permisos_usuario->getId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_usuario_idusuario">Usuarios</label>
			<select class="form-control" id="permisos_usuario_idusuario" name="idusuario">
			<?php foreach (Usuarios::getAll() as $usuarios): ?>
				<option <?php if ($permisos_usuario->getIdusuario() === $usuarios->id ) echo 'selected="selected"' ?> value="<?php echo $usuarios->id ?>"><?php echo $usuarios->nombres ?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="permisos_usuario_idpermiso">Permisos</label>
			<select class="form-control" id="permisos_usuario_idpermiso" name="idpermiso">
			<?php foreach (Permisos::doSelect() as $permisos): ?>
				<option <?php if ($permisos_usuario->getIdpermiso() === $permisos->id) echo 'selected="selected"' ?> value="<?php echo $permisos->id ?>"><?php echo $permisos->nombre ?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white" class="btn btn-primary" type="submit" value="<?php echo $permisos_usuario->isNew() ? "Guardar" : "Guardar" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>