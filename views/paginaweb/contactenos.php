<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand navbar-link" style="color: white; font-weight: 700; margin-top: 10px">
                PROYECTO AWAJUN
            </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" ><a href="<?= site_url('/') ?>" class="navbar-1">INICIO </a></li>
                <li role="presentation"><a href="<?= site_url('/nosotros') ?>" class="navbar-1">NOSOTROS </a></li>
                <li role="presentation"><a href="<?=site_url('/servicios') ?>" class="navbar-1">SERVICIOS </a></li>
                <li role="presentation"><a href="<?=site_url('/galeria') ?>" class="navbar-1">GALERÍA </a></li>
                <li role="presentation" class="active"><a href="<?= site_url('/contactenos') ?>" class="navbar-1">CONTÁCTENOS </a></li>
                <li role="presentation" id="lista-login" >
                    <div id="cuadrito-login">
                        <?php if ($_SESSION): ?>
                            <a href="<?= site_url('/admin') ?>" class="">Aceder al Sistema</a>
                        <?php else: ?>
                            <a href="<?= site_url('/login') ?>" class="login">INICIAR SESIÓN</a>
                        <?php endif; ?>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <div class=" col-md-4 col-md-12" id="nosotros" align="center">
        <h2>CONTÁCTENOS</h2>
    </div>
</div>
<div class="row" id="contactenos">
    <div class="col-md-6"   style="padding-right: 100px; padding-left: 100px; padding-top: 20px">
        <div id="cuadrito-co" >
            <br><br>
            <form action="<?= site_url('/contactenos') ?>" method="POST">
                <?php View::load('messages', compact('messages')) ?>
                <div style="padding-left: 10px" align="center">
                    <a class="contacto-1"><input required type="text" name="nombres" style="width: 80%" class="form-control" placeholder="Nombre"></a><br>
                    <a class="contacto-1"><input required type="number" name="telefono" style="width: 80%" class="form-control" placeholder="Teléfono"></a><br>
                    <a class="contacto-1"><input required type="email" name="email" style="width: 80%" class="form-control" placeholder="Email"></a><br>

                    <a class="contacto-1">
                        <textarea style="width: 80%" required name="asunto"  class="form-control" id=""
                                  cols="10" rows="3" placeholder="Asunto"></textarea>
                    </a><br>
                    <input type="hidden" name="estado" value="no_leido">
                    <button type="submit" class="btn"  style="color:white; border-radius: 0px; background: #D55F11"> ENVIAR</button>

                </div>
            </form>
            <br>

        </div>
    </div>
    <div class="col-md-6" style="background: white; border-right: 2px solid black; padding-top: 60px; padding-right: 200px; padding-left: 100px; padding-bottom: 60px" align="center">
        <img class="materialbox" src= <?= site_url('assets/img/1.jpg') ?> width="450" height="300"/><br>
    </div>

</div>

<div class="row">
    <div class="col-md-4 servicios" align="center" >

    </div>
    <div class="col-md-4 servicios" align="center">

    </div>
    <div class="col-md-4 servicios" align="left" style="padding-top: 0px">
        <h3>REDES SOCIALES</h3>
        <div class="form-inline">

            <a href="" target="_blank">
                <i class="fa fa-3x fa-twitter-square"></i>
            </a>
            <a href="" target="_blank">
                <i  class="fa fa-3x fa-facebook-square" style="color: white"></i>
            </a>
            <a href="" target="_blank">
                <i class="fa fa-3x fa-instagram" style="color: #2A5B83"></i>
            </a>
        </div>

    </div>
</div>