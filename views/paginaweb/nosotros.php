<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand navbar-link" style="color: white; font-weight: 700; margin-top: 10px">
                PROYECTO AWAJUN
            </a>
            <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" ><a href="<?= site_url('/') ?>" class="navbar-1">INICIO </a></li>
                <li role="presentation" class="active"><a href="<?= site_url('/nosotros') ?>" class="navbar-1">NOSOTROS </a></li>
                <li role="presentation"><a href="<?=site_url('/servicios') ?>" class="navbar-1">SERVICIOS </a></li>
                <li role="presentation"><a href="<?=site_url('/galeria') ?>" class="navbar-1">GALERÍA </a></li>
                <li role="presentation" ><a href="<?= site_url('/contactenos') ?>" class="navbar-1">CONTÁCTENOS </a></li>
                <li role="presentation" id="lista-login">
                    <div id="cuadrito-login">
                        <?php if ($_SESSION): ?>
                            <a href="<?= site_url('/admin') ?>" class="">Aceder al Sistema</a>
                        <?php else: ?>
                            <a href="<?= site_url('/login') ?>" class="login">INICIAR SESIÓN</a>
                        <?php endif; ?>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <div class="col-md-12" id="nosotros" align="center">
        <h2>NOSOTROS</h2>
    </div>
</div>
<div class="row" id="nosotros-c">
    <div class="col-md-8" id="descripcion-no" align="center">
        <p align="justify">

        </p>
    </div>
    <div class="col-md-4" style="padding-right: 30px; padding-left: 20px; padding-top: 20px">
        <div id="cuadrito-no" >
            <br>
            <strong><h4 align="center">CONTACTO</h4></strong><br>
            <div style="padding-left: 40px">
                <a class="contacto"><strong>Teléfono </strong> </a><br><br>
                <a class="contacto"><strong>Email</strong>       awajun </a><br><br>
                <a class="contacto"><strong>Dirección</strong>    </a> <br><br>
                <a class="contacto"><strong>WhattsApp</strong>    </a>
            </div>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 servicios" align="center" >

    </div>
    <div class="col-md-4 servicios" align="center">

    </div>
    <div class="col-md-4 servicios" align="left" style="padding-top: 0px">
        <h3>REDES SOCIALES</h3>
        <div class="form-inline">

            <a href="" target="_blank">
                <i class="fa fa-3x fa-twitter-square"></i>
            </a>
            <a href="" target="_blank">
                <i  class="fa fa-3x fa-facebook-square" style="color: white"></i>
            </a>
            <a href="" target="_blank">
                <i class="fa fa-3x fa-instagram" style="color: #2A5B83"></i>
            </a>
        </div>

    </div>
</div>