<div id="usa">
<h4 align="center"><?php echo $siembra->isNew() ? "Nuevo" : "Editar" ?>    Siembra</h4>
<form method="post" action="<?php echo site_url('siembras/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($siembra->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="siembra_siembra">Siembra</label>
			<input id="siembra_siembra"
                   class="form-control"
                   type="text" name="siembra" value="<?php echo h($siembra->getSiembra()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="siembra_descripcion">Descripcion</label>
			<input id="siembra_descripcion"
                   class="form-control"
                   type="text" name="descripcion" value="<?php echo h($siembra->getDescripcion()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $siembra->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>