<h3 align="center">Mostrar Siembra</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('siembras/editar/' . $siembra->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Siembra">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Siembra"		onclick="conf_eliminar('Eliminar Siembra','¿Está seguro de Eliminar?','<?php echo site_url('siembras/eliminar/' . $siembra->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('cultivos?tipo_siembra=' . $siembra->getId()) ?>"
		class="btn btn-outline-primary" title="Cultivos Siembra">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Siembra</strong>:
		<?php echo h($siembra->getSiembra()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Descripcion</strong>:
		<?php echo h($siembra->getDescripcion()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($siembra->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($siembra->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($siembra->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>