<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, shrink-to-fit=no">

    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="<?php echo site_url('css/font-awesome.css', true) ?>">
    <link rel="stylesheet" href="<?php echo site_url('css/simple-line-icons.min.css', true) ?>">

        <link href="<?php echo site_url('css/toastr.min.css', true) ?>" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/bootstrap3.min.css', true) ?>">

        <link type="text/css" rel="stylesheet" href="<?php echo site_url('css/style.css', true) ?>">


</head>
<body>


<div class="content-wrapper ui-widget">
    <div class="content">
        <?php View::load($content_view, $params) ?>
    </div>
</div>


    <script src="<?= site_url('js/jquery.min1.js') ?>"></script>
    <script src="<?= site_url('js/bootstrap3.min.js') ?>" ></script>
    <script src="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/js/mdb3/mdb.min.js"></script>
    <script src="<?php echo site_url('js/toastr.min.js', true) ?>"></script>


    <script>
        $('.materialboxed').materialbox();
        <?php if(isset($errors)): ?>
        toastr.error("<?php View::load('errors', compact('errors')) ?>", 'Eliminado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
        <?php if(isset($messages)): ?>
        toastr.success("<?php View::load('messages', compact('messages')) ?>", 'Guardado', {
            closeButton: true,
            progressBar: true,
        });
        <?php endif ?>
    </script>




</body>
</html>