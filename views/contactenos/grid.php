<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid contactenos-grid table table-condensed table-bordered table table-striped" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Contactenos::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::NOMBRES))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Contactenos::NOMBRES): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombres
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::TELEFONO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Contactenos::TELEFONO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Telefono
				</a>
			</th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::EMAIL))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Contactenos::EMAIL): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Email
                </a>
            </th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::ASUNTO))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Contactenos::ASUNTO): ?>
                        <span  class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>

                    <?php endif ?>
                   Asunto
                </a>
            </th>
            <th class="ui-widget-header ">
                <a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Contactenos::ESTADO))) ?>">
                    <?php if ( @$_REQUEST['order_by'] == Contactenos::ESTADO): ?>
                        <span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
                    <?php endif ?>
                    Estado
                </a>
            </th>
            <th>
                Accion
            </th>




        </tr>
	</thead>
	<tbody>
<?php foreach ($contactenos as $key => $contactenos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($contactenos->getId()) ?>&nbsp;</td>
			<td><?php echo h($contactenos->getNombres()) ?>&nbsp;</td>
			<td><?php echo h($contactenos->getTelefono()) ?>&nbsp;</td>
			<td><?php echo h($contactenos->getEmail()) ?>&nbsp;</td>
			<td ><?php

                $string = strip_tags($contactenos->getAsunto());

                if (strlen($string) > 40) {

                // truncate string
                $stringCut = substr($string, 0, 30);

                // make sure it ends in a word so assassinate doesn't become ass...
                $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'... 
                    <a href="'.site_url('contactenos/mostrar/'.$contactenos->getId()).'">Leer Mas</a>';
                }
                echo $string;?>
            </td>
			<td>
                <?php
                echo h($contactenos->getEstado())
                ?>&nbsp;
            </td>
            <td>
                <?php if($contactenos->getEstado()=='no_leido'):  ?>

                    <a href="<?= site_url('contactenos/guardar/'.$contactenos->getId()) ?>">
                        <button type="button" class="btn btn-outline-primary">Leer</button>
                    </a>

                    <?php else: ?>
                    <a href="<?= site_url('contactenos/eliminar/'.$contactenos->getId()) ?>">
                        <button type="button" class="btn btn-outline-danger">Eliminar</button>
                    </a>

                <?php endif; ?>
            </td>

		</tr>
<?php endforeach ?>
	</tbody>
</table>