<div id="usa">
<h4 align="center"><?php echo $ubigeo->isNew() ? "Nuevo" : "Editar" ?>    Ubigeo</h4>
<form method="post" action="<?php echo site_url('ubigeos/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($ubigeo->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ubigeo_departamento">Departamento</label>
			<input id="ubigeo_departamento"
                   class="form-control"
                   type="text" name="departamento" value="<?php echo h($ubigeo->getDepartamento()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ubigeo_provincia">Provincia</label>
			<input id="ubigeo_provincia"
                   class="form-control"
                   type="text" name="provincia" value="<?php echo h($ubigeo->getProvincia()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="ubigeo_distrito">Distrito</label>
			<input id="ubigeo_distrito"
                   class="form-control"
                   type="text" name="distrito" value="<?php echo h($ubigeo->getDistrito()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $ubigeo->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>