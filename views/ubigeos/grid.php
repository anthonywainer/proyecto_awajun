<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm ubigeo-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Ubigeo::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Ubigeo::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Ubigeo::DEPARTAMENTO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Ubigeo::DEPARTAMENTO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Departamento
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Ubigeo::PROVINCIA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Ubigeo::PROVINCIA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Provincia
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Ubigeo::DISTRITO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Ubigeo::DISTRITO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Distrito
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($ubigeos as $key => $ubigeo): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($ubigeo->getId()) ?>&nbsp;</td>
			<td><?php echo h($ubigeo->getDepartamento()) ?>&nbsp;</td>
			<td><?php echo h($ubigeo->getProvincia()) ?>&nbsp;</td>
			<td><?php echo h($ubigeo->getDistrito()) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Ubigeo"
					href="<?php echo site_url('ubigeos/mostrar/' . $ubigeo->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Ubigeo"
					href="<?php echo site_url('ubigeos/editar/' . $ubigeo->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Ubigeo"
					href="#"
					onclick="conf_eliminar('Eliminar Ubigeo','¿Está seguro de Eliminar?','<?php echo site_url('ubigeos/eliminar/' . $ubigeo->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('usuarios?ubigeo=' . $ubigeo->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>