<div class="container-fluid">
<h3 align="center">
	Lista de Ubigeos</h3>
    <div class="row">
        <div class="form-group col-md-12">

            <div class="input-group">
                <i href="<?=  site_url('ubigeos/editar') ?>" onclick="openmodal(this)"
                   class="button"
                   data-icon="plusthick"
                   title="Nuevo Ubigeo">
                    <button class="btn btn-default" style="background: #3C8DBC; color: white">
                        Registrar
                    </button>
                </i>
                <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                       href="<?= site_url('ubigeos/index?search=') ?>"
                       value="<?= $_GET['search'] ?>"
                       class="form-control" placeholder="Buscar Ubigeo" style="margin-left: 40%"/>
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
    </div>

<div class="ui-widget-content ui-corner-all">
	<?php View::load('ubigeos/grid', $params) ?></div>

<?php View::load('pager', compact('pager')) ?><?php View::load('layouts/modal') ?></div>
