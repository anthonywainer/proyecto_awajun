<h3 align="center">Mostrar Ubigeo</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('ubigeos/editar/' . $ubigeo->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Ubigeo">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Ubigeo"		onclick="conf_eliminar('Eliminar Ubigeo','¿Está seguro de Eliminar?','<?php echo site_url('ubigeos/eliminar/' . $ubigeo->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('usuarios?ubigeo=' . $ubigeo->getId()) ?>"
		class="btn btn-outline-primary" title="Usuarios Ubigeo">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Departamento</strong>:
		<?php echo h($ubigeo->getDepartamento()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Provincia</strong>:
		<?php echo h($ubigeo->getProvincia()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Distrito</strong>:
		<?php echo h($ubigeo->getDistrito()) ?>
	</div>
</div>