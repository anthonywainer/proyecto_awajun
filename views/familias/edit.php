<div id="usa">
<h4 align="center"><?php echo $familia->isNew() ? "Nuevo" : "Editar" ?>    Familia</h4>
<form method="post" action="<?php echo site_url('familias/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($familia->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="familia_nombre_familia">Nombre Familia</label>
			<input id="familia_nombre_familia"
                   class="form-control"
                   type="text" name="nombre_familia" value="<?php echo h($familia->getNombreFamilia()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="familia_codigo">Codigo</label>
			<input id="familia_codigo"
                   class="form-control"
                   type="text" name="codigo" value="<?php echo h($familia->getCodigo()) ?>" />
		</div>

	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $familia->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>