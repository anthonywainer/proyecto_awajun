<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm familia-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Familia::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Familia::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Familia::NOMBRE_FAMILIA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Familia::NOMBRE_FAMILIA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombre Familia
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Familia::CODIGO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Familia::CODIGO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Codigo
				</a>
			</th>

			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($familias as $key => $familia): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($familia->getId()) ?>&nbsp;</td>
			<td><?php echo h($familia->getNombreFamilia()) ?>&nbsp;</td>
			<td><?php echo h($familia->getCodigo()) ?>&nbsp;</td>

			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Familia"
					href="<?php echo site_url('familias/mostrar/' . $familia->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Familia"
					href="<?php echo site_url('familias/editar/' . $familia->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Familia"
					href="#"
					onclick="conf_eliminar('Eliminar Familia','¿Está seguro de Eliminar?','<?php echo site_url('familias/eliminar/' . $familia->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('miembros-familias?familia=' . $familia->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('predios?familia_id=' . $familia->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>