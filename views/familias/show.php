<h3 align="center">Mostrar Familia</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('familias/editar/' . $familia->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Familia">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Familia"	onclick="conf_eliminar('Eliminar Familia','¿Está seguro de Eliminar?','<?php echo site_url('familias/eliminar/' . $familia->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('miembros-familias?familia=' . $familia->getId()) ?>"
		class="btn btn-outline-primary" title="Miembros Familias Familia">
        <i class="fa fa-plus fa-lg"></i>
	</a>
	<a href="<?php echo site_url('predios?familia_id=' . $familia->getId()) ?>"
		class="btn btn-outline-primary" title="Predios Familia">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Nombre Familia</strong>:
		<?php echo h($familia->getNombreFamilia()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Codigo</strong>:
		<?php echo h($familia->getCodigo()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($familia->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($familia->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($familia->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>