<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm tipo_zona-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TipoZona::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TipoZona::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => TipoZona::TIPO_ZONA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == TipoZona::TIPO_ZONA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Tipo Zona
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($tipo_zonas as $key => $tipo_zona): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($tipo_zona->getId()) ?>&nbsp;</td>
			<td><?php echo h($tipo_zona->getTipoZona()) ?>&nbsp;</td>
			<td><?php echo h($tipo_zona->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($tipo_zona->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($tipo_zona->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Tipo_zona"
					href="<?php echo site_url('tipo-zonas/mostrar/' . $tipo_zona->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Tipo_zona"
					href="<?php echo site_url('tipo-zonas/editar/' . $tipo_zona->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Tipo_zona"
					href="#"
					onclick="conf_eliminar('Eliminar Tipo Zona','¿Está seguro de Eliminar?','<?php echo site_url('tipo-zonas/eliminar/' . $tipo_zona->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('zonas?tipo_zona=' . $tipo_zona->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>