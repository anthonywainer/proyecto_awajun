<div id="usa">
<h4 align="center"><?php echo $tipo_zona->isNew() ? "Nuevo" : "Editar" ?>    Tipo Zona</h4>
<form method="post" action="<?php echo site_url('tipo-zonas/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($tipo_zona->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="tipo_zona_tipo_zona">Tipo Zona</label>
			<input id="tipo_zona_tipo_zona"
                   class="form-control"
                   type="text" name="tipo_zona" value="<?php echo h($tipo_zona->getTipoZona()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $tipo_zona->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>