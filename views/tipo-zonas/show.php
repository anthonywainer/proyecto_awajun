<h3 align="center">Mostrar Tipo Zona</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('tipo-zonas/editar/' . $tipo_zona->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Tipo_zona">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Tipo_zona"		onclick="conf_eliminar('Eliminar Tipo Zona','¿Está seguro de Eliminar?','<?php echo site_url('tipo-zonas/eliminar/' . $tipo_zona->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('zonas?tipo_zona=' . $tipo_zona->getId()) ?>"
		class="btn btn-outline-primary" title="Zonas Tipo_zona">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Tipo Zona</strong>:
		<?php echo h($tipo_zona->getTipoZona()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($tipo_zona->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($tipo_zona->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($tipo_zona->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>