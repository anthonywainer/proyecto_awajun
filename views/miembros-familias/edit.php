<div id="usa">
<h4 align="center"><?php echo $miembros_familia->isNew() ? "Nuevo" : "Editar" ?>    Miembros Familia</h4>
<form method="post" action="<?php echo site_url('miembros-familias/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($miembros_familia->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="miembros_familia_rol_miembro">Rol Miembro</label>
			<select id="miembros_familia_rol_miembro" class="form-control" name="rol_miembro">
			<?php foreach (RolMiembro::doSelect() as $rol_miembro): ?>
				<option <?php if ($miembros_familia->getRolMiembro() === $rol_miembro->getId()) echo 'selected="selected"' ?> value="<?php echo $rol_miembro->getId() ?>"><?php echo $rol_miembro->nombre_miembro?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="miembros_familia_familia">Familia</label>
			<select id="miembros_familia_familia" class="form-control" name="familia">
			<?php foreach (Familia::doSelect() as $familia): ?>
				<option <?php if ($miembros_familia->getFamilia() === $familia->getId()) echo 'selected="selected"' ?> value="<?php echo $familia->getId() ?>"><?php echo $familia->getNombreFamilia()?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="miembros_familia_persona">Persona</label>
			<select id="miembros_familia_persona" class="form-control" name="persona">
			<?php foreach (Usuarios::doSelect() as $usuarios): ?>
				<option <?php if ($miembros_familia->getPersona() === $usuarios->getId()) echo 'selected="selected"' ?> value="<?php echo $usuarios->getId() ?>"><?php echo $usuarios->getNombres()?></option>
			<?php endforeach ?>
			</select>
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $miembros_familia->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>