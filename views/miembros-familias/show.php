<h3 align="center">Mostrar Miembros Familia</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('miembros-familias/editar/' . $miembros_familia->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Miembros_familia">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Miembros_familia"		onclick="conf_eliminar('Eliminar Miembros Familia','¿Está seguro de Eliminar?','<?php echo site_url('miembros-familias/eliminar/' . $miembros_familia->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('alquilers?miembro_id=' . $miembros_familia->getId()) ?>"
		class="btn btn-outline-primary" title="Alquilers Miembros_familia">
        <i class="fa fa-plus fa-lg"></i>
	</a>
	<a href="<?php echo site_url('zonas?miembro_id=' . $miembros_familia->getId()) ?>"
		class="btn btn-outline-primary" title="Zonas Miembros_familia">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Rol Miembro</strong>:
		<?php echo h($miembros_familia->getRolMiembroRelatedByRolMiembro()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Familia</strong>:
		<?php echo h($miembros_familia->getFamiliaRelatedByFamilia()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Persona</strong>:
		<?php echo h($miembros_familia->getUsuariosRelatedByPersona()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($miembros_familia->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($miembros_familia->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($miembros_familia->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>