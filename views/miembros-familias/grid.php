<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm miembros_familia-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MiembrosFamilia::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MiembrosFamilia::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MiembrosFamilia::ROL_MIEMBRO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MiembrosFamilia::ROL_MIEMBRO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Rol Miembro
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MiembrosFamilia::FAMILIA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MiembrosFamilia::FAMILIA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Familia
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => MiembrosFamilia::PERSONA))) ?>">
					<?php if ( @$_REQUEST['order_by'] == MiembrosFamilia::PERSONA): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Persona
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($miembros_familias as $key => $miembros_familia): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($miembros_familia->getId()) ?>&nbsp;</td>
			<td><?php echo h($miembros_familia->getRolMiembroRelatedByRolMiembro()->nombre_miembro) ?>&nbsp;</td>
			<td><?php echo h($miembros_familia->getFamiliaRelatedByFamilia()->nombre_familia) ?>&nbsp;</td>
			<td><?php echo h($miembros_familia->getUsuariosRelatedByPersona()->nombres) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Miembros_familia"
					href="<?php echo site_url('miembros-familias/mostrar/' . $miembros_familia->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Miembros_familia"
					href="<?php echo site_url('miembros-familias/editar/' . $miembros_familia->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Miembros_familia"
					href="#"
					onclick="conf_eliminar('Eliminar Miembros Familia','¿Está seguro de Eliminar?','<?php echo site_url('miembros-familias/eliminar/' . $miembros_familia->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('alquilers?miembro_id=' . $miembros_familia->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('zonas?miembro_id=' . $miembros_familia->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>