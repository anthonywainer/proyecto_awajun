<h3 align="center">Mostrar Contrato</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('contratos/editar/' . $contrato->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Contrato">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Contrato"		onclick="conf_eliminar('Eliminar Contrato','¿Está seguro de Eliminar?','<?php echo site_url('contratos/eliminar/' . $contrato->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('alquilers?contrato=' . $contrato->getId()) ?>"
		class="btn btn-outline-primary" title="Alquilers Contrato">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Numero Contrato</strong>:
		<?php echo h($contrato->getNumeroContrato()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Folio</strong>:
		<?php echo h($contrato->getFolioRelatedByFolioId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Fecha Contrato</strong>:
		<?php echo h($contrato->getFechaContrato(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Fecha Inicio</strong>:
		<?php echo h($contrato->getFechaInicio(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Fecha Termino</strong>:
		<?php echo h($contrato->getFechaTermino(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($contrato->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($contrato->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($contrato->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>