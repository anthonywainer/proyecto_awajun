<div id="usa">
<h4 align="center"><?php echo $contrato->isNew() ? "Nuevo" : "Editar" ?>    Contrato</h4>
<form method="post" action="<?php echo site_url('contratos/guardar') ?>" onsubmit="registrar_contrato(this); return false">
	<div class="row">
		<input type="hidden" name="id" value="<?php echo h($contrato->getId()) ?>" />
		<div class="col-md-6">
			<label class="form-field-label" for="contrato_numero_contrato">Número de Contrato</label>
			<input required id="contrato_numero_contrato"
                   class="form-control"
                   type="text" name="numero_contrato" value="<?php echo h($contrato->getNumeroContrato()) ?>" />
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="contrato_folio_id">Folio</label>
			<select required id="contrato_folio_id" class="form-control" name="folio_id">
			<?php foreach (Folio::doSelect() as $folio): ?>
				<option <?php if ($contrato->getFolioId() === $folio->getId()) echo 'selected="selected"' ?>
                        value="<?php echo $folio->getId() ?>"><?php echo $folio->getSerie()?></option>
			<?php endforeach ?>
			</select>
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="contrato_fecha_contrato">Fecha Contrato</label>
			<input id="contrato_fecha_contrato"
                   class="form-control datetimepicker"
                   type="text" name="fecha_contrato" value="<?php echo h($contrato->getFechaContrato(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="contrato_fecha_inicio">Fecha Inicio</label>
			<input id="contrato_fecha_inicio"
                   class="form-control datetimepicker"
                   type="text" name="fecha_inicio" value="<?php echo h($contrato->getFechaInicio(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
		<div class="col-md-6">
			<label class="form-field-label" for="contrato_fecha_termino">Fecha Término</label>
			<input id="contrato_fecha_termino"
                   class="form-control datetimepicker"
                   type="text" name="fecha_termino" value="<?php echo h($contrato->getFechaTermino(VIEW_TIMESTAMP_FORMAT)) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $contrato->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>