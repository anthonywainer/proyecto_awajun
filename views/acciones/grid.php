<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="object-grid acciones-grid table table-sm table-bordered" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					ACCIÓN
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($acciones as $key => $acciones): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($acciones->getId()) ?>&nbsp;</td>
			<td><?php echo h($acciones->getAccion()) ?>&nbsp;</td>
			<td>
                <i class="btn btn-outline-primary"  onclick="openmodal(this)" href="<?php echo site_url('acciones/editar/' . $acciones->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>
                </i>
                <a  class="btn btn-outline-danger"   href="#"
                    onclick="conf_eliminar('Eliminar Acciones','¿Está seguro de Eliminar?','<?php echo site_url('acciones/eliminar/' . $acciones->getId()) ?>')">
                    <i   class="fa fa-trash-o fa-lg"></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>