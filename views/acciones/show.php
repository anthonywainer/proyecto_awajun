<h1>View Acciones</h1>
<div class="action-buttons ui-helper-clearfix">
	<a href="<?php echo site_url('acciones/edit/' . $acciones->getId()) ?>"
		class="button" data-icon="pencil" title="Edit Acciones">
		Edit	</a>
	<a href="<?php echo site_url('acciones/delete/' . $acciones->getId()) ?>"
		class="button" data-icon="trash" title="Delete Acciones"
		onclick="return confirm('Are you sure?');">
		Delete	</a>
	<a href="<?php echo site_url('permisos?idaccion=' . $acciones->getId()) ?>"
		class="button" data-icon="carat-1-e" title="Permisos Acciones">
		Permisos	</a>
</div>
<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
	<div class="field-wrapper">
		<span class="field-label">Accion</span>
		<?php echo h($acciones->getAccion()) ?>
	</div>
</div>