
<div id="usa">
<h6><div align="center" > <?php echo $acciones->isNew() ? "NUEVA" : "EDITAR" ?> ACCIONES</h6>
<form method="post" action="<?php echo site_url('acciones/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($acciones->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="acciones_accion">Acción</label>

			<input required class="form-control" onkeypress="return soloLetras(event)" id="acciones_accion" type="text" name="accion" value="<?php echo h($acciones->getAccion()) ?>" />
		</div>
	</div>
    <br>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
        <span class="button" data-icon="disk">
			<input style="background: #3C8DBC; color: white"  class="btn btn-primary" type="submit" value="<?php echo $acciones->isNew() ? "Guardar" : "Guardar " ?>" />
		</span>

        <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-danger" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
        <?php endif ?>


	</div>
</form>