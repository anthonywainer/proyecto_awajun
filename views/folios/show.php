<h3 align="center">Mostrar Folio</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('folios/editar/' . $folio->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Folio">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Folio"		onclick="conf_eliminar('Eliminar Folio','¿Está seguro de Eliminar?','<?php echo site_url('folios/eliminar/' . $folio->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('contratos?folio_id=' . $folio->getId()) ?>"
		class="btn btn-outline-primary" title="Contratos Folio">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Serie</strong>:
		<?php echo h($folio->getSerie()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Hojas</strong>:
		<?php echo h($folio->getHojas()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($folio->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($folio->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($folio->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>