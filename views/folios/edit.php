<div id="usa">
<h4 align="center"><?php echo $folio->isNew() ? "Nuevo" : "Editar" ?>    Folio</h4>
<form method="post" action="<?php echo site_url('folios/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($folio->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="folio_serie">Serie</label>
			<input id="folio_serie"
                   class="form-control"
                   type="text" name="serie" value="<?php echo h($folio->getSerie()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="folio_hojas">Hojas</label>
			<input id="folio_hojas"
                   class="form-control"
                   type="text" name="hojas" value="<?php echo h($folio->getHojas()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $folio->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>