<div class="container-fluid">
<h3 align="center">
	Lista de Predios</h3>
    <div class="row">
        <div class="form-group col-md-12">

            <div class="input-group">
                <a href="/proyecto_awajun/public/predios/editar"
                   class="button"
                   data-icon="plusthick"
                   title="Nuevo Predio">
                    <button class="btn btn-default" style="background: #3C8DBC; color: white">
                        Registrar
                    </button>
                </a>
                <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                       href="/proyecto_awajun/public/predios/index?search="
                       value="<?= $_GET['search'] ?>"
                       class="form-control" placeholder="Buscar Predio" style="margin-left: 40%"/>
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
    </div>

<div class="ui-widget-content ui-corner-all">
	<?php View::load('predios/grid', $params) ?></div>

<?php View::load('pager', compact('pager')) ?><?php View::load('layouts/modal') ?></div>
