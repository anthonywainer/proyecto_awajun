<style>
    #map_canvas_container{
        border: 1px solid #aaaaaa;
        padding:15px;
        border-radius: 10px;
    }
    #map_canvas{
        height:300px;width:100%;
    }
    .maps_divs{width:100%;height:330px;}
    #plano-coordenadas-p{
        font-size: 0.7em;
    }
</style>
<div class="container-fluid">
    <h3 align="left">Mostrar Predio</h3>
	<a href="<?php echo site_url('predios/editar/' . $predio->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Predio">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Predio"		onclick="conf_eliminar('Eliminar Predio','¿Está seguro de Eliminar?','<?php echo site_url('predios/eliminar/' . $predio->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a  href="<?= site_url('predios/zonas/'.$predio->getId()) ?>"
		class="btn btn-outline-primary" title="Zonas Predio">
        <i class="fa fa-map-marker"></i>
	</a>
</div>
<div class="container-fluid row">
    <div class="col-md-3">
        <div class="field-wrapper">
            <strong class="field-label">Familia</strong>:
            <?php echo h($predio->getFamiliaRelatedByFamiliaId()->getNombreFamilia()) ?>
        </div>
        <div class="field-wrapper">
            <strong class="field-label">Número de Predio</strong>:
            <?php echo h($predio->getNumeroPredio()) ?>
        </div>
        <div class="field-wrapper">
            <strong class="field-label">Fecha Registro</strong>:
            <?php echo h($predio->getFechaRegistro(VIEW_TIMESTAMP_FORMAT)) ?>
        </div>
        <div class="field-wrapper">
            <strong class="field-label">Dirección</strong>:
            <?php echo h($predio->getDireccion()) ?>
        </div>
        <div class="field-wrapper">
            <strong class="field-label">Coordenadas</strong>:
            <p id="plano-coordenadas-p"><?php echo h($predio->getCoordenadas()) ?></p>
        </div>
        <div class="field-wrapper">
            <strong class="field-label">Área</strong>:
            <?php echo h($predio->getArea()).' m&sup2' ?>
            <?php $coor=  "[{'coor':".$predio->getCoordenadas().",'color':'".$predio->getColor()."'}" ?>
            <input type="hidden" name="coordenadas" value="<?=$coor.']'?>">
        </div>
    </div>
    <div class="col-md-9">
        <section class="container clearfix">
            <div id="map_container" class="row">
                <div id="map_canvas_container" class="container maps_divs col-md-12">
                    <div id="map_canvas"></div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACJapLIVhm-uVWitwICh24232jYdkP1SQ&language=es&libraries=places"></script>
<script>
    var map = null;
    var default_zoom = 18;
    var map_div = "map_canvas";

    function initialize() {
        coordenadas_predio = eval($("input[name=coordenadas]").val());

        default_lat = coordenadas_predio[0].coor[0].lat;
        default_lng = coordenadas_predio[0].coor[0].lng;
        var latlng = new google.maps.LatLng(default_lat, default_lng);


        var mapOptions = {
            scaleControl: true,
            zoom: default_zoom,
            zoomControl: true,
            zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
            panControl: false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById(map_div), mapOptions);

        $.each(coordenadas_predio,function (i,v) {
            new google.maps.Polygon({
                map: map,
                path: v.coor,
                geodesic: true,
                strokeColor: v.color,
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: v.color,
                fillOpacity: 0.2
            });
        });


    }

</script>
