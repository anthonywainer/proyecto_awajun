<style>
    #map_canvas_container{
        border: 1px solid #aaaaaa;
        padding:15px;
        border-radius: 10px;
    }
    #map_canvas{
        height:300px;width:100%;
    }
    .maps_divs{width:100%;height:330px;}

</style>
<div id="usa">
<div class="row">
    <div class="col-md-10">
        <h4 align="left">Zonas del Predio N° <?= $predio->numero_predio ?></h4>
    </div>
    <div class="col-md-2" align="right">
        <div class="form-inline">
            <a href="<?= site_url('predios/mostrar/'.$predio->getId()) ?>">Predio</a>  /
             <a href="<?= site_url('predios/zonas/'.$predio->getId()) ?>" >Zonas</a>
        </div>
    </div>
</div>

    <div class="row container">
        <div class="col-md-4">
            <div class="form-field-wrapper">
                <strong>Familia "<?php echo $predio->getFamilia()->nombre_familia ?>"</strong>
                <?php $coor=  "[{'coor':".$predio->getCoordenadas().",'color':'".$predio->getColor()."'}" ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    Miembros:
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombres</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $c=0; foreach (MiembrosFamilia::getAll('where familia='.$predio->getFamilia_id()) as $mf): ?>
                                <tr>
                                    <th><?php $c++; echo $c;?></th>
                                    <th><?= RolMiembro::getAll("where id=".$mf->getRol_miembro())[0]->getNombre_miembro() ?> -
                                        <?= Usuarios::getAll("where id=".$mf->getPersona())[0]->getNombres()." ".Usuarios::getAll("where id=".$mf->getPersona())[0]->getApellidos() ?>
                                    </th>
                                    <th>
                                        <?php $zz= Zonas::getAll('where predio_id='.$predio->getId().' and miembro_id='.$mf->getId()) ?>
                                        <?php if($zz): ?>
                                            <?php $zz=$zz[0]; $coor.=",{'coor':".$zz->getCoordenadas().",'color':'".$zz->getColor()."'}" ?>
                                            <a
                                               title="Editar Zona" href="<?=site_url('zonas/editar/'.$zz->id)?>">
                                                <i class="fa fa-edit fa-lg"></i>
                                            </a>
                                            <a style="color: red"  title="Eliminar Zona" href="#" onclick="conf_eliminar('Eliminar Zona','¿Está seguro de Eliminar?','<?= site_url('zonas/eliminar/'.$zz->id)?>');">
                                                <i class="fa fa-trash-o fa-lg"></i>
                                            </a>
                                            <a style="color: green" title="Ver Cultivos" href="<?=site_url('zonas/cultivos/'.$zz->id)?>">
                                                <i class="fa fa-pagelines fa-lg"></i>
                                            </a>
                                        <?php else: ?>
                                            <a class="btn btn-outline-primary"
                                               title="Agregar Zona" href="<?=site_url('zonas/editar/?ip='.$predio->getId().'&im='.$mf->getId())?>">
                                                <i class="fa fa-plus fa-lg"></i>
                                            </a>
                                        <?php endif; ?>

                                    </th>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
        <div class="col-md-8" >
            <section class="container clearfix">
                <div id="map_container" class="row">
                    <div id="map_canvas_container" class="container maps_divs col-md-12">
                        <div id="map_canvas"></div>
                    </div>
                </div>
            </section>

        </div>
        <input type="hidden" name="coordenadas" value="<?=$coor.']'?>">
    </div>
</form>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACJapLIVhm-uVWitwICh24232jYdkP1SQ&language=es&libraries=places"></script>
<script>
    var map = null;
    var default_zoom = 18;
    var map_div = "map_canvas";

    function initialize() {
        coordenadas_predio = eval($("input[name=coordenadas]").val());

        default_lat = coordenadas_predio[0].coor[0].lat;
        default_lng = coordenadas_predio[0].coor[0].lng;
        var latlng = new google.maps.LatLng(default_lat, default_lng);


        var mapOptions = {
            scaleControl: true,
            zoom: default_zoom,
            zoomControl: true,
            zoomControlOptions: {style: google.maps.ZoomControlStyle.SMALL},
            panControl: false,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.HYBRID
        };
        map = new google.maps.Map(document.getElementById(map_div), mapOptions);

        $.each(coordenadas_predio,function (i,v) {
            new google.maps.Polygon({
                map: map,
                path: v.coor,
                geodesic: true,
                strokeColor: v.color,
                strokeOpacity: 1.0,
                strokeWeight: 2,
                fillColor: v.color,
                fillOpacity: 0.2
            });
        });


    }

</script>
