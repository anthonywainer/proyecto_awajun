<style>
    #map_canvas_container{
        border: 1px solid #aaaaaa;
        padding:15px;
        border-radius: 10px;
    }
    #map_canvas{
        height:300px;width:100%;
    }
    .maps_divs{width:100%;height:330px;}
    #plano-coordenadas-p{
        font-size: 0.7em;
    }
</style>
<div id="usa">
<div class="row">
    <div class="col-md-10">
        <h4 align="left"><?php echo $predio->isNew() ? "Nuevo" : "Editar" ?>    Predio</h4>
    </div>
    <div class="col-md-2" align="right">
        <div class="form-inline">
            <a href="<?= site_url('predios/mostrar/'.$predio->getId()) ?>">Predio</a>  /
            <?php if($predio->getId()): ?>  <a href="<?= site_url('predios/zonas/'.$predio->getId()) ?>" >Zonas</a>
            <?php else: ?>
            Zonas <?php endif; ?>
        </div>
    </div>
</div>

<form method="post" action="<?php echo site_url('predios/guardar') ?>">
    <div class="row container">
        <div class="col-md-4">
            <input type="hidden" id="idpredio" name="id" value="<?php echo h($predio->getId()) ?>" />
            <div class="form-field-wrapper">
                <label class="form-field-label" for="predio_familia_id">Familia:</label>
                <select id="predio_familia_id" required class="form-control" name="familia_id">
                    <?php foreach (Familia::doSelect() as $familia): ?>
                        <option <?php if ($predio->getFamiliaId() === $familia->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $familia->getId() ?>"><?php echo $familia->getNombreFamilia()?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-field-wrapper">
                        <label class="form-field-label" for="predio_numero_predio">Número Predio:</label>
                        <input required id="predio_numero_predio"
                               class="form-control"
                               type="text" name="numero_predio" value="<?php echo h($predio->getNumeroPredio()) ?>" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-field-label" for="predio_fecha_registro">Fecha Registro:</label>
                        <div class='input-group' >
                            <input required id="predio_fecha_registro"
                                   class="form-control datetimepicker"
                                   type="text" name="fecha_registro" value="<?php echo h($predio->getFechaRegistro(VIEW_TIMESTAMP_FORMAT)) ?>" />
                        </div>
                    </div>
                </div>
                <form action="#" class="fromAddress printHidden row">
                    <label for="">Dirección:</label>
                    <div class="col-md-12 col-sm-12 col-xs-12 form-inline">
                        <input id="search_address" type="search"
                               required
                               name="direccion" style="width: 85%;"
                               value="<?= $predio->getDireccion() ?>"
                               class="textBefore vpsClearOnFocus form-control"
                               placeholder="calle, número de casa, ciudad, etc..." />
                        <button id="btn_show_search_result" type="submit"
                                class="submitOn btn btn-default fa fa-search" >
                        </button>
                        <div class="clear"></div>
                    </div>
                </form>
                <div class="col-md-12">
                    <br>
                    <label for="">Plano Coordenadas:</label>
                    <p id="plano-coordenadas-p"><?= $predio->getCoordenadas() ?></p>
                    <input  type="hidden" name="coordenadas"
                           value="<?= $predio->getCoordenadas() ?>"
                           id="plano-coordenadas">
                </div>
                <div class="col-md-12">
                    <br>
                    <strong>Área:</strong><br>
                    <div id="map_calculation_result_container">
                        <div id="polyArea">
                            <?= $predio->getArea().' m&sup2' ?><br>
                            <?= ($predio->getArea()/10000).' Hectárea' ?>
                        </div>
                        <input type="hidden" name="area" value="<?= $predio->getArea() ?>">
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-8" >
            <section class="container clearfix">
                <div id="map_container" class="row">

                    <div id="search_street_container" class="col-md-12">
                        <div id="map_controls_options" >
                            <div class="clearfix row" >
                                <div class="col-md-12 form-inline">
                                    <p style="text-align: right">
                                        <input type="color" required value="<?= $predio->getColor()? $predio->getColor():'#ffffff' ?>" name="color" id="colorp" >
                                        <input class="btn btn-info" type="button" onclick="clearMarkers();" value="Borrar Marcadores">
                                        <input class="btn btn-danger" type="button" onclick="removeLastMarker();" value="Borrar último Marcador">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="map_canvas_container" class="container maps_divs col-md-12">
                        <div id="map_canvas"></div>
                    </div>
                </div>
            </section>

            <div align="right">
            <span class="button" data-icon="disk">
                <input type="submit"  class="btn btn-primary" value="<?php echo $predio->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
            </span>
            <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
            <a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                Cancelar
            </a>
            </div>
            <?php endif ?>
        </div>
    </div>
</form>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACJapLIVhm-uVWitwICh24232jYdkP1SQ&language=es&libraries=places"></script>
<script src="<?= site_url('js/coords.js') ?>"></script>
