<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm predio-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Predio::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Predio::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					#
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Predio::NUMERO_PREDIO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Predio::NUMERO_PREDIO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Número de Predio
				</a>
			</th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Predio::FECHA_REGISTRO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Predio::FECHA_REGISTRO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha de Registro
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Predio::FAMILIA_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Predio::FAMILIA_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Familia
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">Acción</th>
		</tr>
	</thead>
	<tbody>
<?php $c=0;foreach ($predios as $key => $predio): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php $c++; echo $c ?>&nbsp;</td>
			<td><?php echo h($predio->getNumeroPredio()) ?>&nbsp;</td>
			<td><?php echo h($predio->getFechaRegistro(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($predio->getFamiliaRelatedByFamiliaId()->nombre_familia) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Predio"
					href="<?php echo site_url('predios/mostrar/' . $predio->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Predio"
					href="<?php echo site_url('predios/editar/' . $predio->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Predio"
					href="#"
					onclick="conf_eliminar('Eliminar Predio','¿Está seguro de Eliminar?','<?php echo site_url('predios/eliminar/' . $predio->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('alquilers?predio_id=' . $predio->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>
				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('zonas?predio_id=' . $predio->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>