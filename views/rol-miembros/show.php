<h3 align="center">Mostrar Rol Miembro</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('rol-miembros/editar/' . $rol_miembro->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Rol_miembro">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Rol_miembro"		onclick="conf_eliminar('Eliminar Rol Miembro','¿Está seguro de Eliminar?','<?php echo site_url('rol-miembros/eliminar/' . $rol_miembro->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
	<a href="<?php echo site_url('miembros-familias?rol_miembro=' . $rol_miembro->getId()) ?>"
		class="btn btn-outline-primary" title="Miembros Familias Rol_miembro">
        <i class="fa fa-plus fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Nombre Miembro</strong>:
		<?php echo h($rol_miembro->getNombreMiembro()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($rol_miembro->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($rol_miembro->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($rol_miembro->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>