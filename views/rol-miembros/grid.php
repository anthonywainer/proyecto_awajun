<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm rol_miembro-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => RolMiembro::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == RolMiembro::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => RolMiembro::NOMBRE_MIEMBRO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == RolMiembro::NOMBRE_MIEMBRO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Nombre Miembro
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($rol_miembros as $key => $rol_miembro): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($rol_miembro->getId()) ?>&nbsp;</td>
			<td><?php echo h($rol_miembro->getNombreMiembro()) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Rol_miembro"
					href="<?php echo site_url('rol-miembros/mostrar/' . $rol_miembro->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Rol_miembro"
					href="<?php echo site_url('rol-miembros/editar/' . $rol_miembro->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Rol_miembro"
					href="#"
					onclick="conf_eliminar('Eliminar Rol Miembro','¿Está seguro de Eliminar?','<?php echo site_url('rol-miembros/eliminar/' . $rol_miembro->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-plus"
					href="<?php echo site_url('miembros-familias?rol_miembro=' . $rol_miembro->getId()) ?>">
                    <i class="fa fa-plus fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>