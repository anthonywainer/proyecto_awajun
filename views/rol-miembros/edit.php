<div id="usa">
<h4 align="center"><?php echo $rol_miembro->isNew() ? "Nuevo" : "Editar" ?>    Rol Miembro</h4>
<form method="post" action="<?php echo site_url('rol-miembros/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($rol_miembro->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="rol_miembro_nombre_miembro">Nombre Miembro</label>
			<input id="rol_miembro_nombre_miembro"
                   class="form-control"
                   type="text" name="nombre_miembro" value="<?php echo h($rol_miembro->getNombreMiembro()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $rol_miembro->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>