<div id="usa">
<form class="container row" method="post" action="<?php echo site_url('alquilers/guardar') ?>">
	<div class="col-md-8">
        <h4 align="center"><?php echo $alquiler->isNew() ? "Nuevo" : "Editar" ?> Alquiler </h4>
		<input type="hidden" name="id" value="<?php echo h($alquiler->getId()) ?>" />
        <div class="row">
            <div class="col-md-6">
                <label class="form-field-label" for="alquiler_fecha_alquiler">Fecha Alquiler</label>
                <input id="alquiler_fecha_alquiler"
                       class="form-control datetimepicker"
                       type="text" name="fecha_alquiler" value="<?php echo h($alquiler->getFechaAlquiler(VIEW_TIMESTAMP_FORMAT)) ?>" />
            </div>
            <div class="col-md-6">
                <label class="form-field-label" for="alquiler_arrendatario">Arrendatario</label>
                <div class="form-inline">
                    <select id="alquiler_arrendatario" class="form-control" name="arrendatario">
                        <?php foreach (UsuariosGrupo::getAll('where grupo_id=5 and deleted_at is null') as $u): ?>
                            <option <?php if($u){ $usuarios=$u->getUsuarios();  if ($alquiler->getArrendatario() === $usuarios->getId()) echo 'selected="selected"' ?>
                                value="<?php echo $usuarios->getId() ?>">
                                <?php echo $usuarios->dni." - ".$usuarios->getNombres()." ".$usuarios->getApellidos()?></option>
                            <?php } endforeach ?>
                        <option value="">--> Elija <--</option>
                    </select>
                    <i onclick="openmodal(this)" href="<?=site_url('usuarios/editar?grupos=5')?>" style="color: deepskyblue; cursor: pointer;" class="fa fa-plus-circle"></i>
                </div>
            </div>
            <div class="col-md-6">
                <label class="form-field-label" for="alquiler_contrato">Contrato</label>
                <div class="form-inline">
                    <select id="alquiler_contrato" class="form-control" name="contrato">
                        <?php foreach (Contrato::getAll('where deleted_at is null') as $contrato): ?>
                            <option <?php if ($alquiler->getContrato() === $contrato->getId()) echo 'selected="selected"' ?> value="<?php echo $contrato->getId() ?>">
                                <?php echo $contrato->getNumero_contrato()?></option>
                        <?php endforeach ?>
                        <option value="">--> Elija <--</option>
                    </select>
                    <i onclick="openmodal(this)" href="<?=site_url('contratos/editar')?>" style="color: deepskyblue; cursor: pointer;" class="fa fa-plus-circle"></i>
                </div>
            </div>
            <hr>

            <div class="col-md-10">
                <label for="">Cultivos</label>
                <select name="cultivos[]" class="form-control" id="" multiple>
                    <?php
                    $ss="and id NOT IN ( SELECT cultivo_id AS id FROM cultivo_alquiler where deleted_at is null )";
                    if ($alquiler->getId()){
                        $ss='';
                    }
                    foreach (Cultivo::getAll('where deleted_at is null '.$ss) as $c): ?>
                        <option value="<?=$c->id?>"
                            <?php if ($alquiler->getId()){
                                $alq=CultivoAlquiler::getAll('where deleted_at is null and cultivo_id='.$c->id.' and alquiler_id='.$alquiler->getId());
                                if ($alq){ echo "selected"; }
                            }  ?>
                        >
                            <?php
                            $uuu = Usuarios::getAll('where id= '.Zonas::getAll('where id='.$c->getZona())[0]->getMiembro()->getPersona())[0];
                            echo Siembra::getAll('where id='.$c->getTipoSiembra())[0]->getSiembra().' -> '.$uuu->getDni()." - ".$uuu->getNombres().' '.$uuu->getApellidos()  ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <hr>
            <br><br><br><br><br><br>
            <div class="col-md-12" align="center">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $alquiler->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
                <?php if (isset($_SERVER['HTTP_REFERER'])): ?>
                    <a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                        Cancelar
                    </a>
                <?php endif ?>
            </div>
        </div>
	</div>
    <hr>

</form>
</div>
<?php View::load('layouts/modal') ?>
<script>
    function checkDni(t){
        if($(t).val().length>0 && $(t).val().length<=7) {
            $("#mensaje").html("<span style='color:red'> DNI INCOMPLETO</span>");
            $('#btnEnviar').attr('disabled',true);
        }
        else if($(t).val().length==8) {
            $("#mensaje").html("<span style='color:blue'>DNI VÁLIDO</span>");
            $("#usuarios_clave").val($(t).val());
            $('#btnEnviar').attr('disabled',false);
        }
        else{
            $("#mensaje").html("<span style='color:red'> Error! maximo 8 caracteres</span>");
            $('#btnEnviar').attr('disabled',true);
        }
    }


    function registrar(t) {
        $('input[type="submit"]').attr('disabled',true);
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url, param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#usuarios_dni").val() + "," + $("#usuarios_nombres").val() + " " + $("#usuarios_apellidos").val();
            var option = new Option(nom, id);
            $("#alquiler_arrendatario").append(option);
            $("#alquiler_arrendatario").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
    function registrar_contrato(t) {
        $('input[type="submit"]').attr('disabled',true);
        url = $(t).attr('action');
        param = $(t).serialize();
        $.post(url, param+'&json=1').done(function (id) {
            toastr.success('guardado correctamente');
            nom = $("#contrato_numero_contrato").val();
            var option = new Option(nom, id);
            $("#alquiler_contrato").append(option);
            $("#alquiler_contrato").val(id).trigger('change');
            $("#primaryModal").modal("hide");
        });
    }
</script>

