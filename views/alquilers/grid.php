<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm alquiler-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Alquiler::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Alquiler::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Alquiler::ARRENDATARIO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Alquiler::ARRENDATARIO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Arrendatario
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Alquiler::CONTRATO))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Alquiler::CONTRATO): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Contrato
				</a>
			</th>

			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Alquiler::FECHA_ALQUILER))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Alquiler::FECHA_ALQUILER): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Fecha de Alquiler
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => Alquiler::REGISTRADOR))) ?>">
					<?php if ( @$_REQUEST['order_by'] == Alquiler::REGISTRADOR): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Registrador
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($alquilers as $key => $alquiler): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($alquiler->getId()) ?>&nbsp;</td>
			<td><?php echo h($alquiler->getUsuariosRelatedByArrendatario()->nombres) ?>&nbsp;</td>
			<td><?php echo h($alquiler->getContratoRelatedByContrato()->numero_contrato) ?>&nbsp;</td>
			<td><?php echo h($alquiler->getFechaAlquiler(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($alquiler->getUsuariosRelatedByRegistrador()->nombres) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Alquiler"
					href="<?php echo site_url('alquilers/mostrar/' . $alquiler->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Alquiler"
					href="<?php echo site_url('alquilers/editar/' . $alquiler->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Alquiler"
					href="#"
					onclick="conf_eliminar('Eliminar Alquiler','¿Está seguro de Eliminar?','<?php echo site_url('alquilers/eliminar/' . $alquiler->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>