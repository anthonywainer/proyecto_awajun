<div class="container-fluid">
<h3 align="center">
	Lista de Alquilers</h3>
    <div class="row">
        <div class="form-group col-md-12">

            <div class="input-group">
                <a href="<?=  site_url('alquilers/editar') ?>"
                   class="button"
                   data-icon="plusthick"
                   title="Nuevo Alquiler">
                    <button class="btn btn-default" style="background: #3C8DBC; color: white">
                        Registrar
                    </button>
                </a>
                <input id="filtrar" type="search" onkeyup="buscar_tabla_ajax(this)"
                       href="<?= site_url('alquilers/index?search=') ?>"
                       value="<?= $_GET['search'] ?>"
                       class="form-control" placeholder="Buscar Alquiler" style="margin-left: 40%"/>
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
            </div>
        </div>
    </div>

<div class="ui-widget-content ui-corner-all">
	<?php View::load('alquilers/grid', $params) ?></div>

<?php View::load('pager', compact('pager')) ?></div>
