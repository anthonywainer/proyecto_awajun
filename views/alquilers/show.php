<h3 align="center">Mostrar Alquiler</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('alquilers/editar/' . $alquiler->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Alquiler">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Alquiler"		onclick="conf_eliminar('Eliminar Alquiler','¿Está seguro de Eliminar?','<?php echo site_url('alquilers/eliminar/' . $alquiler->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Arrendatario</strong>:
		<?php echo h($alquiler->getUsuariosRelatedByArrendatario()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Contrato</strong>:
		<?php echo h($alquiler->getContratoRelatedByContrato()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Miembro Id</strong>:
		<?php echo h($alquiler->getMiembroId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Fecha Alquiler</strong>:
		<?php echo h($alquiler->getFechaAlquiler(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($alquiler->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($alquiler->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($alquiler->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Registrador</strong>:
		<?php echo h($alquiler->getUsuariosRelatedByRegistrador()) ?>
	</div>
</div>