<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<table class="table table-sm cultivo_alquiler-grid" cellspacing="0">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => CultivoAlquiler::ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == CultivoAlquiler::ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => CultivoAlquiler::CULTIVO_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == CultivoAlquiler::CULTIVO_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Cultivo Id
				</a>
			</th>
			<th class="ui-widget-header ">
				<a href="?<?php echo http_build_query(array_merge($_get_args, array('order_by' => CultivoAlquiler::ALQUILER_ID))) ?>">
					<?php if ( @$_REQUEST['order_by'] == CultivoAlquiler::ALQUILER_ID): ?>
						<span class="ui-icon ui-icon-carat-1-<?php echo isset($_REQUEST['dir']) ? 's' : 'n' ?>"></span>
					<?php endif ?>
					Alquiler Id
				</a>
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($cultivo_alquilers as $key => $cultivo_alquiler): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($cultivo_alquiler->getId()) ?>&nbsp;</td>
			<td><?php echo h($cultivo_alquiler->getCultivoId()) ?>&nbsp;</td>
			<td><?php echo h($cultivo_alquiler->getAlquilerId()) ?>&nbsp;</td>
			<td><?php echo h($cultivo_alquiler->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($cultivo_alquiler->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td><?php echo h($cultivo_alquiler->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>&nbsp;</td>
			<td>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-search"
					title="Mostrar Cultivo_alquiler"
					href="<?php echo site_url('cultivo-alquilers/mostrar/' . $cultivo_alquiler->getId()) ?>">
                    <i class="fa fa-search fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-primary"
					data-icon="fa-edit"
					title="Editar Cultivo_alquiler"
					href="<?php echo site_url('cultivo-alquilers/editar/' . $cultivo_alquiler->getId()) ?>">
                    <i class="fa fa-edit fa-lg"></i>

				</a>
				<a
					class="btn btn-outline-danger"
					data-icon="fa-trash-o"
					title="Eliminar Cultivo_alquiler"
					href="#"
					onclick="conf_eliminar('Eliminar Cultivo Alquiler','¿Está seguro de Eliminar?','<?php echo site_url('cultivo-alquilers/eliminar/' . $cultivo_alquiler->getId()) ?>');">
                    <i class="fa fa-trash-o fa-lg"></i>

				</a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>