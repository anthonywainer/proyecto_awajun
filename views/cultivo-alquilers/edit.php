<div id="usa">
<h4 align="center"><?php echo $cultivo_alquiler->isNew() ? "Nuevo" : "Editar" ?>    Cultivo Alquiler</h4>
<form method="post" action="<?php echo site_url('cultivo-alquilers/guardar') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($cultivo_alquiler->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="cultivo_alquiler_cultivo_id">Cultivo Id</label>
			<input id="cultivo_alquiler_cultivo_id"
                   class="form-control"
                   type="text" name="cultivo_id" value="<?php echo h($cultivo_alquiler->getCultivoId()) ?>" />
		</div>
		<div class="form-field-wrapper">
			<label class="form-field-label" for="cultivo_alquiler_alquiler_id">Alquiler Id</label>
			<input id="cultivo_alquiler_alquiler_id"
                   class="form-control"
                   type="text" name="alquiler_id" value="<?php echo h($cultivo_alquiler->getAlquilerId()) ?>" />
		</div>
	</div>
    <hr>
	<div class="form-action-buttons ui-helper-clearfix" align="right">
		<span class="button" data-icon="disk">
			<input type="submit"  class="btn btn-primary" value="<?php echo $cultivo_alquiler->isNew() ? "Guardar" : "Guardar Cambios" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="btn btn-danger" data-dismiss="modal" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancelar
		</a>
		<?php endif ?>
	</div>
</form>
</div>