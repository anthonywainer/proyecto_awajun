<h3 align="center">Mostrar Cultivo Alquiler</h3>
<div class="container-fluid">
	<a href="<?php echo site_url('cultivo-alquilers/editar/' . $cultivo_alquiler->getId()) ?>"
		class="btn btn-outline-primary" title="Editar Cultivo_alquiler">
        <i class="fa fa-edit fa-lg"></i>
	</a>
	<a href="#"
		class="btn  btn-outline-danger " title="Eliminar Cultivo_alquiler"		onclick="conf_eliminar('Eliminar Cultivo Alquiler','¿Está seguro de Eliminar?','<?php echo site_url('cultivo-alquilers/eliminar/' . $cultivo_alquiler->getId()) ?>');">
        <i class="fa fa-trash-o fa-lg"></i>
	</a>
</div>
<div class="container-fluid">
	<div class="field-wrapper">
		<strong class="field-label">Cultivo Id</strong>:
		<?php echo h($cultivo_alquiler->getCultivoId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Alquiler Id</strong>:
		<?php echo h($cultivo_alquiler->getAlquilerId()) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Created At</strong>:
		<?php echo h($cultivo_alquiler->getCreatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Updated At</strong>:
		<?php echo h($cultivo_alquiler->getUpdatedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
	<div class="field-wrapper">
		<strong class="field-label">Deleted At</strong>:
		<?php echo h($cultivo_alquiler->getDeletedAt(VIEW_TIMESTAMP_FORMAT)) ?>
	</div>
</div>