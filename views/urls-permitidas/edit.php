<h1><?php echo $urls_permitidas->isNew() ? "New" : "Edit" ?> Urls Permitidas</h1>
<form method="post" action="<?php echo site_url('urls-permitidas/save') ?>">
	<div class="ui-widget-content ui-corner-all ui-helper-clearfix">
		<input type="hidden" name="id" value="<?php echo h($urls_permitidas->getId()) ?>" />
		<div class="form-field-wrapper">
			<label class="form-field-label" for="urls_permitidas_url">Url</label>
			<input id="urls_permitidas_url" type="text" name="url" value="<?php echo h($urls_permitidas->getUrl()) ?>" />
		</div>
	</div>
	<div class="form-action-buttons ui-helper-clearfix">
		<span class="button" data-icon="disk">
			<input type="submit" value="<?php echo $urls_permitidas->isNew() ? "Save" : "Save Changes" ?>" />
		</span>
		<?php if (isset($_SERVER['HTTP_REFERER'])): ?>
		<a class="button" data-icon="cancel" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
			Cancel
		</a>
		<?php endif ?>
	</div>
</form>