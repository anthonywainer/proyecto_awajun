<?php
$_get_args = (array) @$_GET;

if (isset($_REQUEST['dir'])) {
	unset($_get_args['dir']);
} elseif (isset($_REQUEST['order_by'])) {
	$_get_args['dir'] = 'DESC';
}
?>
<link type="text/css" rel="stylesheet" href="<?php echo site_url('css/estilosh.css', true) ?>">
<table class="object-grid permisos-grid table table-condensed table-bordered table table-striped" cellspacing="0" id="table-responsive">
	<thead>
		<tr>
			<th class="ui-widget-header ui-corner-tl">
					ID
			</th>
			<th class="ui-widget-header ">
					ID ACCIÓN
			</th>
            <th class="ui-widget-header ">
                    MÓDULO
            </th>
			<th class="ui-widget-header ">
					ID SUBMÓDULO
			</th>
			<th class="ui-widget-header ">
					NOMBRE
			</th>
			<th class="ui-widget-header grid-action-column ui-corner-tr">&nbsp;ACCIONES</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($permisos as $key => $permisos): ?>
		<tr class="<?php echo ($key & 1) ? 'even' : 'odd' ?> ui-widget-content">
			<td><?php echo h($permisos->getId()) ?>&nbsp;</td>
			<td><?php echo Acciones::getAll('where id= '.$permisos->idaccion)[0]->getAccion() ?>&nbsp;</td>
            <td><?php if($permisos->getIdmodulo()) echo $permisos->getModulos($permisos->getIdmodulo())->nombre ?>&nbsp;</td>
			<td><?php if($permisos->getIdSubmodulo()) echo Submodulo::getAll('where id='.$permisos->getIdSubmodulo())[0]->nombre ?>&nbsp;</td>
			<td><?php echo h($permisos->getNombre()) ?>&nbsp;</td>
			<td>
                <i class=" btn btn-outline-primary" onclick="openmodal(this)" href="<?php echo site_url('permisos/editar/' . $permisos->getId()) ?>">
                    <i  class="fa fa-edit fa-lg "></i>
                </i>
                <a class="btn btn-outline-danger" href="#"
                   onclick="conf_eliminar('Eliminar Permisos','¿Está seguro de Eliminar?', '<?php echo site_url('permisos/eliminar/' . $permisos->getId()) ?>')">
                    <i  class="fa fa-trash-o fa-lg"></i>
                </a>
			</td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>