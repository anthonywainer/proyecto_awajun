<style>
    .circle-user{
        width: 90%;
        border-radius: 60%;

    }
    .card{
     min-height:40em ;

    }
    .sidebar-fixed .sidebar {

        height: calc(100vh - 20px);
    }

</style>

<header class="app-header navbar" style="background: #096a88; margin-top: -60px">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button" style="margin-top: 4px;border: none; margin-left: 2%">
        <span class=" fa fa-bars" style="color: white; margin-top: 1%; font-size: 2em;margin-left:-1%"></span>
    </button>
    <h5 align="center" style="color: white;  font-weight: 600; margin-top:8px; margin-left: 9px">Proyecto Awajun</h5>
    <button class="navbar-toggler sidebar-toggler d-md-down-none h-100" type="button" style="border: none" >
        <span class="fa fa-bars" style="color: white; font-size: 0.8em"></span>
    </button>

    <ul class="nav navbar-nav ml-auto" >


        <li class="nav-item dropdown d-md-down-none">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-envelope-letter" style="color: white; font-size: 16px"></i><span class="badge badge-pill badge-info" style="color: white">
                    <?php $c=Contactenos::getAll('where estado="no_leido"'); echo count($c) ?>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
                <div class="dropdown-header text-center">
                    <strong>Usted Tiene <?= count($c) ?></strong>
                </div>
                <a href="#" class="dropdown-item">
                    <?php foreach ($c as $i ):?>
                        <div class="message">
                            <?= $i->getEmail().' '.$i->getAsunto() ?>
                        </div>
                    <?php endforeach; ?>
                </a>
                <a href="<?= site_url('contactenos/index')?>" class="dropdown-item text-center">
                    <strong>Ver todos los mensajes</strong>
                </a>
            </div>
        </li>
     <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-settings" style="color: white; font-size: 16px"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="<?= site_url('logout') ?>"><i class="fa fa-lock"></i> Cerrar Sesión</a>
                <a class="dropdown-item" href="<?= site_url('/') ?>"><i class="fa fa-www"></i> Web</a>
            </div>
        </li>


    </ul>
</header>

<div class="app-body">
    <div class="sidebar" style="margin-top: -35px;  ">
        <div class="sidebar-header">
           <!-- <li class="divider" style="list-style: none"></li>-->
            <li class="nav-item" align="center" style="list-style: none">
                <img style="width: 60%" src="<?php echo site_url('imagenes/user.png') ?>" class="img-avatar circle-user">
            </li>
            <li class="nav-title text-center" style="list-style: none; margin-left: -12%">
                <span><?php echo $_SESSION['user']->correo ?></span>
            </li>
        </div>
        <nav class="sidebar-nav">
            <li class="nav-item" style="list-style: none">
                <a class="nav-link" href="<?php echo site_url('admin')?>"><i class="fa fa-power-off fa-lg"></i> INICIO </a>
            </li>
                <ul class="nav">

                <!--<li class="divider"></li>-->
                 <?php  foreach ($modulosPrincipal as $m): ?>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle" href="#" >
                            <i  class="<?php echo $m["icon"] ?>"></i> <?php echo $m["nombre"] ?>
                        </a>
                        <ul class="nav-dropdown-items">
                            <?php foreach ($SubmodulosPrincipal as $sm){ if($sm['idmodulo']==$m["id"]){?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo site_url($sm["url"]) ?>">
                                    <i class="<?php echo $sm["icon"] ?>"></i> <?php echo $sm["nombre"] ?>
                                </a>
                            </li>
                            <?php }} ?>
                        </ul>
                    </li>
                    <?php endforeach; ?>
                </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <!-- Main content -->
    <main class="main" id="main" >

        <div class="container-fluid" style="padding:1%;" >
            <div class="animated fadeIn mostrar">
                <div class="card" style="margin-left: 10px; margin-top: 0px">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <?php echo $params['u'] ?>
                        </li>
                    </ol>

                    <div class="card-block" style="padding: 3px" >
                        <div class="row">
                            <div class="col-sm-12">
                                <?php View::load($params['u'], $params) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

</div>
