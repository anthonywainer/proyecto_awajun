/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 100126
Source Host           : localhost:3306
Source Database       : proyecto_awajun

Target Server Type    : MYSQL
Target Server Version : 100126
File Encoding         : 65001

Date: 2017-12-31 17:05:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for acciones
-- ----------------------------
DROP TABLE IF EXISTS `acciones`;
CREATE TABLE `acciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of acciones
-- ----------------------------
INSERT INTO `acciones` VALUES ('1', 'index');
INSERT INTO `acciones` VALUES ('2', 'editar');
INSERT INTO `acciones` VALUES ('3', 'eliminar');
INSERT INTO `acciones` VALUES ('4', 'guardar');
INSERT INTO `acciones` VALUES ('5', 'permisos');
INSERT INTO `acciones` VALUES ('6', 'aperturar');
INSERT INTO `acciones` VALUES ('7', 'cerrar');
INSERT INTO `acciones` VALUES ('9', 'mostrar');
INSERT INTO `acciones` VALUES ('10', 'tipohabitacion');
INSERT INTO `acciones` VALUES ('11', 'tipoprecio');
INSERT INTO `acciones` VALUES ('12', 'descuento');
INSERT INTO `acciones` VALUES ('13', 'aumento');

-- ----------------------------
-- Table structure for alquiler
-- ----------------------------
DROP TABLE IF EXISTS `alquiler`;
CREATE TABLE `alquiler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `arrendatario` int(11) NOT NULL,
  `contrato` int(11) NOT NULL,
  `plano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dimensiones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `miembro_id` int(11) NOT NULL,
  `predio_id` int(11) NOT NULL,
  `fecha_alquiler` datetime NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_termino` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `registrador` int(11) NOT NULL,
  `estado_alquiler` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alquiler_ibfk_1` (`contrato`),
  KEY `alquiler_ibfk_2` (`miembro_id`),
  KEY `alquiler_ibfk_3` (`predio_id`),
  KEY `alquiler_ibfk_4` (`arrendatario`),
  KEY `registrador` (`registrador`),
  CONSTRAINT `alquiler_ibfk_1` FOREIGN KEY (`contrato`) REFERENCES `contrato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alquiler_ibfk_2` FOREIGN KEY (`miembro_id`) REFERENCES `miembros_familia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alquiler_ibfk_3` FOREIGN KEY (`predio_id`) REFERENCES `predio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alquiler_ibfk_4` FOREIGN KEY (`arrendatario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alquiler_ibfk_5` FOREIGN KEY (`registrador`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of alquiler
-- ----------------------------

-- ----------------------------
-- Table structure for contactenos
-- ----------------------------
DROP TABLE IF EXISTS `contactenos`;
CREATE TABLE `contactenos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `asunto` text NOT NULL,
  `estado` varchar(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contactenos
-- ----------------------------
INSERT INTO `contactenos` VALUES ('3', 'giane', '9349392', 'djfjfmd@gmail.com', 'jdfeuialjwso', 'leido');
INSERT INTO `contactenos` VALUES ('4', 'mirian', '958828292', 'florcita-5tauro@hotmail.com', 'askdkas', 'leido');
INSERT INTO `contactenos` VALUES ('5', ' vgbhjn', '123123', 'Alexa-15-28@hotmail.com', 'weqwe', 'leido');

-- ----------------------------
-- Table structure for contrato
-- ----------------------------
DROP TABLE IF EXISTS `contrato`;
CREATE TABLE `contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_contrato` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folio_id` int(11) NOT NULL,
  `fecha_contrato` datetime NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_termino` datetime NOT NULL,
  `contratista` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `estado_contrato` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `folio_id` (`folio_id`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`folio_id`) REFERENCES `folio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of contrato
-- ----------------------------

-- ----------------------------
-- Table structure for cultivo
-- ----------------------------
DROP TABLE IF EXISTS `cultivo`;
CREATE TABLE `cultivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plano` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dimensiones` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_cultivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_siembra` int(255) DEFAULT NULL,
  `zona` int(255) DEFAULT NULL,
  `cultivador` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `cantidad_produccion` int(11) DEFAULT NULL,
  `fecha_produccion` datetime DEFAULT NULL,
  `cantidad_sembrada` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_siembra` (`tipo_siembra`),
  KEY `zona` (`zona`),
  KEY `cultivador` (`cultivador`),
  CONSTRAINT `cultivo_ibfk_1` FOREIGN KEY (`tipo_siembra`) REFERENCES `siembra` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cultivo_ibfk_2` FOREIGN KEY (`zona`) REFERENCES `zonas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cultivo_ibfk_3` FOREIGN KEY (`cultivador`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cultivo
-- ----------------------------

-- ----------------------------
-- Table structure for familia
-- ----------------------------
DROP TABLE IF EXISTS `familia`;
CREATE TABLE `familia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_familia` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of familia
-- ----------------------------
INSERT INTO `familia` VALUES ('1', 'APIKAI', 'f00012', '2017-12-31 14:12:16', null, null);

-- ----------------------------
-- Table structure for folio
-- ----------------------------
DROP TABLE IF EXISTS `folio`;
CREATE TABLE `folio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hojas` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of folio
-- ----------------------------
INSERT INTO `folio` VALUES ('1', 'Fo0001', '100', '2017-12-31 00:12:57', null, null);

-- ----------------------------
-- Table structure for grupos
-- ----------------------------
DROP TABLE IF EXISTS `grupos`;
CREATE TABLE `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grupos
-- ----------------------------
INSERT INTO `grupos` VALUES ('2', 'Administrador', '2017-12-31 00:00:00', null, null);
INSERT INTO `grupos` VALUES ('3', 'Recepcionista', '2017-11-07 00:00:00', null, null);
INSERT INTO `grupos` VALUES ('4', 'Contador', '2017-11-07 00:00:00', null, null);

-- ----------------------------
-- Table structure for miembros_familia
-- ----------------------------
DROP TABLE IF EXISTS `miembros_familia`;
CREATE TABLE `miembros_familia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol_miembro` int(11) NOT NULL,
  `familia` int(11) NOT NULL,
  `persona` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `familia` (`familia`),
  KEY `tipo_familia` (`rol_miembro`),
  KEY `miembro` (`persona`),
  CONSTRAINT `miembros_familia_ibfk_1` FOREIGN KEY (`familia`) REFERENCES `familia` (`id`),
  CONSTRAINT `miembros_familia_ibfk_2` FOREIGN KEY (`rol_miembro`) REFERENCES `rol_miembro` (`id`),
  CONSTRAINT `miembros_familia_ibfk_3` FOREIGN KEY (`persona`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of miembros_familia
-- ----------------------------
INSERT INTO `miembros_familia` VALUES ('1', '1', '1', '4', '2017-12-31 15:12:04', null, null);
INSERT INTO `miembros_familia` VALUES ('2', '4', '1', '5', '2017-12-31 16:12:36', null, null);

-- ----------------------------
-- Table structure for modulos
-- ----------------------------
DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modulos
-- ----------------------------
INSERT INTO `modulos` VALUES ('1', 'CONFIGURACIÃ“N', 'configuraciÃ³n', 'fa fa-cog');
INSERT INTO `modulos` VALUES ('2', 'MANTENIMIENTOS', 'mantenimientos', 'fa fa-home');
INSERT INTO `modulos` VALUES ('6', 'REPORTES', 'reportes', 'fa fa-file');
INSERT INTO `modulos` VALUES ('7', 'seguridad', '#', 'icon-puzzle');

-- ----------------------------
-- Table structure for permisos
-- ----------------------------
DROP TABLE IF EXISTS `permisos`;
CREATE TABLE `permisos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idaccion` int(11) NOT NULL,
  `idsubmodulo` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idaccion` (`idaccion`),
  KEY `idmodulo` (`idsubmodulo`),
  KEY `permisos_ibfk_3` (`idmodulo`),
  CONSTRAINT `permisos_ibfk_1` FOREIGN KEY (`idaccion`) REFERENCES `acciones` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_ibfk_2` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_ibfk_3` FOREIGN KEY (`idsubmodulo`) REFERENCES `submodulo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos
-- ----------------------------
INSERT INTO `permisos` VALUES ('1', '1', null, 'ver configuraciÃ³n', '1');
INSERT INTO `permisos` VALUES ('2', '1', '22', 'ver acciones', null);
INSERT INTO `permisos` VALUES ('3', '2', '22', 'editar acciones', null);
INSERT INTO `permisos` VALUES ('4', '3', '22', 'eliminar acciones', null);
INSERT INTO `permisos` VALUES ('5', '4', '22', 'guardar acciones', null);
INSERT INTO `permisos` VALUES ('6', '1', '23', 'ver permisos', null);
INSERT INTO `permisos` VALUES ('7', '1', '1', 'ver modulos', null);
INSERT INTO `permisos` VALUES ('8', '1', '2', 'ver submodulo', null);
INSERT INTO `permisos` VALUES ('9', '1', '24', 'ver permisos por grupo', null);
INSERT INTO `permisos` VALUES ('36', '1', null, 'ver reportes', '6');
INSERT INTO `permisos` VALUES ('37', '1', '13', 'Ver productos', null);
INSERT INTO `permisos` VALUES ('38', '1', '14', 'ver ventas', null);
INSERT INTO `permisos` VALUES ('39', '1', '15', 'ver clientes', null);
INSERT INTO `permisos` VALUES ('40', '1', '14', 'ver ventas', null);
INSERT INTO `permisos` VALUES ('41', '1', null, 'ver mantenimientos', '2');
INSERT INTO `permisos` VALUES ('42', '1', '16', 'ver usuarios', null);
INSERT INTO `permisos` VALUES ('43', '1', '17', 'ver grupos', null);
INSERT INTO `permisos` VALUES ('49', '1', null, 'ver reporte', '6');
INSERT INTO `permisos` VALUES ('50', '1', '15', 'ver cliente', null);
INSERT INTO `permisos` VALUES ('55', '1', '25', 'ver urls permitidas', null);
INSERT INTO `permisos` VALUES ('56', '2', '23', 'editar permisos', null);
INSERT INTO `permisos` VALUES ('57', '3', '23', 'eliminar permisos', null);
INSERT INTO `permisos` VALUES ('58', '4', '23', 'guardar permisos', null);
INSERT INTO `permisos` VALUES ('60', '2', '24', 'editar permisos grupo', null);
INSERT INTO `permisos` VALUES ('61', '3', '24', 'eliminar permisos grupo', null);
INSERT INTO `permisos` VALUES ('62', '4', '24', 'guardar permisos grupo', null);
INSERT INTO `permisos` VALUES ('63', '4', '17', 'guardar grupos', null);
INSERT INTO `permisos` VALUES ('64', '2', '17', 'agregar/editar grupos', null);
INSERT INTO `permisos` VALUES ('65', '3', '17', 'eliminar grupos', null);
INSERT INTO `permisos` VALUES ('66', '2', '1', 'agregar/editar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('67', '3', '1', 'eliminar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('68', '4', '1', 'guardar mÃ³dulos', null);
INSERT INTO `permisos` VALUES ('69', '2', '2', 'agregar/editar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('70', '3', '2', 'eliminar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('71', '4', '2', 'guardar submÃ³dulos', null);
INSERT INTO `permisos` VALUES ('76', '5', '16', 'ver permisos de usuario', null);
INSERT INTO `permisos` VALUES ('141', '2', '16', 'agregar/editar usuarios', null);
INSERT INTO `permisos` VALUES ('142', '3', '16', 'eliminar usuarios', null);
INSERT INTO `permisos` VALUES ('143', '4', '16', 'guardar cambios usuarios', null);
INSERT INTO `permisos` VALUES ('145', '5', '2', 'ver permisos', null);
INSERT INTO `permisos` VALUES ('149', '1', '39', 'Ver permisos-usuarios', null);
INSERT INTO `permisos` VALUES ('150', '2', '39', 'Editar Permisos Usuario', null);
INSERT INTO `permisos` VALUES ('151', '3', '39', 'Eliminar Permisos Usuarios', null);
INSERT INTO `permisos` VALUES ('174', '1', '43', 'index ventas', null);
INSERT INTO `permisos` VALUES ('175', '2', '43', 'editar ventas', null);
INSERT INTO `permisos` VALUES ('176', '3', '43', 'eliminar ventas', null);
INSERT INTO `permisos` VALUES ('177', '4', '43', 'guardar ventas', null);
INSERT INTO `permisos` VALUES ('270', '1', null, 'ver seguridad', '7');
INSERT INTO `permisos` VALUES ('271', '1', '44', 'index unidad de medida', null);
INSERT INTO `permisos` VALUES ('272', '2', '44', 'editar unidad de medida', null);
INSERT INTO `permisos` VALUES ('273', '3', '44', 'eliminar unidad de medida', null);
INSERT INTO `permisos` VALUES ('274', '4', '44', 'guardar unidad de medida', null);
INSERT INTO `permisos` VALUES ('275', '9', '44', 'mostrar unidad de medida', null);
INSERT INTO `permisos` VALUES ('280', '1', '45', 'index Folios', null);
INSERT INTO `permisos` VALUES ('281', '2', '45', 'editar Folios', null);
INSERT INTO `permisos` VALUES ('282', '3', '45', 'eliminar Folios', null);
INSERT INTO `permisos` VALUES ('283', '4', '45', 'guardar Folios', null);
INSERT INTO `permisos` VALUES ('284', '9', '45', 'mostrar Folios', null);
INSERT INTO `permisos` VALUES ('285', '1', '46', 'index familias', null);
INSERT INTO `permisos` VALUES ('286', '2', '46', 'editar familias', null);
INSERT INTO `permisos` VALUES ('287', '3', '46', 'eliminar familias', null);
INSERT INTO `permisos` VALUES ('288', '4', '46', 'guardar familias', null);
INSERT INTO `permisos` VALUES ('289', '9', '46', 'mostrar familias', null);
INSERT INTO `permisos` VALUES ('290', '1', '47', 'index miembros de las familias', null);
INSERT INTO `permisos` VALUES ('291', '2', '47', 'editar miembros de las familias', null);
INSERT INTO `permisos` VALUES ('292', '3', '47', 'eliminar miembros de las familias', null);
INSERT INTO `permisos` VALUES ('293', '4', '47', 'guardar miembros de las familias', null);
INSERT INTO `permisos` VALUES ('294', '9', '47', 'mostrar miembros de las familias', null);
INSERT INTO `permisos` VALUES ('295', '1', '48', 'index rol de los miembros', null);
INSERT INTO `permisos` VALUES ('296', '2', '48', 'editar rol de los miembros', null);
INSERT INTO `permisos` VALUES ('297', '3', '48', 'eliminar rol de los miembros', null);
INSERT INTO `permisos` VALUES ('298', '4', '48', 'guardar rol de los miembros', null);
INSERT INTO `permisos` VALUES ('299', '9', '48', 'mostrar rol de los miembros', null);

-- ----------------------------
-- Table structure for permisos_grupo
-- ----------------------------
DROP TABLE IF EXISTS `permisos_grupo`;
CREATE TABLE `permisos_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpermiso` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `permisos_grupo_ibfk_1` (`idpermiso`),
  KEY `permisos_grupo_ibfk_2` (`idgrupo`),
  CONSTRAINT `permisos_grupo_ibfk_1` FOREIGN KEY (`idpermiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_grupo_ibfk_2` FOREIGN KEY (`idgrupo`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7920 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos_grupo
-- ----------------------------
INSERT INTO `permisos_grupo` VALUES ('4053', '41', '3');
INSERT INTO `permisos_grupo` VALUES ('4054', '42', '3');
INSERT INTO `permisos_grupo` VALUES ('4055', '76', '3');
INSERT INTO `permisos_grupo` VALUES ('4056', '43', '3');
INSERT INTO `permisos_grupo` VALUES ('4057', '64', '3');
INSERT INTO `permisos_grupo` VALUES ('4115', '36', '3');
INSERT INTO `permisos_grupo` VALUES ('7854', '1', '2');
INSERT INTO `permisos_grupo` VALUES ('7855', '7', '2');
INSERT INTO `permisos_grupo` VALUES ('7856', '66', '2');
INSERT INTO `permisos_grupo` VALUES ('7857', '67', '2');
INSERT INTO `permisos_grupo` VALUES ('7858', '68', '2');
INSERT INTO `permisos_grupo` VALUES ('7859', '8', '2');
INSERT INTO `permisos_grupo` VALUES ('7860', '69', '2');
INSERT INTO `permisos_grupo` VALUES ('7861', '70', '2');
INSERT INTO `permisos_grupo` VALUES ('7862', '71', '2');
INSERT INTO `permisos_grupo` VALUES ('7863', '145', '2');
INSERT INTO `permisos_grupo` VALUES ('7864', '42', '2');
INSERT INTO `permisos_grupo` VALUES ('7865', '76', '2');
INSERT INTO `permisos_grupo` VALUES ('7866', '141', '2');
INSERT INTO `permisos_grupo` VALUES ('7867', '142', '2');
INSERT INTO `permisos_grupo` VALUES ('7868', '143', '2');
INSERT INTO `permisos_grupo` VALUES ('7869', '43', '2');
INSERT INTO `permisos_grupo` VALUES ('7870', '63', '2');
INSERT INTO `permisos_grupo` VALUES ('7871', '64', '2');
INSERT INTO `permisos_grupo` VALUES ('7872', '65', '2');
INSERT INTO `permisos_grupo` VALUES ('7873', '2', '2');
INSERT INTO `permisos_grupo` VALUES ('7874', '3', '2');
INSERT INTO `permisos_grupo` VALUES ('7875', '4', '2');
INSERT INTO `permisos_grupo` VALUES ('7876', '5', '2');
INSERT INTO `permisos_grupo` VALUES ('7877', '6', '2');
INSERT INTO `permisos_grupo` VALUES ('7878', '56', '2');
INSERT INTO `permisos_grupo` VALUES ('7879', '57', '2');
INSERT INTO `permisos_grupo` VALUES ('7880', '58', '2');
INSERT INTO `permisos_grupo` VALUES ('7881', '41', '2');
INSERT INTO `permisos_grupo` VALUES ('7882', '271', '2');
INSERT INTO `permisos_grupo` VALUES ('7883', '272', '2');
INSERT INTO `permisos_grupo` VALUES ('7884', '273', '2');
INSERT INTO `permisos_grupo` VALUES ('7885', '274', '2');
INSERT INTO `permisos_grupo` VALUES ('7886', '275', '2');
INSERT INTO `permisos_grupo` VALUES ('7887', '280', '2');
INSERT INTO `permisos_grupo` VALUES ('7888', '281', '2');
INSERT INTO `permisos_grupo` VALUES ('7889', '282', '2');
INSERT INTO `permisos_grupo` VALUES ('7890', '283', '2');
INSERT INTO `permisos_grupo` VALUES ('7891', '284', '2');
INSERT INTO `permisos_grupo` VALUES ('7892', '285', '2');
INSERT INTO `permisos_grupo` VALUES ('7893', '286', '2');
INSERT INTO `permisos_grupo` VALUES ('7894', '287', '2');
INSERT INTO `permisos_grupo` VALUES ('7895', '288', '2');
INSERT INTO `permisos_grupo` VALUES ('7896', '289', '2');
INSERT INTO `permisos_grupo` VALUES ('7897', '290', '2');
INSERT INTO `permisos_grupo` VALUES ('7898', '291', '2');
INSERT INTO `permisos_grupo` VALUES ('7899', '292', '2');
INSERT INTO `permisos_grupo` VALUES ('7900', '293', '2');
INSERT INTO `permisos_grupo` VALUES ('7901', '294', '2');
INSERT INTO `permisos_grupo` VALUES ('7902', '295', '2');
INSERT INTO `permisos_grupo` VALUES ('7903', '296', '2');
INSERT INTO `permisos_grupo` VALUES ('7904', '297', '2');
INSERT INTO `permisos_grupo` VALUES ('7905', '298', '2');
INSERT INTO `permisos_grupo` VALUES ('7906', '299', '2');
INSERT INTO `permisos_grupo` VALUES ('7907', '36', '2');
INSERT INTO `permisos_grupo` VALUES ('7908', '9', '2');
INSERT INTO `permisos_grupo` VALUES ('7909', '60', '2');
INSERT INTO `permisos_grupo` VALUES ('7910', '61', '2');
INSERT INTO `permisos_grupo` VALUES ('7911', '62', '2');
INSERT INTO `permisos_grupo` VALUES ('7912', '55', '2');
INSERT INTO `permisos_grupo` VALUES ('7913', '149', '2');
INSERT INTO `permisos_grupo` VALUES ('7914', '150', '2');
INSERT INTO `permisos_grupo` VALUES ('7915', '151', '2');
INSERT INTO `permisos_grupo` VALUES ('7916', '174', '2');
INSERT INTO `permisos_grupo` VALUES ('7917', '175', '2');
INSERT INTO `permisos_grupo` VALUES ('7918', '176', '2');
INSERT INTO `permisos_grupo` VALUES ('7919', '177', '2');

-- ----------------------------
-- Table structure for permisos_usuario
-- ----------------------------
DROP TABLE IF EXISTS `permisos_usuario`;
CREATE TABLE `permisos_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL,
  `idpermiso` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permisos_usuario_ibfk_1` (`idpermiso`),
  KEY `permisos_usuario_ibfk_2` (`idusuario`),
  CONSTRAINT `permisos_usuario_ibfk_1` FOREIGN KEY (`idpermiso`) REFERENCES `permisos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permisos_usuario_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permisos_usuario
-- ----------------------------

-- ----------------------------
-- Table structure for predio
-- ----------------------------
DROP TABLE IF EXISTS `predio`;
CREATE TABLE `predio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_predio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plano` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_registro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `familia_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `familia_id` (`familia_id`),
  CONSTRAINT `predio_ibfk_1` FOREIGN KEY (`familia_id`) REFERENCES `familia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of predio
-- ----------------------------

-- ----------------------------
-- Table structure for rol_miembro
-- ----------------------------
DROP TABLE IF EXISTS `rol_miembro`;
CREATE TABLE `rol_miembro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_miembro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rol_miembro
-- ----------------------------
INSERT INTO `rol_miembro` VALUES ('1', 'Padre', '2017-12-31 15:12:26', null, null);
INSERT INTO `rol_miembro` VALUES ('2', 'Madre', '2017-12-31 16:12:42', null, null);
INSERT INTO `rol_miembro` VALUES ('3', 'TÃ­o', '2017-12-31 16:12:47', null, null);
INSERT INTO `rol_miembro` VALUES ('4', 'Hijo', '2017-12-31 16:12:56', null, null);

-- ----------------------------
-- Table structure for siembra
-- ----------------------------
DROP TABLE IF EXISTS `siembra`;
CREATE TABLE `siembra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siembra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tiempo_siembra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of siembra
-- ----------------------------

-- ----------------------------
-- Table structure for submodulo
-- ----------------------------
DROP TABLE IF EXISTS `submodulo`;
CREATE TABLE `submodulo` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `idmodulo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pk_mod` (`idmodulo`),
  CONSTRAINT `submodulo_ibfk_1` FOREIGN KEY (`idmodulo`) REFERENCES `modulos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of submodulo
-- ----------------------------
INSERT INTO `submodulo` VALUES ('1', 'MÃ³dulos', 'modulos', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('2', 'Sub MÃ³dulos', 'submodulos', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('13', 'Productos', 'productos', 'fa fa-angle-double-right', '6');
INSERT INTO `submodulo` VALUES ('14', 'Ventas', 'ventas', 'fa fa-angle-double-right', '6');
INSERT INTO `submodulo` VALUES ('15', 'Clientes', 'clientes', 'fa fa-angle-double-right', '6');
INSERT INTO `submodulo` VALUES ('16', 'Usuarios', 'usuarios', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('17', 'Grupos', 'grupos', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('18', 'Permisos', 'permisos', 'fa fa-angle-double-right', '2');
INSERT INTO `submodulo` VALUES ('22', 'Acciones', 'acciones', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('23', 'Permisos', 'permisos', 'fa fa-angle-double-right', '1');
INSERT INTO `submodulo` VALUES ('24', 'Permisos por grupo', 'permisos_grupos', 'fa fa-angle-double-right', '7');
INSERT INTO `submodulo` VALUES ('25', 'urls permitidas', 'Urls-permitidas', 'fa fa-angle-double-right', '7');
INSERT INTO `submodulo` VALUES ('39', 'Permisos Usuario', 'permisos-usuarios', 'fa fa-angle-double-right  ', '7');
INSERT INTO `submodulo` VALUES ('43', 'Ventas', 'ventas', 'fa fa-angle-double-right  ', '7');
INSERT INTO `submodulo` VALUES ('44', 'unidad de medida', 'unidad-medidas', 'fa fa-edit', '2');
INSERT INTO `submodulo` VALUES ('45', 'Folios', 'folios', 'fa fa-search', '2');
INSERT INTO `submodulo` VALUES ('46', 'familias', 'familias', 'fa fa-users', '2');
INSERT INTO `submodulo` VALUES ('47', 'miembros de las familias', 'miembros-familias', 'fa fa-user-plus ', '2');
INSERT INTO `submodulo` VALUES ('48', 'rol de los miembros', 'rol-miembros', 'fa fa-android', '2');

-- ----------------------------
-- Table structure for tipo_zona
-- ----------------------------
DROP TABLE IF EXISTS `tipo_zona`;
CREATE TABLE `tipo_zona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_zona` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_zona
-- ----------------------------

-- ----------------------------
-- Table structure for ubicacion_zona
-- ----------------------------
DROP TABLE IF EXISTS `ubicacion_zona`;
CREATE TABLE `ubicacion_zona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zona` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ubicacion_zona
-- ----------------------------

-- ----------------------------
-- Table structure for unidad_medida
-- ----------------------------
DROP TABLE IF EXISTS `unidad_medida`;
CREATE TABLE `unidad_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `simbolo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of unidad_medida
-- ----------------------------
INSERT INTO `unidad_medida` VALUES ('1', 'metro cuadrado', '2017-12-30 18:12:00', '2017-12-30 18:12:42', '2017-12-30 18:12:25', 'm2');
INSERT INTO `unidad_medida` VALUES ('2', 'hectÃ¡rea ', '2017-12-30 23:12:46', null, null, 'Ht');

-- ----------------------------
-- Table structure for urls_permitidas
-- ----------------------------
DROP TABLE IF EXISTS `urls_permitidas`;
CREATE TABLE `urls_permitidas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of urls_permitidas
-- ----------------------------
INSERT INTO `urls_permitidas` VALUES ('1', 'generator');
INSERT INTO `urls_permitidas` VALUES ('2', 'admin');
INSERT INTO `urls_permitidas` VALUES ('3', 'index');
INSERT INTO `urls_permitidas` VALUES ('4', 'logout');
INSERT INTO `urls_permitidas` VALUES ('5', 'nosotros');
INSERT INTO `urls_permitidas` VALUES ('6', 'servicios');
INSERT INTO `urls_permitidas` VALUES ('7', 'galeria');
INSERT INTO `urls_permitidas` VALUES ('8', 'contactenos');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `esta_activo` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('4', 'Administrador', 'Administrador', '123', '', 'administrador@gmail.com', '978654387', 'Tarapoto', '2017-11-07 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios` VALUES ('5', 'Anthony', 'Cachay guivin', '123', '', 'guivin95@gmail.com', '123123123', 'JirÃ³n guepi 734', '2017-12-31 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for usuarios_grupo
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_grupo`;
CREATE TABLE `usuarios_grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario_id` (`usuario_id`),
  KEY `grupo_id` (`grupo_id`),
  CONSTRAINT `usuarios_grupo_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarios_grupo_ibfk_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuarios_grupo
-- ----------------------------
INSERT INTO `usuarios_grupo` VALUES ('29', '4', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `usuarios_grupo` VALUES ('30', '5', '2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for zonas
-- ----------------------------
DROP TABLE IF EXISTS `zonas`;
CREATE TABLE `zonas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unidad_medida_id` int(11) NOT NULL,
  `zona_ubicacion_id` int(11) NOT NULL,
  `predio_id` int(11) NOT NULL,
  `tipo_zona` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `miembro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zonas_ibfk_1` (`tipo_zona`),
  KEY `zonas_ibfk_2` (`zona_ubicacion_id`),
  KEY `miembro_id` (`miembro_id`),
  KEY `predio_id` (`predio_id`),
  KEY `unidad_medida_id` (`unidad_medida_id`),
  CONSTRAINT `zonas_ibfk_1` FOREIGN KEY (`tipo_zona`) REFERENCES `tipo_zona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zonas_ibfk_2` FOREIGN KEY (`zona_ubicacion_id`) REFERENCES `ubicacion_zona` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zonas_ibfk_3` FOREIGN KEY (`miembro_id`) REFERENCES `miembros_familia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zonas_ibfk_4` FOREIGN KEY (`predio_id`) REFERENCES `predio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zonas_ibfk_5` FOREIGN KEY (`unidad_medida_id`) REFERENCES `unidad_medida` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of zonas
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
